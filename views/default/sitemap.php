<?xml version="1.0" encoding="UTF-8"?>
<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9" >
<?php
// if(){
// #search
// #welcom
// list of all apps
// } 

foreach ($list as $key => $value) 
{
	if(isset($value["name"]) || isset($value["slug"]))
	{ ?>
  <url>
    <loc>https://<?php echo $host; ?>/#!@<?php echo ( isset($value["slug"])) ? $value["slug"] : $value["name"]?></loc>
    <lastmod><?php echo date( "Y-m-d",( isset($value["updated"])) ? $value["updated"] : @$value["elemUpdated"])?></lastmod>
  </url>

<?php
	}
}
?>
</urlset>

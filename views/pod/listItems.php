<style type="text/css">
	.listsItemsPod .hover-underline{
		text-decoration: none;
		height: 50px;
		width: 50px;
		display: inline-block;
		margin-top: 3px;
	}
	.listsItemsPod .seeAll{
		background-color: rgb(34, 37, 42);
	    color: white;
	    line-height: 48px;
	    font-size: 16px;
	}
	.listsItemsPod .seeAll:hover{
		font-weight: 800;
	}
</style>
<div class="col-xs-12 listsItemsPod <?php echo @$containerClass ?>">
<?php 
	$urlContext="#page.type.".@$contextType.".id.".$contextId.".view.directory.dir.".$connectType;
	$group=Link::groupFindAppendAttribute($links);
	//if($connectType=="projects") var_dump($group);
	echo "<h4 class='col-xs-12 ".@$titleClass."'>".$title."</h4>";
	$nbShow=1;
	$groupCount=count($group);
	foreach ($group as $k => $e) { 
		if(!isset($number) || (isset($number) && $nbShow <=$number )){
			$nbShow++;
			if(!empty($e)){
				$grayscale = "grayscale";
				$addCogFlag=true;
				$addCogFlag=15;
				//print_r($e);
				if(is_array(@$e["name"]))
					$name = @$e["name"][0];
				else
					$name = @$e["name"];
				$status="";
				$statTit="";
				if (@$e["isAdmin"])
					$statTit="<span class='text-red'>Administrateur</span>";
					
				$rolesStr="";
				if(isset($e["roles"]) && !empty($e["roles"])){
					if(!empty($statTit)) $rolesStr.="<br/>";
					$rolesStr.="<span class='text-blue'>Rôle(s)</span>: ";
					$i=0;
					foreach($e["roles"] as $role){
						if($i > 0) $rolesStr.=", ";
						$rolesStr.=$role;
						$i++;
					}
				}
				if (@$e["isAdminPending"] || @$e["toBeValidated"])
					$status= "<span class='text-grey'>Statut</span>: ".Yii::t("common","waiting for validation");
				else if (@$e["tobeactivated"])
					$status= "<span class='text-grey'>Statut</span>: ".Yii::t("common","not activated");
				else if (@$e["pending"])
					$status= "<span class='text-grey'>Statut:</span> ".Yii::t("common","unregistred");	
				if(!empty($status) && (!empty($statTit) || !empty($rolesStr))) $status="<br/>".$status;
				
				$heightWidth=(isset($heightWidth) && !empty($heightWidth)) ? $heightWidth : 50;
				$getImageSize=($heightWidth>50) ? "profilMediumImageUrl" : "profilThumbImageUrl";
				
				$imgPath=(!empty($e[$getImageSize])) ? Yii::app()->createUrl($e[$getImageSize]) :  $this->module->assetsUrl."/images/thumb/default_".$e["type"].".png";
				$anId = $k; 
				$contentPop=$statTit.$rolesStr.$status;
			?>
			<a href="#page.type.<?php echo $e["type"] ?>.id.<?php echo $anId; ?>" class="hover-underline" data-toggle="popover" title="<?php echo $name ?>" data-content="<?php echo $contentPop ?>">
				<img width="<?php echo $heightWidth ?>" height="<?php echo $heightWidth ?>"  alt="image" class="<?php echo @$imgClass ?>" src="<?php echo $imgPath ?>">
			</a>

	<?php 	}
		}
	}
	if(isset($number) && $groupCount > $number){ ?>
		<a href="<?php echo $urlContext ?>" class="lbh hover-underline tooltips seeAll" data-toggle="tooltip" title="Tout voir"  data-placement="top" data-original-title="<?php echo Yii::t("common", "See all"); ?>">
			<span>+<?php echo ($groupCount-$number) ?></span>
		</a>
	<?php } ?>
</div>
<script type="text/javascript">
	jQuery(document).ready(function() {
		coInterface.bindLBHLinks();
		coInterface.bindTooltips();
		$('[data-toggle="popover"]').popover({
		  	placement: 'top',
		  	html:true,
		  	trigger: 'hover'
		});
	});
</script>
<style type="text/css">
	.carousel-media > ol > li.active{
	   margin:1px;
	   width: 60px;
	   height: 65px ;
	     border-radius: inherit;
	    text-indent: inherit;
	}
	.carousel-media > ol > li{
		width: 60px;
	    background-color: inherit;
	    height: 65px ;
	    border-radius: inherit;
	    text-indent: inherit;
	}
	
	.carousel-media > ol > li > img{
	   float:left;
	   width:60px;
	   height:60px;
	}
	.carousel-media > ol > li > span{
		position: relative;
		float:left;
		top: -60px;
		line-height: 60px;
		width:60px;
	}
	.carousel-media > ol > li > span > i{
		vertical-align: middle;
	}
	.carousel-media > ol{
		bottom: -85px
	}
	.carousel-media .carousel-indicators{
		left:30%;
		width: 100%;
	}
	.carousel-media{
		margin-bottom: 100px;
	}
	.carousel-media .carousel-inner{
		max-height: 380px;
		background-color: black;

	}

	#mediaAbout .carousel-media > ol > li{
		border-top: inherit !important;
	}
	#mediaAbout .carousel-media > ol{
		text-align: left;
		left:31%;
		z-index: 1;
	}
	.carousel-inner>.item>a>img, .carousel-inner>.item>img{
		margin:auto; 
	}
</style>

<?php if(@$onlyItem){ ?>
<div class="col-xs-12 no-padding carousel-media">
	<!-- Indicators -->
	<ol class="carousel-indicators">
<?php 
			$i=0;
			if(!empty($medias)){
			foreach ($medias as $data){
				if(@$data["content"]["image"] && @$data["content"]["videoLink"]){ ?>
				 <li><img src="<?php echo $data["content"]["image"] ?>" alt=""><span class="text-white text-center"><i class="fa fa-2x fa-play-circle-o"></i></span></li>
	   
		<?php $i++;
		 } } }?>
		 <?php 
		 	if(!empty($images)){
			foreach ($images as $data){ ?>
				 <li><img src="<?php echo $data["imageThumbPath"] ?>" alt=""></li>
	   
		<?php $i++;
		 } } ?>
	    <!--<li data-target="#myCarousel" data-slide-to="0" class="active"><img src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/img/reunion/reunion4.jpg" alt="Reunion 1"></li>
	    <li data-target="#myCarousel" data-slide-to="1"><img src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/img/reunion/reunion5.jpg" alt="Reunion 1"></li>-->
	</ol>
</div>
<?php }else{ ?>
<div id="myCarousel" class="col-xs-12 no-padding carousel carousel-media slide" data-ride="carousel">
	<!-- Indicators -->
	<ol class="carousel-indicators">
		<?php 
			$i=0;
			if(!empty($medias)){
			foreach ($medias as $data){ 
				if(@$data["content"]["image"] && @$data["content"]["videoLink"]){ 
				?>
				 <li data-target="#myCarousel" data-slide-to="<?php echo $i; ?>" class="<?php if($i==0) echo "active"; ?>"><img src="<?php echo $data["content"]["image"] ?>" alt=""><span class="text-white text-center"><i class="fa fa-2x fa-play-circle-o"></i></span></li>
	   
		<?php $i++;
		 } } } ?>
		 <?php 
		 	if(!empty($images)){
		 		if(!isset($simplePuce) || count($images) > 1){
					foreach ($images as $data){ ?>
						 <li data-target="#myCarousel" data-slide-to="<?php echo $i; ?>" class="<?php if($i==0) echo "active"; ?>">
						 <?php if(!isset($simplePuce)){ ?><img src="<?php echo $data["imageThumbPath"] ?>" alt=""><?php } ?></li>
			   
				<?php $i++;
				 	} 
				} 
			} ?>
	</ol>

	<!-- Wrapper for slides -->
	<div class="carousel-inner">
		<?php 
			$i=0;
			if(!empty($medias)){
			foreach ($medias as $data){
				if(@$data["content"]["image"] && @$data["content"]["videoLink"]){ ?>
				 <div class="item <?php if($i==0) echo "active"; ?>">
			      <iframe width="100%" height="380" src="<?php echo @$data["content"]["videoLink"] ?>" frameborder="0" allowfullscreen></iframe>
			    </div>
		<?php $i++;
		 } } } ?>
		 <?php 
		 	if(!empty($images)){
			foreach ($images as $data){ ?>
				<div class="item <?php if($i==0) echo "active"; ?> text-center">
	    		  <img src="<?php echo $data["imagePath"] ?>" class="img-responsive"  alt="<?php echo @$data["name"] ?>">
	    		</div>
		<?php $i++;
		 } } ?>
	</div>
</div>
<script type="text/javascript">
	docSlider=<?php echo $i; ?>;
	jQuery(document).ready(function() {
		if(docSlider>0){
			$("#myCarousel").carousel();
			// Enable Carousel Indicators
			$("ol.carousel-indicators > li").click(function(){
			    $("#myCarousel").carousel($(this).data("slide-to"));
			});
		}
	});
</script>
<?php } ?>
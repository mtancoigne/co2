<?php
$cs = Yii::app()->getClientScript(); 
$cssAnsScriptFilesTheme = array(
	//SELECT2
	'/plugins/bootstrap-toggle/css/bootstrap-toggle.min.css',
	'/plugins/bootstrap-toggle/js/bootstrap-toggle.min.js' , 
	'/plugins/bootstrap-switch/dist/css/bootstrap3/bootstrap-switch.min.css',
  	'/plugins/bootstrap-switch/dist/js/bootstrap-switch.min.js' ,
);
HtmlHelper::registerCssAndScriptsFiles($cssAnsScriptFilesTheme, Yii::app()->request->baseUrl);    
?>
<style>
	

</style>
<div id="community-settings" class="contain-section-params col-xs-12 no-padding">
    <div class="settingsHeader bg-white no-padding">
     	<div class="settings-header">
    		<h4 class="title"><i class="fa fa-bell"></i> <?php echo Yii::t("settings", "Settings on your community's notifications") ?></h4>
    	</div>
    	<div id="settingsScrollByType" class="pull-left"></div>
    		<a href="javascript:;" id="btnSettingsInfos" class="text-dark pull-right margin-right-20"><i class="fa fa-info-circle"></i> <span class="hidden-xs"> <?php echo Yii::t("common", "Infos") ?></span></a>
    	<input type="text" id="search-in-settings" class="form-control" placeholder="<?php echo Yii::t("common","Search name, slug, postal code, city ...") ?>">
    </div>
    <div id="community-settings-list"></div>
</div>
<?php  $this->renderPartial('co2.views.settings.modalExplainCommunitySettings', array()); ?>
<script type="text/javascript">
jQuery(document).ready(function() {
	settings.getCommunitySettings("notifications");
    if(searchInCommunity!=""){
        $("#search-in-settings").val(searchInCommunity);
        settings.filterSettingsCommunity(searchInCommunity);
    }
});
</script>
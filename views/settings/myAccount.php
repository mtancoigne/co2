<?php
$cs = Yii::app()->getClientScript(); 
// $cssAnsScriptFilesTheme = array(
// 	//SELECT2
// 	'/plugins/bootstrap-toggle/css/bootstrap-toggle.min.css',
// 	'/plugins/bootstrap-toggle/js/bootstrap-toggle.min.js' , 
// 	'/plugins/bootstrap-switch/dist/css/bootstrap3/bootstrap-switch.min.css',
//   	'/plugins/bootstrap-switch/dist/js/bootstrap-switch.min.js' ,
// );
//if ($type == Project::COLLECTION)
//	array_push($cssAnsScriptFilesTheme, "/assets/plugins/Chart.js/Chart.min.js");
//HtmlHelper::registerCssAndScriptsFiles($cssAnsScriptFilesTheme, Yii::app()->request->baseUrl);
$cssAnsScriptFiles = array(
     '/assets/css/default/settings.css',
);
HtmlHelper::registerCssAndScriptsFiles($cssAnsScriptFiles, Yii::app()->theme->baseUrl); 

    
$cssAnsScriptFilesModule = array(
    '/js/default/settings.js',
);
HtmlHelper::registerCssAndScriptsFiles($cssAnsScriptFilesModule, $this->module->assetsUrl);

?>
<div id="myAccount-settings" class="contain-section-params col-xs-12 bg-white shadow2 margin-bottom-20">
	<div class="settings-header">
		<h3 class="title"><i class="fa fa-cogs"></i> <?php echo Yii::t("settings", "Manage your account") ?></h3>
	</div>
	<div class="col-xs-12 no-padding">
		<h4 style="text-transform: none;" ><i class="fa fa-unlock-alt  "></i> <?php echo Yii::t("common","Change password"); ?> <a href="javascript:;" id="btn-update-password-setting" class="btn btn-success btn-sm"> <i class="fa fa-pencil "></i> </a> </h4>
	</div>

	<div class="block-account col-xs-12 no-padding">
		<h4 style="text-transform: none;"><i class="fa fa-id-badge"></i> <?php echo Yii::t("common", "Edit slug"); ?> <a href="javascript:;" onclick="updateSlug();" id="" class="btn btn-success btn-sm"> <i class="fa fa-pencil "></i> </a></h4>
	</div>

	<div class="block-account col-xs-12 no-padding">
		<h4 style="text-transform: none;"><i class='fa fa-download'></i> <?php echo Yii::t("common", "Download your profil") ?> <a href="javascript:;" id="downloadProfil-setting" class="btn btn-success btn-sm"> <i class="fa fa-pencil "></i> </a> </h4>
	</div>


	<?php 
	if ( Authorisation::canDeleteElement( (String)$element["_id"], $type, Yii::app()->session["userId"]) && 
			//!@$deletePending && 
			!empty(Yii::app()->session["userId"]) ) {

			$this->renderPartial('../element/confirmDeleteModal', array("id" =>(String)$element["_id"], "type"=>$type));

	?>
	<div class="block-account col-xs-12 no-padding">
		<h4 style="text-transform: none;"><i class='fa fa-trash '></i> <?php echo "Supprimer mon compte"; ?> <a href="javascript:;" id="btn-delete-element-setting" class="btn btn-danger btn-sm"> <i class="fa fa-trash "></i> </a></h4>
	</div>
<?php } ?>
</div>
<div class="col-xs-12 notificationsPod bg-white shadow2 margin-bottom-20">
	<?php echo $this->renderPartial("co2.views.settings.notificationsAccount", array("preferences"=>$element["preferences"]), true); ?>
</div>
<div class="col-xs-12 confidentialityPod bg-white shadow2 margin-bottom-20">
	<?php $params=array(  "element" => @$element, 
			"type" => $type,
			"id" => (string)$element["_id"], 
			"edit" => true,
			"openEdition" => false,
			"modal"=>false
		);
    	echo $this->renderPartial("co2.views.settings.confidentialityPanel", $params, true); ?>
</div>
<script type="text/javascript">
contextData = <?php echo json_encode(@$element) ?>;
jQuery(document).ready(function() {
	settings.bindMyAccountSettings(contextData);
	//ajaxPost('.notificationsPod' ,baseUrl+'/'+moduleId+"/settings/notificationsaccount",
	//		 null,function(){},"html");
	//ajaxPost('.confidentialityPod' ,baseUrl+'/'+moduleId+"/settings/confidentiality/type/citoyens/id/"+userId,
	//		 null,function(){},"html");
});
</script>
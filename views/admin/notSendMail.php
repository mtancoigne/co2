<div class="col-xs-12" id="divRemove" style="margin-top: 10px">

	<legend><?php echo Yii::t("common","Not send mails"); ?> :</legend>
	<div class="col-xs-12 padding-10">
		<label for="email"><?php echo Yii::t("common","Your mail"); ?> : </label> <input type="text" name="email" id="email" />
	</div>
	<a href="<?php echo Yii::app()->request->baseUrl. '/co2/admin/getMailingList'; ?> " target="_blank" id="csv"><i class='fa fa-2x fa-table text-green'></i></a>

	<a href="<?php echo Yii::app()->request->baseUrl. '/co2/admin/getMailingList/type/cofinanceur'; ?> " target="_blank" id="csv"><i class='fa fa-2x fa-table text-green'></i></a>

	<a href="<?php echo Yii::app()->request->baseUrl. '/co2/admin/getMailingList/type/other'; ?> " target="_blank" id="csv"><i class='fa fa-2x fa-table text-green'></i></a>
	<div class="col-xs-12">
		<button id="btn-valider" class="btn btn-default" ><!-- <i class="fa fa-times"></i> --><?php echo Yii::t("common","Validate"); ?></button>
	</div>
	<div class="col-xs-12 padding-10">
		<div id="success" class="hidden text-green" style="margin-top: 30px;">
			<h4><?php echo Yii::t("common","An e-mail has just been sent to you to validate the request"); ?> .</h4>
		</div>
		<div id="error" class="hidden text-red" style="margin-top: 30px;">
			<h4 id="errormsg"></h4>
		</div>
	</div>

	<div class="pageTable col-md-12 col-sm-12 col-xs-12 padding-20 text-center"></div>
	<div class="panel-body">
		<div>	
			<table class="table table-striped table-bordered table-hover  directoryTable" id="panelAdmin">
				<thead>
					<tr>
						<th>Email</th>
						<th>Date</th>
					</tr>
				</thead>
				<tbody class="directoryLines">
					
				</tbody>
			</table>
		</div>
	</div>
	<div class="pageTable col-md-12 col-sm-12 col-xs-12 padding-20"></div>
</div>

<script type="text/javascript">
var results = <?php echo json_encode($results) ?>;
jQuery(document).ready(function() {
	setTitle("Not send mail","cog");
	initViewTable(results);

	$("#btn-valider").off().on( "click", function(){
		$("#success").addClass("hidden");
		$("#error").addClass("hidden");

		var param = {
			email : $("#email").val()
		} ;


		mylog.log("param", param);
		$.ajax({
			type: "POST",
			url: baseUrl+"/"+moduleId+"/admin/addnotsendmail/",
			dataType: "json",
			data : param,
			success: function(data){
				
				if (data.res.result) {
					toastr.success(data.res.msg);
					location.reload();
				} else {
					$("#error").removeClass("hidden");
					$("#success").addClass("hidden");
					toastr.error(data.res.msg);
					$("#errormsg").html(data.res.msg);
				}
			}
		});
		
	});
});

function initViewTable(data){
	$('#panelAdmin .directoryLines').html("");
	$.each(data,function(key,values){
		entry=buildDirectoryLine(key, values );
			$("#panelAdmin .directoryLines").append(entry);
	});
	//bindAdminBtnEvents();
}

function buildDirectoryLine(key,  e ){
	mylog.log("here", e);
	strHTML="";

	actions = "";
	classes = "";
	strHTML += '<tr id="'+key+'">';
		
		strHTML += '<td>'+e.target.email+'</td>';
		strHTML += '<td class="center status">'+moment(e.date).local().format("DD-MM-YYYY HH:mm");
		strHTML += '</td>';
		/* **************************************
		* ACTIONS
		***************************************** */
		// if(searchAdmin.mode=="source"){
		// 	action='<button data-id="'+id+'" data-type="'+type+'" data-action="remove" data-setkey="source" class="margin-right-5 setSourceAdmin btn bg-red text-white"><i class="fa fa-ban"></i> Remove from source</button>';
		// }else if (searchAdmin.mode=="reference"){
		// 	action='<button data-id="'+id+'" data-type="'+type+'" data-action="remove" data-setkey="reference" class="margin-right-5 setSourceAdmin btn bg-red text-white"><i class="fa fa-ban"></i> Remove from reference</button> ';
		// }
		// else if (searchAdmin.mode=="open"){
		// 	action='<button data-id="'+id+'" data-type="'+type+'" data-action="add" data-setkey="reference" class="margin-right-5 setSourceAdmin btn bg-green text-white"><i class="fa fa-plus"></i> Add as reference</button>';
		// }
		// strHTML += '<td class="center">'; 
		// 	strHTML += '<div class="btn-group">'+action
		// 				'</div>';
		// strHTML += '</td>';
	
	strHTML += '</tr>';
	return strHTML;
}
<?php
	//var_dump("exit"); exit;
	$cssAnsScriptFilesModule = array(
	    '/js/default/admin.js',
	);
  	HtmlHelper::registerCssAndScriptsFiles($cssAnsScriptFilesModule, $this->module->assetsUrl);

  	$adminConstruct=Yii::app()->session["paramsConfig"]["adminPanel"];
	if(isset(Yii::app()->session["costum"])){
		$cssJsCostum=array();
		if(isset($adminConstruct["js"]))
			array_push($cssJsCostum, '/js/'.Yii::app()->session["costum"]["slug"].'/admin.js');
		if(isset($adminConstruct["css"]))
			array_push($cssJsCostum, '/css/'.Yii::app()->session["costum"]["slug"].'/admin.css');
		if(!empty($cssJsCostum))
			HtmlHelper::registerCssAndScriptsFiles($cssJsCostum, Yii::app()->getModule( "costum" )->getAssetsUrl());
	}
	$logo = (@Yii::app()->session['costum']["logo"]) ? Yii::app()->session['costum']["logo"] : Yii::app()->theme->baseUrl."/assets/img/LOGOS/CO2/logo-min.png";
		
?>
<!-- start: PAGE CONTENT -->
<style type="text/css">
	#content-view-admin, #goBackToHome{
		display: none;
	}
	#content-admin-panel{
		min-height:700px;
		background-color: white;
	}
</style>

<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 no-padding" id="content-admin-panel">

	<?php if($authorizedAdmin || Role::isSourceAdmin(Role::getRolesUserId(Yii::app()->session["userId"]))){
		$title=(@Yii::app()->session["userIsAdmin"]) ? Yii::t("common","Administration portal") : Yii::t("common","Public administration portal");
		?>
	<div class="col-md-12 col-sm-12 col-xs-12" id="navigationAdmin">
		<div class="col-sm-12 col-xs-12 text-center">
			<!-- <img src="<?php echo $logo ?>" height="100"><br/> -->
	         <h3><?php echo $title ?></h3>
   		</div> 


   		<div class="col-xs-12  col-sm-offset-1 col-sm-10 padding-50  links-main-menu" 
			 id="div-admin-select-create">
	   		<?php 
		    //Filtering button add element if costum
		    if(isset($adminConstruct["add"]) && $adminConstruct["add"]){ ?>
				
				<div class="col-xs-12  col-sm-offset-1 col-sm-10  shadow2 bg-white ">
			       
			       <h5 class="text-center margin-top-15 infoPanelAddContent">
			       	<i class="fa fa-plus-circle"></i> <?php echo Yii::t("form","Add items") ?>
			       	<br>
			       	<small><?php echo Yii::t("form","What kind of content will you create ?") ?></small>
			       </h5>

			        <div class="col-md-12 col-sm-12 col-xs-12 contain-admin-add"><hr></div>
			    </div>
		    <?php } ?>

			<ul class="list-group text-left no-margin menuAdmin">
			<?php 
			if(!empty($menu)){
				foreach($adminConstruct["menu"] as $key => $v) {
					$show=(isset($v["show"])) ? $v["show"] : true;
					$show= (isset($v["costumAdmin"]) && (!isset(Yii::app()->session["isCostumAdmin"]) || empty(Yii::app()->session["isCostumAdmin"])) && (!isset(Yii::app()->session["userIsAdmin"]) || empty(Yii::app()->session["userIsAdmin"])) ) ? false : $show;
					$show=(isset($v["super"]) && (!isset(Yii::app()->session["userIsAdmin"]) || empty(Yii::app()->session["userIsAdmin"]))) ? false : $show;
					if($show){
						$dataAttr="";
						$dataAttr=(isset($v["dataHref"])) ? "data-href='".$v["dataHref"]."' " : "";
						$dataAttr.=(isset($v["view"]) && !empty($v["view"])) ? "data-view='".$v["view"]."' " : "";
						$dataAttr.=(isset($v["action"]) && !empty($v["action"])) ? "data-action='".$v["action"]."' " : "";
							?>
						<li class="text-center list-group-item col-xs-12  col-sm-offset-1 col-sm-10">
							<a href="javascript:;" class="btnNavAdmin <?php echo @$v["class"] ?>" id="<?php echo @$v["id"] ?>" <?php echo $dataAttr; ?> style="cursor:pointer;">
								<i class="fa fa-<?php echo @$v["icon"] ?> fa-2x"></i>
								<?php echo Yii::t("admin", @$v["label"]); ?>
							</a>
						</li>
					<?php 
					}
				}
			} 
		 ?>
			</ul>
		</div>
	</div>
	<div class="col-md-12 col-sm-12 col-xs-12 no-padding" id="goBackToHome">
		<a href="javascript:;" class="btnNavAdmin col-md-12 col-sm-12 col-xs-12 padding-20 text-center bg-orange" data-view="index" style="font-size:20px;"><i class="fa fa-home"></i> <?php echo Yii::t("common", "Back to admin home") ?></a>
	</div>
	<div id="content-view-admin" class="col-md-12 col-sm-12 col-xs-12 no-padding"></div>
	<?php }else{ ?>
	<div class="col-md-12 col-sm-12 col-xs-12 text-center margin-top-50">
			<img src="<?php echo $logo ?>" 
	                     class="" height="100"><br/>
	         <h3><?php echo Yii::t("common","Administration portal") ?></h3>
   		</div>
		<div class="col-md-10 col-sm-10 col-xs-10 alert-danger text-center margin-top-20"><strong><?php echo Yii::t("common","You are not authorized to acces adminastrator panel ! <br/>Connect you or contact us in order to become admin system") ?></strong></div>
	<?php } ?>
</div>
<!-- end: PAGE CONTENT-->
<script type="text/javascript">
	var superAdmin="<?php echo @Yii::app()->session["userIsAdmin"] ?>";
	var sourceAdmin="<?php echo @Yii::app()->session["userIsAdminPublic"] ?>";
	var authorizedAdmin=<?php echo json_encode(@$authorizedAdmin) ?>;
	var edit=true;
	var paramsAdmin= <?php echo json_encode($adminConstruct) ?>;
	var subView=<?php echo (isset($_POST["subView"])) ? json_encode(true) : json_encode(false); ?>;
	adminPanel.params={
		hashUrl : "#admin",
    	view : "<?php echo @$_GET['view']; ?>",
    	action : null,
		dir: "<?php echo @$_GET['dir']; ?>",
		subView:false
	};
	if(notEmpty(subView)){
	 	adminPanel.params.view = "<?php echo @$_GET['subview']; ?>";
	 	adminPanel.params.subView = true;
	 }
	jQuery(document).ready(function() {
		mylog.log("---admin/index.php");
		if(superAdmin == "" && sourceAdmin == "" && !authorizedAdmin){
			urlCtrl.loadByHash("");
			bootbox.dialog({message:'<div class="alert-danger text-center"><strong><?php echo Yii::t("common","You are not authorized to acces adminastrator panel ! <br/>Connect you or contact us in order to become admin system") ?></strong></div>'});
		}
		adminPanel.init();
	});
</script>
<!-- end: PAGE CONTENT-->


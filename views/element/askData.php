<?php

$cssAnsScriptFiles = array(
'/assets/vendor/jquery_realperson_captcha/jquery.realperson.css',
'/assets/vendor/jquery_realperson_captcha/jquery.plugin.js',
'/assets/vendor/jquery_realperson_captcha/jquery.realperson.min.js'
);
HtmlHelper::registerCssAndScriptsFiles($cssAnsScriptFiles, Yii::app()->theme->baseUrl);

$layoutPath = 'webroot.themes.'.Yii::app()->theme->name.'.views.layouts.';
$this->renderPartial($layoutPath.'header', 
                            array(  "layoutPath"=>$layoutPath ,
                                    "page" => "welcome",
                                    "type" => @$type));
?>

<div class="row">
	<div class="col-md-12 margin-top-15 text-dark menuInvite">
		<ul class="nav nav-tabs">
			<li role="presentation">
				<a href="javascript:" class='radius-10 padding-10 text-green' id="menuRemove">
						<h4><i class="fa fa-trash"></i> 
						<?php echo Yii::t("common","Remove my data"); ?>  </h4>
				</a>
			</li>
			<li role="presentation">
				<a href="javascript:" class="radius-10 padding-10 text-dark" id="menuGet">
					<h4><i class="fa fa-info-circle"></i> 
					<?php echo Yii::t("common","Get my data"); ?> </h4>
				</a>
			</li>
		</ul>
	</div>
</div>

<div class="col-xs-12" id="divRemove" style="margin-top: 10px">

	<legend><?php echo Yii::t("common","Please choose your actions"); ?> :</legend>
	<div class="col-xs-12">
		<input type="checkbox" id="remove1" name="remove" value="remove1">
		<label for="remove1"><?php echo Yii::t("common","Delete my email associated with items"); ?></label>
	</div>
	<div class="col-xs-12">
		<input type="checkbox" id="remove2" name="remove" value="remove2">
		<label for="remove2"><?php echo Yii::t("common","No longer allow my email to be informed about the platform"); ?></label>
	</div>
	<div class="col-xs-12 padding-10">
		<label for="email"><?php echo Yii::t("common","Your mail"); ?> : </label> <input type="text" name="email" id="email" />
	</div>
	<div class="col-xs-12">
		<div class="col-xs-4 padding-10">
			<input placeholder="<?php echo Yii::t("terla", "copy the code here"); ?>" class="col-xs-12 txt-captcha" id="captcha">
		</div>
	</div>
	<div class="col-xs-12">
		<button id="btn-valider" class="btn btn-default" ><!-- <i class="fa fa-times"></i> --><?php echo Yii::t("common","Validate"); ?></button>
	</div>
	<div class="col-xs-12 padding-10">
		<div id="success" class="hidden text-green" style="margin-top: 30px;">
			<h4><?php echo Yii::t("common","An e-mail has just been sent to you to validate the request"); ?> .</h4>
		</div>
		<div id="error" class="hidden text-red" style="margin-top: 30px;">
			<h4 id="errormsg"></h4>
		</div>
	</div>
</div>


<div class="col-xs-12 hidden" id="divGet" style="margin-top: 10px">
	<div class="col-xs-12">
		<legend><?php echo Yii::t("common","Your data associated with your email address"); ?> :</legend>
		<label for="email-info"><?php echo Yii::t("common","Your mail"); ?> : </label> <input type="text" name="email-info" id="email-info" />
	</div>
	<div class="col-xs-12">
		<div class="col-xs-4 padding-10">
			<input placeholder="<?php echo Yii::t("terla", "copy the code here"); ?>" class="col-xs-12 txt-captcha" id="captchaGet">
		</div>
	</div>
	<div class="col-xs-12">
		<button id="btn-valid-info" class="btn btn-default" ><?php echo Yii::t("common","Validate"); ?></button>
	</div>
	<div class="col-xs-12 padding-10">
		<div id="successGet" class="hidden text-green" style="margin-top: 30px;">
			<h4><?php echo Yii::t("common","Vous allez recevoir un e-mail avec vos informations"); ?> .</h4>
		</div>
		<div id="errorGet" class="hidden text-red" style="margin-top: 30px;">
			<h4 id="errormsgget"></h4>
		</div>
	</div>
	
</div>
<script type="text/javascript">
jQuery(document).ready(function() {
	setTitle("Remove donnée : Répertoire","cog");
	$("#captcha").realperson({length: 4});
	$("#captchaGet").realperson({length: 4});


	$("#btn-valider").off().on( "click", function(){
		$("#success").addClass("hidden");
		$("#error").addClass("hidden");

		if($('[name=remove').is(':checked') == true ) {
			var param = {
				removeMail : $("#remove1").prop("checked"),
				notMail  :$("#remove2").prop("checked"),
				email : $("#email").val(),
				captchaUserVal: $("#captcha").val(),
				captchaHash: $("#captcha").realperson('getHash')
			} ;


			mylog.log("param", param);
			$.ajax({
				type: "POST",
				url: baseUrl+"/"+moduleId+"/mailmanagement/removedata/",
				dataType: "json",
				data : param,
				success: function(data){
					
					if (data.result) {
						toastr.success(data.msg);
						$("#email").val("");
						$("#success").removeClass("hidden");
						$("#error").addClass("hidden");
					} else {
						$("#error").removeClass("hidden");
						$("#success").addClass("hidden");
						toastr.error(data.msg);
						$("#errormsg").html(data.msg);
					}
				}
			});
		} else {
			$("#error").removeClass("hidden");
			$("#success").addClass("hidden");
			toastr.error("Select at least one action");
			$("#errormsg").html("Select at least one action");
		}
		
	});

	$("#btn-valid-info").off().on( "click", function(){
		var param = {
			email : $("#email-info").val(),
			captchaUserVal: $("#captchaGet").val(),
			captchaHash: $("#captchaGet").realperson('getHash'),
		} ;

		$.ajax({
			type: "POST",
			url: baseUrl+"/"+moduleId+"/mailmanagement/askdata/",
			dataType: "json",
			data : param,
			success: function(data){
				
				if (data.result) {
					toastr.success(data.msg);
					$("#email-info").val("");
					$("#successGet").removeClass("hidden");
					$("#errorGet").addClass("hidden");
				} else {
					$("#errorGet").removeClass("hidden");
					$("#success").addClass("hidden");
					toastr.error(data.msg);
					$("#errormsgget").html(data.msg);
				}
			}
		});
	});

	$("#menuRemove").click(function() {
		mylog.log("menuRemove");
		$("#divGet").addClass("hidden");
		$("#divRemove").removeClass("hidden");
		$("#menuRemove").addClass("text-green");
		$("#menuGet").removeClass("text-green");
	});
	$("#menuGet").click(function() {
		mylog.log("menuGet");
		$("#divRemove").addClass("hidden");
		$("#divGet").removeClass("hidden");
		$("#menuGet").addClass("text-green");
		$("#menuRemove").removeClass("text-green");
	});

});


</script>
<?php $cssAnsScriptFilesTheme = array(
		"/plugins/jquery-cropbox/jquery.cropbox.css",
		"/plugins/jquery-cropbox/jquery.cropbox.js",
	);

	HtmlHelper::registerCssAndScriptsFiles($cssAnsScriptFilesTheme, Yii::app()->request->baseUrl);
?>
<form  method="post" id="banner_photoAdd" enctype="multipart/form-data">
	<?php 
		$editBtnStr = (isset($profilBannerUrl) && !empty($profilBannerUrl)) ? Yii::t("common","Edit banner") : Yii::t("common","Add a banner"); 
		$editBtnIcon = (isset($profilBannerUrl) && !empty($profilBannerUrl)) ? "pencil" : "plus";

	if( ( $edit || $openEdition ) && !empty(Yii::app()->session["userId"])){ 

	?>
		<div class="user-image-buttons padding-10" style="position: absolute;z-index: 100;">
			<a class="btn btn-blue btn-file btn-upload fileupload-new btn-sm" id="banner_element" >
				<span class="fileupload-new">
					<i class="fa fa-<?php echo $editBtnIcon ?>"></i> 
					<span class="hidden-xs"> 
					<?php echo $editBtnStr ?>
					</span>
				</span>
				<input type="file" accept=".gif, .jpg, .png" name="banner" id="banner_change" class="hide">
				<input class="banner_isSubmit hidden" value="true"/>
			</a>
		</div>
	<?php } ?>
</form>
<div id="modal-crop-banner" class="portfolio-modal modal" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="col-xs-12 no-padding">
        <h3 class="title-comment pull-left"></h3>
        <div class="close-modal" data-dismiss="modal"><div class="lr"><div class="rl"></div></div></div>
    </div>
	<div id="uploadScropResizeAndSaveImage" style="padding:0px 60px;">
		<?php $imgLogo=(@Yii::app()->session['costum']["logo"]) ? Yii::app()->session['costum']["logo"] : Yii::app()->theme->baseUrl."/assets/img/LOGOS/CO2/logo-head-search.png"; ?>
		<!--<div class="col-lg-12">
			<img src="<?php echo $imgLogo ?>" 
				 class="inline margin-top-25 margin-bottom-5" height="50">
	        <br>
		</div>-->
		<div class="modal-header text-dark">
			<h3 class="modal-title text-center" id="ajax-modal-modal-title">
				<i class="fa fa-crop"></i> <?php echo Yii::t("common","Resize and crop your image to render a beautiful banner") ?>
			</h3>
		</div>
		<div class="panel-body">
			<div class='' id='cropContainer'>
				<img src='' id='cropImage' class='' style=''/>
				<div class='col-xs-12 text-center'>
					<button class='btn btn-success text-white imageCrop saveBanner'><i class="fa fa-send"></i> <?php echo Yii::t("common","Save") ?></button>
				</div>
			</div>
		</div>
	</div>
</div>
<script>
var contextId=<?php echo json_encode($id); ?>;
var contextType=<?php echo json_encode($type); ?>;
var contextName=<?php echo json_encode($name); ?>;
jQuery(document).ready(function() {
	$("#col-banner").mouseenter(function(){
		$("#banner_element").show();
	}).mouseleave(function(){
		$("#banner_element").hide();
	});
	//IMAGE CHANGE//
	$("#uploadScropResizeAndSaveImage .close-modal").click(function(){
		$.unblockUI();
	});


	$("#banner_element").click(function(event){
			if (!$(event.target).is('input')) {
					$(this).find("input[name='banner']").trigger('click');
			}
		//$('#'+contentId+'_avatar').trigger("click");		
	});
	
	$('#banner_change').off().on('change.bs.fileinput', function () {
		setTimeout(function(){
			var files = document.getElementById("banner_change").files;
			if (files[0].size > 5000000)
				toastr.warning("<?php echo Yii::t('common','Size maximum 5Mo') ?>");
			else {
				for (var i = 0; i < files.length; i++) {           
			        var file = files[i];
			       	var imageType = /image.*/;     
			        if (!file.type.match(imageType)) {
			            continue;
			        }           
			        var img=document.getElementById("cropImage");            
			        img.file = file;    
			        var reader = new FileReader();
			        reader.onload = (function(aImg) { 
			        	var image = new Image();
							image.src = reader.result;
							img.src = reader.result;
							image.onload = function() {
   							// access image size here 
   						 	var imgWidth=this.width;
   						 	var imgHeight=this.height;
   							if(imgWidth>=400 && imgHeight>=150){
   								$("#modal-crop-banner").modal("show");
               					$("#uploadScropResizeAndSaveImage").parent().css("padding-top", "0px !important");
								setTimeout(function(){
									var setImage={"width":1600,"height":400};
									var parentWidth=$("#cropContainer").width();
									if(parentWidth > 800)
										heightCrop=300;
									else if(parentWidth > 600)
										heightCrop=250;
									else if(parentWidth <= 600)
										heightCrop=200;
									//var parentHeight=setImage.height*(parentWidth/setImage.width);
									var crop = $('#cropImage').cropbox({
										width: parentWidth,
										height: heightCrop,
										zoomIn:true,
										zoomOut:true}, function() {
											cropResult=this.result;
											
									}).on('cropbox', function(e, crop) {
										cropResult=crop;
						        		
						        
									});
									$(".saveBanner").click(function(){
								        
								        //var cropResult=cropResult;
								        $(this).prop("disabled",true);
								        $(this).find("i").removeClass("fa-send").addClass("fa-spin fa-spinner");
								        $("#banner_photoAdd").submit();
									});
									$("#banner_photoAdd").off().on('submit',(function(e) {
										//alert(moduleId);
										$(".banner_isSubmit").val("true");
										e.preventDefault();
										
										var fd = new FormData(document.getElementById("banner_photoAdd"));
										fd.append("parentId", contextId);
										fd.append("parentType", contextType);
										fd.append("formOrigin", "banner");
										//fd.append("contentKey", "banner");
										fd.append("cropW", cropResult.cropW);
										fd.append("cropH", cropResult.cropH);
										fd.append("cropX", cropResult.cropX);
										fd.append("cropY", cropResult.cropY);
										$.ajax({
											url : baseUrl+"/"+moduleId+"/document/uploadSave/dir/communecter/folder/"+contextType+"/ownerId/"+contextId+"/input/banner/docType/image/contentKey/banner",
											type: "POST",
											data: fd,
											contentType: false,
											cache: false, 
											processData: false,
											dataType: "json",
											success: function(data){
										        if(data.result){
										        	$(".saveBanner").prop("disabled",false);
								        			$(".saveBanner").find("i").removeClass("fa-spin fa-spinner").addClass("fa-send");
										        	newBanner='<a href="'+baseUrl+data.src.profilRealBannerUrl+'" class="thumb-info" data-title="'+trad.coverImageof+' '+contextName+'"" data-lightbox="all">'+
										        		'<img class="col-md-12 col-sm-12 col-xs-12 no-padding img-responsive" src="'+baseUrl+data.src.profilBannerUrl+'" style="">'+
										        		'</a>';
										        	$("#contentBanner").html(newBanner);
										        	$(".contentHeaderInformation").addClass("backgroundHeaderInformation");
										        	$('#modal-crop-banner').modal("hide");
	    								    	}
										    }
										});
									}));
								}, 300);
							}
   						 	else
   						 		toastr.warning(trad["minsizebanner"]);
						};
			        });
			        reader.readAsDataURL(file);
			    }  
			}
		}, 400);
	});
});
</script>
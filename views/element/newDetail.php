<?php
	$cssAnsScriptFilesModule = array(
		//Data helper
		'/js/dataHelpers.js',
        '/js/default/editInPlace.js'
	);
	HtmlHelper::registerCssAndScriptsFiles($cssAnsScriptFilesModule, $this->module->getParentAssetsUrl());
	$elementParams=@Yii::app()->session['paramsConfig']["element"];
	if(isset(Yii::app()->session["costum"])){
		$cssJsCostum=array();
		//if(isset($elementParams["js"]))
		//	array_push($cssJsCostum, '/js/'.Yii::app()->session["costum"]["slug"].'/about.js');
		//if(isset($elementParams["css"]))
		//	array_push($cssJsCostum, '/css/'.Yii::app()->session["costum"]["slug"].'/about.css');
		if(!empty($cssJsCostum))
			HtmlHelper::registerCssAndScriptsFiles($cssJsCostum, Yii::app()->getModule( "costum" )->getAssetsUrl());
    }  
?>

<style>
.description{
    border: 1px #e2e2e2 solid;
    margin-top: 2%;
    padding: 1%;
    box-shadow: 0px 0px 20px -2px #e2e2e2;
}
.photo{
    border : 1px #e2e2e2 solid;
    padding: 1%;
    box-shadow: 0px 0px 20px -2px #e2e2e2;
    margin-top: 2%;
}
.file{
    border : 1px #e2e2e2 solid;
    margin-top : 2%;
    padding: 1%;
    box-shadow: 0px 0px 20px -2px #e2e2e2;
}
h2{
    background: #00aca9;
    padding: 1%;
    margin-top: -1%;
    margin-left: -1%;
    margin-right: -1%;
    color: white;
    font-size: 1.8vw;
}
.longDescription{
    font-size : 1.5rem;
}
.reseaux{
    border: 1px #e2e2e2 solid;
    margin-top: 2%;
    padding: 1%;
    box-shadow: 0px 0px 20px -2px #e2e2e2;
}
.adress{
    border: 1px#e2e2e2 solid;
    margin-top: 2%;
    padding: 1%;
    box-shadow: 0px 0px 20px -2px#e2e2e2;
}
.info{
    border: 1px#e2e2e2 solid;
    margin-top: 2%;
    padding: 1%;
    box-shadow: 0px 0px 20px -2px#e2e2e2;
}
span{
    font-size:1vw;
}
@media (max-width:768px){
    h2{
        font-size: 2.2rem;
    }
    span {
        font-size: 1.75vw;
    }
    .visible-xs{
        font-size: 3.5vw;
    }
    .btn{
        font-size:8px;
    }
}

</style>

<!--Description rapido -->
<div class="description col-xs-12">
<h2><i class="fa fa-book"></i> Description
<?php if($edit==true || ( $openEdition==true && Yii::app()->session["userId"] != null ) ){?>
            <button class="btn-update-descriptions btn btn-default letter-blue pull-right tooltips" 
                data-toggle="tooltip" data-placement="top" title="" alt="" data-original-title="<?php echo Yii::t("common","Update description") ?>">
                <b><i class="fa fa-pencil"></i> <?php echo Yii::t("common", "Edit") ?></b>
            </button>
         <?php } ?>
</h2>

<div id="ficheInfo" class="panel panel-white col-lg-12 col-md-12 col-sm-12 no-padding shadow2">
    <div class="panel-body no-padding">
        <div class="col-md-12 col-sm-12 col-xs-12 contentInformation no-padding">
            <div class="col-md-3 col-sm-3 col-xs-3 hidden-xs labelAbout padding-10">
                <span><i class="fa fa-quote-left"></i></span> Résumé 
            </div>
            <div id="shortDescriptionAbout" class="col-md-8 col-sm-8 col-xs-12 valueAbout padding-10">
                <span class="visible-xs col-xs-12 no-padding"><i class="fa fa-quote-left"></i> <?php echo Yii::t("common", "Short description") ?>: </span><?php echo (@$element["shortDescription"]) ? $element["shortDescription"] : '<i>'.Yii::t("common","Not specified").'</i>'; ?>
            </div>
            <span id="shortDescriptionAboutEdit" name="shortDescriptionAboutEdit"  class="hidden" ><?php echo (!empty($element["shortDescription"])) ? $element["shortDescription"] : ""; ?></span>
        </div>
        <div class="col-md-12 col-sm-12 col-xs-12 contentInformation no-padding">
            <div class="col-md-3 col-sm-3 col-xs-3 hidden-xs labelAbout padding-10">
                <span><i class="fa fa-paragraph"></i></span> <?php echo Yii::t("common", "Description") ?>
            </div>
            <div class="col-md-8 col-sm-8 col-xs-12 valueAbout padding-20" 
                    style="word-wrap: break-word; overflow:hidden;">
                <span class="visible-xs col-xs-12 no-padding">
                    <i class="fa fa-paragraph"></i> <?php echo Yii::t("common", "Description") ?>:
                </span>
                <div id="descriptionAbout"><?php echo (@$element["description"]) ? $element["description"] : '<i>'.Yii::t("common","Not specified").'</i>'; ?>
                </div>
            </div>
        </div>
    </div>
</div>


</div>


<!-- information générales --> 
<div class="info col-xs-12">
    <h2> <i class="fa fa-address-card-o"></i> <?php echo Yii::t("common","General information") ?>		
    <?php if($edit==true || ( $openEdition==true && Yii::app()->session["userId"] != null ) ){?>
			<button class="btn-update-info btn btn-default letter-blue pull-right tooltips" 
				data-toggle="tooltip" data-placement="top" title="" alt="" data-original-title="<?php echo Yii::t("common","Update general information") ?>">
				<b><i class="fa fa-pencil"></i> <?php echo Yii::t("common", "Edit") ?></b>
			</button>
		<?php } ?> 
        </h2>
    <div id="ficheInfo" class="panel panel-white col-lg-12 col-md-12 col-sm-12 no-padding shadow2">
	<div class="panel-body no-padding">
		<div class="col-md-12 col-sm-12 col-xs-12 contentInformation no-padding">
			<div class="col-md-4 col-sm-4 col-xs-4 hidden-xs labelAbout padding-10">
				<span><i class="fa fa-pencil"></i></span> <?php echo Yii::t("common", "Name") ?>
			</div>
			<div id="nameAbout" class="col-md-8 col-sm-8 col-xs-12 valueAbout padding-10">
				<span class="visible-xs pull-left margin-right-5"><i class="fa fa-pencil"></i> <?php echo Yii::t("common", "Name") ?> :</span> <?php echo $element["name"]; ?>
			</div>
		</div>
		<?php if($type==Project::COLLECTION){ ?>
			<div class="col-md-12 col-sm-12 col-xs-12 contentInformation no-padding">
				<div class="col-md-4 col-sm-4 col-xs-4 hidden-xs labelAbout padding-10">
					<span><i class="fa fa-line-chart"></i></span> <?php echo Yii::t("project","Project maturity"); ?>
				</div>
				<div  id="avancementAbout" class="col-md-8 col-sm-8 col-xs-12 valueAbout padding-10">
					<span class="visible-xs pull-left margin-right-5"><i class="fa fa-line-chart"></i> <?php echo Yii::t("project","Project maturity"); ?> :</span><?php echo (@$element["properties"]["avancement"]) ? Yii::t("project",$element["properties"]["avancement"]) : '<i>'.Yii::t("common","Not specified").'</i>' ?>
				</div>
			</div>
		<?php } ?>

		<?php if($type==Person::COLLECTION){ ?>
			<div class="col-md-12 col-sm-12 col-xs-12 contentInformation no-padding">
				<div class="col-md-4 col-sm-4 col-xs-4 hidden-xs labelAbout padding-10">
					<span><i class="fa fa-user-secret"></i></span> <?php echo Yii::t("common","Username"); ?>
				</div>
				<div id="usernameAbout" class="col-md-8 col-sm-8 col-xs-12 valueAbout padding-10">
					<span class="visible-xs pull-left margin-right-5"><i class="fa fa-user-secret"></i><?php echo Yii::t("common","Username"); ?> :</span><?php echo (@$element["username"]) ? $element["username"] : '<i>'.Yii::t("common","Not specified").'</i>' ?>
				</div>
			</div>
		<?php if(Preference::showPreference($element, $type, "birthDate", Yii::app()->session["userId"])){ ?>
			<div class="col-md-12 col-sm-12 col-xs-12 contentInformation no-padding">
				<div class="col-md-4 col-sm-4 col-xs-4 hidden-xs labelAbout padding-10">
					<span><i class="fa fa-birthday-cake"></i></span> <?php echo Yii::t("person","Birth date"); ?>
				</div>
				<div id="birthDateAbout" class="col-md-8 col-sm-8 col-xs-12 valueAbout padding-10">
					<span class="visible-xs pull-left margin-right-5"><i class="fa fa-birthday-cake"></i> <?php echo Yii::t("person","Birth date"); ?> :</span><?php echo (@$element["birthDate"]) ? date("d/m/Y", strtotime($element["birthDate"]))  : '<i>'.Yii::t("common","Not specified").'</i>'; ?>
				</div>
			</div>
		<?php }
		} 


 		if($type==Organization::COLLECTION || $type==Event::COLLECTION){ ?>
 				<div class="col-md-12 col-sm-12 col-xs-12 contentInformation no-padding">
					<div class="col-md-4 col-sm-4 col-xs-4 hidden-xs labelAbout padding-10">
						<span><i class="fa fa-angle-right"></i></span><?php echo Yii::t("common", "Type"); ?> 
					</div>
					<div id="typeAbout" class="col-md-8 col-sm-8 col-xs-12 valueAbout padding-10">
						<span class="visible-xs pull-left margin-right-5"><i class="fa fa-angle-right"></i> <?php echo Yii::t("common", "Type"); ?> :</span>

						<?php
						// var_dump($typesList);
						// echo "<br/>";
						// var_dump($element);
						if(@$typesList && @$element["type"] && !empty($typesList[$element["type"]]))
							$showType=Yii::t( "category",$typesList[$element["type"]]);
						else if (@$element["type"])
							$showType=Yii::t( "category",$element["type"]);
						else
							$showType='<i>'.Yii::t("common","Not specified").'</i>';
						echo $showType; ?>
					</div>
				</div>
		<?php }

		if( (	$type==Person::COLLECTION && 
				Preference::showPreference($element, $type, "email", Yii::app()->session["userId"]) ) || 
		  	in_array($type, [Organization::COLLECTION, Project::COLLECTION, Event::COLLECTION])) { ?>
		  	<div class="col-md-12 col-sm-12 col-xs-12 contentInformation no-padding">
				<div class="col-md-4 col-sm-4 col-xs-4 hidden-xs labelAbout padding-10">
					<span><i class="fa fa-envelope"></i></span> <?php echo Yii::t("common","E-mail"); ?>
				</div>
				<div id="emailAbout" class="col-md-8 col-sm-8 col-xs-12 valueAbout padding-10">
					<span class="visible-xs pull-left margin-right-5"><i class="fa fa-envelope"></i> <?php echo Yii::t("common","E-mail"); ?> :</span><?php echo (@$element["email"]) ? $element["email"]  : '<i>'.Yii::t("common","Not specified").'</i>'; ?>
				</div>
			</div>
		<?php } ?>



		<?php if( $type != Person::COLLECTION /*&& $type != Organization::COLLECTION */ ){ ?>
			<div class="col-md-12 col-sm-12 col-xs-12 contentInformation no-padding">
				<div class="col-md-4 col-sm-4 col-xs-4 hidden-xs labelAbout padding-10">
					<span><i class="fa fa-link"></i></span> <?php echo Yii::t("common","Carried by"); ?>
				</div>
				<div id="parentAbout" class="col-md-8 col-sm-8 col-xs-12 valueAbout padding-10">
					<span class="visible-xs pull-left margin-right-5"><i class="fa fa-desktop"></i> <?php if($type == Event::COLLECTION) echo Yii::t("common","Planned on"); else echo Yii::t("common","Carried by"); ?> :</span>
				<?php 
					if(!empty($element["parent"])){
						$count=count($element["parent"]);
						foreach($element['parent'] as $key =>$v){
							$heightImg=($count>1) ? 35 : 25;
							$imgPath = (@$v["profilThumbImageUrl"] && !empty($v["profilThumbImageUrl"])) ? Yii::app()->createUrl('/'.$v["profilThumbImageUrl"]) : $this->module->assetsUrl.'/images/thumb/default_'.$v["type"].'.png' ?> 
							<a href="#page.type.<?php  echo $v['type']; ?>.id.<?php  echo $key; ?>" 
								class="lbh tooltips"
								<?php if($count>1) echo 'data-toggle="tooltip" data-placement="left" title="'.$v["name"].'"' ?>> 
								<img src="<?php echo $imgPath ?>" class="img-circle" width='<?php echo $heightImg ?>' height='<?php echo $heightImg ?>' />
								<?php  if ($count==1) echo $v['name']; ?>
							</a>
				<?php 	}
				 	}else
						echo '<i>'.Yii::t("common","Not specified").'</i>';
				?>
				</div>
			</div>
		<?php } ?>

		<?php if($type == Event::COLLECTION){ ?>
			<div class="col-md-12 col-sm-12 col-xs-12 contentInformation no-padding">
				<div class="col-md-4 col-sm-4 col-xs-4 hidden-xs labelAbout padding-10">
					<span><i class="fa fa-link"></i></span> <?php echo Yii::t("common","Organized by"); ?>
				</div>
				<div id="organizerAbout" class="col-md-8 col-sm-8 col-xs-12 valueAbout padding-10">
					<span class="visible-xs pull-left margin-right-5"><i class="fa fa-desktop"></i> <?php echo Yii::t("common","Organized by"); ?> :</span>
				<?php 
					if(!empty($element["organizer"])){
						$count=count($element["organizer"]);
						foreach($element['organizer'] as $key =>$v){
							$heightImg=($count>1) ? 35 : 25;
							$imgPath = (@$v["profilThumbImageUrl"] && !empty($v["profilThumbImageUrl"])) ? Yii::app()->createUrl('/'.$v["profilThumbImageUrl"]) : $this->module->assetsUrl.'/images/thumb/default_'.$v["type"].'.png' ?>
							<a href="#page.type.<?php  echo $v['type']; ?>.id.<?php  echo $key; ?>" 
								class="lbh tooltips"
								<?php if($count>1) echo 'data-toggle="tooltip" data-placement="left" title="'.$v["name"].'"' ?>> 
								<img src="<?php echo $imgPath ?>" class="img-circle" width='<?php echo $heightImg ?>' height='<?php echo $heightImg ?>' /> 
								<?php  if ($count==1) echo $v['name']; ?>
							</a>
				<?php 	} 
				 	}else
						echo '<i>'.Yii::t("common","Not specified").'</i>';
				?>
				</div>
			</div>
		<?php } ?>





		<?php if($type!=Poi::COLLECTION){ ?>
			<div class="col-md-12 col-sm-12 col-xs-12 contentInformation no-padding">
				<div class="col-md-4 col-sm-4 col-xs-4 hidden-xs labelAbout padding-10">
					<span><i class="fa fa-desktop"></i></span> <?php echo Yii::t("common","Website URL"); ?>
				</div>
				<div id="webAbout" class="col-md-8 col-sm-8 col-xs-12 valueAbout padding-10">
					<span class="visible-xs pull-left margin-right-5"><i class="fa fa-desktop"></i> <?php echo Yii::t("common","Website URL"); ?> :</span>
				<?php 
					if(@$element["url"]){
						//If there is no http:// in the url
						$scheme = ( (!preg_match("~^(?:f|ht)tps?://~i", $element["url"]) ) ? 'http://' : "" ) ;
					 	echo '<a href="'.$scheme.$element['url'].'" target="_blank" id="urlWebAbout" style="cursor:pointer;">'.$element["url"].'</a>';
					}else
						echo '<i>'.Yii::t("common","Not specified").'</i>'; ?>
				</div>
			</div>
		<?php } ?>
		<?php  if($type==Organization::COLLECTION || $type==Person::COLLECTION){ ?>
			<div class="col-md-12 col-sm-12 col-xs-12 contentInformation no-padding">
				<div class="col-md-4 col-sm-4 col-xs-4 hidden-xs labelAbout padding-10">
					<span><i class="fa fa-phone"></i></span> <?php echo Yii::t("common","Phone"); ?>
				</div>
				<div id="fixeAbout" class="col-md-8 col-sm-8 col-xs-12 valueAbout padding-10">
					<span class="visible-xs pull-left margin-right-5"><i class="fa fa-phone"></i> <?php echo Yii::t("common","Phone"); ?> :</span><?php
						$fixe = '<i>'.Yii::t("common","Not specified").'</i>';
						if( !empty($element["telephone"]["fixe"]))
							$fixe = ArrayHelper::arrayToString($element["telephone"]["fixe"]);
						
						echo $fixe;
					?>	
				</div>
			</div>
			<div class="col-md-12 col-sm-12 col-xs-12 contentInformation no-padding">
				<div class="col-md-4 col-sm-4 col-xs-4 hidden-xs labelAbout padding-10">
					<span><i class="fa fa-mobile"></i></span> <?php echo Yii::t("common","Mobile"); ?>
				</div>
				<div id="mobileAbout" class="col-md-8 col-sm-8 col-xs-12 valueAbout padding-10">
					<span class="visible-xs pull-left margin-right-5">
						<i class="fa fa-mobile"></i> <?php echo Yii::t("common","Mobile"); ?> :
					</span>
					<?php
						$mobile = '<i>'.Yii::t("common","Not specified").'</i>';
						if( !empty($element["telephone"]["mobile"]))
							$mobile = ArrayHelper::arrayToString($element["telephone"]["mobile"]);	
						echo $mobile;
					?>	
				</div>
			</div>
			<div class="col-md-12 col-sm-12 col-xs-12 contentInformation no-padding">
				<div class="col-md-4 col-sm-4 col-xs-4 hidden-xs labelAbout padding-10">
					<span><i class="fa fa-fax"></i></span> <?php echo Yii::t("common","Fax"); ?>
				</div>
				<div id="faxAbout" class="col-md-8 col-sm-8 col-xs-12 valueAbout padding-10">
					<span class="visible-xs pull-left margin-right-5">
						<i class="fa fa-fax"></i> <?php echo Yii::t("common","Fax"); ?> :
					</span>
					<?php
						$fax = '<i>'.Yii::t("common","Not specified").'</i>';
						if( !empty($element["telephone"]["fax"]) )
							$fax = ArrayHelper::arrayToString($element["telephone"]["fax"]);		
						echo $fax;
					?>
				</div>
			</div>
		<?php } ?>

			<div class="col-md-12 col-sm-12 col-xs-12 contentInformation no-padding">
				<div class="col-md-4 col-sm-4 col-xs-4 hidden-xs labelAbout padding-10">
					<span><i class="fa fa-hashtag"></i></span> <?php echo Yii::t("common","Tags"); ?>
				</div>
				<div id="tagsAbout" class="col-md-8 col-sm-8 col-xs-12 valueAbout padding-10">
					<span class="visible-xs pull-left margin-right-5">
						<i class="fa fa-hashtag"></i> <?php echo Yii::t("common","Tags"); ?> :
					</span>
					<?php 	
						if(!empty($element["tags"])){
							foreach ($element["tags"]  as $key => $tag) { 
	        					echo '<a href="#search?text=#'.$tag.'" class="lbh badge letter-red bg-white">'.$tag.'</a>';
	   						}
						}else{
							echo '<i>'.Yii::t("common","Not specified").'</i>';
						} ?>	
				</div>
			</div>
			

			<!--<div class="col-md-12 col-sm-12 col-xs-12 contentInformation no-padding">
				<div class="col-md-4 col-sm-4 col-xs-4 hidden-xs labelAbout padding-10">
					<span><i class="fa fa-child"></i></span> <?php echo Yii::t("common","Gamification"); ?>
				</div>
				<div id="tgamificationAbout" class="col-md-8 col-sm-8 col-xs-12 valueAbout padding-10">
					<span class="visible-xs pull-left margin-right-5">
						<i class="fa fa-child"></i> <?php echo Yii::t("common","Gamification"); ?> :
					</span>
					<?php 	
						echo Gamification::calcPoints( $type,(string)$element["_id"] ).Yii::t("common","pts , based on Links and Posts popularity");
					?>	
				</div>
			</div>-->
			
			<!--<div class="col-md-12 col-sm-12 col-xs-12 contentInformation no-padding">
				<div class="col-md-4 col-sm-4 col-xs-4 hidden-xs labelAbout padding-10">
					<a href="/co2/badges"><span><i class="fa fa-bookmark"></i></span> <?php echo Yii::t("common","Badge"); ?></a>
				</div>
				<div id="tbadgeAbout" class="col-md-8 col-sm-8 col-xs-12 valueAbout padding-10">
					<span class="visible-xs pull-left margin-right-5">
						<a href="/co2/badges"><i class="fa fa-bookmark"></i> <?php echo Yii::t("common","Badge"); ?></a> :
					</span>
					<style type="text/css">
						.badgePH{ 
							cursor: pointer;
							display: inline-block;
							margin-right: 10px;
							border: 1px solid #666;
						}
						.badgePH .fa-stack .main { font-size:2.2em}
						.badgePH .fa-stack .mainTop { margin-top:-3px}
						.badgePH .fa-stack .fa-circle-o{ font-size:4em;}
						/* Tooltip container */
						.opendata .mainTop{
						    color: black;
						    font-size: 1.3em;
						    padding: 5px;
						}
						.opendata .main{
						    color: #00cc00;
						}
					</style>
					
					<?php 

					if( $type==Person::COLLECTION && !empty($element["tags"])){
							
							$badges = PHDB::find("badges",array("tag" => array('$in' =>$element["tags"])));
							foreach ($badges as $k => $v) {
								//keep only badge definitions used 
								if(in_array( $v["tag"], $element["tags"]))
									$badges[$v["tag"]] = $v;
								unset($badges[$k]);
							}
							if(count($badges)){								
								foreach ($element["tags"] as $k => $v) {
								?>
								<div class="badgePH pull-left" data-title="<?php echo @$value["name"] ?>">
									<span class="fa-stack tooltips" style="margin-bottom:5px" data-toggle="tooltip" data-placement="bottom" title='<?php echo Yii::t("badge",@$value["name"], null, Yii::app()->controller->module->id)?>'>
										<?php if( @$badges[$v]["profilThumbImageUrl"] != "" ){ ?>
											<img class="img-responsive" src="<?php echo @$badges[$v]["profilThumbImageUrl"] ?>">
										<?php } else if(is_array(@$badges[$v]["icon"]) && count($badges[$v]["icon"]) == 1 ){ ?>
										<i class="fa <?php echo $badges[$v]["icon"][0] ?> main fa-2x fa-stack-1x" style="color:<?php echo (@$badges[$v]["color"]) ? $badges[$v]["color"] : "red" ?>"></i>
										<?php } else if(is_array(@$badges[$v]["icon"]) && count($badges[$v]["icon"]) == 2 ){ ?>
										<i class="fa <?php echo $badges[$v]["icon"][0] ?> main fa-2x fa-stack-1x" style="color:<?php echo (@$badges[$v]["color"]) ? $badges[$v]["color"] : "red"?>"></i>
										<i class="fa <?php echo $badges[$v]["icon"][0] ?> mainTop fa-stack-1x text-white"></i>
										<?php } else {?>
										<i class="fa <?php echo @$badges[$v]["icon"] ?> main fa-2x fa-stack-1x "  style="color:<?php echo (@$badges[$v]["color"]) ? $badges[$v]["color"] : "red"?>"></i>
										<?php } ?>
									</span>
								</div>
							<?php 
								}
							}
							

							// if( $badges = Badge::getBadgesByLinks($element["links"]) ){ 
							// 	foreach ($badges as $key => $b) {
								/* ?>
								 <div class="badgePH pull-left">
									<span class="fa-stack tooltips" style="margin-bottom:5px" data-toggle="tooltip" data-placement="bottom" title='<?php echo $b["name"]?>'>
										<?php if(@$b["img"]){ ?>
										<img src="<?php echo $b["img"]?>" class="img-circle" style="height:40px" >
										<?php } else { ?>
											<i class="fa fa-<?php echo Element::getFaIcon ( $b["type"] ) ?> main fa-2x fa-stack-1x text-red"></i>
										<?php } ?>

									</span>
								</div> 
							<?php */
						// }
						// 	} 
							?>


						<?php } ?>
				</div>
			</div>-->
	</div>
	
</div>

</div>
<!-- Adresse -->
<div class="adress col-xs-12">
    <h2>	<i class="fa fa-map-marker"></i> <?php echo Yii::t("common","Localitie(s)"); ?></h2>
	<div id="adressesAbout" class="panel panel-white col-lg-12 col-md-12 col-sm-12 col-xs-12 no-padding shadow2">

		<div class="panel-body no-padding">		

			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 labelAbout padding-10">
				<span><i class="fa fa-home"></i></span> <?php echo Yii::t("common", "Main locality") ?>
				<?php if (!empty($element["address"]["codeInsee"]) && ( $edit==true || ( $openEdition==true && Yii::app()->session["userId"] != null ) ) ) { 
					// echo '<a href="javascript:;" id="btn-remove-geopos" class="pull-right tooltips" data-toggle="tooltip" data-placement="bottom" title="'.Yii::t("common","Remove Locality").'">
					// 			<i class="fa text-red fa-trash-o"></i>
					// 		</a> 
					echo '<a href="javascript:;" class="btn-update-geopos pull-right tooltips margin-right-15" data-toggle="tooltip" data-placement="bottom" title="'.Yii::t("common","Update Locality").'" >
								<i class="fa text-red fa-map-marker"></i>
							</a> ';	
				} ?>
			</div>
			<div class="col-md-12 col-xs-12 valueAbout no-padding" style="padding-left: 25px !important">
			<?php 
				if( ($type == Person::COLLECTION && Preference::showPreference($element, $type, "locality", Yii::app()->session["userId"])) ||  $type!=Person::COLLECTION) {
					$address = "";
					$address .= '<span id="detailAddress"> '.
									(( @$element["address"]["streetAddress"]) ? 
										$element["address"]["streetAddress"]."<br/>": 
										((@$element["address"]["codeInsee"])?"":Yii::t("common","Unknown Locality")));
					$address .= (( @$element["address"]["postalCode"]) ?
									 $element["address"]["postalCode"].", " :
									 "")
									." ".(( @$element["address"]["addressLocality"]) ? 
											 $element["address"]["addressLocality"] : "") ;
					$address .= (( @$element["address"]["addressCountry"]) ?
									 ", ".OpenData::$phCountries[ $element["address"]["addressCountry"] ] 
					 				: "").
					 			'</span>';
					echo $address;
					if( empty($element["address"]["codeInsee"]) && Yii::app()->session["userId"] == (String) $element["_id"]) { ?>
						<br><a href="javascript:;" class="cobtn btn btn-danger btn-sm" style="margin: 10px 0px;">
								<?php echo Yii::t("common", "Connect to your city") ?></a> 
							<a href="javascript:;" class="whycobtn btn btn-default btn-sm explainLink" style="margin: 10px 0px;" onclick="showDefinition('explainCommunectMe',true)">
								<?php echo  Yii::t("common", "Why ?") ?></a>
					<?php }
			}else
				echo '<i>'.Yii::t("common","Not specified").'</i>';
			?>
			</div>
		</div>
		<?php if( !empty($element["addresses"]) ){ ?>
			<div class="col-md-12 col-xs-12 labelAbout padding-10">
				<span><i class="fa fa-map"></i></span> <?php echo Yii::t("common", "Others localities") ?>
			</div>
			<div class="col-md-12 col-xs-12 valueAbout no-padding" style="padding-left: 25px !important">
			<?php	foreach ($element["addresses"] as $ix => $p) { ?>			
				<span id="addresses_<?php echo $ix ; ?>">
					<span>
					<?php 
					$address = '<span id="detailAddress_'.$ix.'"> '.
									(( @$p["address"]["streetAddress"]) ? 
										$p["address"]["streetAddress"]."<br/>": 
										((@$p["address"]["codeInsee"])?"":Yii::t("common","Unknown Locality")));
					$address .= (( @$p["address"]["postalCode"]) ?
									 $p["address"]["postalCode"].", " :
									 "")
									." ".(( @$p["address"]["addressLocality"]) ? 
											 $p["address"]["addressLocality"] : "") ;
					$address .= (( @$p["address"]["addressCountry"]) ?
									 ", ".OpenData::$phCountries[ $p["address"]["addressCountry"] ] 
					 				: "").
					 			'</span>';
					echo $address;
					?>

					<?php //if( $edit==true || ( $openEdition==true && Yii::app()->session["userId"] != null ) ) { ?>
						<!-- <a href='javascript:removeAddresses("<?php //echo $ix ; ?>");'  class="addresses pull-right tooltips margin-right-15" data-toggle="tooltip" data-placement="bottom" title="<?php //echo Yii::t("common","Remove Locality");?>"><i class="fa text-red fa-trash-o"></i></a>
						<a href='javascript:updateLocalityEntities("<?php //echo $ix ; ?>", <?php //echo json_encode($p);?>);' class=" pull-right pull-right tooltips margin-right-15" data-toggle="tooltip" data-placement="bottom" title="<?php //echo Yii::t("common","Update Locality");?>"><i class="fa text-red fa-map-marker addresses"></i></a></span> -->
					<?php //} ?>
				</span>
				<hr/>
			<?php 	} ?>
			</div>
		<?php } ?>
		<div class="text-right padding-10">
			<?php if(empty($element["address"]) && $type!=Person::COLLECTION && ($edit==true || ( $openEdition==true && Yii::app()->session["userId"] != null ) )){ ?>
				<b><a href="javascript:;" class="btn btn-default letter-blue margin-top-5 addresses btn-update-geopos">
					<i class="fa fa-map-marker"></i>
					<span class="hidden-sm"><?php echo Yii::t("common","Add a primary address") ; ?></span>
				</a></b>
			<?php	}
			//if($type!=Person::COLLECTION && !empty($element["address"]) && ($edit==true || ( $openEdition==true && Yii::app()->session["userId"] != null ) )) { ?>
				<!-- <b><a href='javascript:updateLocalityEntities("<?php //echo count(@$element["addresses"]) ; ?>");' id="btn-add-geopos" class="btn btn-default letter-blue margin-top-5 addresses" style="margin: 10px 0px;">
					<i class="fa fa-plus" style="margin:0px !important;"></i> 
					<span class="hidden-sm"><?php //echo Yii::t("common","Add an address"); ?></span>
				</a></b> -->
			<?php //} ?>						
		</div>
	</div>
</div>

<!-- Réseaux sociaux -->

<div class="reseaux col-xs-12">
<h2><i class="fa fa-connectdevelop"></i> <?php echo Yii::t("common","Socials"); ?> 
<?php if($edit==true || ( $openEdition==true && Yii::app()->session["userId"] != null ) ) {?>
                <button class="btn-update-network btn btn-default letter-blue pull-right tooltips" 
                    data-toggle="tooltip" data-placement="top" title="" alt="" data-original-title="<?php echo Yii::t("common","Update network") ?>">
                    <b><i class="fa fa-pencil"></i></b>
                </button>
            <?php } ?>
</h2>

    <div class="col-xs-12" style="font-size: 1.25vw;">
        <div class="col-xs-3">
        <i class="fa fa-skype"></i> <span id="divSkype"> <span id="skypeAbout"><?php echo (@$element["socialNetwork"]["skype"]) ?  '<a href="'.$element["socialNetwork"]["skype"].'" target="_blank">'.$element["socialNetwork"]["skype"].'</a>' : '<i>'.Yii::t("common","Not specified").'</i>'; ?></span></span>
        </div>
        <div class="col-xs-3"> 
        <i class="fa fa-github"></i> <span id="divGithub"> <span id="githubAbout"><?php echo (@$element["socialNetwork"]["github"]) ?  '<a href="'.$element["socialNetwork"]["github"].'" target="_blank">'.$element["socialNetwork"]["github"].'</a>' : '<i>'.Yii::t("common","Not specified").'</i>'; ?></span></span>
        </div>
        <div class="col-xs-3">
        <i class="fa fa-google-plus"></i> <span id="divGpplus"><span id="gpplusAbout"><?php echo (@$element["socialNetwork"]["googleplus"]) ?  '<a href="'.$element["socialNetwork"]["googleplus"].'" target="_blank">'.$element["socialNetwork"]["googleplus"].'</a>' : '<i>'.Yii::t("common","Not specified").'</i>'; ?></span></span>
        </div>
        <div class="col-xs-3">
        <i class="fa fa-twitter"></i> <span id="divTwitter"><span id="twitterAbout"><?php echo (@$element["socialNetwork"]["twitter"]) ?  '<a href="'.$element["socialNetwork"]["twitter"].'" target="_blank">'.$element["socialNetwork"]["twitter"].'</a>' : '<i>'.Yii::t("common","Not specified").'</i>'; ?></span></span>
        </div>
        <div class="col-xs-3"> 
        <i class="fa fa-facebook"></i>  <span id="divFacebook"><span id="facebookAbout"><?php echo (@$element["socialNetwork"]["facebook"]) ?  '<a href="'.$element["socialNetwork"]["facebook"].'" target="_blank">'.$element["socialNetwork"]["facebook"].'</a>' : '<i>'.Yii::t("common","Not specified").'</i>'; ?></span></span>
        </div>
        <div  class="col-xs-3">
        <i class="fa fa-instagram"></i> <span id="divInstagram"> <span id="instagramAbout"><?php echo (@$element["socialNetwork"]["instagram"]) ?  '<a href="'.$element["socialNetwork"]["instagram"].'" target="_blank">'.$element["socialNetwork"]["instagram"].'</a>' : '<i>'.Yii::t("common","Not specified").'</i>'; ?></span></span>
        </div>
        <div class="col-xs-3">
        <i class="fa fa-telegram"></i> <span id="divDiaspora"><span id="diasporaAbout"><?php echo (@$element["socialNetwork"]["diaspora"]) ?  '<a href="'.$element["socialNetwork"]["diaspora"].'" target="_blank">'.$element["socialNetwork"]["diaspora"].'</a>' : '<i>'.Yii::t("common","Not specified").'</i>'; ?></span></span>
        </div>
        <div class="col-xs-3">
        <i class="fa fa-mastodon"></i> <span id="divMastodon"><span id="mastodonAbout"><?php echo (@$element["socialNetwork"]["mastodon"]) ?  '<a href="'.$element["socialNetwork"]["mastodon"].'" target="_blank">'.$element["socialNetwork"]["mastodon"].'</a>' : '<i>'.Yii::t("common","Not specified").'</i>'; ?></span></span>
        </div>
    </div>
</div>


<!-- Espace document --> 
<div class="file col-xs-12">
    <h2><i class="fa fa-file"></i> Documents</h2>
    <div class="col-xs-12"><ul class="file-description"></ul></div>

    <div class="col-xs-12 no-padding" style="border-top: 2px #e2e2e2 dashed; margin-top: 1%; text-align:right;">
    <a class="btn btn-warning ssmla filter-folder-gallery-documents" data-view="gallery" style="margin-top: 1%;"><i class="fa fa-plus"></i> Voir plus</a>
    </div>
</div>



<!-- Espace photo -->
<div class="photo col-xs-12">
    <div class="col-xs-12"><h2 style="margin-left: -2.37%; margin-right: -2.37%;"><i class="fa fa-camera"></i> Photos</h2></div>
    <!-- AFFICHE 4 DERNIERE PHOTO -->
    <div class="img-description"></div>
    
    <div class="col-xs-12 no-padding" style="border-top: 2px #e2e2e2 dashed; margin-top: 1%; text-align:right;">
    <div class="portfolio-item portfolio-item-album openFolder" data-folder="" data-folder-name="Photos" data-doctype="image" data-key="">
        <a href="javascript:;" class="btn btn-warning filter-folder-gallery-img" style="margin-top: 1%;"><i class="fa fa-plus"></i> Voir plus</a>
    </div>
    </div>
</div>

<script>

$(document).ready(function(){
    bindDynFormEditable();

    $(".filter-folder-gallery-img").click(function(){
		$(this).attr("href","#@"+contextData.slug+".view.gallery.dir.image");
        $("#btn-start-detail").removeClass("active");
        $("#btn-start-gallery").addClass("active");

        $('html, body').animate({
            scrollTop: $("#header-gallery")
        }, "slow");

    });

    $(".filter-folder-gallery-documents").click(function(){
		$(this).attr("href","#@"+contextData.slug+".view.gallery.dir.file");
        $("#btn-start-detail").removeClass("active");
        $("#btn-start-gallery").addClass("active");

        $('html, body').animate({
            scrollTop: $("#header-gallery")
        }, "slow");
    });

});
</script>
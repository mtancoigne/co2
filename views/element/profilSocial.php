<?php 
	HtmlHelper::registerCssAndScriptsFiles( 
		array( 
			'/vendor/colorpicker/js/colorpicker.js',
			'/vendor/colorpicker/css/colorpicker.css',
			'/css/default/directory.css',
			'/css/default/settings.css',	
			'/css/profilSocial.css',
			'/css/calendar.css',
		), Yii::app()->theme->baseUrl. '/assets'
	);
	//var_dump($this->module->assetsUrl);
 	$cssAnsScriptFilesModule = array(
    	'/js/default/calendar.js',
	    '/js/default/profilSocial.js',
	    '/js/default/editInPlace.js',
	    '/js/default/settings.js'
	);
  	HtmlHelper::registerCssAndScriptsFiles($cssAnsScriptFilesModule, $this->module->assetsUrl);

	$cssAnsScriptFilesTheme = array(
		// SHOWDOWN
		//'/plugins/showdown/showdown.min.js',
		//MARKDOWN
		//'/plugins/to-markdown/to-markdown.js',
		'/plugins/jquery.qrcode/jquery-qrcode.min.js',
		'/plugins/fullcalendar/fullcalendar/fullcalendar.min.js',
        '/plugins/fullcalendar/fullcalendar/fullcalendar.css', 
        '/plugins/fullcalendar/fullcalendar/locale/'.Yii::app()->language.'.js',
        "/plugins/d3/d3.js",
        "/plugins/d3/d3-flextree.js",
        "/plugins/d3/view.mindmap.js",
        "/plugins/d3/view.mindmap.css",
        '/plugins/bootstrap-toggle/css/bootstrap-toggle.min.css',
		'/plugins/bootstrap-toggle/js/bootstrap-toggle.min.js' , 
		'/plugins/bootstrap-switch/dist/css/bootstrap3/bootstrap-switch.min.css',
	  	'/plugins/bootstrap-switch/dist/js/bootstrap-switch.min.js' 
	);

	HtmlHelper::registerCssAndScriptsFiles($cssAnsScriptFilesTheme, Yii::app()->request->baseUrl);
	$elementParams=@Yii::app()->session['paramsConfig']["element"];
	if(isset(Yii::app()->session["costum"])){
		$cssJsCostum=array();
		if(isset($elementParams["js"]))
			array_push($cssJsCostum, '/js/'.Yii::app()->session["costum"]["slug"].'/pageProfil.js');
		if(isset($elementParams["css"]))
			array_push($cssJsCostum, '/css/'.Yii::app()->session["costum"]["slug"].'/pageProfil.css');
		if(!empty($cssJsCostum))
			HtmlHelper::registerCssAndScriptsFiles($cssJsCostum, Yii::app()->getModule( "costum" )->getAssetsUrl());
	}




	$imgDefault = $this->module->assetsUrl.'/images/thumbnail-default.jpg';

    if(empty($element["_id"])){ 
    	$what=(@$element["type"]) ? "the ".Element::getControlerByCollection($element["type"]) : "the element";
    	?>
    	<script type="text/javascript">
    		urlCtrl.loadByHash("");
			bootbox.dialog({message:"<div class='alert-danger text-center'><strong><?php echo Yii::t("common","{what}, that you are looking for, has been deleted or doesn't exist", array("{what}"=>ucfirst(Yii::t("common", $what)))) ?></strong></div>"});
		</script>
    <?php 
		exit;
	} 
    if(@Yii::app()->params["front"]) $front = Yii::app()->params["front"];
    $layoutPath = 'webroot.themes.'.Yii::app()->theme->name.'.views.layouts.';
	$auth = Authorisation::canParticipate(Yii::app()->session['userId'], $type, (string)$element["_id"]);
	$imgDefault = $this->module->assetsUrl.'/images/thumbnail-default.jpg';
	$thumbAuthor =  (isset($element['profilThumbImageUrl']) && !empty($element['profilThumbImageUrl'])) ? Yii::app()->createUrl('/'.@$element['profilThumbImageUrl']) 
                      : $this->module->assetsUrl.'/images/thumbnail-default.jpg';
	?>

<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 no-padding <?php echo $typeItem ?>-profil <?php echo $typeItemHead ?>-item <?php echo $categoryItem ?>">	
    <!-- Header Banner in element -->
    <?php
    $this->renderPartial('co2.views.element.header', 
			        			array(	"iconColor"=>$iconColor,
			        					"icon"=>$icon,
			        					"type"=>$type,
			        					"element"=>$element,
			        					"pageConfig"=>$pageConfig,
			        					"linksBtn"=>$linksBtn,
		        						"invitedMe"=>@$invitedMe,
			        					"elementId"=>(string)$element["_id"],
			        					"elementType"=>$type,
			        					"elementName"=> $element["name"],
			        					"edit" => @$canEdit,
			        					"openEdition" => @$openEdition) 
			        			);
	?>
    <!-- END HEADER ELEMENT BANNER -->

    <!-- ///////////////////////// MENUTOP //////////////////////////////////
			//// Containing image profil & group of button /////////// -->
    <?php if(isset($elementParams["menuTop"]) && !empty($elementParams["menuTop"])){ ?>
    <div id="menu-top-profil-social" class="sub-menu-social no-padding <?php echo $elementParams["containerClass"]["menuTop"] ?>">
    	<?php if(isset($elementParams["menuTop"]["img"]) && !empty($elementParams["menuTop"]["img"]) ){ ?> 
    	<div id="menu-top-profil-image" class="<?php echo $elementParams["containerClass"]["menuTopImage"] ?>">
			<?php 	
				if(@$element["profilMediumImageUrl"] && !empty($element["profilMediumImageUrl"]))
					$images=array(
					 	"medium"=>$element["profilMediumImageUrl"],
					 	"large"=>$element["profilImageUrl"]
					);
				else $images="";	
				$this->renderPartial('co2.views.pod.fileupload', 
					array(
						"itemId" => (string) $element["_id"],
						"itemName" => $element["name"],
						"type" => $type,
						"resize" => false,
						"contentId" => Document::IMG_PROFIL,
						"show" => true,
						"editMode" => $canEdit,
						"image" => $images,
						"openEdition" => $openEdition) 
				); 
			?>
		</div>
		<?php } ?>
		<?php if(isset($elementParams["menuTop"]["btnGroup"]) && !empty($elementParams["menuTop"]["btnGroup"]) ){ ?> 
	    	<div id="menu-top-btn-group" class="<?php echo $elementParams["containerClass"]["menuTopGroupBtn"] ?>">
			    <?php $params = array(  
			    					"element" => @$element, 
		                            "type" => @$type, 
		                            "edit" => @$canEdit,
		                        	"id"=>$id,    
		                            "openEdition" => $openEdition,
		                            "iconColor"=>$iconColor,
		                            "thumbAuthor"=>$thumbAuthor,
		                            "openMenuXs"=>@$elementParams["menuTop"]["openMenuXs"],
		                            "buttonTop"=>$elementParams["menuTop"]["btnGroup"]
		                        );
			    	$this->renderPartial('co2.views.element.menus.top', $params );
			    ?> 
			</div>
		<?php }	?>
		</div>
	<?php } ?>
	<!-- ///////////// END MENU TOP //////////////--> 
	<?php if(isset($elementParams["menuLeft"]) && !empty($elementParams["menuLeft"])){ ?>
	<div id="menu-left-container" class="<?php echo $elementParams["containerClass"]["menuLeft"] ?>" >  		
	    <?php 
	    	$params = array(  
		    	"element" => @$element, 
	            "type" => @$type, 
	            "edit" => @$canEdit,
	            "countNotifElement"=>@$countNotifElement,
	            "invitedMe" => @$invitedMe,
	            "openEdition" => $openEdition,
	            "linksBtn" => $linksBtn,
	            "elementParams"=>$elementParams
	        );
	    	$this->renderPartial('co2.views.element.menus.left', $params ); 
	    ?>
	</div>
	<?php } ?>
	<!-- <div id="mapPage" class="<?php //echo $elementParams["containerClass"]["centralSection"] ?>" style="height: 600px"></div> -->
	<div class="<?php echo $elementParams["containerClass"]["centralSection"] ?>  no-padding central-section">
		<div class="col-xs-12 padding-50 links-main-menu" id="div-select-create">
			<div class="col-md-12 col-sm-12 col-xs-12 padding-15 shadow2 bg-white ">
		       
		       	<h4 class="text-center margin-top-15" style="">
			       	<img class="img-circle" src="<?php echo $thumbAuthor; ?>" height=30 width=30 style="margin-top:-10px;">
			       	<a class="btn btn-link pull-right text-dark" id="btn-close-select-create" style="margin-top:-10px;">
			       		<i class="fa fa-times-circle fa-2x"></i>
			       	</a>
			       	<span class="name-header"><?php echo @$element["name"]; ?></span>
			       	<br>
			       	<i class="fa fa-plus-circle"></i> <?php echo Yii::t("form","Create content link to this page") ?>
			       	<br>
			       	<small><?php echo Yii::t("form","What kind of content will you create ?") ?></small>
		       	</h4>
		        <div class="col-md-12 col-sm-12 col-xs-12 elementCreateButton"><hr></div>
		    </div>
	    </div>

	    <div class="col-xs-12 <?php echo @$elementParams["containerClass"]["centralSectionSub"] ?>" id="central-container" data-active-view="">
		</div>
	</div>
</div>	

<?php 
	//render of modal for coop spaces 
	$params = array(  "element" => @$element, 
                        "type" => @$type, 
                        "edit" => @$canEdit,
                        "thumbAuthor"=>@$thumbAuthor,
                        "openEdition" => $openEdition,
                        "iconColor" => $iconColor
                    );

	$this->renderPartial('dda.views.co.pod.modals', $params ); 

	$this->renderPartial('../element/confirmDeleteModal', array("id" =>(String)$element["_id"], "type"=>$type)); 

	if (@$element["status"] == "deletePending" 
		&& Authorisation::isElementAdmin((String)$element["_id"], $type, Yii::app()->session["userId"])) 
		$this->renderPartial('co2.views.element.confirmDeletePendingModal', array(	"element"=>$element)); 

	/*$this->renderPartial('co2.views.pod.rocketChat', 
		array("id" => $id,
			  "element" => $element,
			  "type" => $type,
			  "editMode" => $edit,
			  "openEdition" => $openEdition) );*/ 

	$this->renderPartial('co2.views.pod.confidentiality',
			array(  "element" => @$element, 
					"type" => @$type, 
					"edit" => @$canEdit,
					"controller" => $controller,
					"openEdition" => $openEdition,
				) );
	$this->renderPartial('co2.views.pod.qrcode',
		array("type" => @$type,
			"name" => @$element['name'],
			"address" => @$address,
			"address2" => @$address2,
			"email" => @$element['email'],
			"url" => @$element["url"],
			"tel" => @$tel,
			"img"=>@$element['profilThumbImageUrl']));
	$this->renderPartial($layoutPath.'forms.'.Yii::app()->params["CO2DomainName"].'.formContact', 
		array("element"=>@$element));
	
?>

<script type="text/javascript">
	var contextData = <?php echo json_encode( Element::getElementForJS(@$element, @$type) ); ?>; 
	mylog.log("init contextData", contextData);
    var params = <?php echo json_encode(@$params); ?>; 
    var canEdit =  <?php echo json_encode(@$canEdit) ?>;
	var canParticipate =  <?php echo json_encode(@$canParticipate)?>;
	var canSee =  <?php echo json_encode(@$canSee) ?>;
	var elementParams =  <?php echo json_encode(@$elementParams) ?>;
	var openEdition = ( ( '<?php echo (@$openEdition == true); ?>' == "1") ? true : false );
    var dateLimit = 0;
    var typeItem = "<?php echo $typeItem; ?>";
    var liveScopeType = "";
    var navInSlug=false;
   	var pageConfig=<?php echo json_encode($pageConfig) ?>;
   	if(typeof contextData.slug != "undefined")
     	navInSlug=true;
   
	var hashUrlPage= ( (typeof contextData.slug != "undefined") ? 
						"#@"+contextData.slug : 
						"#page.type."+contextData.type+".id."+contextData.id);
    
    if(location.hash.indexOf("#page")>=0){
    	strHash="";
    	if(location.hash.indexOf(".view")>0){
    		hashPage=location.hash.split(".view");
    		strHash=".view"+hashPage[1];
    	}
    	replaceSlug=true;
    	history.replaceState("#page.type."+contextData.type+".id."+contextData.id, "", hashUrlPage+strHash);

    }
    
    var cropResult;
    var idObjectShared = new Array();

    var personCOLLECTION = "<?php echo Person::COLLECTION; ?>";
    pageProfil.params={
    	view : "<?php echo @$_GET['view']; ?>",
    	subview : "<?php echo @$_GET['subview']; ?>",
    	action : null,
		dir: "<?php echo @$_GET['dir']; ?>",
		key : "<?php echo @$_GET['key']; ?>",
		folderKey : "<?php echo @$_GET['folder']; ?>",
	};
	if(notNull(pageConfig) && typeof pageConfig.initView != "undefined" && pageProfil.params.view=="") pageProfil.params.view=pageConfig.initView;
	var roomId = "<?php echo @$_GET['room']; ?>";
	var proposalId = "<?php echo @$_GET['proposal']; ?>";
	var resolutionId = "<?php echo @$_GET['resolution']; ?>";
	var actionId = "<?php echo @$_GET['action']; ?>";
	var isLiveNews = "";
	var connectTypeElement="<?php echo Element::$connectTypes[$type] ?>";
	
	if(contextData.type == "citoyens") var currentRoomId = "";
	
	//var mapCO = {} ;
			
	jQuery(document).ready(function() {
		typeObj.buildCreateButton(".elementCreateButton", false, {
			addClass:"col-xs-6 col-sm-6 col-md-4 col-lg-4 uppercase btn-open-form",
			bgIcon:true,
			textColor:true,
			inElement:true,
			allowIn:true,
			contextType: contextData.type,
			bgColor : "white",
			explain:true,
			inline:false
		});
		pageProfil.init();
		//bindButtonMenu();
		//inintDescs();
		
		$(".hide-"+contextData.type).hide();
		
		//loadActionRoom();

		//coInterface.scrollTo("#topPosKScroll");
		getContextDataLinks();
		if(typeof contextData.links != "undefined" && typeof rolesList != "undefined")
			pushListRoles(contextData.links);
		//Sig.showMapElements(Sig.map, mapElements);
		var elemSpec = dyFInputs.get("<?php echo $type?>");
		buildQRCode( elemSpec.ctrl ,"<?php echo (string)$element["_id"]?>");
		
		affixPageMenu=$("#headerBand").outerHeight()+$("#mainNav").outerHeight()+$("#social-header").outerHeight()-$('.sub-menu-social').outerHeight();
	    $('.sub-menu-social').affix({
			offset: {
			  top: Number(affixPageMenu)
			}
	    }).on('affixed.bs.affix', function(){
	        $(this).css({"top":$("#mainNav").outerHeight()});
	    }).on('affixed-top.bs.affix', function(){
	         $(this).css({"top":"initial"});
	    });

		if(typeof networkJson != "undefined" && notNull(networkJson)){
			$(".main-menu-left").hide();
			$("#vertical").remove();
			$("#menuApp").remove();

		}

		// var paramsMapCOPage = {
		// 	container : "mapPage",
		// 	hideContainer : "central-container",
		// 	activePopUp : true,
		// 	tile : "mapbox",
		// 	menuRight : false,
		// 	btnHide : true
		// };
		// mapCO = mapObj.init(paramsMapCOPage);
		// $("#mapPage").hide();
	});

</script>

<button type="button" class="btn btn-default bold menu-left-min visible-xs pull-left" onclick="pageProfil.menuLeftShow();">
		<i class="fa fa-bars"></i>
</button>
 <?php
 	foreach($buttonTop as $key => $v){
 		if($key=="imgProfil"){ ?>		
 			<img class="<?php echo $v["class"] ?>" src="<?php echo $thumbAuthor; ?>" height=45>
 		<?php }
 		else if($key=="nameProfil"){ ?>
 			<div class="identity-min">
	    	  <div class="pastille-type-element bg-<?php echo $iconColor; ?> pull-left"></div>
			  <div class="<?php echo $v["class"] ?> pull-left no-padding">
	    	  	<div class="text-left padding-left-15" id="second-name-element">
					<span id="nameHeader">
						<h5 class="elipsis"><?php echo @$element["name"]; ?></h5>
					</span>	
				</div>
	    	  </div>
    	  </div>
 		<?php } 
 		else if($key=="params" && isset($v["dropdown"])){ 
 			$label=(@Yii::app()->session["userId"] && $edit==true) ? $v["label"] : "";
 			$icon= (@Yii::app()->session["userId"] && $edit==true) ? $v["icon"] : "chevron-down"; ?>
 			<ul class="nav navbar-nav <?php echo @$v["class"]; ?>" id="paramsMenu">
				<li class="dropdown dropdown-profil-menu-params">
					<button type="button" class="btn btn-default bold">
						<?php if(@Yii::app()->session["userId"] && $edit==true){ ?>
			  			<i class="fa fa-<?php echo $icon ?>"></i> <span class="<?php echo @$v["labelClass"]; ?>"><?php echo Yii::t("common", $label); ?>
			  			<?php }else{ ?>
			  			<i class="fa fa-chevron-down"></i>
			  			<?php } ?>
			  			</span>
			  		</button>
			  		<ul class="dropdown-menu arrow_box menu-params">
	                	<?php foreach($v["dropdown"] as $k => $li){
	                		if(!empty($li)){ 
		                		$dataAction=(isset($li["action"])) ? "data-action='".$li["action"]."'" : "";
		                		$dataView=(isset($li["view"])) ? "data-view='".$li["view"]."'" : "";
		                		$show=true;
		                		if(isset($li["edit"]) && empty($edit)) $show=false;
		                		if(isset($li["typeAllow"]) && !in_array($type, $li["typeAllow"])) $show=false;
		                		if($show){ ?>
			                		<li class="text-left">
										<a href="javascript:;" class="bg-white ssmla <?php echo @$li["class"] ?>" <?php echo $dataAction ?> <?php echo $dataView ?>>
											<i class="fa fa-<?php echo $li["icon"] ?>"></i> <?php echo Yii::t("common", $li["label"]); ?>
											</a>
									</li>
	                	<?php 	}
	                		} 
	                	} ?>
	                </ul>
			  	</li>
			 </ul>
 		<?php }
 		else if(!empty($v)){
 			$dataAttr="";
 			$dataAttr.=(@$v["action"]) ? "data-action='".$v["action"]."' " : "";
    		$dataAttr.=(@$v["view"]) ? "data-view='".$v["view"]."' " : "";
    		if(isset($v["dataAttr"])){
    			$dataAttr.=(isset($v["dataAttr"]["dir"])) ? "data-dir='".$v["dataAttr"]["dir"]."' " : "";
    			$dataAttr.=(isset($v["dataAttr"]["toggle"])) ? "data-toggle='".$v["dataAttr"]["toggle"]."' " : "";
    			$dataAttr.=(isset($v["dataAttr"]["target"])) ? "data-target='".$v["dataAttr"]["target"]."' ":"";
    		}
    		$show=true;
    		if(isset($v["openEdition"]) && empty($openEdition)) $show=false;
    		if(isset($v["edit"]) && empty($edit)) $show=false;
    		if(isset($v["onlyMember"])) $show=Authorisation::isElementMember((string)$element["_id"],$type, Yii::app()->session["userId"]);
    		if(isset($v["typeAllow"]) && !in_array($type, $v["typeAllow"])) $show=false;
    		if(isset($v["userConnected"]) && !isset(Yii::app()->session["userId"])) $show=false;
    		if(isset($v["categoryAllow"]) && (!isset($element["category"]) || $element["category"]!=$v["categoryAllow"])) $show=false; 
    		if($show){ ?>
				<a href="javascript:;" class="bg-white ssmla btn btn-default pull-left <?php echo @$v["class"] ?>" <?php echo @$dataAttr ?>>
					<i class="fa fa-<?php echo $v["icon"] ?>"></i> <span class="<?php @$v["labelClass"] ?>"><?php echo Yii::t("common", @$v["label"]); ?></span>
				</a>
 		<?php }
 		}
 	}
 	if(isset($openMenuXs) && !empty($openMenuXs)){ ?>
 		<ul class="menu-xs-only-top">
 		<?php foreach($buttonTop as $key => $v){
 			if(isset($v["class"]) && strpos($v["class"], "hidden-xs") !== false && !empty($v) && $v!==true){
	 			$dataAttr="";
	 			$dataAttr.=(@$v["action"]) ? "data-action='".$v["action"]."' " : "";
	    		$dataAttr.=(@$v["view"]) ? "data-view='".$v["view"]."' " : "";
	    		if(isset($v["dataAttr"])){
	    			$dataAttr.=(isset($v["dataAttr"]["dir"])) ? "data-dir='".$v["dataAttr"]["dir"]."' " : "";
	    			$dataAttr.=(isset($v["dataAttr"]["toggle"])) ? "data-toggle='".$v["dataAttr"]["toggle"]."' " : "";
	    			$dataAttr.=(isset($v["dataAttr"]["target"])) ? "data-target='".$v["dataAttr"]["target"]."' ":"";
	    		}
	    		$show=true;
	    		if(isset($v["openEdition"]) && empty($openEdition)) $show=false;
	    		if(isset($v["edit"]) && empty($edit)) $show=false;
	    		if(isset($v["onlyMember"])) $show=Authorisation::isElementMember((string)$element["_id"],$type, Yii::app()->session["userId"]);
	    		if(isset($v["typeAllow"]) && !in_array($type, $v["typeAllow"])) $show=false;
	    		if(isset($v["userConnected"]) && !isset(Yii::app()->session["userId"])) $show=false;
	    		if(isset($v["categoryAllow"]) && (!isset($element["category"]) || $element["category"]!=$v["categoryAllow"])) $show=false; 
	    		if($show){ ?>
	    			<li>
						<a href="javascript:;" class="bg-white ssmla btn btn-default pull-left" <?php echo @$dataAttr ?>>
							<i class="fa fa-<?php echo @$v["icon"] ?>"></i> <span class="<?php @$v["labelClass"] ?>"><?php echo Yii::t("common", @$v["label"]); ?></span>
						</a>
					</li>
				<li>
					<a href="<?php echo Yii::app()->createUrl('/costum/co/index/slug/'.$element["slug"]); ?>
							">
					<i class="fa  fa-heart-o"></i> <?php echo Yii::t("common","Customize") ?>
							</a>
						</li>
		
 		<?php 	}
 			} ?>
 		
 <?php } ?>
 	</ul>
 <?php	}
?>
		  <!--<button type="button" class="btn btn-default bold hidden-xs btn-start-mystream">
		  		<i class="fa fa-rss"></i> <?php echo Yii::t("common","Newspaper"); ?>
		  </button>

		  <?php /*if((@Yii::app()->session["userId"] && $isLinked==true) || @Yii::app()->session["userId"] == $element["_id"]){ ?>
		  <button type="button" class="btn btn-default bold hidden-xs btn-start-notifications hidden">
		  	<i class="fa fa-bell"></i> 
		  	<span class="hidden-xs hidden-sm">
		  		<?php if (@Yii::app()->session["userId"] == $element["_id"]) 
		  					echo Yii::t("common","My notif<span class='hidden-md'>ication</span>s"); 
		  			  else  echo Yii::t("common","Notif<span class='hidden-md'>ication</span>s"); ?>
		  	</span>
		  	<span class="badge notifications-countElement <?php if(!@$countNotifElement || (@$countNotifElement && $countNotifElement=="0")) echo 'badge-transparent hide'; else echo 'badge-success'; ?>">
		  		<?php echo @$countNotifElement ?>
		  	</span>
		  </button>
		  <?php }*/ ?>

		  
		 

		  <?php if(@Yii::app()->session["userId"])
		  		if( $type == Organization::COLLECTION || $type == Project::COLLECTION ){ ?>
		  <button type="button" class="btn btn-default bold hidden-xs letter-turq" data-toggle="modal" data-target="#modalCoop" 
		  		  id="open-co-space" style="border-right:0px!important;">
		  		  <?php 
			$name = Yii::t("cooperation", "CO-space");
			if(@Yii::app()->session['costum']["modules"]["dda"]["name"])
				$name = Yii::app()->session['costum']["modules"]["dda"]["name"];

			$icon = "fa-connectdevelop";
			if(@Yii::app()->session['costum']["modules"]["dda"]["icon"])
				$icon = Yii::app()->session['costum']["modules"]["dda"]["icon"];
			?>
		  		<i class="fa <?php echo $icon; ?>"></i> <?php echo $name; ?>
		  </button>
		  <?php } ?>


		  <?php if(@Yii::app()->session["userId"])
		  		if( ($type!=Person::COLLECTION && ((@$edit && $edit) || (@$openEdition && $openEdition))) || 
		  			($type==Person::COLLECTION && (string)$element["_id"]==@Yii::app()->session["userId"])){ ?>

			
		  
		  <button type="button" class="btn btn-default bold letter-green hidden-xs" 
		  		  id="open-select-create" style="border-right:0px!important;">
		  		<i class="fa fa-plus-circle fa-2x"></i> <?php //echo Yii::t("common", "Créer") ?>
		  </button>
		  <?php } ?>
		</div>
		
		
		<div class="btn-group pull-right col-xs-3 col-sm-2 col-md-4 no-padding">
			<?php 
			$role = Role::getRolesUserId(@Yii::app()->session["userId"]) ; 
			?>
				<ul class="nav navbar-nav pull-right" id="paramsMenu">
					<li class="dropdown dropdown-profil-menu-params">
					<button type="button" class="btn btn-default bold">
						<?php if(@Yii::app()->session["userId"] && $edit==true){ ?>
			  			<i class="fa fa-cogs"></i> <span class="hidden-xs hidden-sm"><?php echo Yii::t("common", "Settings"); ?>
			  			<?php }else{ ?>
			  			<i class="fa fa-chevron-down"></i>
			  			<?php } ?>
			  			</span>
			  		</button> 
			  		<!--<button type="button" class="btn btn-default bold">
						<i class="fa fa-chevron-down"></i>
			  		</button>-->
			  		<!--<ul class="dropdown-menu arrow_box menu-params">
	                	<?php  
	            		if(@Yii::app()->session["userId"] && $edit==true){ 

	            			if($type ==Person::COLLECTION){ ?>

		            			<li class="text-left">
									<a href="#settings.page.myAccount" class="lbh bg-white">
										<i class="fa fa-cogs"></i> <?php echo Yii::t("common", "My parameters") ; ?>
									</a>
								</li>

					<?php 	} else {  ?>
		            			<li class="text-left">
									<a href="#settings.page.confidentialityCommunity?slug=<?php echo $element['slug'] ; ?>" id="" class="lbh bg-white ">
										<i class="fa fa-cogs"></i> <?php echo Yii::t("common", "Confidentiality params"); ?>
										</a>
								</li>

								<li class="text-left">
									<a href="#settings.page.notificationsCommunity?slug=<?php echo $element['slug'] ; ?>" class="lbh bg-white">
										<i class="fa fa-bell"></i> <?php echo Yii::t("common", "Notifications preferences"); ?>
									</a>
								</li>

								<li class="text-left">
									<a href="javascript:;" onclick="updateSlug();" id="" class="bg-white">
										<i class="fa fa-id-badge"></i> <?php echo Yii::t("common", "Edit slug"); ?>
										</a>
					            </li>

					            <li class="text-left">
									<a href='javascript:' id="downloadProfil">
										<i class='fa fa-download'></i> <?php echo Yii::t("common", "Download your profil") ?>
									</a>
								</li>

					<?php 	}

						} ?>
						
						<li>
							<a href="javascript:;" onclick="showDefinition('qrCodeContainerCl',true)">
								<i class="fa fa-qrcode"></i> <?php echo Yii::t("common","QR Code") ?>
							</a>
						</li>

						<li>
							<a href="<?php echo Yii::app()->createUrl('/costum/co/index/slug/'.$element["slug"]); ?>
							">
							 <!-- 
							 fa-black-tie
							 fa-code-fork
							 fa-certificate -->
						<!--		<i class="fa  fa-heart-o"></i> <?php echo Yii::t("common","Customize") ?>
							</a>
						</li>

			  			<?php 

			  			if($type !=Person::COLLECTION){ 

			  				if($openEdition==true){ ?>
				  				
				  				<li class="text-left">
									<a href="javascript:;" class="btn-show-activity">
										<i class="fa fa-history"></i> <?php echo Yii::t("common","History")?> 
									</a>
								</li>
				<?php 		} 
						} else { 

							if(@Yii::app()->session["userId"] && $edit==true){ ?>

				            	<li class="text-left">
									<a href='javascript:;' onclick='rcObj.settings();' >
										<i class='fa fa-comments'></i> <?php echo Yii::t("common","Chat Settings"); ?>
									</a>
					            </li>

								<li class="text-left">
					               	<a href='javascript:;' onclick='loadMD()' >
										<i class='fa fa-file-text-o'></i> <?php echo Yii::t("common","Markdown Version"); ?>
									</a>
					            </li>
								
								<li class="text-left">
									<a href='javascript:;' onclick='loadMindMap()' >
										<i class='fa fa-sitemap'></i> <?php echo Yii::t("common","Mindmap View"); ?>
									</a>
					            </li>
					<?php 	}
			        	} ?>
						<li class="text-left">
							<a href='javascript:;' onclick='co.graph()' >
								<i class='fa fa-share-alt'></i> <?php echo Yii::t("common","Graph View"); ?>
							</a>
						</li>
						<li class="text-left">
							<a href='javascript:;' onclick="javascript:window.print();" >
								<i class='fa fa-print'></i> <?php echo Yii::t("home","Print out") ?>
							</a>
						</li>

						<?php 
						if ( Authorisation::canDeleteElement( (String)$element["_id"], $type, Yii::app()->session["userId"]) && 
							!@$deletePending && 
							!empty(Yii::app()->session["userId"]) && 
							$type != Person::COLLECTION	) { ?>

				  			<li class="text-left">
								<a href="javascript:;" id="btn-delete-element" class="bg-white text-red" data-toggle="modal">
									<i class="fa fa-trash"></i> 
									<?php echo Yii::t("common", "Delete {what}", array("{what}"=> Yii::t("common","this ".Element::getControlerByCollection($type)))); ?>
								</a>
				            </li>

			            <?php } ?>
			  		</ul>
		  		</li>
		  	</ul>
		  	<?php if(isset(Yii::app()->session["userId"]) && $type!=Person::COLLECTION){ ?>
		  		<button 	class='btn btn-default bold btn-share letter-green pull-right' style="border:0px!important;"
							data-ownerlink='share' data-id='<?php echo $element["_id"]; ?>' data-type='<?php  ?>' 
	                    	data-isShared='false'>
	                    	<i class='fa fa-share'></i> <span class="hidden-xs hidden-sm"><?php echo Yii::t("common","Share") ?></span>
	          	</button>
	         <?php } ?>
		</div>-->

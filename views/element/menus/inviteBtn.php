<?php 
$urlLink = "#element.invite.type.".$contextType.".id.".$contextId;
$inviteLink = "people";
$inviteText =  Yii::t("common","Invite people");
if ($contextType == Organization::COLLECTION) { 
	$inviteLink = "members";
	$inviteText =  Yii::t("common",'Invite members') ;
}else if ($contextType == Project::COLLECTION ){ 
	$inviteLink = "contributors";
	$inviteText =  Yii::t("common",'Invite contributors') ;
}
$whereConnect= ($contextType!=Person::COLLECTION) ? 'to the '.Element::getControlerByCollection($contextType) : ""; 
$classHref=(isset($class) && !empty($class)) ? $class : "text-red"; 
$classHref.=(isset($tooltip) && !empty($tooltip)) ? " tooltips" : ""; ?>
<li class="inviteContent">
	<a 	href="<?php echo $urlLink ; ?>" 
		class="lbhp <?php echo $classHref ?>"
		data-placement="bottom" 
		data-original-title="<?php echo Yii::t("common","Invite {what} {where}",array("{what}"=> Yii::t("common",$inviteLink),"{where}"=>Yii::t("common", $whereConnect))); ?>" > 
        <i class="fa fa-user-plus "></i> <?php echo $inviteText ?>
    </a>
</li>
<?php if(isset($separator) && !empty($separator)){ ?>
	<li><hr></li>
<?php } ?>
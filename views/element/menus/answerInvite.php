<?php if(@$invitedMe && !empty($invitedMe)){
		$inviteRefuse="Refuse";
		$inviteAccept="Accept";
		$verb="Join";
		$labelAdmin="";
		$option=null;
		$linkValid=Link::IS_INVITING;
		$msgRefuse=Yii::t("common","Are you sure to refuse this invitation");
		if(@$invitedMe["isAdminInviting"]){
			$verb="Administrate";
			$option="isAdminInviting";
			$labelAdmin=" to administrate";
			$linkValid=Link::IS_ADMIN_INVITING;
			$msgRefuse=Yii::t("common","Are you sure to refuse to administrate {what}", array("{what}"=>Yii::t("common"," this ".Element::getControlerByCollection($elementType))));
		}
		$labelInvitation=Yii::t("common", "{who} invited you".$labelAdmin, array("{who}"=>"<a href='#page.type.".Person::COLLECTION.".id.".$invitedMe["invitorId"]."' class='lbh'>".$invitedMe["invitorName"]."</a>"));
		$tooltipAccept=$verb." this ".Element::getControlerByCollection($elementType);
		if ($elementType == Event::COLLECTION){
			$inviteRefuse="Not interested";
			$inviteAccept="I go";
		}
		echo "<div class='no-padding containInvitation'>".
			"<div class='padding-5'>".
				$labelInvitation.": <br/>".
				'<a class="btn btn-xs tooltips btn-accept" href="javascript:links.validate(\''.$elementType.'\',\''.$elementId.'\', \''.Yii::app()->session["userId"].'\',\''.Person::COLLECTION.'\',\''.$linkValid.'\')" data-placement="bottom" data-original-title="'.Yii::t("common",$tooltipAccept).'">'.
					'<i class="fa fa-check "></i> '.Yii::t("common",$inviteAccept).
				'</a>'.
				'<a class="btn btn-xs tooltips btn-refuse margin-left-5" href="javascript:links.disconnect(\''.$elementType.'\',\''.$elementId.'\',\''.Yii::app()->session["userId"].'\',\''.Person::COLLECTION.'\',\''.Element::$connectTypes[$elementType].'\',null,\''.$option.'\',\''.$msgRefuse.'\')" data-placement="bottom" data-original-title="'.Yii::t("common","Not interested by the invitation").'">'.
					'<i class="fa fa-remove"></i> '.Yii::t("common",$inviteRefuse).
				'</a>'.
			"</div>".
		"</div>";
	}
?>
<?php 
	$cssAnsScriptFilesModule = array(
		//Data helper
		'/js/dataHelpers.js',
		'/js/activityHistory.js',
		'/js/news/index.js',
		'/js/default/editInPlace.js',
	);
	HtmlHelper::registerCssAndScriptsFiles($cssAnsScriptFilesModule, $this->module->getParentAssetsUrl());

	$imgDefault = $this->module->assetsUrl.'/images/thumbnail-default.jpg';
	$thumbAuthor =  @$element['profilThumbImageUrl'] ? 
                      Yii::app()->createUrl('/'.@$element['profilThumbImageUrl']) 
                      : "";

	
?>
<?php $this->renderPartial('co2.views.element.menus.answerInvite', 
    			array(  "invitedMe"      => $invitedMe,
    					"elementId"   => (string)$element["_id"],
    					"elementType" => $type
    					) 
    			); 
 	if(@Yii::app()->session["userId"] && @Yii::app()->session["userId"] != (string)$element["_id"]){ ?>
	<div class="pull-left col-xs-12 no-padding margin-bottom-15 shadow2 boxBtnLink">
        	<?php $this->renderPartial('co2.views.element.menus.links', 
    			array(  "linksBtn"      => $linksBtn,
    					"elementId"   => (string)$element["_id"],
    					"elementType" => $type,
    					"elementName" => $element["name"],
    					"openEdition" => $openEdition ) 
    			); 
    		?>
	</div>
<?php } ?>

<ul id="subsubMenuLeft" class="pull-left col-xs-12 no-padding">
	
	<?php if (($edit==true || $openEdition==true) && @Yii::app()->session["userId"]){ ?>
		<li class="visible-xs">
			<a href="javascript:" class="letter-green ssmla open-create-form-modal">
		  		<i class="fa fa-plus-circle fa-2x"></i> <?php echo Yii::t("common", "Create") ?>
		  	</a>
		</li>
		<?php // INVITE BUTTON ELEMENT
		$this->renderPartial('co2.views.element.menus.inviteBtn', 
    			array(  "contextType"      => $type,
    					"contextId"   => (string)$element["_id"],
    					"separator" => true,
    					"tooltip"=>true
    			) 
    	); 
	}	
		// MENU LEFT OF THE ELEMENT 
		// - List of this menu is define on the document CO2/config/CO2/params.json at @element.menuLeft
		// - $variable stocked in session variable {paramsConfig} during the initialization in mainSearch.php with @CO2::getThemeParams()  
		// - customized if necessary with @CO2::filterThemeInCustom() during the process in co2/views/custom/init.php
		// Rest::json($themeParams);
		// Rest::json(Yii::app()->session['paramsConfig']); exit;
		// if(empty($themeParams)){
		// 	$themeParams = Yii::app()->session['paramsConfig'];
		// 	Rest::json($themeParams); exit;
		// }

		foreach($elementParams["menuLeft"] as $key => $v){ 
			if(!empty($v)){ 
				$labelButon=($key=="events" && $type=="events")? Yii::t("common","Program"): Yii::t("common", $v["label"]);
				// Case special for values menu 
				// 1-- If no chart already fulfill, redirection on chart forms
				// 2-- If no chart already fulfill & user can't edit, the button is hide
				$separator = (isset($v["separator"])) ? "<li><hr></li>" : "";
				$dataAttr="";
				$dataAttr.=(@$v["action"]) ? "data-action='".$v["action"]."' " : "";
	    		$dataAttr.=(@$v["view"]) ? "data-view='".$v["view"]."' " : "";
	    		if(isset($v["dataAttr"])){
	    			$dataAttr.=(isset($v["dataAttr"]["dir"])) ? "data-dir='".$v["dataAttr"]["dir"]."' " : "";
	    			$dataAttr.=(isset($v["dataAttr"]["toggle"])) ? "data-toggle='".$v["dataAttr"]["toggle"]."' " : "";
	    			$dataAttr.=(isset($v["dataAttr"]["target"])) ? "data-target='".$v["dataAttr"]["target"]."' ":"";
	    			$dataAttr.=(isset($v["dataAttr"]["icon"])) ? "data-target='".$v["icon"]."' ":"";
	    		}
				$show=true;
				if(isset($v["openEdition"]) && empty($openEdition)) $show=false;
	    		if(isset($v["edit"]) && empty($edit)) $show=false;
	    		if(isset($v["typeAllow"]) && !in_array($type, $v["typeAllow"])) $show=false;
	    		if(isset($v["userConnected"]) && !isset(Yii::app()->session["userId"])) $show=false;
				if(isset($v["view"]) && in_array($v["view"], ["community", "directory"]) && $type == Person::COLLECTION 
					&& !Preference::showPreference( $element, $type, "directory", Yii::app()->session["userId"]) )
					$show=false;
				if($key=="chart"){	
					$countChart=(@$element["properties"] && @$element["properties"]["chart"]) ? count($element["properties"]["chart"]): 0; 
					$dataAttr="data-view='".(($countChart > 0) ? "chart" : "editChart")."' ";
					$labelButon.=($countChart > 0) ? " (".$countChart.")": "";
					if($countChart < 1 && !$edit && !$openEdition && !@Yii::app()->session["userId"])
						$show=false;
				}
				if($show){
				?>
					<li class="">
						<a href="javascript:;" id="<?php echo @$v["id"] ?>" class="ssmla <?php echo @$v["class"] ?>" <?php echo @$dataAttr ?>>
							<i class="fa fa-<?php echo $v["icon"] ?>"></i> <?php echo $labelButon ?>
						</a>
					</li>
					<?php 
					echo $separator;
				} 
		  } }
	?>
	<!-- /////////////////////////////////////////////////////////////////////////////////////////////////////
	///// Button not integrated for the moment :: NOTIFICATIONS XS-VIEW / MAP CREATOR / ONE PAGE VIEW ////////
	////////////////////////////////////////////////////////////////////////////////////////////////////////// 
	<li class="visible-xs">
			<a href="javascript:" class="ssmla btn-start-notifications">
		  	<i class="fa fa-bell"></i><?php if (@Yii::app()->session["userId"] == $element["_id"]) echo "Mes n"; else echo "N"; ?>otifications
		  	<span class="badge notifications-countElement <?php if(!@$countNotifElement || (@$countNotifElement && $countNotifElement=="0")) echo 'badge-transparent hide'; else echo 'badge-success'; ?>">
		  		<?php echo @$countNotifElement ?>
		  	</span>
		  	</a>
		</li> 
		<li class="">
			<a href="javascript:" data-toggle="modal" data-target="#selectCreate" 
				id="btn-start-networks" class="ssmla">
				<i class="fa fa-map-o"></i> <?php //echo Yii::t("common","My maps"); ?>
			</a>
		</li>  
		<?php if (in_array($type,[Project::COLLECTION,Organization::COLLECTION,Event::COLLECTION,Person::COLLECTION])){  
			$hash = @$element["slug"] ? "#".$element["slug"] : "#page.type.".$type.".id.".$element["_id"]; ?>
			<li>
				<a href="<?php echo $hash; ?>.net"  class="lbh letter-blue">
					<i class="fa fa-desktop"></i> <?php echo Yii::t("common","My web page"); ?>
				</a>
			</li>			
		<?php } ?>
	//////////////// END LIST OF BUTTON NOT INTEGRATED //////////////////////////////////////////////// -->
</ul>

<script type="text/javascript">
jQuery(document).ready(function() {
    mylog.log("render","modules/co2/views/pod/menuLeftElement");
});
</script>


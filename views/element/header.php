<section class="col-md-12 col-sm-12 col-xs-12" id="social-header" 
	<?php if (!@$element["profilBannereUrl"] || (@$element["profilBannereUrl"] && empty($element["profilBannereUrl"]))){ 
		if(!empty(Yii::app()->session["costum"]) && isset(Yii::app()->session["costum"]["htmlConstruct"]) 
			&& isset(Yii::app()->session["costum"]["htmlConstruct"]["element"])
				&& isset(Yii::app()->session["costum"]["htmlConstruct"]["element"]["banner"]["img"]))
			$url=Yii::app()->getModule( "costum" )->assetsUrl.Yii::app()->session["costum"]["htmlConstruct"]["element"]["banner"]["img"];
		else
			$url=Yii::app()->theme->baseUrl.'/assets/img/background-onepage/connexion-lines.jpg';
		?> 
		style=" background: url('<?php echo $url;?>') center bottom;"
	<?php } ?>>
	<div id="topPosKScroll"></div>
	
	<?php 
		$this->renderPartial('co2.views.element.banner', 
					array(	"iconColor"=>$iconColor,
							"icon"=>$icon,
							"type"=>$type,
							"element"=>$element,
							"pageConfig"=>$pageConfig,
							"linksBtn"=>$linksBtn,
							"invitedMe"=>@$invitedMe,
							"elementId"=>(string)$element["_id"],
							"elementType"=>$type,
							"elementName"=> $element["name"],
							"edit" => @$edit,
							"openEdition" => @$openEdition) 
					); 
	?>
</section>
<?php  

//init will always be exeecuted in a JS document ready

if( @$_GET["el"] || @$custom )
{ 
    $stum = (@$custom) ?  explode(".",  $custom ) : explode(".",  $_GET["el"] );
    $el = null;
    $c = null;
    if($stum[0]=="city") {
        $el = City::getByInsee($stum[1]);
        $c = array( "id"   => (string) $el["_id"],
                                               "type" => City::COLLECTION,
                                               "url" => "/custom?el=".$_GET["el"],
                                               "title"=> "Le port"
                                        );
        if(@$el["custom"])
            $c = array_merge( $c , $el["custom"] );

        if(@$el["custom"]["logo"]) {
            $el["custom"]["logo"]= Yii::app()->getModule("eco")->getAssetsUrl(true).$el["custom"]["logo"];
        
            $c["logo"] = substr($el["custom"]["logo"], strpos($el["custom"]["logo"], '/assets'), strlen($el["custom"]["logo"]));
        }
    }
    else if( @$stum[1] == "cte" ){
        $el = PHDB::findOne( $stum[0], array("id"=>$stum[1]) );
        $c = array( 
            "id"   => (string) $el["_id"],
            "type" => Form::COLLECTION,
            "url" => "/survey/co/index/id/cte",
            "title"=> $el["title"] );
        if(@$el["custom"])
            $c = array_merge( $c , $el["custom"] );

        if(@$el["custom"]["logo"])
            $c["logo"]=Yii::app()->getModule("survey")->getAssetsUrl(true).$el["custom"]["logo"];
    }
    else if( @$stum[0] == "costum" ){
        $c = $el;
    }
    else {
        if($stum[0] == "o")
            $stum[0] = Organization::COLLECTION;
        if($stum[0] == "p")
            $stum[0] = Project::COLLECTION;
        
        if( empty($stum[0]) && empty($stum[1])){
            throw new CTKException("Cannot get Ellement : check type, ID or Slug");
        }
        
        //soit on a un ID soit un slug
        if (is_string($stum[1]) && strlen($stum[1]) == 24 && ctype_xdigit($stum[1]) )
            $el = Element::getByTypeAndId( $stum[0] , $stum[1] );
        else 
            $el = Slug::getElementBySlug( $stum[1] )["el"];
        //CHECK CUSTOM SAVED IN COLLECTION COSTUM INSTEAD OF ELEMENT DIRECTLY
        if(@$el["custom"] && is_string($el["custom"]) && strlen($el["custom"]) == 24 && ctype_xdigit($el["custom"])  )
            $el["custom"] = PHDB::findOne( "costum" , array("_id"=> new MongoId($el["custom"])));
        $c = array( "id"   => (string) $el["_id"],
                   "type" => $stum[0],
                   "title"=> $el["name"],
                   "description"=> @$el["shortDescription"],
                   "tags"=> @$el["tags"],
                   "assetsUrl"=> (@$el["custom"]["module"]) ? Yii::app()->getModule($el["custom"]["module"])->getAssetsUrl(true) : Yii::app()->getModule($this->module->id)->getAssetsUrl(true),
                   "url"=> "/custom?el=".@$_GET["el"] );
        
        if(@$el["custom"])
            $c = array_merge( $c , $el["custom"] );
        else 
            $c = array_merge( $c , array( "custom" => array( "welcomeTpl" => "../custom/".$stum[1]."/index")) );

        if (@$el["custom"]["logo"]) 
            $c["logo"] = Yii::app()->getModule($el["custom"]["module"])->getAssetsUrl(true).$el["custom"]["logo"];
        else if( @$el["profilImageUrl"] ) {
            $c["logo"] = Yii::app()->createUrl($el["profilImageUrl"]);
            $el["custom"]["logo"]=Yii::app()->createUrl($el["profilImageUrl"]);
        }
        if(@$el["custom"]["metaImg"]){
            $c["metaImg"] = Yii::app()->getRequest()->getBaseUrl(true).Yii::app()->getModule( Yii::app()->params["module"]["parent"] )->getAssetsUrl().$el["custom"]["metaImg"];
        }
        if(@$el["custom"]["logoMin"]){
            $c["logoMin"] = Yii::app()->getModule( Yii::app()->params["module"]["parent"] )->getAssetsUrl().$el["custom"]["logoMin"];
        }
       
        if (@$el["custom"]["favicon"]) 
            $c["favicon"] = Yii::app()->getModule( Yii::app()->params["module"]["parent"] )->getAssetsUrl().$el["custom"]["favicon"];
        $c["admins"]= Element::getCommunityByTypeAndId($c["type"], $c["id"], Person::COLLECTION,"isAdmin");
        if(@Yii::app()->session["userIsAdmin"] && !@$c["admins"][Yii::app()->session["userId"]]){
            $c["admins"][Yii::app()->session["userId"]]=array("type"=>Person::COLLECTION, "isAdmin"=>true);
        }
        $c["request"]["sourceKey"]=[$el["slug"]];
    }

    Yii::app()->session['costum'] = $c;
    Costum::filterThemeInCustom(Yii::app()->session["paramsConfig"]);   
    ?>
        <meta name="title" content="<?php echo $c["title"]; ?>">
        <meta name="description" content="<?php echo @$c["description"]; ?>">
        <meta name="author" content="pixelhumain">
        <meta property="og:image" content="<?php echo @$c["metaImg"]; ?>"/>
        <meta property="og:description" content="<?php echo @$c["description"]; ?>"/>
        <meta property="og:title" content="<?php echo @$c["title"];; ?>"/>
        <?php $keywords = (isset($c["tags"])) ? implode(",", @$c["tags"]) : ""; ?>
        <meta name="keywords" lang="<?php echo Yii::app()->language; ?>" content="<?php echo CHtml::encode($keywords); ?>" > 
        <title><?php echo $c["title"]; ?></title>
        <link rel='shortcut icon' type='image/x-icon' href="<?php echo @$c["favicon"]; ?>" />
    <?php   
} else {
    //Yii::app()->session["costum"] = null; 

    //delete custom; 
}

if( @Yii::app()->session['costum'] ){  ?>

<script type="text/javascript">
    var costum = <?php echo json_encode(Yii::app()->session['costum']) ?>;
    //if(typeof custom.appRendering != "undefined")
    themeParams=<?php echo json_encode(Yii::app()->session['paramsConfig']) ?>;
    costum.init = function(where){
        if(costum.logo){
            $(".topLogoAnim").remove();
            $(".logo-menutop, .logoLoginRegister").attr({'src':costum.logo});
        }
        if(typeof costum.filters != "undefined"){
            if(typeof costum.filters.scopes != "undefined"){
                if(typeof costum.filters.scopes.cities != "undefined")
                    setOpenBreadCrum({'cities': costum.filters.scopes.cities });
            }
            if(typeof costum.filters.sourceKey != "undefined")
                searchObject.sourceKey=costum.request.sourceKey;
        }
        if(typeof costum.css != "undefined"){
            initCssCustom(costum.css);
        }
        if(typeof costum.headerParams != "undefined"){
            $.each(costum.headerParams, function(e, v){
                if(typeof headerParams[e] != "undefined"){
                    $.each(v, function(name, value){
                        headerParams[e][name]=value;
                    });
                }
            });
        }
        if(typeof costum.htmlConstruct != "undefined" && typeof costum.htmlConstruct.directoryViewMode != "undefined")
            directoryViewMode=costum.htmlConstruct.directoryViewMode;

    };
    costum.initMenu = function(where){
    }
   
    if(typeof costum.css != "undefined"){
        if(typeof costum.css.loader !="undefined"){ 
            color1=(typeof costum.css.loader.ring1 != "undefined" && costum.css.loader.ring1.color != "undefined") ? costum.css.loader.ring1.color : "#ff9205";
            color2=(typeof costum.css.loader.ring2 != "undefined" && costum.css.loader.ring2.color != "undefined") ? costum.css.loader.ring2.color : "#3dd4ed";
            themeObj.blockUi = {
                processingMsg :'<div class="lds-css ng-scope">'+
                        '<div style="width:100%;height:100%" class="lds-dual-ring">'+
                            '<img src="'+costum.logo+'" class="loadingPageImg" height="60">'+
                            '<div style="border-color: transparent '+color2+' transparent '+color2+';"></div>'+
                            '<div style="border-color: transparent '+color1+' transparent '+color1+';"></div>'+
                        '</div>'+
                    '</div>', 
                errorMsg : '<img src="'+costum.logo+'" class="logo-menutop" height=80>'+
                  '<i class="fa fa-times"></i><br>'+
                   '<span class="col-md-12 text-center font-blackoutM text-left">'+
                    '<span class="letter letter-red font-blackoutT" style="font-size:40px;">404</span>'+
                   '</span>'+

                  '<h4 style="font-weight:300" class=" text-dark padding-10">'+
                    'Oups ! Une erreur s\'est produite'+
                  '</h4>'+
                  '<span style="font-weight:300" class=" text-dark">'+
                    'Vous allez être redirigé vers la page d\'accueil'+
                  '</span>'
            };
        }
    }
    function getStyleCustom(cssObject){
        cssAdd="";
        if(typeof cssObject.background != "undefined")
            cssAdd+="background-color:"+cssObject.background+" !important;";
        if(typeof cssObject.border != "undefined")
            cssAdd+="border:"+cssObject.border+" !important;";
        if(typeof cssObject.borderBottom != "undefined")
            cssAdd+="border-bottom:"+cssObject.borderBottom+" !important;";
        if(typeof cssObject.box != "undefined")
            cssAdd+="box-shadow:"+cssObject.box+" !important;";
        if(typeof cssObject.boxShadow != "undefined")
            cssAdd+="box-shadow:"+cssObject.boxShadow+" !important;";
        if(typeof cssObject.fontSize != "undefined")
            cssAdd+="font-size:"+cssObject.fontSize+"px !important;";
        if(typeof cssObject.color != "undefined")
            cssAdd+="color:"+cssObject.color+" !important;";
        if(typeof cssObject.paddingTop != "undefined")
            cssAdd+="padding-top:"+cssObject.paddingTop+" !important;";
        if(typeof cssObject.height != "undefined")
            cssAdd+="height:"+cssObject.height+"px !important;";
        if(typeof cssObject.width != "undefined")
            cssAdd+="width:"+cssObject.width+"px !important;";
        if(typeof cssObject.top != "undefined")
            cssAdd+="top:"+cssObject.top+"px !important;";
        if(typeof cssObject.bottom != "undefined")
            cssAdd+="bottom:"+cssObject.bottom+"px !important;";
        if(typeof cssObject.right != "undefined")
            cssAdd+="right:"+cssObject.right+"px !important;";
        if(typeof cssObject.left != "undefined")
            cssAdd+="left:"+cssObject.left+"px !important;";
        if(typeof cssObject.borderWidth != "undefined")
            cssAdd+="border-width:"+cssObject.borderWidth+"px !important;";
        if(typeof cssObject.borderColor != "undefined")
            cssAdd+="border-color:"+cssObject.borderColor+" !important;";
        if(typeof cssObject.borderRadius != "undefined")
            cssAdd+="border-radius:"+cssObject.borderRadius+"px !important;";
        if(typeof cssObject.lineHeight != "undefined")
            cssAdd+="line-height:"+cssObject.lineHeight+"px !important;";
        if(typeof cssObject.padding != "undefined")
            cssAdd+="padding:"+cssObject.padding+" !important;";
        if(typeof cssObject.display != "undefined")
            cssAdd+="display:"+cssObject.display+" !important;";
        
        return cssAdd;
    }
    function initCssCustom(style){
        str="<style type='text/css'>";
        if(typeof style.font != "undefined"){
            str+="@font-face {font-family: 'customFont';src: url('"+assetPath+"/"+style.font.url+"')}";
            str+="body, h1, h2, h3, h4, h5, h6, button, input, select, textarea, a, p, span{font-family: 'customFont' !important;}";
        }
        if(typeof style.menuTop != "undefined"){
            str+="#mainNav{"+getStyleCustom(style.menuTop)+"}";
            if(typeof style.menuTop.logo != "undefined")
                str+="#mainNav .logo-menutop{"+getStyleCustom(style.menuTop.logo)+"}";
            if(typeof style.menuTop.button != "undefined")
                str+="#mainNav .menu-btn-top{"+getStyleCustom(style.menuTop.button)+"}";
            if(typeof style.menuTop.badge != "undefined")
                str+=".btn-menu-notif .notifications-count, .btn-menu-chat .chatNotifs, .btn-dashboard-dda .coopNotifs{"+getStyleCustom(style.menuTop.badge)+"}";
            if(typeof style.menuTop.scopeBtn != "undefined"){
                str+=".menu-btn-scope-filter{"+getStyleCustom(style.menuTop.scopeBtn)+"}";
                if(typeof style.menuTop.scopeBtn.hover != "undefined")
                    str+=".menu-btn-scope-filter:hover, .menu-btn-scope-filter.active{"+getStyleCustom(style.menuTop.scopeBtn.hover)+"}";
            }
            if(typeof style.menuTop.filterBtn != "undefined")
                str+=".btn-show-filters{"+getStyleCustom(style.menuTop.filterBtn)+"}";
            if(typeof style.menuTop.connectBtn != "undefined")
                str+="#mainNav .btn-menu-connect{"+getStyleCustom(style.menuTop.connectBtn)+"}";
        }
        if(typeof style.menuApp != "undefined"){
            str+="#territorial-menu{"+getStyleCustom(style.menuApp)+"}";
            if(typeof style.menuApp.button != "undefined")
                str+="#territorial-menu .btn-menu-to-app{"+getStyleCustom(style.menuApp.button)+"}";
            if(typeof style.menuApp.button.hover != "undefined")
                str+="#territorial-menu .btn-menu-to-app:hover, #territorial-menu .btn-menu-to-app:active,#territorial-menu .btn-menu-to-app:focus, #territorial-menu .btn-menu-to-app.active{"+getStyleCustom(style.menuApp.button.hover)+"}";
        }
        if(typeof style.progress != "undefined"){
            if(style.progress.bar != "undefined")
                str+=".progressTop::-webkit-progress-bar { "+getStyleCustom(style.progress.bar)+" }";
            if(style.progress.value != "undefined")
                str+=".progressTop::-webkit-progress-value{"+getStyleCustom(style.progress.value)+"}.progressTop::-moz-progress-bar{"+getStyleCustom(style.progress.value)+"}";
        }
        if(typeof style.loader != "undefined"){
            if(typeof style.loader.background != "undefined")
                str+=".blockUI.blockMsg.blockPage{background-color:"+style.loader.background+" !important}";
            if(typeof style.loader.ring1 != "undefined")
                str+=".lds-dual-ring div{"+getStyleCustom(style.loader.ring1)+"}";
            if(typeof style.loader.ring2 != "undefined")
                str+=".lds-dual-ring div:nth-child(2){"+getStyleCustom(style.loader.ring2)+"}";
        }
        if(typeof style.button != "undefined"){
            if(typeof style.button.footer != "undefined"){
                if(typeof style.button.footer.add != "undefined")
                    str+="#show-bottom-add{"+getStyleCustom(style.button.footer.add)+"}";
                if(typeof style.button.footer.toolbarAdds != "undefined")
                    str+=".toolbar-bottom-adds{"+getStyleCustom(style.button.footer.toolbarAdds)+"}";
                if(typeof style.button.footer.donate != "undefined")
                    str+="#donation-btn{"+getStyleCustom(style.button.footer.donate)+"}";
            }
        }
        if(typeof style.color != "undefined"){
            colorCSS=["purple", "orange", "red", "black", "green", "green-k", "yellow", "yellow-k", "vine", "brown"];
            $.each(colorCSS, function(e,v){
                if(typeof style.color[v] != "undefined"){
                    str+=".text-"+v+"{color:"+style.color[v]+" !important;}.bg-"+v+"{background-color:"+style.color[v]+" !important;}";
                }
            });
        }
        str+="@media (max-width: 767px){"+
                "#mainNav{margin-top:0px !important;}"+
                "#mainNav .menu-btn-top{font-size:22px !important}}"+
                "#mainNav .logo-menutop{height:40px}"+
                "}";
        str+="</style>"; 

        $("head").append(str);
    }
</script>
<?php
    }
?>

<?php 
    
    $cssAnsScriptFilesTheme = array(
        '/plugins/showdown/showdown.min.js',
        '/plugins/to-markdown/to-markdown.js'
    );
    HtmlHelper::registerCssAndScriptsFiles($cssAnsScriptFilesTheme, Yii::app()->request->baseUrl);

    $layoutPath = 'webroot.themes.'.Yii::app()->theme->name.'.views.layouts.';
    //header + menu
    $onepageKey = @Yii::app()->session['paramsConfig']["onepageKey"];
    
   /* if( $this->module->id != "network" && 
        !empty($onepageKey) &&
        !in_array($view, $onepageKey) ){
            //le param USEHEADER de params.json sert à afficher ou non le header, 
        //donc normalement pas besoin de faire de IF ici
            $this->renderPartial($layoutPath.'header', 
                            array(  "layoutPath"=>$layoutPath , 
                                    "page" => "page",
                                    "dontShowMenu"=>true,
                                    "useFilter"=>false) ); 
    }*/

?>

<div class="col-md-12 col-sm-12 col-xs-12 no-padding social-main-container">
	<div class="" id="onepage">
		<?php
        //var_dump(Yii::app()->session['costum']['htmlConstruct']['element']); exit;
            if( !empty(Yii::app()->session['costum']) && 
                        !empty(Yii::app()->session['costum']['htmlConstruct']) && 
                        !empty(Yii::app()->session['costum']['htmlConstruct']['element']) &&
                        !empty(Yii::app()->session['costum']['htmlConstruct']['element']['urlTpl'])&&
                        !empty(Yii::app()->session['costum']['htmlConstruct']['element']['urlTpl'][$type]) ) {


                $tpl = Yii::app()->session['costum']['htmlConstruct']['element']['urlTpl'][$type] ;
                $url = ( !empty($tpl[$element["type"]]) ? $tpl[$element["type"]] : $tpl["default"] ) ;
                $params = array("element"=>$element , 
                                "id" => @$id,
                                "type" => @$type,
                                "page" => "page",
                                "canEdit"=>$canEdit,
                                "openEdition" => $openEdition,
                                "linksBtn" => $linksBtn,
                                "isLinked" => $isLinked,
                                "controller" => $controller,
                                "iconColor" => @$iconColor,
                                "typeItem" => $typeItem,
                                "typeItemHead" => $typeItemHead,
                                "categoryItem" => $categoryItem,
                                "icon" => @$icon,
                                "countStrongLinks" => $countStrongLinks,
                                "countInvitations" => $countInvitations,
                                "countries" => $countries,
                                "useBorderElement" => $useBorderElement,
                                "pageConfig" => $pageConfig,
                                "addConfig" => $addConfig );
                echo $this->renderPartial($url,$params);
            } else if( in_array($type,[Person::COLLECTION,Event::COLLECTION,Project::COLLECTION,Organization::COLLECTION,Place::COLLECTION]) ){
        			$params = array("element"=>$element , 
                                    "id" => @$id,
                                    "type" => @$type,
        							"page" => "page",
        							"canEdit"=>@$canEdit,
        							"openEdition" => @$openEdition,
        							"linksBtn" => $linksBtn,
        							"isLinked" => $isLinked,
        							"controller" => $controller,
        							"countStrongLinks" => $countStrongLinks,
        							"countInvitations" => $countInvitations,
        							"countries" => $countries,
                                    "typeItem" => $typeItem,
                                    "typeItemHead" => $typeItemHead,
                                    "categoryItem" => $categoryItem,
                                    "icon" => $icon,
                                    "iconColor" => $iconColor,
                                    "useBorderElement" => $useBorderElement,
                                    "pageConfig" => $pageConfig,
                                    "addConfig" => $addConfig );

                    if(@$members) $params["members"] = $members;
                    if(@$invitedMe) $params["invitedMe"] = $invitedMe;
    
                    if(isset($view) && !empty($view) && !empty($onepageKey) && in_array($view, $onepageKey))
                        $this->renderPartial("co2.views.element.onepage", $params);
                    else 
                        $this->renderPartial('co2.views.element.profilSocial', $params ); 
                } else if($type == News::COLLECTION){
                    $params = array("element"=>$element , 
                                    "page" => "page",
                                    "type" => $type,
                                    "controller" => $controller,
                                    );

                    if(@$members) $params["members"] = $members;
                    if(@$invitedMe) $params["invitedMe"] = $invitedMe;

                    $this->renderPartial('news.views.co.standalone', $params ); 
                }
                else if($type == Survey::COLLECTION){
                    $params = array("survey"=>$element , 
                                    "page" => "page",
                                    "type" => $type,
                                    "controller" => $controller,
                                    );

                    $this->renderPartial('co2.views.survey.entryStandalone', $params ); 
                } else if($type == Classified::COLLECTION){
                    $params = array("element"=>$element , 
                                    "page" => "page",
                                    "type" => $type,
                                    "controller" => $controller,
                                    );

                    if(@$members) $params["members"] = $members;
                    if(@$invitedMe) $params["invitedMe"] = $invitedMe;

                    $this->renderPartial('eco.views.co.standalone', $params ); 
                    
                }else if($type == Poi::COLLECTION){
                        $params = array("element"=>$element , 
                                    "page" => "page",
                                    "type" => $type,
                                    "controller" => $controller,
                                    );
                        $this->renderPartial('co2.views.poi.standalone', $params ); 
                
                }
		?>
	</div>
</div>
<script type="text/javascript" >

var type = "<?php echo $type; ?>";
var id = "<?php echo $id; ?>";
var view = "<?php echo @$view; ?>";
var indexStepGS = 20;


</script>
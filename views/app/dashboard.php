<?php 

	$cssAnsScriptFilesTheme = array(
		"/plugins/jquery-counterUp/waypoints.min.js",
		"/plugins/jquery-counterUp/jquery.counterup.min.js",
	); HtmlHelper::registerCssAndScriptsFiles($cssAnsScriptFilesTheme, Yii::app()->request->baseUrl);
?>
<style type="text/css">
	.counterBlock{height:350px;}
	.wborder{border: 2px solid #002861; border-radius: 20px;}
</style>
<div class="col-md-12 col-sm-12 col-xs-12 no-padding dashboard app-<?php echo @$page ?>">
        <div class="col-xs-12 text-center" style="margin-top: 20px; color:#24284D">
        	<h1><?php echo $title ?></h1>
        </div>
       <div classs="col-xs-12" style="margin-top: 50px">
			<?php //echo $this->renderPartial("graph.views.co.menuSwitcher",array()); ?> 
			<div class="col-xs-12" id="graphSection">
       				<?php   
       				$borderClass = "wborder";     					
       				$blocksize = floor(12/count($blocks));
       				 foreach ($blocks as $id => $d) {
       				?> 	
       				<div class="col-md-4  text-center padding-10"  >
						<h1><span class="counter"><?php echo $d["counter"]?></span></h1>
		                <h3><?php echo $d["title"]?></h3>
				    	<div class="counterBlock <?php echo $borderClass?>" id="<?php echo $id?>" >
				        
				      	</div>
				    </div>
       				 <?php } ?>
				    

       		</div>
       </div>
</div>

<script type="text/javascript">
//prepare global graph variable needed to build generic graphs
<?php  foreach ($blocks as $id => $d) {
	if( isset($d["graph"]) ) {
		?>
		var <?php echo $d["graph"]["key"]."Data" ?> = <?php echo ( isset( $d["graph"]['data'] ) ) ? json_encode( $d["graph"]["data"] ) : "null" ?>;
		mylog.log('url','<?php echo $d["graph"]["url"]?>',<?php echo $d["graph"]["key"] ?>Data);
<?php }
} ?>

jQuery(document).ready(function() {
	alert("CTE : <?php echo $title ?>iii");
	mylog.log("render","/modules/co2/views/app/dashboard.php");

	<?php  foreach ($blocks as $id => $d) {
		if( isset($d["graph"]) ) { ?>
			mylog.log('url graphs','<?php echo $d["graph"]["url"]?>',<?php echo $d["graph"]["key"] ?>Data);
			ajaxPost('#<?php echo $id?>', baseUrl+'<?php echo $d["graph"]["url"]?>', null, function(){},"html");
	<?php }
	} ?>

	$('.counter').counterUp({
	  delay: 10,
	  time: 2000
	});
	$('.counter').addClass('animated fadeInDownBig');
	$('h3').addClass('animated fadeIn');

});
</script>
<?php 
    $cssAnsScriptFilesModule = array(
        //'/js/default/search.js',
        //'/js/default/live.js',
    );
    HtmlHelper::registerCssAndScriptsFiles($cssAnsScriptFilesModule, $this->module->assetsUrl);

    $page = "live";
    
    $layoutPath = 'webroot.themes.'.Yii::app()->theme->name.'.views.layouts.';
    //header + menu
   /* $this->renderPartial($layoutPath.'header', 
                        array(  "layoutPath"=>$layoutPath ,
                                "type" => @$type,
                                "page" => $page,
                                "dontShowMenu"=>true,
                                //"explain"=> "Live public : retrouvez tous les messages publics selon vos lieux favoris") 
                                ));*/
    //$page = "live";
    //$randImg = 1;
//$filliaireCategories = CO2::getContextList("filliaireCategories"); 
  
?>

<style>
     
	
   /* #formCreateNewsTemp .form-create-news-container{
    max-width: inherit !important;
	}
	.item-globalscope-checker.inactive{
        color:#DBBCC1 !important;
        border-bottom:0px;
        margin(top:-6px;)
    }
    .item-globalscope-checker:hover,
    .item-globalscope-checker:active,
    .item-globalscope-checker:focus{
        color:#e6344d !important;
        border-bottom:1px solid #e6344d;
        text-decoration: none !important;
    }
   


@media (min-width: 991px) {
    .subModuleTitle{
        width: 100% !important;
        margin-left: 11% !important;
    }
}*/
 #noMoreNews {
        position: relative;
        padding: 0px 40px;
        bottom: 0px;
        width: 100%;
        text-align: center;
        background: white;
    }
    #newsstream .loader,
    #noMoreNews{
       border-radius: 50px;
        margin-left: auto;
        margin-right: auto;
        display: table;
        padding: 15px;
        margin-top: 15px;
    }
</style>
<div class="row padding-10 bg-white live-container">
<?php 
    //$this->renderPartial($layoutPath.'headers/pod/'.$CO2DomainName.'/dayQuestion', array());
?>

    <div class="col-md-12 col-sm-12 col-xs-12 bg-white top-page" id="" style="padding-top:0px!important;">
    	<div class="col-lg-1 col-md-1 hidden-sm hidden-xs text-right hidden-xs" id="sub-menu-left"></div>

    	<div class="col-lg-10 col-md-12 col-sm-12 col-xs-12 margin-top-10">
    		<div id="newsstream"></div>
    	</div>	
    </div>
</div>


<script type="text/javascript" >

searchObject.initType="news";
var titlePage = "<?php echo @Yii::app()->session['paramsConfig']["pages"]["#".$page]["subdomainName"]; ?>";
//var filliaireCategories = <?php  ?>;
var liveParams =<?php echo json_encode(@Yii::app()->session['paramsConfig']["pages"]["#".$page]); ?>;
//var loadContent = '<?php echo @$_GET["content"]; ?>';
directory.appKeyParam=(location.hash.indexOf("?") >= 0) ? location.hash.split("?")[0] : location.hash;
jQuery(document).ready(function() {
	$(".subsub").hide();
	$('#btn-start-search').click(function(e){
		startNewsSearch(true);
    });
		
	
    
    //searchPage = true;
    searchInterface.initSearchParams();
	startNewsSearch(true);


});
function startNewsSearch(isFirst){
    var urlLive = "/news/co/index/type/city/isLive/true";
    var dataSearchLive={search:true};
    if(typeof liveParams != "undefined"){
       if(typeof liveParams.slug != "undefined" && notNull(costum)){
            urlLive = "/news/co/index/type/"+costum.contextType+"/id/"+costum.contextId;
            dataSearchLive=null; 
       }
       if(typeof liveParams.formCreate != "undefined")
            urlLive += "/formCreate/false";
    }
    if(typeof liveParams.setParams != "undefined"){
        myScopes.type="open";
        myScopes.open=[];
        if(typeof liveParams.setParams.source != "undefined") urlLive += "/source/"+liveParams.setParams.source;   
        //dataSearchLive=null;
    }
    /*var loading = "<div class='loader bold letter-blue shadow2 text-center'>"+
                    "<i class='fa fa-spin fa-circle-o-notch'></i> "+
                    "<span>"+trad.currentlyloading+" ...</span>" + 
                "</div>";*/
    coInterface.showLoader("#newsstream");
    //$("#newsstream").html(loading);
    coInterface.simpleScroll(0, 500);
    ajaxPost("#newsstream",baseUrl+"/"+urlLive, dataSearchLive, function(news){ spinSearchAddon();}, "html");
}

</script>
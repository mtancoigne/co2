<style>
   #city-main-container{
    padding-top:80px;
    display: inline-block;
   }
   .cityHeadSection {
        position: absolute;
        min-height: 500px !important;
        width: 100%;
    }
    .btn-discover{
        color:white;
    }

</style>

<div class="bg-white" id="city-main-container"></div>


<script type="text/javascript" >

var insee = "<?php echo @$insee; ?>";
var postalCode = "<?php echo @$postalCode; ?>";

jQuery(document).ready(function() {
    
    getAjax("#city-main-container" ,baseUrl+'/'+moduleId+"/city/detail/insee/"+insee+"/postalCode/"+postalCode,function(data){ 
    },"html");

});

</script>
<script type="text/javascript">

var paramsSearchMap = {};
directory.appKeyParam=(location.hash.indexOf("?") >= 0) ? location.hash.split("?")[0] : location.hash;
var filtres = {
	types : ["projects", "organizations"],
	limit : 30
};

jQuery(document).ready(function() {

    showMap(true);

    
    if(	typeof themeParams != "undefined" && 
    	typeof themeParams.pages != "undefined" && 
    	typeof themeParams.pages[directory.appKeyParam] != "undefined" &&
    	typeof themeParams.pages[directory.appKeyParam].filtres != "undefined" ){
    	filtres = themeParams.pages[directory.appKeyParam].filtres;
    }
    mylog.log("map.php filtres", filtres);
    if(typeof costum != "undefined" && notNull(costum) && typeof costum.app != "undefined" && typeof directory.appKeyParam != "undefined" && typeof costum.app[directory.appKeyParam] != "undefined" && typeof costum.app[directory.appKeyParam].filtres != "undefined" && typeof costum.app[directory.appKeyParam].filtres.types != "undefined")
        filtres.types=costum.app[directory.appKeyParam].filtres.types;

    paramsSearchMap=searchInterface.constructObjectAndUrl();
	paramsSearchMap.searchType = filtres.types;

	paramsSearchMap.indexStep = filtres.limit;

	$.ajax({
		type: "POST",
		url: baseUrl+"/" + moduleId + "/search/globalautocomplete",
		data: paramsSearchMap,
		dataType: "json",
		error: function (data){
			mylog.log(">>> error autocomplete search"); 
			mylog.dir(data);   
			$("#dropdown_search").html(data.responseText);  
			//signal que le chargement est terminé
			loadingData = false;     
		},
		success: function(data){ 
		mylog.log(">>> success autocomplete search !!!! ", data); //mylog.dir(data);
			if(!data){ 
				toastr.error(data.content); 
			} 
			else{ 
				mapCO.clearMap();
				mapCO.addElts(data.results);
			}
		}
	});
});


</script>
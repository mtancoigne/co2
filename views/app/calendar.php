<!-- <div class="col-xs-12 no-padding calendar margin-bottom-20"></div>
<div class="responsive-calendar-init hidden"> 
	<div class="responsive-calendar light col-md-12 no-padding">   
		<div class="day-headers">
			<div class="day header"><?php //echo Yii::t("translate","Mon") ?></div>
			<div class="day header"><?php //echo Yii::t("translate","Tue") ?></div>
			<div class="day header"><?php //echo Yii::t("translate","Wed") ?></div>
			<div class="day header"><?php //echo Yii::t("translate","Thu") ?></div>
			<div class="day header"><?php //echo Yii::t("translate","Fri") ?></div>
			<div class="day header"><?php //echo Yii::t("translate","Sat") ?></div>
			<div class="day header"><?php //echo Yii::t("translate","Sun") ?></div>
		</div>
		<div class="days" data-group="days"></div>   
		<div class="controls">
			<a id="btn-month-before" class="text-white" data-go="prev">
				<div class="btn"><i class="fa fa-arrow-left"></i></div>
			</a>
			<h4 class="text-white"><span data-head-month></span> <span data-head-year></span></h4>
			<a id="btn-month-next" class="text-white" data-go="next">
				<div class="btn"><i class="fa fa-arrow-right"></i></div>
			</a>
		</div>
	</div>
</div> -->
<?php
$cssAnsScriptFilesTheme = array(
	'/plugins/fullcalendar/fullcalendar/fullcalendar.min.js',
	'/plugins/fullcalendar/fullcalendar/fullcalendar.css', 
	'/plugins/fullcalendar/fullcalendar/locale/'.Yii::app()->language.'.js',       
);
HtmlHelper::registerCssAndScriptsFiles($cssAnsScriptFilesTheme, Yii::app()->request->baseUrl);
$cssAnsScriptFilesModule = array(
	'/js/default/calendar.js',
);
HtmlHelper::registerCssAndScriptsFiles($cssAnsScriptFilesModule, $this->module->assetsUrl);
HtmlHelper::registerCssAndScriptsFiles( array(  '/css/calendar.css') , Yii::app()->theme->baseUrl. '/assets');
$calendarParams=(@Yii::app()->session["paramsConfig"]["pages"]["#agenda"] && @Yii::app()->session["paramsConfig"]["pages"]["#agenda"]["calendar"]) ?Yii::app()->session["paramsConfig"]["pages"]["#agenda"]["calendar"] : null;
if(isset(Yii::app()->session["costum"]) && !empty($calendarParams)){
	$cssJsCostum=array();
	if(isset($calendarParams["js"]))
		array_push($cssJsCostum, '/js/'.Yii::app()->session["costum"]["slug"].'/calendar.js');
	if(isset($calendarParams["css"]))
		array_push($cssJsCostum, '/css/'.Yii::app()->session["costum"]["slug"].'/calendar.css');
	if(!empty($cssJsCostum))
		HtmlHelper::registerCssAndScriptsFiles($cssJsCostum, Yii::app()->getModule( "costum" )->getAssetsUrl());
}
?>

<style type="text/css">
	
	.fc-unthemed .fc-popover {
	   	position: absolute;
	    top: 30px !important;
	    left: 40% !important;
	}


</style>
<div class="col-md-12 no-padding" id="repertory" style="background-color: white">
	<div id="dropdown_search_result" class="col-md-12 col-sm-12 col-xs-12"></div>
	<!-- <div class='col-xs-12 margin-bottom-10'>
		<a href='javascript:;' id='showHideCalendar' class='text-azure' data-hidden='0'><i class='fa fa-caret-up'></i> Hide calendar</a>
	</div> -->
	<div id='profil-content-calendar' class='col-xs-12 margin-bottom-20'></div>
	<!-- <div class="col-xs-12">
		<div id='situate-day' class="col-xs-12">
			<h2 class="text-white date" style="text-transform:inherit;padding-left:15px;"><i class="fa fa-angle-down"></i> <span class="date-label"><?php //echo Yii::t("common","Coming events"); ?></span></h2>
		</div>
	</div> -->
</div>

<script type="text/javascript">

jQuery(document).ready(function() {
	mylog.log("calendar.php", searchObject.startDate);
	
});

</script>
# Welcome to COTOOLS 
- [CO aka COmmunecter](https://communecter.org) 
- [APP](https://doc.co.tools/books/2---utiliser-loutil/page/applications) (Android, iOS, Windows, Mac, Linux) 

## [INSTALLATION de Communecter](https://doc.co.tools/books/4---documentation-technique/chapter/installer-communecter)
## [INTRODUCTION au code](http://co.tools/intro)
## [FEATURES](https://doc.co.tools/books/2---utiliser-loutil/page/liste)

## INSTANCES 
> (running) on CO's base code

- [GRANDDIR.re](http://www.granddir.re) 
- [NOTRAGORA](http://www.notragora.com)
- [KGOUGLE.nc](http://kgougle.nc)
- Et bientôt plein de petits [CoPi](https://doc.co.tools/books/4---documentation-technique/page/copi)

## OPEN SOURCE [TOOLS WE USE TO ORGANIZE THE PROJECT](https://doc.co.tools/books/3---contribuer/page/outils-internes)

## CONNECTING THINGS TOGETHER
- [COPI.concept](https://docs.google.com/presentation/d/1efQiAdOt54_XoxJaYZPazxCK0T83jdgdmer-le9NwDY/edit#slide=id.gdd654f576_0_6) ([doc](http://co.tools/CoPi))
- [INTEROPERABILITY](https://doc.co.tools/books/4---documentation-technique/page/interop%C3%A9rabilit%C3%A9) (OpenStreetMap, Pôle emploi, Wikipédia, ...)

## OPEN SOURCE PROJECTS WE USE AND LOVE
- [PHP](http://php.net/)
- [MONGO DB](https://www.mongodb.com/)
- [JQUERY](https://jquery.com/)
- [BOOTSTRAP](http://getbootstrap.com/)
- [METEOR](https://www.meteor.com/)
- [NODEJS](https://nodejs.org/)
- [NOUN PROJECT](https://thenounproject.com/)
- and lots and lots and lots of pluggins !
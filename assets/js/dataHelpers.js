//Retrieve the countries in ajax and return an array of value
//The selectType change the value key : 
//for select input : {"value":"FR", "text":"France"}
//for select2 input : {"id":"FR", "text":"France"}


function getCountries(selectType) {
	mylog.log("getCountries");
	var result = new Array();
	$.ajax({
		url: baseUrl+'/'+moduleId+"/opendata/getcountries",
		type: 'post',
		global: false,
		async: false,
		dataType: 'json',
		success: function(data) {
			$.each(data, function(i,value) {
				if (selectType == "select2") {
					result.push({"id" : value.value, "text" :value.text});
				} else {
					result.push({"value" : value.value, "text" :value.text});
				}
			}) 
		}
	});
	return result;
}

function formatDataForSelect(data, selectType) {
	var result = new Array();
	$.each(data, function(key,value) {
		if (selectType == "select2") {
			result.push({"id" : key, "text" :value});
		} else {
			result.push({"value" : key, "text" :value});
		}
	});
	return result;
}

function getCitiesByPostalCode(postalCode, selectType) {
	var result =new Array();
	$.ajax({
		url: baseUrl+'/'+moduleId+"/opendata/getcitiesbypostalcode/",
		data: {postalCode: postalCode},
		type: 'post',
		global: false,
		async: false,
		dataType: 'json',
		success: function(data) {
			$.each(data, function(key,value) {
				if (selectType == "select2") {
					result.push({"id" : value.insee, "text" :value.name});
				} else {
					result.push({"value" : value.insee, "text" :value.name});
				}
			});
		}
	});
	return result;
}

/** added by tristan **/
function getCitiesGeoPosByPostalCode(postalCode, selectType) {
	var result =new Array();
	$.ajax({
		url: baseUrl+'/'+moduleId+"/opendata/getcitiesgeoposbypostalcode/",
		data: {postalCode: postalCode},
		type: 'post',
		global: false,
		async: false,
		dataType: 'json',
		success: function(data) { //mylog.dir(data);
			result.push(data);
		}
	});
	return result;
}
function isUniqueUsername(username) {
	var response;
	$.ajax({
		url: baseUrl+'/'+moduleId+"/person/checkusername/",
		data: {username: username},
		type: 'post',
		global: false,
		async: false,
		dataType: 'json',
		success: function(data) {
		    response = data;
		}
	});
	mylog.log("isUniqueUsername=", response);
	return response;
}
function checkUniqueEmail(email) {
	var response;
	$.ajax({
		url: baseUrl+'/'+moduleId+"/person/checkemail/",
		data: {email: email},
		type: 'POST',
		dataType: 'json',
		success: function(data) {
		    response = data.res;
		}
	});
	mylog.log("isUniqueEmail=", response);
	return response;
}

function slugUnique(searchValue){
	
	//searchType = (types) ? types : ["organizations", "projects", "events", "needs", "citoyens"];
	var response;
	var data = {
		"id":contextData.id,
		"type": contextData.type,
		"slug" : searchValue
	};
	//$("#listSameSlug").html("<i class='fa fa-spin fa-circle-o-notch'></i> Vérification d'existence");
	//$("#similarLink").show();
	//$("#btn-submit-form").html('<i class="fa  fa-spinner fa-spin"></i>').prop("disabled",true);
	mylog.log("slugUnique data", data);
	$.ajax({
      type: "POST",
          url: baseUrl+"/" + moduleId + "/slug/check",
          data: data,
          dataType: "json",
          error: function (data){
             mylog.log("error"); mylog.dir(data);
             //$("#btn-submit-form").html('Valider <i class="fa fa-arrow-circle-right"></i>').prop("disabled",false);
          },
          success: function(data){
 			//var msg = "Ce pseudo est déjà utilisé";
 			//$("#btn-submit-form").html('Valider <i class="fa fa-arrow-circle-right"></i>').prop("disabled",false);
 			var msgSlug = "";
			if (!data.result) {
				//response=false;
				if(!$("#ajaxFormModal #slug").parent().hasClass("has-error")){
					$("#ajaxFormModal #slug").parent().removeClass('has-success').addClass('has-error');//.find("span").text("cucu");
					$("#ajaxFormModal .slugtext .help-blockk").removeClass('letter-green').addClass('letter-red');
				}
				//.addClass("has-error").parent().find("span").text("cretin");
				msgSlug = "<small>www." + data.domaineName + "#"+ searchValue + "</small>" +
						  "<br><i class='fa fa-times'></i> " + tradDynForm["This URL is already used"];

				$("#ajaxFormModal #btn-submit-form").attr('disabled','disabled');
				//coInterface.bindLBHLinks();
			} else {
				//response=true;
				if(!$("#ajaxFormModal #slug").parent().hasClass("has-success")){
					$("#ajaxFormModal #slug").parent().removeClass('has-error').addClass('has-success');
					$("#ajaxFormModal .slugtext .help-blockk").removeClass('letter-red').addClass('letter-green');
				}
				//.addClass("has-success").parent().find("span").text("good peydé");
				
				msgSlug = "<small>www." + data.domaineName + "#"+ searchValue + "</small>" +
						  "<br><i class='fa fa-check-circle'></i> " + tradDynForm["This URL is not used"];

				$("#ajaxFormModal #btn-submit-form").removeAttr('disabled');
				//$("#slug").html("<span class='txt-green'><i class='fa fa-thumbs-up text-green'></i> Aucun pseudo avec ce nom.</span>");

			}

			$("#ajaxFormModal .slugtext .help-blockk").html(msgSlug);
			//$("#ajaxFormModal .slugtext .help-block").html(msgSlug).show();
			$("#ajaxFormModal #slug").data("checking", false);
            
          }
 	});
 	//return response;
}
function addCustomValidators() {
	mylog.log("addCustomValidators");
	//Validate a postalCode
	jQuery.validator.addMethod("validPostalCode", function(value, element) {
	    var response;
	    $.ajax({
			url: baseUrl+'/'+moduleId+"/opendata/getcitiesbypostalcode/",
			data: {postalCode: value},
			type: 'post',
			global: false,
			async: false,
			dataType: 'json',
			success: function(data) {
			    response = data;
			}
		});

	    if (Object.keys(response).length > 0) {
	    	return true;
	    } else {
	    	return false;
	    }
	}, "Code postal inconnu");
	/*jQuery.validator.addMethod("uniqueSlug", function(value, element) {
	    //Check unique username
	   	return slugUnique(value);
	}, "This slug already exists. Please choose an other one.");*/
	jQuery.validator.addMethod("validUserName", function(value, element) {
	    //Check authorized caracters
		var usernameRegex = /^[a-zA-Z0-9\-]+$/;
    	var validUsername = value.match(usernameRegex);
    	if (validUsername == null) {
        	return false;
    	} else {
    		return true;
    	}
    }, tradDynForm.invalidUsername);

	jQuery.validator.addMethod("uniqueUserName", function(value, element) {
	    //Check unique username
	   	return isUniqueUsername(value);
	}, "A user with the same username already exists. Please choose an other one.");

	/*jQuery.validator.addMethod("lengthMin", function(value, element, params) {
	    //Check unique username
	    //if(Object.keys(finder.object[element]).length >=value )
	   	//	return true;
	   	//else
	   		return false;
	}, "Select at least {value} item");*/
	jQuery.validator.addMethod("agreeValidation", function(value, element) {
	  //alert();
	  return false;
	  //return this.optional(element) || /^http:\/\/mycorporatedomain.com/.test(value);
	}, "Vous navez pas ");
	jQuery.validator.addMethod("inArray", function(value, element) {
	    //Check authorized caracters
		test = $.inArray( element, value );
    	if (test >= 0) 
    		return true;
    	else
    		return false;
    }, "Invalid : please stick to given values.");

    jQuery.validator.addMethod("greaterThan", function(value, element, params) {
    	mylog.log("greaterThan", value, params);
    	mylog.log(moment(value, "DD/MM/YYYY HH:mm").format());
    	mylog.log(!/Invalid|NaN/.test(new Date(moment(value, "DD/MM/YYYY HH:mm").format()))); 
	    //if (!/Invalid|NaN/.test(new Date(value))) {
	    if (!/Invalid|NaN/.test(new Date(moment(value, "DD/MM/YYYY HH:mm").format()))) {
	    	mylog.log(!/Invalid|NaN/.test(new Date(moment(value, "DD/MM/YYYY HH:mm").format()))); 

	        return moment(value, "DD/MM/YYYY HH:mm").isSameOrAfter(moment($(params[0]).val(), "DD/MM/YYYY HH:mm"));
	    }    
	    return isNaN(value) && isNaN($(params[0]).val()) || (Number(value) > Number($(params[0]).val())); 
	},'Doit ètre aprés {1}.');

// jQuery.validator.addMethod("greaterThanOrSame", function(value, element, params) {
//    	mylog.log("greaterThan", value, params);
//    	mylog.log(moment(value, "DD/MM/YYYY HH:mm").format());
//    	mylog.log(!/Invalid|NaN/.test(new Date(moment(value, "DD/MM/YYYY HH:mm").format()))); 
//     //if (!/Invalid|NaN/.test(new Date(value))) {
//     if (!/Invalid|NaN/.test(new Date(moment(value, "DD/MM/YYYY HH:mm").format()))) {
//     	mylog.log(!/Invalid|NaN/.test(new Date(moment(value, "DD/MM/YYYY HH:mm").format()))); 
//         return moment(value, "DD/MM/YYYY HH:mm").isSameOrAfter(moment($(params[0]).val(), "DD/MM/YYYY HH:mm"));
//     }    
//     return isNaN(value) && isNaN($(params[0]).val()) || (Number(value) > Number($(params[0]).val())); 
// },'Doit ètre aprés {1}.');

	jQuery.validator.addMethod("greaterThanNow", function(value, element, params) {   
		mylog.log("greaterThanNow", value,  params[0]);
		mylog.log(moment(value, params[0])," < ",moment());
	    return moment(value, params[0]).isAfter(moment()); 
	},"Doit être après la date d'aujourd'hui.");

	jQuery.validator.addMethod("duringDates", function(value, element, params) {  
		if( $(params[0]).val() && $(params[1]).val() ){
			//console.warn(moment(value, "DD/MM/YYYY HH:mm"),moment($(params[0]).val()),moment($(params[1]).val()));
	    	return (moment(value, "DD/MM/YYYY HH:mm").isSameOrAfter(moment($(params[0]).val())) 
	    		&& moment(value, "DD/MM/YYYY HH:mm").isSameOrBefore(moment($(params[1]).val())));
	    	//return  ( new Date(value) >= new Date( $(params[0]).val() ) && new Date(value) <= new Date($(params[1]).val()) );
		} 
		return true;
	},"Cette date est exterieure à l'évènement parent.");

	jQuery.extend(jQuery.validator.messages, {
		required: tradDynForm["This field is required."],
		remote: tradDynForm["Please fix this field."],
		email: tradDynForm["Please enter a valid email address."],
		url: tradDynForm["Please enter a valid URL (ex: http://wwww.communecter.org)"],
		date: tradDynForm["Please enter a valid date."],
		dateISO: tradDynForm["Please enter a valid date (ISO)."],
		number: tradDynForm["Please enter a valid number."],
		digits: tradDynForm["Please enter only digits."],
		creditcard: tradDynForm["Please enter a valid credit card number."],
		equalTo: tradDynForm["Please enter the same value again."],
		maxlength: $.validator.format(tradDynForm["Please enter no more than {0} characters."]),
		minlength: $.validator.format(tradDynForm["Please enter at least {0} characters."]),
		rangelength: $.validator.format(tradDynForm["Please enter a value between {0} and {1} characters long."]),
		range: $.validator.format(tradDynForm["Please enter a value between {0} and {1}."]),
		max: $.validator.format(tradDynForm["Please enter a value less than or equal to {0}."]),
		min: $.validator.format(tradDynForm["Please enter a value greater than or equal to {0}."])
	});
}



function showLoadingMsg(msg){
	$("#main-title-public1").html("<i class='fa fa-refresh fa-spin'></i> "+msg+" ...");
	$("#main-title-public1").show(300);
}
function hideLoadingMsg(){
	$("#main-title-public1").html("");
	$("#main-title-public1").hide(300);
}
function dateSecToString(date){
	var yyyy = date.getFullYear().toString();
	var mm = (date.getMonth()+1).toString(); // getMonth() is zero-based
    var dd  = date.getDate().toString();
    var min  = date.getMinutes().toString();
    var ss  = date.getSeconds().toString();
    date = yyyy + "-" + (mm[1]?mm:"0"+mm[0]) + "-" + (dd[1]?dd:"0"+dd[0]) + " " +
    					(min[1]?min:"0"+min[0]) + ":" + (ss[1]?ss:"0"+ss[0]) + ":00"; // padding
    return date;
}
function dateToStr(date, lang, inline, fullMonth){ //work with date formated : yyyy-mm-dd hh:mm:ss ou millisecond

	if(typeof date == "undefined") return;
	if(fullMonth != true) fullMonth = false;

	//mylog.log("convert format date 1", date);
	if(typeof date.sec != "undefined"){
		date = new Date(date.sec);
		date = dateSecToString(date);
	}
	else if(typeof date == "number"){
		date = new Date(date);
		date = dateSecToString(date);
	}
	//mylog.log(date);
	if(lang == "fr"){
		//(year, month, day, hours, minutes, seconds, milliseconds) 
		//mylog.log("convert format date", date);
		var year 	= date.substr(0, 4);
		var month 	= date.substr(5, 2);//getMonthStr(date.substr(5, 2), lang);
		var day 	= date.substr(8, 2);
		var hours 	= date.substr(11, 2);
		var minutes = date.substr(14, 2);
		

		var str = day + "/" + month + "/" + year;

		if(fullMonth) str = day + " " + getMonthStr(month, "fr") + " " + year;


		if(!inline) str += "</br>";
		else str += " - ";
		str += hours + "h" + minutes;
		
		return str;
	}

	function getMonthStr(monthInt, lang){
		if(lang == "fr"){
			if(monthInt == "01") return "Janvier";
			if(monthInt == "02") return "Février";
			if(monthInt == "03") return "Mars";
			if(monthInt == "04") return "Avril";
			if(monthInt == "05") return "Mai";
			if(monthInt == "06") return "Juin";
			if(monthInt == "07") return "Juillet";
			if(monthInt == "08") return "Août";
			if(monthInt == "09") return "Septembre";
			if(monthInt == "10") return "Octobre";
			if(monthInt == "11") return "Novembre";
			if(monthInt == "12") return "Décembre";
		}
	}
}

function getObjectId(object){
	if(object === null) return null;
	if("undefined" != typeof object._id && "undefined" != typeof object._id.$id) 	return object._id.$id.toString();
	if("undefined" != typeof object.id) 	return object.id;
	if("undefined" != typeof object.$id) 	return object.$id;
	return null;
};


function getFullTextCountry(codeCountry){
	var countries = {
		"FR" : "France",
		"RE" : "Réunion",
		"NC" : "Nouvelle-Calédonie",
		"GP" : "Gouadeloupe",
		"GF" : "Guyanne-Française",
		"MQ" : "MartiniqueMQ",
		"PM" : "Saint-Pierre-Et-Miquelon"
	};
	if(typeof countries[codeCountry] != "undefined")
	return countries[codeCountry];
	else return "";
}





var dataHelper = {
	compare : function ( a, b ) {
		if ( a.last_nom < b.last_nom ){
			return -1;
		}
		if ( a.last_nom > b.last_nom ){
			return 1;
		}
			return 0;
	},
	sortObjectByValue : function(list){
		mylog.log("dataHelper.sortObjectByValue", list);
		var listVal = []
		$.each(list, function(kList, vList){
			listVal.push( ( ( typeof tradCategory[vList] != "undefined") ? tradCategory[vList]:vList ) );
		});
		listVal.sort();
		mylog.log("dataHelper.sortObjectByValue listVal", listVal);
		var newList = {};
		$.each(listVal, function(k, v){
			$.each(list, function(kList, vList){
				if(v == ( ( typeof tradCategory[vList] != "undefined") ? tradCategory[vList]:vList ) ){
					mylog.log("dataHelper.sortObjectByValue v", kList, v);
					newList[kList] = v;
				}
			});
		});
		mylog.log("dataHelper.sortObjectByValue end", newList);
		return newList;
	},
	csvToArray : function (csv, separateur, separateurText){
		var lines = csv.split("\n");			
		var result = [];
		$.each(lines, function(key, value){
			if(value.length > 0){
				var colonnes = value.split(separateur);
				var newColonnes = [];
				$.each(colonnes, function(keyCol, valueCol){
					
					if(typeof separateurText == "undefined" || separateurText =="")
						newColonnes.push(valueCol);
					else{
						if(valueCol.charAt(0) == separateurText && valueCol.charAt(valueCol.length-1) == separateurText){
							var elt = valueCol.substr(1,valueCol.length-2);
							newColonnes.push(elt);
						}else{
							newColonnes.push(valueCol);
						}
					}
					
					
				});
				result.push(newColonnes);
			}
			
		});
		return result;
	},

	markdownToHtml : function (str) { 
		var converter = new showdown.Converter();
		var res = converter.makeHtml(str);
		return res;
	},

	convertMardownToHtml : function (text) { 
		var converter = new showdown.Converter();
		return converter.makeHtml(text);
	},
	
	htmlToMarkdown: function (html) {
		var converter = new showdown.Converter();
		var res = converter.makeMarkdown(html);
		return res;
	},

	convertHtmlToMarkdown: function (html) {
		var converter = new showdown.Converter();
		return converter.makeMarkdown(html);
	},

	stringToBool : function (str) {
		var bool = false;
		if(str == "true" || str == true || str == "1" || str == 1)
			bool = true;
		return bool;
	},

	activateMarkdown : function (elem) { 
		//mylog.log("activateMarkdown", elem);

		markdownParams = {
			savable:false,
			iconlibrary:'fa',
			language:'fr',
			onPreview: function(e) {
				var previewContent = "";
				if (e.isDirty()) {
					previewContent = dataHelper.convertMardownToHtml(e.getContent());
				} else {
					previewContent = dataHelper.convertMardownToHtml($(elem).val());
				}
				return previewContent;
			},
			onSave: function(e) {
				mylog.log(e);
			},
			// Callback for the 'onShow' event.
		    //   Here we will transform the custom button into a dropdown button.
		    onShow: function(e) {
		      	// Get the custom button named "mention".
		      	var $button = e.$textarea.closest('.md-editor').find('button[data-handler="bootstrap-markdown-mention"]');
		      
		     	$button.attr('data-toggle', 'dropdown')
		        .css({
		          'float': 'none'
		        })
		        //.before('<input type="text" style="width:70%" id="findMentionName" />');
		        //TODO : select a text and open finder panel
		        
		        
		        $('button[data-handler="bootstrap-markdown-highlight"]').off().on("click",function() {
		        	markMD = $(".md-input").val()+"<mark>your text to highlight</mark>";
					$(".md-input").val(markMD);
		        })
		        $('button[data-handler="bootstrap-markdown-mention"]').off().on("click",function() { 
		      	  	title = "<h1 class='text-center' style='color:#337ab7'>@ Finder<br/>";
					message = '<i class="fa fa-search" style="color:#337ab7"></i> <input type="text" style="height:50px;color:#666" id="findMentionName" />'+
					            			'</h1><h4><div class="text-center" id="mentionResult"></div></h4>';

						        
					smallMenu.open(title+message,null,null,function(){  
						
			        	$( "#findMentionName" ).autocomplete( {
					        source: function( request, response ) {
					        	$("#mentionResult").html("<i class='fa fa-spin fa-circle-o-notch padding-25 fa-2x letter-azure'></i>");
					        	$.ajax( {
						          url: baseUrl+"/" + moduleId + "/search/globalautocomplete",
						          dataType: "json",
						          data: {
						          	jqAuto : 1,
						            name: request.term,
						            searchType: ["citoyens","organizations","projects","events"],
									searchBy: "ALL"
						          },
						          complete: function( data ) 
						          {
						          	if(data.responseText != "[]" ){		        
						          		
							          	var resultList = "<div class='text-center' style='margin-top:20px'><ul  style='list-style:none;' id='finderFilters'></ul><ul class='container row' style='list-style:none;'>";
							          	$.each($.parseJSON(data.responseText),function(i,v) {
							          		tobj = dyFInputs.get(v.type); 
							          		color = (tobj.color) ? tobj.color : "#eee" ;
							          		img = (v.img) ? "<img width='50' src='"+baseUrl+v.img+"'/>" : "<i class='fa fa-2x fa-"+tobj.icon+"'></i>";
							          		resultList += "<li class='col-xs-3 "+v.type+"' style='height:80px; border:1px solid #666;padding:5px;'>"+
									          			"<a href='javascript:;' class='mentionLine' data-name='"+v.label+"' data-id='"+v.id+"' data-type='"+v.type+"' data-slug='"+v.slug+"'>"+img+" "+v.value+"</a>"+
									          		+"</li>";	

							          	});
							          	resultList += "</ul></div>";
							            
							            $("#mentionResult").html(resultList);
									    $(".mentionLine").click(function() {
								    		//TODO position on cursor
								    		//send notif to mentionned person
								    		//link markdown 
								    		linkMD = $(".md-input").val()+"["+$(this).data("name")+"]("+baseUrl+"/co2#@"+$(this).data("slug")+")";
								    		$(".md-input").val(linkMD);
								    		$("#openModal").modal("hide");

								    	});
							        } else {
							        	invite = "#element.invite.type."+contextData.type+".id."+contextData.id;
							        	$("#mentionResult").html("<h4>Aucun résultat correspond. <br/>"+
							        		"<a href='"+invite+"' class='lbhp'>Invité par mail ?</a></h4>");
							        	coInterface.bindLBHLinks();
							        }

								},				    
								    
								} )
					   		},
					      	minLength: 2
					    } );
					    $( "#findMentionName" ).focus();
			        })
		        });


		      $button.dropdown();
		    },
		    additionalButtons: [
		      [{
		        data: [{
		          name: 'mention',
		          title: 'Mention',
		          icon: {
		            'fa': 'fa fa-at'
		          }
		        }]
		      },
		      {
		        name: 'highlight',
		        data: [{
		          name: 'cmdHighLight',
		          title: 'Highlight',
		          icon: {
		            fa: 'fa fa-paint-brush'
		          },
		          callback: function(e) {
		            // Give/remove ** surround the selection
		            var chunk, cursor, selected = e.getSelection(),
		              content = e.getContent();

		            if (selected.length === 0) {
		              // Give extra word
		              chunk = e.__localize('highlighted text');
		            } else {
		              chunk = selected.text;
		            }

		            // transform selection and set the cursor into chunked text
		            if (content.substr(selected.start - 6, 6) === '<mark>' &&
		              content.substr(selected.end, 6) === '<mark>') {
		              e.setSelection(selected.start - 6, selected.end + 6);
		              e.replaceSelection(chunk);
		              cursor = selected.start - 6;
		            } else {
		              e.replaceSelection('<mark>' + chunk + '</mark>');
		              cursor = selected.start + 6;
		            }

		            // Set the cursor
		            e.setSelection(cursor, cursor + chunk.length);
		          }
		        }
		       
		       ]
		      }
		    ]
		    ]
		}

		if( !$('script[src="'+baseUrl+'/plugins/bootstrap-markdown/js/bootstrap-markdown.js"]').length ){
			mylog.log("activateMarkdown if");

			$("<link/>", {
			   rel: "stylesheet",
			   type: "text/css",
			   href: baseUrl+"/plugins/bootstrap-markdown/css/bootstrap-markdown.min.css"
			}).appendTo("head");
			$.getScript( baseUrl+"/plugins/showdown/showdown.min.js", function( data, textStatus, jqxhr ) {

				$.getScript( baseUrl+"/plugins/bootstrap-markdown/js/bootstrap-markdown.js", function( data, textStatus, jqxhr ) {
					mylog.log("HERE", elem);

					$.fn.markdown.messages['fr'] = {
						'Bold': trad.Bold,
						'Italic': trad.Italic,
						'Heading': trad.Heading,
						'URL/Link': trad['URL/Link'],
						'Image': trad.Image,
						'List': trad.List,
						'Preview': trad.Preview,
						'strong text': trad['strong text'],
						'emphasized text': trad['strong text'],
						'heading text': trad[''],
						'enter link description here': trad['enter link description here'],
						'Insert Hyperlink': trad['Insert Hyperlink'],
						'enter image description here': trad['enter image description here'],
						'Insert Image Hyperlink': trad['Insert Image Hyperlink'],
						'enter image title here': trad['enter image title here'],
						'list text here': trad['list text here']
					};
					$(elem).markdown(markdownParams);
				});


			});
		} else {
			mylog.log("activateMarkdown else");
			$(elem).markdown(markdownParams);
		}

		//$(elem).before(tradDynForm["syntaxmarkdownused"]);
		$(elem).before("<small class='block letter-light text-left'><i class='fa fa-info-circle'></i> "+tradDynForm["discovermarkdownsyntax"]+"</small>");
	},
	/*
	params.collection
	params.id
	params.path
	params.value
	*/
	path2Value : function ( params, callback ) 
	{
		dyFObj.path2Value = {"params" : params};
		mylog.log( "path2Value", baseUrl+"/"+moduleId+"/element/updatepathvalue", params);	
		if( !notEmpty(params.collection))
			alert("collection cannot be empty");
		else 
	 		$.ajax({
		        type: "POST",
		        url: baseUrl+"/"+moduleId+"/element/updatepathvalue",
		        data: params,
		       	dataType: "json",
		    	success: function(data){
		    		mylog.log("success path2Value : ",data);
		    		dyFObj.path2Value.result = data;
			    	if(data.result){
						if(typeof callback == "function")
							callback(data);
						else{
							toastr.success(data.msg); 
							urlCtrl.loadByHash(location.hash);
						}
			    	}else{
			    		toastr.error(data.msg);
			    	}
			    },
			    error: function(data){
			    	toastr.error("Something went really bad ! Please contact the administrator.");
			    }
			});
	}
		
}

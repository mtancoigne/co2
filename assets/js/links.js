var links = {
	connectType : {
		"citoyens":"follows",
		"organizations":"members",
		"projects":"contributors",
		"events": "attendees"
	},
	linksTypes : {
        "citoyens" :{
        	"projects" :  "projects",
			"events" :  "events",
            "organizations" :  "memberOf"
        },
    	"organizations" :{
    		"projects" :  "projects",
			"events" :  "events",
            "citoyens" : "members",
            "organizations" : "members"
        },
    	"events" :{
    			"events" : "subEvent",
                "citoyens" : "attendees"
        },
    	"projects" :{
    			"organizations" : "contributors",
				"citoyens" : "contributors",
				"projects" :  "projects"
		}
	},
	getConnect : function(typeParent, typeChild ){
		return links.linksTypes[ typeParent ][ typeChild ];
	},
	disconnect : function(parentType,parentId,childId,childType,connectType, callback, linkOption, msg) {
		mylog.log("links.disconnect ", parentType,parentId,childId,childType,connectType, callback, linkOption, msg);
		var messageBox = (notNull(msg)) ? msg : trad["removeconnection"+connectType];
		$(".disconnectBtnIcon").removeClass("fa-unlink").addClass("fa-spinner fa-spin");
		bootbox.dialog({
	        onEscape: function() {
	            $(".disconnectBtnIcon").removeClass("fa-spinner fa-spin").addClass("fa-unlink");
	        },
	        message: '<div class="row">  ' +
	            '<div class="col-md-12"> ' +
	            '<span>'+messageBox+' ?</span> ' +
	            '</div></div>',
	        buttons: {
	            success: {
	                label: "Ok",
	                className: "btn-primary",
	                callback: function () {
	                	links.disconnectAjax(parentType, parentId, childId,childType,connectType, linkOption, callback);
	                }
	            },
	            cancel: {
	            	label: trad["cancel"],
	            	className: "btn-secondary",
	            	callback: function() {
	            		$(".disconnectBtnIcon").removeClass("fa-spinner fa-spin").addClass("fa-unlink");
	            	}
	            }
	        }
	    });      
	}, 
	disconnectAjax: function (parentType, parentId, childId,childType,connectType, linkOption, callback){
		var formData = {
			"childId" : childId,
			"childType" : childType, 
			"parentType" : parentType,
			"parentId" : parentId,
			"connectType" : connectType,
		};
		if(typeof linkOption != "undefined" && linkOption)
			formData.linkOption=linkOption;
		$.ajax({
			type: "POST",
			url: baseUrl+"/"+moduleId+"/link/disconnect",
			data : formData,
			dataType: "json",
			success: function(data){
				if ( data && data.result ) {
					typeConnect=(formData.parentType==  "citoyens") ? "people" : formData.parentType;
					idConnect=formData.parentId;
					if(formData.parentId==userId){
						typeConnect=(formData.childType==  "citoyens") ? "people" : formData.childType;
						idConnect=formData.childId;
					}
					removeFloopEntity(idConnect, typeConnect);
					toastr.success("Le lien a été supprimé avec succès");
					if (typeof callback == "function") 
						callback();
					else
						urlCtrl.loadByHash(location.hash);
				} else {
				   toastr.error("You leave succesfully");
				}
			}
		});
	},
	// Javascript function used to validate a link between parent and child (ex : member, admin...)
	validate: function(parentType, parentId, childId, childType, linkOption, callback) {
		var formData = {
			"childId" : childId,
			"childType" : childType, 
			"parentType" : parentType,
			"parentId" : parentId,
			"linkOption" : linkOption,
		};

		$.ajax({
			type: "POST",
			url: baseUrl+"/"+moduleId+"/link/validate",
			data: formData,
			dataType: "json",
			success: function(data) {
				if (data.result) {
					toastr.success(data.msg);
					if (typeof callback == "function") 
						callback(parentType, parentId, childId, childType, linkOption);
					else
						urlCtrl.loadByHash(location.hash);

				} else {
					toastr.error(data.msg);
				}
			},
		});  
	},
	follow : function(parentType, parentId, childId, childType, callback){
		mylog.log("follow",parentType, parentId, childId, childType, callback);
		//$(".followBtn").removeClass("fa-link").addClass("fa-spinner fa-spin");
		var formData = {
			"childId" : childId,
			"childType" : childType, 
			"parentType" : parentType,
			"parentId" : parentId,
		};
		$.ajax({
			type: "POST",
			url: baseUrl+"/"+moduleId+"/link/follow",
			data: formData,
			dataType: "json",
			success: function(data) {
				if(data.result){
					if (formData.parentType)
						addFloopEntity(formData.parentId, formData.parentType, data.parent);
					toastr.success(data.msg);	
					if (typeof callback == "function") 
						callback();
					else
						urlCtrl.loadByHash(location.hash);
				}
				else
					toastr.error(data.msg);
			},
		});
	},
	connect: function(parentType, parentId, childId, childType, connectType, parentName, actionAdmin, callback) {
		if(parentType=="events" && connectType=="attendee")
			$(".connectBtn").removeClass("fa-link").addClass("fa-spinner fa-spin");
		else
			$(".becomeAdminBtn").removeClass("fa-user-plus").addClass("fa-spinner fa-spin");
		titleBox="";
		messageBox=(userId==childId) ? trad["suretojoin"+parentType] : trad.validateaddedofthisuser;
		if (connectType=="admin")
			messageBox += " " + trad["as"+connectType];
		if($.inArray(connectType, ["admin","attendee"])< 0){
			titleBox=trad["suretojoin"+parentType]+" "+trad["as"+connectType]+" ?";
			messageBox='<div class="row">  ' +
                '<div class="col-md-12"> ' +
                    '<form class="form-horizontal"> ' +
	                    '<label class="col-md-4 control-label" for="awesomeness">'+tradDynForm.wouldbecomeadmin+'?</label> ' +
	                    '<div class="col-md-4"> <div class="radio"> <label for="awesomeness-0"> ' +
	                    '<input type="radio" name="askForAdmin" id="awesomeness-0" value="admin"> ' +
	                    tradDynForm.yes+' </label> ' +
	                    '</div><div class="radio"> <label for="awesomeness-1"> ' +
	                    '<input type="radio" name="askForAdmin" id="awesomeness-1" value="'+connectType+'" checked="checked"> '+tradDynForm.no+' </label> ' +
	                    '</div> ' +
	                    '</div> </div>' +
               		'</form>'+
               	'</div>'+
            '</div>';
        }
		bootbox.dialog({
                title:  titleBox,
                onEscape: function() {
	                $(".becomeAdminBtn").removeClass("fa-spinner fa-spin").addClass("fa-user-plus");
                },
                message: messageBox,
                buttons: {
                    success: {
                        label: "Ok",
                        className: "btn-primary",
                        callback: function () {
                        	var roles=null;
                        	if($("input[name='askForAdmin']").length){
	                           	roles = ($('#role').val() != "") ? $('#role').val() : null;
	                            connectType = $("input[name='askForAdmin']:checked").val();
	                        }
                            links.connectAjax(parentType, parentId, childId, childType, connectType, roles, callback);
                        }
                    },
                    cancel: {
                    	label: trad["cancel"],
                    	className: "btn-secondary",
                    	callback: function() {
                    		$(".becomeAdminBtn").removeClass("fa-spinner fa-spin").addClass("fa-user-plus");
                    	}
                    }
                }
            }
        );
	},
	connectAjax : function(parentType, parentId, childId, childType, connectType, roles, callback){
		var formData = {
			"childId" : childId,
			"childType" : childType, 
			"parentType" : parentType,
			"parentId" : parentId,
			"connectType" : connectType
		};
		if(typeof roles != "undefined" && notNull(roles) )
			formData.roles=roles;
		$.ajax({
			type: "POST",
			url: baseUrl+"/"+moduleId+"/link/connect",
			data: formData,
			dataType: "json",
			success: function(data) {
				if(data.result){
					addFloopEntity(data.parent["_id"]["$id"], data.parentType, data.parent);
					toastr.success(data.msg);
					if (typeof callback == "function") 
						callback();
					else
						urlCtrl.loadByHash(location.hash);
				}
				else{
					if(typeof(data.type)!="undefined" && data.type=="info")
						toastr.info(data.msg);
					else
						toastr.error(data.msg);
				}
			}
		});
	}  
}
 var searchObject={
    text:"",
    nbPage:0,
    indexMin:0,
    indexStep:30,
    count:true,
    tags:[],
    initType : "",
    filters:{},
    types:[],
    countType:[],
    locality:{},
    reset : function(){
        mylog.log("searchObject.reset");
        $.each(searchObject, function(key,v){
            if($.inArray(key,["startDate","endDate", "searchSType", "section", "subType", "category", "priceMin", "priceMax", "devise", "source", "links", "filters"]) > -1){
                delete searchObject[key];
            }
        });
        searchObject.nbPage=0,
        searchObject.indexMin=0,
        searchObject.indexStep=(typeof costum != "undefined" && notNull(costum) && typeof costum.searchObject != "undefined" && typeof costum.searchObject.indexStep != "undefined") ? costum.searchObject.indexStep : 30,
        searchObject.count=true,
        searchObject.initType="",
        searchObject.types=[];
        searchObject.countType=[];
        searchObject.text="";
        searchObject.tags=[];
        if(typeof costum != "undefined" && notNull(costum) && typeof costum.searchObject != "undefined" && typeof costum.searchObject.sortBy != "undefined")
            searchObject.sortBy=costum.searchObject.sortBy;
        if(typeof costum != "undefined" && notNull(costum) && typeof costum.searchObject != "undefined" && typeof costum.searchObject.links != "undefined")
            searchObject.links=costum.searchObject.links;
    }
};
var searchInterface={
    init : function(typeInit){
        mylog.log("searchInterface.init", typeInit);
        searchInterface.setSearchTypes(typeInit);
        searchInterface.initSearchParams();
        searchInterface.setSearchbar();
        if(searchObject.types.length > 1) 
            searchAllEngine.initSearch();
        else if(searchObject.types.length == 1 && $.inArray( "events", searchObject.types) >= 0){
            searchAllEngine.initSearchEvent();
        }
        if(searchObject.text != "") $(".main-search-bar, #second-search-bar").val(searchObject.text);
        $(".theme-header-filter").off().on("click",function(){
                if(!$("#filter-thematic-menu").is(":visible") || $(this).hasClass("toogle-filter"))
                    $("#filter-thematic-menu").toggle();
        });
        $("#filters-container-menu .theme-header-filter, #filters-container-menu .scope-header-filter").click(function(){
            coInterface.simpleScroll(0, 500);
        });

        $(".scope-header-filter").off().on("click",function(){
            $("#searchOnCity").trigger("click");
        });
        $('#tagsFilterInput, #tagsFilterInput-xs').select2({tags:[trad.writekeywords]});
        $('#tagsFilterInput-xs').on("change", function(){
            $('#tagsFilterInput').val($(this).val().split(",")).trigger("change");
        });
        $(".btn-select-filliaire").off().on("click",function(){
            mylog.log("searchInterface.init .btn-select-filliaire");
            var fKey = $(this).data("fkey");
            myMultiTags = {};
            tagsArray=[];
            $.each(filliaireCategories[fKey]["tags"], function(key, tag){
                tag=(typeof tradTags[tag] != "undefined") ? tradTags[tag] : tag;
                tagsArray.push(tag);
            });
            $('#tagsFilterInput-xs').val(tagsArray).trigger("change");
        });
        $(".btn-tags-start-search").off().on("click", function(){
            searchObject.tags=$('#tagsFilterInput').val().split(",");
            searchObject.nbPage=0;
            pageCount=true;
            searchObject.count=true;
            $('#tagsFilterInput-xs').val(searchObject.tags).trigger("change");
            if(typeof searchObject.ranges != "undefined") searchAllEngine.initSearch();
            $(".dropdown-tags").removeClass("open");
            activeTagsFilter();
            startSearch(0, indexStepInit, searchCallback);
        });
        $(".btn-tags-refresh").off().on("click", function(){
            searchObject.tags=[];
            $('#tagsFilterInput-xs').val("").trigger("change");
            searchObject.nbPage=0;
            pageCount=true;
            searchObject.count=true;
            if(typeof searchObject.ranges != "undefined") searchAllEngine.initSearch();
            $(".dropdown-tags").removeClass("open");
            activeTagsFilter();
            startSearch(0, indexStepInit, searchCallback);
        });
        $(".btn-tags-unik").off().on("click", function(){
            coInterface.showLoader("#dropdown_search",trad.currentlyresearching);
            $(".btn-tags-unik").removeClass("active");
            if(!notNull($(this).data("k")) || $(this).data("k")=="")
                searchObject.tags=[];
            else{
                $(this).addClass("active");
                searchObject.tags=[$(this).data("k")];
            }

            if(typeof agenda != "undefined" ){
                agenda.dayCount = 0;
                agenda.finishSearch = false;
                agenda.countsLaunchSesearch = 0;
                agenda.nbSearch = 0;
            }
            searchObject.nbPage=0;
            searchObject.indexMin=0;
            pageCount=true;
            searchObject.count=true;
            // if(typeof searchObject.ranges != "undefined"){
            //     mylog.log("directory.js startSearch agenda. searchAllEngine");
            //     searchAllEngine.initSearch(); 
            // } 
            mylog.log("directory.js startSearch agenda. indexStepInit", 0, indexStepInit);
            startSearch(0, indexStepInit, searchCallback);
        });


        $(".main-btn-create").off().on("click",function(){
            currentKFormType = $(this).data("ktype");
            var type = $(this).data("type");

            if(type=="all"){
                $("#dash-create-modal").modal("show");
                return;
            }

            if(type=="events") type="event";
            if(type=="vote") type="entry";
            dyFObj.openForm(type);
        });
        if(typeof themeParams!="undefined" && typeof themeParams.appRendering != "undefined" && themeParams.appRendering=="vertical"){
            $(".headerSearchContainer, .bodySearchContainer").removeClass("col-md-10 col-sm-10 col-sm-offset-1 col-md-offset-1");
        }
        
    },
    setSearchbar : function(){
        $("#main-search-bar").off().on("keyup",function(e){
            //$("#second-search-bar").val($(this).val());
            $("#main-search-xs-bar").val($(this).val());
            $("#input-search-map").val($(this).val());
            if(e.keyCode == 13 || $(this).val() == "" ){
                spinSearchAddon(true);
                searchObject.text = $(this).val();
                if(location.hash.indexOf("#live") < 0){
                    searchObject.nbPage=0;
                    pageCount=true;
                    searchObject.count=true;
                    if(typeof searchObject.ranges != "undefined") searchAllEngine.initSearch();
                    startSearch(0, indexStepInit, searchCallback);
                }else{
                    startNewsSearch(); 
                    coInterface.scrollTo("#search-content");
                }
            }
        });
        $("#main-search-xs-bar").off().on("keyup",function(e){
            //$("#second-search-bar").val($(this).val());
            $("#main-search-bar").val($(this).val());
            $("#input-search-map").val($(this).val());
            if(e.keyCode == 13 || $(this).val() == "" ){
                spinSearchAddon(true);
                searchObject.text = $(this).val();
                if(location.hash.indexOf("#live") < 0){
                    searchObject.nbPage=0;
                    pageCount=true;
                    searchObject.count=true;
                    if(typeof searchObject.ranges != "undefined") searchAllEngine.initSearch();
                    startSearch(0, indexStepInit, searchCallback);
                }else{
                    startNewsSearch();
                    coInterface.scrollTo("#search-content");
                }
            }
        });
        $("#second-search-bar").off().on("keyup",function(e){ 
            $("#input-search-map").val($("#second-search-bar").val());
            $("#second-search-xs-bar").val($("#second-search-bar").val());
            if(e.keyCode == 13){
                searchObject.text=$(this).val();
                myScopes.type="open";
                myScopes.open={};
                startGlobalSearch(0, indexStepGS);
             }
        });
        $("#second-search-xs-bar").off().on("keyup",function(e){ 
            $("#input-search-map").val($("#second-search-xs-bar").val());
            $("#second-search-bar").val($("#second-search-xs-bar").val());
            if(e.keyCode == 13){
                searchObject.text=$(this).val();
                myScopes.type="open";
                myScopes.open={};
                startGlobalSearch(0, indexStepGS);
             }
        });
        $("#second-search-bar-addon, #second-search-xs-bar-addon").off().on("click", function(){
            $("#input-search-map").val($("#second-search-bar").val());
            searchObject.text=$("#second-search-bar").val();
            myScopes.type="open";
            myScopes.open={};
            startGlobalSearch(0, indexStepGS);
        });
        /*$("#second-search-bar").off().keyup(function(e){
            $(".main-search-bar").val($(this).val());
            if(e.keyCode == 13 || $(this).val() == ""){
                scrollH= ($("#filter-thematic-menu").is(":visible")) ? 250 : 0;
                coInterface.simpleScroll(scrollH);
                searchObject.nbPage=0;
                searchObject.text = $(this).val();
                searchObject.count=true;
                pageCount=true;
                if(searchObject.initType=="territorial") searchAllEngine.initSearch();
                startSearch(0, indexStepInit, searchCallback);
                $(".btn-directory-type").removeClass("active");
             }
        });*/

        $("#input-search-map").off().keyup(function(e){
            $("#second-search-bar").val($("#input-search-map").val());
            $(".main-search-bar").val($("#input-search-map").val());
            if(e.keyCode == 13){
                if(typeInit == "all") searchInterface.setSearchTypes("allSig");
                else searchInterface.setSearchTypes(typeInit);
                searchObject.text = $(this).val();
                searchObject.count=true;
                pageCount=true;
                if(searchObject.app=="territorial") searchAllEngine.initSearch();
                startSearch(0, indexStepInit, searchCallback);
                $(".btn-directory-type").removeClass("active");
             }
        });
        //.menu-btn-start-search,
        $("#menu-map-btn-start-search, #mainNav  #main-search-bar-addon, #main-search-xs-bar-addon").off().on("click", function(){
            scrollH= ($("#filter-thematic-menu").is(":visible")) ? 250 : 0;
            spinSearchAddon(true);
            coInterface.simpleScroll(scrollH);
            if($(this).hasClass("menu-btn-start-search"))
                searchObject.text=$("#second-search-bar").val();
            else if ($(this).hasClass("input-group-addon"))   
                searchObject.text=$("#main-search-bar").val();
            else if ($(this).hasClass("input-group-addon-xs"))   
                searchObject.text=$("#main-search-xs-bar").val();
            else
                searchObject.text=$("#input-search-map").val();
            $("#second-search-bar, .main-search-bar, #input-search-map").val(searchObject.text);
            searchObject.nbPage=0;
            searchObject.count=true;
            pageCount=true;
            if(typeof searchObject.ranges != "undefined") searchAllEngine.initSearch();
            startSearch(0, indexStepInit, searchCallback);
            $(".btn-directory-type").removeClass("active");
        });
    },
    bindFiltersEvents : function() { 
        $(".btn-directory-type").off().on("click", function(){
            var typeD = $(this).data("type");
           
            if(typeD == "events" && searchObject.initType=="events"){
                if($(this).hasClass("active")){
                    $(this).removeClass("active");    
                    delete searchObject.searchSType;
                    $(".dropdown-types .dropdown-toggle").removeClass("active").html("Type <i class='fa fa-angle-down'></i>");
                }else{
                    $(".btn-directory-type").removeClass("active");
                    $(this).addClass("active");
                    var typeEvent = $(this).data("type-event");
                    $(".dropdown-types .dropdown-toggle").addClass("active").html(tradCategory[typeEvent]+" <i class='fa fa-angle-down'></i>");
                    searchObject.searchSType = typeEvent;
                }
            }else{
                if(typeD=="all")
                    $(".dropdown-types .dropdown-toggle").removeClass("active").html("Type <i class='fa fa-angle-down'></i>");
                else
                    $(".dropdown-types .dropdown-toggle").addClass("active").html(tradCategory[typeD]+" <i class='fa fa-angle-down'></i>");
                if(typeD=="poi")
                    $(".dropdown-category").show();
                else
                    $(".dropdown-section, .dropdown-category").hide();
                $(".btn-directory-type").removeClass("active");
                $(this).addClass("active");
            }
            searchInterface.setSearchTypes(typeD);
            if(typeD=="all"){
                searchAllEngine.initInjectData();
                searchAllEngine.initSearch();
            }
            loadingData = false;
            pageCount=true;
            searchObject.nbPage=0;
            if(Object.keys(searchObject.countType).length>1) searchObject.count=false; 
            else searchObject.count=true;
            searchObject.type=searchType;
            startSearch(0, indexStepInit, searchCallback);
        });
        $(".btn-select-type-anc").off().on("click", function(){
            searchType = [ typeInit ];
            indexStepInit = 100;
            pageCount=true;
            searchObject.count=true;
            searchObject.nbPage=0;
            typeClass = $(this).data("type-anc");
            typeKey = $(this).data("key");
            //Case specific to focus on poleEmploi
            if(typeof searchObject.source != "undefined"){
                delete searchObject.source;
                $(".dropdown-sources .dropdown-toggle").removeClass("active").html(trad.datasource+" <i class='fa fa-angle-down'></i>");
            }    
            if( typeKey == "classifieds" || typeKey == "jobs" || typeKey == "all"){
                $(".dropdown-price").show(200);
                setTimeout(function(){
                    coInterface.scrollTo("#container-scope-filter");
                }, 400);
            }
            else {
                $(".dropdown-price").hide();
                $("#priceMin").val("");
                $("#priceMax").val("");
                delete searchObject.priceMin;
                delete searchObject.priceMax;
                coInterface.scrollTo("#container-scope-filter");
            }
            $(".dropdown-section .dropdown-toggle").html(trad.section+" <i class='fa fa-angle-down'></i>");
            $(".dropdown-category .dropdown-toggle").html(trad.category+" <i class='fa fa-angle-down'></i>");
            $(".dropdown-section .dropdown-toggle, .dropdown-category .dropdown-toggle").removeClass("active");
            if( $(this).hasClass( "active" ) )
            {
                typeKey = null;
                //searchObject.tags=[];
                if(typeof searchObject.searchSType != "undefined") delete searchObject.searchSType;
                $('.classifiedSection').remove();
                $(".label-category, .resultTypes").html("");
                $(".dropdown-types .dropdown-toggle").removeClass("active").html(trad.type+" <i class='fa fa-angle-down'></i>");
                $(".dropdown-section, .dropdown-category").hide(700);
                
            } 
            else 
            {
                $(".dropdown-types .dropdown-toggle").addClass("active").html(typeClass+" <i class='fa fa-angle-down'></i>");
                $(".dropdown-section, .dropdown-category").show();
                searchObject.searchSType=typeKey;
                initCategoryClassifieds(typeKey);
            }
            $(".btn-select-type-anc, .btn-select-section, .btn-select-category, .keycat").removeClass("active");
            

            $(".keycat").addClass("hidden");
            if(typeof searchObject.section != "undefined") delete searchObject.section;
            if(typeof searchObject.category != "undefined") delete searchObject.category;
            if(typeof searchObject.subType != "undefined") delete searchObject.subType;
            if(typeof searchObject.searchSType != "undefined") $(this).addClass("active");
            startSearch(0, indexStepInit, searchCallback);
                
        });
        $(".btn-select-source").click(function(){
            interopSearch($(this).data("key"), $(this).data("source"));
        });


        $(".btn-select-section").off().on("click", function()
        {    
            searchType = [ typeInit ];
            indexStepInit = 100;
            pageCount=true;
            searchObject.count=true;
            searchObject.nbPage=0;
            $(".dropdown-category .dropdown-toggle").html(trad.category+" <i class='fa fa-angle-down'></i>");
            $(".dropdown-category .dropdown-toggle").removeClass("active")
            if( $(this).hasClass( "active" ) )
            {
                sectionKey = null;
                //searchObject.tags=[];
                if(typeof searchObject.section != "undefined") delete searchObject.section;
                $('.classifiedSection').remove();
                $(".label-category, .resultTypes").html("");
                $(".dropdown-section .dropdown-toggle").removeClass("active").html(trad.section+" <i class='fa fa-angle-down'></i>");
            } 
            else 
            {
                section = $(this).data("type-anc");
                sectionKey = $(this).data("key");
                $(".dropdown-section .dropdown-toggle").addClass("active").html(tradCategory[sectionKey]+" <i class='fa fa-angle-down'></i>");
                if( $(this).data("key") == "all" ) delete searchObject.section;//sectionKey = "";
                else searchObject.section =  sectionKey;
            }
            $(".btn-select-section, .btn-select-category, .keycat").removeClass("active");
            $(".keycat").addClass("hidden");
            
            if(typeof searchObject.category != "undefined") delete searchObject.category;
            if(typeof searchObject.subType != "undefined") delete searchObject.subType;
            if(typeof searchObject.section != "undefined")
                $(this).addClass("active");

            startSearch(0, indexStepInit, searchCallback); 

            if(sectionKey && typeof modules[searchObject.searchSType].categories.sections[sectionKey] != "undefined") {
                var label = modules[searchObject.searchSType].categories.sections[sectionKey]["label"];
                $(".label-category").html("<i class='fa fa-"+ modules[searchObject.searchSType].categories.sections[sectionKey]["icon"] + "'></i> " + tradCategory[label]);
                $('.classifiedSection').remove();
                $(".resultTypes").append( "<span class='classifiedSection text-azure text-bold hidden-xs pull-right'><i class='fa fa-"+ modules[searchObject.searchSType].categories.sections[sectionKey]["icon"] + "'></i> " + modules[searchObject.searchSType].categories.sections[sectionKey]["label"]+'<i class="fa fa-times text-red resetFilters"></i></span>');
                $(".label-category").removeClass("letter-blue letter-red letter-green letter-yellow").addClass("letter-azure");//+classifieds[searchObject.searchStype].sections[sectionKey]["color"])
                $(".fa-title-list").removeClass("hidden");
            }
        });

        $(".btn-select-category").off().on("click", function(){ 
            searchType = [ typeInit ];
            var searchTxt = "";
            var classType = $(this).data("keycat");
            // Event for count in DB
            pageCount=true;
            searchObject.count=true;
            searchObject.nbPage=0;
            if(typeof searchObject.subType != "undefined") delete searchObject.subType;
            if( $(this).hasClass( "active" ) ){
                if(typeof searchObject.category != "undefined") delete searchObject.category;
                $(this).removeClass( "active" );
                $(".keycat-"+classType).addClass("hidden").removeClass("active"); 
                $(".dropdown-category .dropdown-toggle").removeClass("active").html(trad.category+" <i class='fa fa-angle-down'></i>");
            }else{
                $(".btn-select-category").removeClass("active");
                $(this).addClass("active");
                $(".keycat").addClass("hidden");
                $(".keycat-"+classType).removeClass("hidden");  
                searchObject.category=classType;
                $(".dropdown-category .dropdown-toggle").addClass("active").html(tradCategory[classType]+" <i class='fa fa-angle-down'></i>");
            }
            startSearch(0, indexStepInit, searchCallback);  
        });

        

        $(".keycat").off().on("click", function(){
            mylog.log("searchInterface.bindFiltersEvents .keycat", typeInit);
            searchObject.types = [ typeInit ];
            var searchTxt = "";
            var classType = $(this).data("categ");
            var classSubType = $(this).data("keycat");
            // Event for count in DB
            pageCount=true;
            searchObject.count=true;
            searchObject.nbPage=0;
            if( $(this).hasClass( "active" ) ){
                if(typeof searchObject.subType != "undefined") delete searchObject.subType;
                $(this).removeClass( "active" );
                $(".dropdown-category .dropdown-toggle").addClass("active").html(tradCategory[searchObject.category]+" <i class='fa fa-angle-down'></i>");
            }else{
                $(".keycat").removeClass("active");
                $(this).addClass("active");
                searchObject.subType=classSubType;
                searchObject.category=classType;
                $(".dropdown-category .dropdown-toggle").addClass("active").html(tradCategory[searchObject.category]+" | "+tradCategory[classSubType]+" <i class='fa fa-angle-down'></i>");
            }

            coInterface.scrollTo("#container-scope-filter");
            startSearch(0, searchObject.indexStep, searchCallback);  
        });
        $('.dropdown-menu[aria-labelledby="dropdownPrice"], .dropdown-menu[aria-labelledby="dropdownTags"], .dropdown-menu[aria-labelledby="dropdownCategory"]').on('click', function(event){
            // The event won't be propagated up to the document NODE and 
            // therefore delegated events won't be fired
            event.stopPropagation();
        });
        $(".btn-price-filter").off().on("click",function(){
            if($(this).data("key")=="reset"){
                $(".dropdown-price .dropdown-toggle").removeClass("active").html(trad.price+" <i class='fa fa-angle-down'></i>");
                if(typeof searchObject.priceMin != "undefined") delete searchObject.priceMin;
                if(typeof searchObject.priceMax != "undefined") delete searchObject.priceMax;
                if(typeof searchObject.devise != "undefined") delete searchObject.devise;
                $("#priceMin, #priceMax, #devise").val("");
            }else{
                if(typeof $("#priceMin").val() != "undefined" && $("#priceMin").val()!="")
                    searchObject.priceMin=$("#priceMin").val();
                else if(typeof searchObject.priceMin != "undefined")
                    delete searchObject.priceMin;
                if(typeof $("#priceMax").val() != "undefined" && $("#priceMax").val()!="")
                    searchObject.priceMax=$("#priceMax").val();
                else if(typeof searchObject.priceMax != "undefined")
                    delete searchObject.priceMax;
                if(typeof $("#devise").val() != "undefined" && $("#devise").val()!="" && $("#devise").val()!="all")
                    searchObject.devise=$("#devise").val();
                else if(typeof searchObject.devise != "undefined")
                    delete searchObject.devise;
                activePriceFilter();
            }
            $(".dropdown-price").removeClass("open");
            pageCount=true;
            searchObject.count=true;
            searchObject.nbPage=0;
            startSearch(0, searchObject.indexStep, searchCallback);
        });
        $("#btn-create-classified").off().on("click", function(){
             dyFObj.openForm('classified');
        });

        $("#priceMin").filter_input({regex:'[0-9]'}); //[a-zA-Z0-9_] 
        $("#priceMax").filter_input({regex:'[0-9]'}); //[a-zA-Z0-9_] 
        $('.main-search-bar, #second-search-bar, #input-search-map').filter_input({regex:'[^@\"\`/\(|\)/\\\\]'}); //[a-zA-Z0-9_] 
    },

/* ----------------------------
        SEARCH ENGINE
-------------------------------*/
    constructObjectAndUrl: function(notUrl){
        mylog.log("searchInterface.constructObjectAndUrl", searchObject);
        getStatus="";
        var searchConstruct={};
        if(searchObject.text != "")
            searchConstruct.name=searchObject.text;
        
        if(typeof searchObject.types != "undefined")
            searchConstruct.searchType=searchObject.types;
        //}else{
          //  searchConstruct.searchType=searchObject.types;
        //}
        if(typeof searchObject.tags != "undefined" && searchObject.tags.length > 0)
            searchConstruct.searchTags=searchObject.tags;
        if(typeof searchObject.searchSType != "undefined")
            searchConstruct.searchSType = searchObject.searchSType;
        if(typeof searchObject.section != "undefined")
            searchConstruct.section = searchObject.section;
        if(typeof searchObject.category != "undefined")
            searchConstruct.category = searchObject.category;
        if(typeof searchObject.subType != "undefined")
            searchConstruct.subType = searchObject.subType;
        if(typeof searchObject.priceMin != "undefined")
            searchConstruct.priceMin = searchObject.priceMin; 
        if(typeof searchObject.priceMax != "undefined")
            searchConstruct.priceMax = searchObject.priceMax;
        if(typeof searchObject.devise != "undefined")
            searchConstruct.devise = searchObject.devise;
        if(typeof searchObject.startDate != "undefined"){
            if(searchObject.text==""){
              searchConstruct.startDate = searchObject.startDate;
              $(".calendar").show(700);
            }else
              $(".calendar").hide(700);
        }
        if(typeof searchObject.endDate != "undefined"){
            if(searchObject.text=="")
              searchConstruct.endDate = searchObject.endDate;
        } 
        if(typeof searchObject.source != "undefined") 
            searchConstruct.source = searchObject.source;
            

        if(typeof searchObject.indexMin != "undefined" && notNull(searchObject.indexMin))
            searchConstruct.indexMin=searchObject.indexMin;
        if(typeof searchObject.initType != "undefined")
            searchConstruct.initType=searchObject.initType;
        if(typeof searchObject.count != "undefined" && searchObject.count)
            searchConstruct.count=searchObject.count;
        if(typeof searchObject.ranges != "undefined" && (typeof searchObject.indexStep == "undefined" || searchObject.indexStep > 0))
            searchConstruct.ranges=searchObject.ranges;
        if(typeof searchObject.countType != "undefined")
            searchConstruct.countType=searchObject.countType;
        if(typeof searchObject.sourceKey != "undefined")
            searchConstruct.sourceKey=searchObject.sourceKey;
        if(typeof searchObject.sortBy != "undefined")
            searchConstruct.sortBy=searchObject.sortBy;
        if(typeof searchObject.indexStep != "undefined")
            searchConstruct.indexStep=searchObject.indexStep;
        if(typeof searchObject.links != "undefined")
            searchConstruct.links=searchObject.links;
        if(typeof searchObject.filters != "undefined")
            searchConstruct.filters=searchObject.filters;
        //COMMUNITY
        if(searchObject.community != false)
            searchConstruct.community = searchObject.community;
        // Locality
        searchConstruct.locality = getSearchLocalityObject();
        searchInterface.countActiveFilters();
        //Construct url with all necessar params

        if(typeof notUrl == "undefined" ||
            notUrl == null ||
            notUrl == false)
            urlCtrl.manageHistory();
        return searchConstruct;
    },
    getUrlSearchParams : function(extraParams=["nbPage"], checkForced=true){
        arrayToSearch=["text","types", "tags", "searchSType","section", "category", "subType","priceMin", "priceMax", "devise","startDate","endDate","source"];
        if(notNull(extraParams)){ arrayToSearch=arrayToSearch.concat(extraParams);}
        getStatus="";
        $.each(arrayToSearch, function(e,label){
            if(typeof searchObject[label] != "undefined"){
                if(!checkForced || !searchInterface.isForced(label)){
                    value=encodeURI((Array.isArray(searchObject[label])) ? searchObject[label].join(",") : searchObject[label]);
                    if(label=="text" && notEmpty(value))//>0 || searchObject[label]!= "")
                        getStatus+=((getStatus!="") ? "&":"")+label+"="+value;
                    else if(label=="nbPage"){
                        if(Number(value) != 0 && notEmpty(value))
                            getStatus+=((getStatus!="") ? "&":"")+label+"="+(Number(value)+1);
                    }
                    else if(label=="types" && typeof searchObject[label] != "undefined"){
                        if(searchObject[label].length==1 && $.inArray(searchObject.initType, ["news","all"]) >= 0)
                            getStatus+=((getStatus!="") ? "&":"")+label+"="+value;

                    }
                    else if($.inArray(label, ["startDate", "endDate"]) >= 0){
                        if(!notEmpty(searchObject.text))
                            getStatus+=((getStatus!="") ? "&":"")+label+"="+value;
                    }else if(notEmpty(value))
                        getStatus+=((getStatus!="") ? "&":"")+label+"="+value;
                }
            }
        });
        // Locality
        return getUrlSearchLocality(getStatus, true);
    },
    getParamsUrlForAlert: function(){
        getStatus=searchInterface.getUrlSearchParams(["sourceKey"]);
        //Construct url with all necessar params
        hashT=location.hash.split("?");
        return hashT[0].substring(0)+"?"+getStatus;
    },
    countActiveFilters: function(){
        count=0;
        $.each($("#filters-nav .dropdown .dropdown-toggle.active"), function($this){
            if($(this).is(":visible"))
                count++;
        });
        if(count>0)
            $(".btn-show-filters .badge").removeClass('hide badge-tranparent').addClass("animated bounceIn badge-success").html(count);
        else
            $(".btn-show-filters .badge").removeClass('animated bounceIn badge-success').addClass("hide badge-tranparent").html("");
    },
    initSearchParams : function(){
        mylog.log("searchInterface.initSearchParams");
        showFiltersInterface();
        if(location.hash.indexOf("?") > -1){
            getParamsUrls=location.hash.split("?");
            var parts = getParamsUrls[1].split("&");
            var $_GET = {};
            var initScopesResearch={"key":"","ids":[]};
            for (var i = 0; i < parts.length; i++) {
                var temp = parts[i].split("=");
                $_GET[decodeURIComponent(temp[0])] = decodeURIComponent(temp[1]);
            }
            if(Object.keys($_GET).length > 0){
                $.each($_GET, function(e,v){
                    v=decodeURI(v);
                    if(e=="scopeType") initScopesResearch.key=v; else searchObject[e]=v;
                    // Check on types on search app
                    if((searchObject.initType!= "all" && searchObject.initType!= "news") && e=="types"){ delete searchObject[e];}
                    //else if (e=="types"){searchObject[e]=[v]; delete searchObject.ranges;}
                    if(searchObject.initType!="classifieds" && $.inArray(e,["devise","priceMin","priceMax", "source"]) > -1) delete searchObject[e];
                    if(searchObject.initType!="events" && $.inArray(e,["startDate","endDate"]) > -1) delete searchObject[e];
                    //if(searchObject.initType=="all" && e=="searchSType") delete searchObject[e];  
                    if($.inArray(searchObject.initType, ["events", "news"])>-1 && $.inArray(e,["section","category","subType"]) > -1) delete searchObject[e];
                    if(searchObject.initType=="all" && $.inArray(e,["section","subType"]) > -1) delete searchObject[e];
                    if($.inArray(e,["cities","zones","cp"]) > -1) $.each(v.split(","), function(i, j){ initScopesResearch.ids.push(j) });
                    if(e=="tags"){
                        searchObject.tags=[];
                        $.each(v.split(","), function(i, j){searchObject.tags.push(j) });
                    } 
                    if(typeof searchObject.types=="string"){
                        mylog.log("searchInterface.initSearchParams searchObject.types", v);
                        searchObject.types=[v];
                    } 
                    if(typeof searchObject[e] != "undefined") activeFiltersInterface(e, v, searchObject.initType); 
                }); 
                if(initScopesResearch.key!="" && initScopesResearch.ids.length > 0) checkMyScopeObject(initScopesResearch, $_GET);
            }
        }else{
            appendScopeBreadcrum();
            activeFiltersInterface("tags", searchObject.tags);
        }
        if(searchObject.initType=="classifieds") 
            activeClassifiedFilters();
        if(searchObject.initType=="all")
            searchInterface.initTypeFiltersSearch();
        if(searchObject.initType=="events")
             searchInterface.initCategoriesEvent();
    },
    setSearchTypes: function(typeInit){
        mylog.log("searchInterface.setSearchTypes", typeInit);
        if(typeInit == "all") {
            if(isCustom("types") && notNull(directory.appKeyParam)){
                mylog.log("searchInterface.setSearchTypes here");
              searchObject.types = [];
              $.each(costum.app[directory.appKeyParam].filters.types, function(e, v){
                    mylog.log("searchInterface.setSearchTypes here", e, v);
                    // if(v=="organizations")
                    //     searchObject.types=searchObject.types.concat(["NGO", "Group", "GovernmentOrganization", "LocalBusiness"]);
                    // else searchObject.types.push(v);

                    searchObject.types.push(v);
              });
            }
            else{
                mylog.log("searchInterface.setSearchTypes else");
              //make sure headerParams (initjs.php) contains thany type of the searchObj
              searchObject.types = ["NGO", "Group", "GovernmentOrganization", "LocalBusiness", "projects", "events", "poi", "classifieds"];
              if(searchObject.text != "" || Object.keys(getSearchLocalityObject()).length > 0) 
                searchObject.types.push("persons");
            }
            if(searchObject.types.length == 1){
                searchObject.nbPage=0;
                searchObject.count=true;
                pageCount=true;
            }
        } else if(typeInit == "allSig"){
          searchObject.types = ["persons", "organizations", "projects", "poi", "cities"];
          searchObject.indexStep = 50;
        } else{
            if(typeInit != "all") pageCount = true;
            searchObject.types = [ typeInit ];
            delete searchObject.ranges;
        }
        mylog.log("searchInterface.setSearchTypes end searchObject.types", searchObject.types);
    },
    removeSearchType:function(type){
      var index = searchType.indexOf(type);
      if (index > -1) {
        searchType.splice(index, 1);
        $(".search_"+type).removeClass("fa-check-circle-o");
        $(".search_"+type).addClass("fa-circle-o");
      }
    },
    initTypeFiltersSearch: function(){
        str='<button class="btn btn-directory-type text-dark col-xs-12 btn-default" data-type="all">'+
                '<i class="fa fa-refresh"></i> '+
                '<span class="elipsis label-filter">'+trad.all+'</span>'+
            '</button>';
        $.each(categoriesFilters, function(e,v){
            elt=directory.getTypeObj(e);
            textColor= (typeof elt.color != "undefined") ? "text-"+elt.color : "";
            str+='<button class="btn btn-directory-type padding-10 col-xs-12 '+textColor+'" data-type="'+e+'">'+
                    '<i class="fa fa-'+elt.icon+'"></i> '+
                    '<span class="elipsis label-filter">'+elt.name+'</span>'+
                    '<span class="count-badge-filter" id="count'+e+'"></span>'+
                '</button>';
        });
        $(".dropdown-types .dropdown-menu").html(str);
        searchInterface.initCategoryPoi();
        if(searchObject.types.length==1 && searchObject.types[0]=="poi" 
            && (typeof searchObject.forced== "undefined" 
                || typeof searchObject.forced.category == "undefined" 
                || (typeof searchObject.forced.category == "object" && typeof searchObject.forced.category.length > 1)))
            $(".dropdown-category").show();
        else
            $(".dropdown-category").hide();
        searchInterface.bindFiltersEvents();
    },
    initCategoriesEvent:function(){
        str='';
        $.each(categoriesFilters, function(e,v){
            dataType="data-type-event='"+e+"' data-type='events'";
            textColor= (typeof v.color != "undefined") ? "text-"+v.color : "";
            icon = (typeof v.icon != "undefined") ? "<i class='fa fa-"+v.icon+"'></i> " : "";
            str+='<button class="btn btn-directory-type dropDesign col-xs-12 padding-10 '+textColor+'" '+dataType+'>'+
                '<div class="checkbox-filter pull-left"><label>'+
                            '<input type="checkbox" class="checkbox-info">'+
                            '<span class="cr"><i class="cr-icon fa fa-circle"></i></span>'+
                    '</label></div>'+
                    icon+ 
                    '<span class="elipsis label-filter">'+tradCategory[e]+'</span>'+
                '</button><br class="hidden-xs">';
        });
        $(".dropdown-types .dropdown-menu").html(str);
        searchInterface.bindFiltersEvents();
    },
    initCategoryPoi : function(){
        if( jsonHelper.notNull("modules.poi") ){
            var filters = modules.poi.categories; 
            directory.sectionFilter(filters);
            searchInterface.bindFiltersEvents();
            if(typeof searchObject.category != "undefined")
                activeFiltersInterface("category", searchObject.category, "poi");
        
        }
    },
    isForced: function(label){
        res = false;
        checkAnd=[];
        checkOr=[];
        if(label.indexOf(',') > -1)
            checkAnd= label.split(","); 
        else if(label.indexOf('|') > 0)
            checkOr= label.split("|");
        else
            checkOr=[label];
        if(typeof checkAnd != "undefined" && notEmpty(checkAnd)){
            $.each(checkAnd, function(e, v){
                if(typeof searchObject.forced != "undefined" && typeof searchObject.forced[v] != "undefined")
                    res = true;
                else{
                    res = false;return false;
                }
            });
        }else{
            $.each(checkOr, function(e, v){
                if(typeof searchObject.forced != "undefined" && typeof searchObject.forced[v] != "undefined"){
                    res=true;return false;
                }
            });
        }
        return res;
    }
};
function activeClassifiedFilters(){
    if(typeof searchObject.priceMin != "undefined" || typeof searchObject.priceMax != "undefined" || typeof searchObject.devise != "undefined")
       activePriceFilter();
    if(typeof searchObject.searchSType =="undefined")
        $(".dropdown-section, .dropdown-category, .dropdown-sources").hide();
    else
        initCategoryClassifieds(searchObject.searchSType);
    if(typeof searchObject.searchSType != "undefined")
        activeFiltersInterface("searchSType", searchObject.searchSType, "classifieds");
    if(typeof searchObject.section != "undefined")
        activeFiltersInterface("section", searchObject.section, "classifieds");
    if(typeof searchObject.category != "undefined")
        activeFiltersInterface("category", searchObject.category, "classifieds");
    if(typeof searchObject.subType != "undefined")
        activeFiltersInterface("subType", searchObject.subType, "classifieds");
}
function initCategoryClassifieds(typeKey){
    if( jsonHelper.notNull("modules."+typeKey) ){
        var filters = modules[typeKey].categories; 
        var what = { title : filters.label, 
                     icon : filters.icon }
        directory.sectionFilter( filters, ".classifiedFilters",what);
        searchInterface.bindFiltersEvents ();
        if(typeKey=="jobs") $(".dropdown-sources").show(); else $(".dropdown-sources").hide();
        if(typeKey=="ressources") $(".dropdown-price").hide(); else $(".dropdown-price").show();
    }/*else if (typeKey== "poi"){
        var filters = poiCategories; 
        var what = { title : "point of interest", 
                     icon : "marker" }
        directory.sectionFilter( filters, ".poiFilters",what);
        searchInterface.bindFiltersEvents ();
        $(".dropdown-sources").hide();
        $(".dropdown-price").hide();
    }*/
}
function activePriceFilter(){
    str="";
    if(typeof searchObject.priceMin != "undefined")
        str+=(typeof searchObject.priceMax != "undefined") ?  searchObject.priceMin+" - " : "Sup. à "+searchObject.priceMin;
    if(typeof searchObject.priceMax != "undefined")
        str+=(typeof searchObject.priceMin != "undefined") ?  searchObject.priceMax : "Inf. à "+searchObject.priceMax;
    if(typeof searchObject.devise != "undefined")
        str+= " "+searchObject.devise;
    $(".dropdown-price .dropdown-toggle").addClass("active").html(str+" <i class='fa fa-angle-down'></i>");
}
function activeTagsFilter(){
    countTags=0;
    labelTags="";
    $.each(searchObject.tags, function(e, v){
        if(v!=""){
            countTags++;
            if(countTags <=2)
                labelTags+= (labelTags!="") ? ", "+v : v;
        }else{
            searchObject.tags.splice(e,1);
        } 
    });
    labelEnd=(countTags>2) ? labelTags+ " +"+(countTags-2) : labelTags;
    if(labelEnd!="")
        $(".dropdown-tags .dropdown-toggle").addClass("active").html(labelEnd+" <i class='fa fa-angle-down'></i>");
    else
        $(".dropdown-tags .dropdown-toggle").removeClass("active").html(trad.themes+" <i class='fa fa-angle-down'></i>");
}     
function isCustom(labelFilter){
    // mylog.log("isCustom");
    res=false;
    if(typeof costum != "undefined" 
            && costum != null 
            && notNull(directory.appKeyParam)
            && typeof costum.app != "undefined" 
            && typeof costum.app[directory.appKeyParam] != "undefined" 
            && typeof costum.app[directory.appKeyParam].filters != "undefined"){
            if(notNull(labelFilter)){
                if(typeof costum.app[directory.appKeyParam].filters[labelFilter] != "undefined")
                    return true;
                else
                    return false;
            }else if(_.isObject(costum.app[directory.appKeyParam].filters))
                return true;
    }
    return res;
}
function showFiltersInterface(){
    if(isCustom())
        customFiltersInterface();
    else{
        if(searchObject.initType=="classifieds"){
            $(".dropdown-section, .dropdown-types, .dropdown-category, .dropdown-price").show();
            str="";
            $.each(["classifieds","ressources","jobs"], function(key,entry){
                    str+='<button class="btn btn-default col-md-12 col-sm-12 padding-10 bold elipsis btn-select-type-anc dropDesign" '+
                        'data-type-anc="'+tradCategory[modules[entry].categories.labelFront]+'" data-key="'+entry+'" '+
                        'data-type="classifieds">'+
                            '<div class="checkbox-filter pull-left"><label>'+
                                '<input type="checkbox" class="checkbox-info">'+
                                '<span class="cr"><i class="cr-icon fa fa-circle"></i></span>'+
                            '</label></div>'+
                            '<i class="fa fa-'+modules[entry].categories.icon+'"></i> '+
                            tradCategory[modules[entry].categories.labelFront]+
                    '</button>';
            });
            $(".dropdown-types .dropdown-menu").html(str);
            searchInterface.bindFiltersEvents();
        }else if(searchObject.initType=="events"){
            $(".dropdown-section, .dropdown-category, .dropdown-sources, .dropdown-price").hide();
            $(".dropdown-tags, .dropdown-types").show();
        }else if(searchObject.initType=="all"){
            $(".dropdown-section, .dropdown-sources, .dropdown-price").hide();
            $(".dropdown-tags, .dropdown-types").show();
        }else if(searchObject.initType=="news"){
            str="";
            $.each([{"key":"all", "label":"all", "icon":"newspaper-o"},{"key":"news", "label":"onlynews", "icon":"rss"},{"key":"activityStream","label":"activityCommunecter", "icon":"map-marker"},{"key":"surveys", "label":"surveys", "icon":"gavel"}], function(key,entry){
                    str+='<div class="col-xs-12 no-padding">'+
                            '<button class="btn btn-default bold elipsis btn-news-type-filters col-xs-12 dropDesign" '+
                            'data-type-anc="'+entry.key+'" data-key="'+entry.key+'" data-label="'+entry.label+'" '+
                            'data-type="news">'+
                                '<div class="checkbox-filter pull-left"><label>'+
                                    '<input type="checkbox" class="checkbox-info">'+
                                    '<span class="cr"><i class="cr-icon fa fa-circle"></i></span>'+
                                '</label></div>'+
                                '<i class="fa fa-'+entry.icon+'"></i> '+
                                tradCategory[entry.label]+
                            '</button>'+
                        '</div>';
                //});
            });
            $(".dropdown-types .dropdown-menu").html(str);
            $(".dropdown-tags, .dropdown-types").show();
            $(".dropdown-section, .dropdown-category, .dropdown-sources, .dropdown-price").hide();
        }
    }
}
function customFiltersInterface(){
    // mylog.log("customFiltersInterface");
    show={
        all:["tags", "types", "category", "community"],
        events:["tags", "types"],
        news:["tags", "types"],
        classifieds:["tags", "types", "section", "category", "price", "source"]};
    delete searchObject.forced;
    if(notNull(directory.appKeyParam) && typeof costum.app[directory.appKeyParam].filters != "undefined"){
        notShow=[];
        $.each(costum.app[directory.appKeyParam].filters, function(e, v){
        //    if(v.length > 1){
//                costum.categories=new Object;
  //            costum.categories=v;
                if(e=="types"){
                    if(typeof categoriesFilters != "undefined"){
                        $.each(categoriesFilters, function(i, content){
                            if($.inArray(i, ["NGO", "LocalBusiness", "Group", "GovernmentOrganization"]) >= 0){
                                if( $.inArray("organizations", v) < 0) delete categoriesFilters[i];                          
                            }else if($.inArray(i, v) < 0)
                                delete categoriesFilters[i];
                        });
                    }
                }

                keyfilter=e;
                if(e=="types" && $.inArray(searchObject.initType,["news", "all"]) < 0){
                    keyfilter="searchSType";
                }
                if(typeof searchObject.forced == "undefined") searchObject.forced=new Object;
                if(typeof v == "array" && v.length==1){
                    notShow.push(e);
                    searchObject.forced[keyfilter]=v[0];
                }
                else if(typeof v== "object" && v.length==1){                    
                    notShow.push(e);
                    searchObject.forced[keyfilter]=v;
                }
                // else if(typeof v== "object"){                    
                //     notShow.push(e);
                //     searchObject.forced[keyfilter]=v;
                //     searchObject[keyfilter]=v;
                // }
                else if(typeof v== "string"){
                    notShow.push(e);
                    searchObject.forced[keyfilter]=v;
                    searchObject[keyfilter]=v;
                }
                else if(typeof v== "boolean"){
                    notShow.push(e);
                    searchObject.forced[keyfilter]=v;
                    searchObject[keyfilter]=v;
                } else if(e=="community" ){
                    notShow.push(e);
                    searchObject.forced[keyfilter]=v;
                    searchObject[keyfilter]=v;
                }
                //searchObject[keyfilter]=(searchObject.initType=="news") ? [v[0]] : v[0];

               // delete show[searchObject.initType][e];
                //if(v.length)
                //$("#filters-nav-list .dropdown-"+e).remove();
            //}
        });
    }
    menuToShow="";
    $.each(show[searchObject.initType], function(e,v){
        if($.inArray(v, notShow)<0)
            menuToShow+=(menuToShow != "") ? ", .dropdown-"+v : ".dropdown-"+v;
    });
    $(menuToShow).show();
}
function activeFiltersInterface(filter,value){
    if(filter=="section"){
        $(".dropdown-section .dropdown-toggle").addClass("active").html(tradCategory[value]+" <i class='fa fa-angle-down'></i>");
        $(".btn-select-section[data-key='"+value+"']").addClass("active");
    }
    if(filter=="searchSType"){
        labelInit=(searchObject.initType=="classifieds") ? tradCategory[modules[value].categories.labelFront] :tradCategory[value];
        $(".dropdown-types .dropdown-toggle").addClass("active").html(labelInit+" <i class='fa fa-angle-down'></i>");
        if(searchObject.initType=="events")
            $(".btn-directory-type[data-type-event='"+value+"']").addClass("active");
        else{
            $(".dropdown-section, .dropdown-category").show();
            $(".btn-select-type-anc[data-key='"+value+"']").addClass("active");
        }
    }
    if(filter=="tags"){
        $('#tagsFilterInput, #tagsFilterInput-xs').val(searchObject.tags).trigger("change");
        countTags=0;
        labelTags="";
        $.each(searchObject.tags, function(e, v){
            countTags++;
            labelTags+= (labelTags!="") ? ", "+v : v; 
        });
        activeTagsFilter();
    }
    if(filter=="category"){
        $(".dropdown-category .dropdown-toggle").addClass("active").html(tradCategory[value]+" <i class='fa fa-angle-down'></i>");
        $(".btn-select-category[data-keycat='"+value+"']").addClass("active");
        $(".keycat[data-categ='"+value+"']").removeClass("hidden");
    }
    if(filter=="subType"){
        $(".dropdown-category .dropdown-toggle").addClass("active").html(tradCategory[searchObject.category]+" | "+ tradCategory[value]+" <i class='fa fa-angle-down'></i>");
        $(".keycat[data-keycat='"+value+"'][data-categ='"+searchObject.category+"']").addClass("active");
    }
    if(filter=="types"){
        $(".dropdown-types .dropdown-toggle").addClass("active").html(tradCategory[value]+" <i class='fa fa-angle-down'></i>");
        $(".btn-directory-type").removeClass("active");
        $(".btn-directory-type[data-type='"+value+"']").addClass("active");
    }
    if(filter=="priceMin" || filter=="priceMax"){
        $("#section-price #"+filter).val(value);
    }
    if(filter=="devise")
        $('#section-price #devise option[value="'+value+'"]').attr("selected",true)
    if(filter=="nbPage"){
        searchObject.nbPage=(Number(value)-1);
        searchObject.indexMin=searchObject.indexStep*searchObject.nbPage;

        if(searchObject.initType != "all" || (typeof searchObject.types != "undefined" && searchObject.types.length == 1 ))
            pageCount=true;
    }
}
var searchAllEngine = {
    searchInProgress : false,
    injectData : {},
    allResults : {},
    searchCount: {},
    initInjectData: function(){
        searchAllEngine.injectData={
            NGO : 0,
            Group : 0,
            LocalBusiness : 0,
            GovernmentOrganization:0,
            projects : 0,
            events : 0,
            citoyens : 0,
            classifieds : 0,
            poi : 0,
            news : 0,
            places : 0,
            ressources : 0,
            bookmarks : 0,
            proposals : 0
            //proposals : 0
        };
    },
    initRanges: function(){
        searchInterface.setSearchTypes("all");
        searchObject.ranges={
            NGO : { indexMin : 0, indexMax : 30, waiting : 30 },
            Group : { indexMin : 0, indexMax : 30, waiting : 30 },
            LocalBusiness : { indexMin : 0, indexMax : 30, waiting : 30 },
            GovernmentOrganization:{ indexMin : 0, indexMax : 30, waiting : 30 },
            projects : { indexMin : 0, indexMax : 30, waiting : 30 },
            events : { indexMin : 0, indexMax : 30, waiting : 30 },
            citoyens : { indexMin : 0, indexMax : 30, waiting : 30 },
            classifieds : { indexMin : 0, indexMax : 30, waiting : 30 },
            poi : { indexMin : 0, indexMax : 30, waiting : 30 },
            news : { indexMin : 0, indexMax : 30, waiting : 30 },
            places : { indexMin : 0, indexMax : 30, waiting : 30 },
            ressources : { indexMin : 0, indexMax : 30, waiting : 30},
            cities : { indexMin : 0, indexMax : 30, waiting : 30 },
            bookmarks : {indexMin : 0, indexMax : 30, waiting: 30},
            proposals : { indexMin : 0, indexMax : 30, waiting : 30 }
        };
        $.each(searchObject.types, function(e, v){
            entry=e;
            if(searchObject.ranges[v] =="undefined"){
                delete searchObject.ranges[v];
            }
        });
        if(searchObject.ranges.length==1){
                searchObject.nbPage=0;
                pageCount=true;
                searchObject.count=true;
                delete searchObject.ranges;
        }
    },
    initSearch: function (){
         mylog.log("searchAllEngine.initSearch");
        //Search on all
        searchAllEngine.initRanges();
        searchAllEngine.allResults={};
        if(typeof searchObject.nbPage != "undefined") delete searchObject.nbPage;
        pageCount=false;
        scrollEnd=false;
        $(window).bind("scroll",function(){ 
            mylog.log("searchAllEngine.initSearch scroll", loadingData, scrollEnd, isMapEnd, typeof searchObject.ranges ); 
            // Dans le scroll searchObject.ranges est là pour vérifier qu'on est bien sur un multi-type et non un seul type
            if(!loadingData && !scrollEnd && !isMapEnd  && searchObject.types.length > 1 ){
                var heightWindow = $("#search-content").height();//$("html").height();// - $("body").height();
                if( $(this).scrollTop() >= (heightWindow-650)){
                    startSearch(10, 30, null);
                }
            }
        });
    },
    initSearchEvent: function (){
        mylog.log("searchAllEngine.initSearchEvent");
        if(typeof searchObject.nbPage != "undefined") delete searchObject.nbPage;
        pageCount=false;
        scrollEnd=false;
        agenda.scroll();
    },
    prepareAllSearch: function(data){
        mylog.log("searchAllEngine.prepareAllSearch", data, searchAllEngine);
        sorting=[];
        //searchObject.types=[];
        $i=0;
        resToShow={};
        searchAllEngine.initInjectData();
        $.each(data, function(e,v){
            searchAllEngine.allResults[e]=v;
        });
        $.each(searchAllEngine.allResults, function(e,v ){
            // typeKey=(v.type=="organizations") ? v.typeOrga : v.type;
            // if (searchObject.types.indexOf(typeKey) == -1)
            //     searchObject.types.push(typeKey);
            sorting.push(v.sorting);

            //mylog.log("searchAllEngine.prepareAllSearch searchObject.types", searchObject.types);
        });
        sorting.sort().reverse();
        sorting=sorting.splice(0,30);
        $.each(sorting, function(e, v){
            $.each(searchAllEngine.allResults, function(key, value){
              if(v==value.sorting){
                resToShow[key]=value;
                typeKey=(value.type=="organizations") ? value.typeOrga : value.type;
                searchAllEngine.injectData[typeKey]++;
                delete searchAllEngine.allResults[key];
                $i++;
              }
            });
        });
        $.each(searchAllEngine.injectData, function (type, v){ 
            if(v==0)
              searchInterface.removeSearchType(type);
            else{
              searchObject.ranges[type].indexMin=searchObject.ranges[type].indexMax;
              searchObject.ranges[type].indexMax=searchObject.ranges[type].indexMin+v;
            }
          });
        return resToShow;
    }
};
function spinSearchAddon(bool){
    removeClass= (bool) ? "fa-arrow-circle-right" : "fa-spin fa-circle-o-notch";
    addClass= (bool) ? "fa-spin fa-circle-o-notch" : "fa-arrow-circle-right";
    $(".main-search-bar-addon, .second-search-bar-addon").find("i").removeClass(removeClass).addClass(addClass);
}
function interopSearch(keyS, nameS){
    mylog.log("interopSearch");
    if(keyS == "co"){
        delete searchObject.source;
        //initCountType();
        pageCount=true;
        searchObject.count=true;
        searchObject.nbPage=0;
        $(".dropdown-sources .dropdown-toggle").removeClass("active").html(trad.datasource+" <i class='fa fa-angle-down'></i>");
        $(".dropdown-section, .dropdown-price").show();
        $(".btn-select-category").show();
        startSearch(0, searchObject.indexStep, searchCallback);
    }else{
        $(".dropdown-section, .dropdown-price").hide();
        $(".btn-select-category").hide();
        $(".keycat-joboffer").removeClass("hidden");
        //searchObject.page=0;
        
        searchObject.source = keyS;
        if( typeof interop == "undefined" ){
             mylog.log("interopSearch 1");
            lazyLoad( modules.interop.assets+'/js/interop.js', null , function(data){
                lazyLoad( modules.interop.assets+'/js/init.js', null, function(data){
                    nameS = interopObj[keyS].name ;
                    $(".dropdown-sources  .dropdown-toggle").addClass("active").html(nameS+" <i class='fa fa-angle-down'></i>");
                    interopObj[keyS].startSearch(searchObject.indexMin, searchObject.indexStep);
                } ); 
            });
        } else {
            nameS = interopObj[keyS].name ;
            $(".dropdown-sources  .dropdown-toggle").addClass("active").html(nameS+" <i class='fa fa-angle-down'></i>");
            interopObj[keyS].startSearch(searchObject.indexMin, searchObject.indexStep);
        }
    }
}
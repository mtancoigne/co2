/* eslint-disable no-global-assign */
/* global $, moment, toastr, bootbox, baseUrl, moduleId, userId, assetPath, modules, domainName, myContacts, mentionsInit, collection, parentModuleUrl, trad, tradCategory, tradDynForm, smallMenu, dataHelper, typeObj, themeParams, onchangeClick, directoryViewMode, urlCtrl, links, contextData, dyFInputs, dyFObj, uiCoop, agenda, STARTDATE, showMap, costum, mapCO, isMapEnd, pageCount, spinSearchAddon, searchObject, searchAllEngine, searchInterface, interopSearch, isCustom, co, coInterface, mylog, notNull, notEmpty, checkAndCutLongString, linkify, escapeHtml, updateContact, personCOLLECTION, getObjectId, networkJson, getTypeInteropData, slugify, addslashes, inArray, myMultiTags, myScopes, mainLanguage, toggle */

/*
searchObject, searchAllEngine, searchInterface, interopSearch, isCustom modules/co2/assets/js/default/search.js
co, coInterface, mylog, linkify modules/co2/assets/js/co.js
mylog, notNull, inArray, toggle, checkAndCutLongString pixelhumain/ph/js/api.js
costum pixelhumain/ph/themes/CO2/views/layouts/initJs.php && modules/co2/views/custom/init.php
trad, tradCategory, tradDynForm modules/co2/views/translation/trad.php
mapCO pixelhumain/ph/themes/CO2/views/layouts/mainSearch.php
isMapEnd pixelhumain/ph/themes/CO2/views/layouts/initJs.php
baseUrl, moduleId pixelhumain/ph/themes/CO2/views/layouts/initJs.php && ...
agenda modules/co2/assets/js/default/calendar.js
themeParams, onchangeClick pixelhumain/ph/themes/CO2/views/layouts/initJs.php
showMap modules/co2/assets/js/default/index.js
spinSearchAddon modules/co2/assets/js/default/globalsearch.js
pageCount modules/co2/views/app/search.php && ...
STARTDATE modules/co2/views/app/search.php && ...
links modules/co2/assets/js/links.js
contextData modules/co2/views/element/profilSocial.php && ...
dataHelper, getObjectId modules/co2/assets/js/dataHelpers.js
uiCoop modules/co2/assets/js/cooperation/uiCoop.js && modules/dda/assets/js/uiCoop.js
urlCtrl modules/co2/assets/js/co.js && code/pixelhumain/ph/themes/CO2/assets/js/coController.js ...
userId, directoryViewMode pixelhumain/ph/themes/CO2/views/layouts/initJs.php
smallMenu modules/co2/assets/js/co.js && modules/news/assets/js/init.js
notEmpty modules/co2/assets/js/co.js && pixelhumain/ph/js/api.js
dyFInputs, dyFObj pixelhumain/ph/plugins/jquery.dynForm.js
myContacts, parentModuleUrl, assetPath, modules, domainName, mainLanguage, myScopes pixelhumain/ph/themes/CO2/views/layouts/initJs.php
collection modules/co2/assets/js/co.js && ...
mentionsInit modules/co2/assets/js/co.js && ...
escapeHtml modules/co2/assets/js/co.js && modules/news/assets/js/init.js
typeObj pixelhumain/ph/themes/CO2/assets/js/typeObj.js
updateContact modules/co2/assets/js/default/editInPlace.js && modules/co2/views/pod/contactsList.php
personCOLLECTION modules/co2/views/default/directory.php && modules/co2/views/element/profilSocial.php && ...
networkJson pixelhumain/ph/themes/network/views/layouts/mainSearch.php
getTypeInteropData modules/co2/assets/js/interoperability/interoperability.js
slugify pixelhumain/ph/js/api.js && pixelhumain/ph/plugins/jquery.dynForm.js
myMultiTags pixelhumain/ph/themes/CO2/views/layouts/scopes/multi_tag.php && ...
*/

var indexStepInit = 30;
var indexStep = indexStepInit;
var currentIndexMin = 0;
var currentIndexMax = indexStep;
var scrollEnd = false;
var totalData = 0;

var timeout = null;
//WARNING searchType est utilisé dans search.js
var searchType = '';
var searchSType = '';
var pageEvent=false;
var loadingData = false;
var mapElements = new Array(); 
var favTypes = [];
var searchPage = 0;
var translate = {'organizations':'Organisations',
	'projects':'Projets',
	'events':'Événements',
	'people':'Citoyens',
	'followers':'Ils nous suivent'};

function startSearch(indexMin, indexMax, callBack, notUrl){
	searchAllEngine.searchInProgress = true;
	mylog.log('directory.js startSearch agenda.', indexMin, indexMax);
	mylog.log('directory.js startSearch searchObject.types agenda.', searchObject.types);
	if(typeof searchObject.source != 'undefined'){
		interopSearch(searchObject.source, searchObject.source);
	} else {
		if(searchObject.text.indexOf('co.') === 0 ){
			var searchT = searchObject.text.split('.');
			if( searchT[1] && typeof co[ searchT[1] ] == 'function' ){
				co[ searchT[1] ](searchObject.text);
				return;
			} else {
				co.mands();
			}
		}
		if(location.hash.indexOf('#live') < 0){
			//  startNewsSearch(true);
			//else{

			mylog.log('directory.js startSearch ', typeof callBack, callBack, loadingData);

			if(loadingData) return;
			loadingData = true;
			//scrollH= 0;
			//MAPREMOVE
			//showIsLoading(true);

			mylog.log('directory.js startSearch', searchObject.indexMin, indexMax, searchObject.indexStep, searchObject.types);
			searchObject.indexMin = (typeof indexMin == 'undefined') ? 0 : indexMin;
			autoCompleteSearch(indexMin, indexMax, callBack, notUrl);
		}
	}
    
}

// eslint-disable-next-line no-unused-vars
function initCountType(){
	mylog.log('initCountType ', searchObject.initType);
	if(searchObject.initType=='all'){
		if(isCustom('types') && notNull(directory.appKeyParam)){
			searchObject.countType = [];
			$.each(costum.app[directory.appKeyParam].filters.types, function(e, v){
				if(v=='organizations')
					searchObject.countType=searchObject.countType.concat('NGO', 'Group', 'GovernmentOrganization', 'LocalBusiness');
				else  
					searchObject.countType.push(v);
			});
		}
		else{
			searchObject.countType=['NGO', 'Group', 'GovernmentOrganization', 'LocalBusiness', 'citoyens', 'projects', 'events', 'poi',  'classifieds'];
		}
	}
	//else if(searchObject.initType=="ressources") searchObject.countType=["ressources"];
	else if(searchObject.initType=='classifieds') searchObject.countType=['classifieds'];
	else if(searchObject.initType=='events') searchObject.countType=['events'];
	else if(searchObject.initType=='proposals') searchObject.countType=['proposals'];
}

// eslint-disable-next-line no-unused-vars
function removeSearchType(type){
	var index = searchObject.types.indexOf(type);
	if (index > -1 && searchObject.types.length > 1) {
		searchObject.types.splice(index, 1);
		$('.search_'+type).removeClass('active'); //fa-check-circle-o");
		//$(".search_"+type).addClass("fa-circle-o");
	}
}

function autoCompleteSearch(indexMin, indexMax, callBack, notUrl){
	mylog.log("START -------- autoCompleteSearch agenda. ", typeof callBack, searchObject.types);
	var data=searchInterface.constructObjectAndUrl(notUrl);    
	mylog.log('autoCompleteSearch! agenda. searchObject.types', searchObject.types);
  
	loadingData = true;

	var str = '<i class=\'fa fa-circle-o-notch fa-spin\'></i>';
	$('.btn-start-search').html(str);
	$('.btn-start-search').addClass('bg-azure');
	$('.btn-start-search').removeClass('bg-dark');

	if((indexMin==0 || (typeof pageEvent != 'undefined' && pageEvent)) && directory.appKeyParam != '#agenda'  ){
		if(directory.scrollTop === true)
			coInterface.simpleScroll(0, 400);  
		coInterface.showLoader('#dropdown_search',trad.currentlyresearching);
		mapCO.showLoader();

	}else if(indexMin>0)
		coInterface.showLoader('#btnShowMoreResult',trad.currentlyresearching);
	directory.scrollTop = true;
	if(isMapEnd)
		coInterface.showLoader('#map-loading-data', trad.currentlyloading);

	if(typeof data.startDate != 'undefined' && data.startDate != null){
		data.startDateUTC = moment(data.startDate*1000).local().format();
	}

	mylog.log("START -------- autoCompleteSearch! agenda. beforeAjax ", data);
	$.ajax({
		type: 'POST',
		url: baseUrl+'/' + moduleId + '/search/globalautocomplete',
		data: data,
		dataType: 'json',
		error: function (data){
			mylog.log('>>> error autocomplete search'); 
			mylog.dir(data);   
			$('#dropdown_search').html(data.responseText);  
			//signal que le chargement est terminé
			loadingData = false;     
		},
		success: function(data){
			mylog.log('>>> success autocomplete search agenda. !!!! ', data);
			if(!data){ 
				toastr.error(data.content); 
			} else {
				//Get results object
				var results=data.results;
				//Get count object
				if(typeof data.count != 'undefined')
					searchAllEngine.searchCount=data.count;


				if((searchObject.indexMin==0 && !pageEvent) || (typeof searchObject.count != "undefined" && searchObject.count)){
					//Prepare footer and header of directory 
					mylog.log('autoCompleteSearch headerSearchContainer');

					$('.headerSearchContainer').html( directory.headerHtml(searchObject.indexMin) );
					$('.footerSearchContainer').html( directory.footerHtml() );
				}
				str = '';

				if( typeof agenda != 'undefined' && 
					searchObject.types.length == 1 && 
					$.inArray( 'events', searchObject.types) >= 0 &&
					(   results.length > 0 ||
					( results.length == 0 && agenda.noResult == false) ) ){
					mylog.log('agenda. autoCompleteSearch moment', moment().get('date'), agenda.dayCount);
					var startMoment = agenda.getStartMoment(agenda.dayCount);
					mylog.log('agenda. autoCompleteSearch startMoment', startMoment);
					str = agenda.getDateHtml(startMoment);
				}

				if(typeof searchObject.ranges != 'undefined' && (typeof searchObject.indexStep == 'undefined' || searchObject.indexStep > 0))
					results=searchAllEngine.prepareAllSearch(results);
				//Add correct number to type filters
				if(searchObject.count)
					refreshCountBadge();

				//parcours la liste des résultats de la recherche
				str += directory.showResultsDirectoryHtml(results);
				if(	( notNull(directory.appKeyParam) && 
						typeof themeParams.pages[directory.appKeyParam] != 'undefined' && 
						typeof themeParams.pages[directory.appKeyParam].showMap != 'undefined') || 
					location.hash.indexOf('onMap=true')>=1 )
					showMap(true);


				if(str == '') {
					if( typeof agenda != 'undefined' &&
						typeof agenda.noResult != 'undefined' && 
						agenda.noResult === true &&
						searchObject.types.length == 1 && 
						$.inArray( 'events', searchObject.types) >= 0 ){
						// loadingData =false;
						// agenda.addDay();
					} else {
						$.unblockUI();
						$('.btn-start-search').html('<i class=\'fa fa-refresh\'></i>'); 
						str=directory.endOfResult(true);
						if(indexMin == 0){
							$('#dropdown_search').html(str);
						}else{
							$(".search-loader").remove();
							$("#dropdown_search .processingLoader").remove();
							$("#dropdown_search").append(str);
						}
					}
				} else {
					$("#dropdown_search .processingLoader").remove();     

					//CHARGEMENT DE TYPE SCROLL => ALL ELEMENTS OR MORE THAN ONE TYPE 
					//si on n'est pas sur une première recherche (chargement de la suite des résultat)
					var typeElement;
					if(indexMin > 0 && (typeof pageEvent == 'undefined' || !pageEvent)){
						//on affiche le résultat à l'écran
						$('#dropdown_search').append(str);

						if($('.search-loader').length) $('.search-loader').remove();
						if($('.pageTable').html()=='' && (searchObject.initType!= 'all' || searchObject.types.length==1)){
							typeElement=(searchObject.types=='persons') ? 'citoyens' : searchObject.types;
							initPageTable(searchAllEngine.searchCount[typeElement]);
						}
					}else{
						//on affiche le résultat à l'écran
						$('#dropdown_search').html(str);

						if(searchObject.initType == "all" && Object.keys(results).length < searchObject.indexStep){
							str=directory.endOfResult();   
							$('#dropdown_search').append(str);
						}

						if(directory.appKeyParam == '#agenda'){
							if(searchObject.text != '')
								$('#search-content .calendar').hide(700);
							else if(!$('#search-content .calendar').is(':visible'))
								$('#search-content .calendar').show(700);
						}

						if(searchObject.initType!= 'all' || searchObject.types.length==1){
							typeElement=(searchObject.types=='persons') ? 'citoyens' : searchObject.types;
							initPageTable(searchAllEngine.searchCount[typeElement]);
						}

						pageEvent=false;
					}
				}
				//remet l'icon "loupe" du bouton search
				$('.btn-start-search').html('<i class=\'fa fa-refresh\'></i>');
				//active les link lbh
				coInterface.bindLBHLinks();
				searchObject.count=false;
				$.unblockUI();
				$('#map-loading-data').html('');

				//initialise les boutons pour garder une entité dans Mon répertoire (boutons links)
				directory.bindBtnElement();
				coInterface.bindButtonOpenForm();
				//signal que le chargement est terminé
				loadingData = false;

				//quand la recherche est terminé, on remet la couleur normal du bouton search
				$('.btn-start-search').removeClass('bg-azure');
				$('#btn-my-co').removeClass('hidden');
			}


			if(mapElements.length==0 || indexMin == 0) 
				mapElements = results;
			else 
				$.extend(mapElements, results);
			if(location.hash == '#search' && searchObject.types.length > 1)
				directory.switcherViewer(mapElements);
			else
				directory.switcherViewer(results);
			//affiche les éléments sur la carte


			mapCO.clearMap();
			mapCO.addElts(mapElements);
			mapCO.map.invalidateSize();
			$('.btn-show-map').off().click(function(){
				if(typeof mapCO != 'undefined')
					showMap();
			});
			spinSearchAddon();
			searchAllEngine.searchInProgress = false;
			if(typeof callBack == 'function')
				callBack(results);
		}
	});


}


function initPageTable(number){
    numberPage=(number/searchObject.indexStep);
    $('.pageTable').show();
    $('.pageTable').pagination({
          items: numberPage,
          itemOnPage: 15,
          currentPage: (searchObject.nbPage+1),
          hrefTextPrefix:"?page=",
          cssStyle: 'light-theme',
          prevText: '<i class="fa fa-chevron-left"></i> ' + trad["previous"],
          nextText: trad["next"] + ' <i class="fa fa-chevron-right"></i>',
          onInit: function () {
              // fire first page loading
          },
          onPageClick: function (page, evt) {
              // some code
              //alert(page);
            pageCount=false;
            searchObject.nbPage=(page-1);
            scrollEnd=false;
            searchObject.indexMin=searchObject.indexStep*searchObject.nbPage;
            pageEvent=true;
            if(typeof searchObject.source != "undefined")
              interopSearch(searchObject.source, searchObject.source);
            else
              startSearch(searchObject.indexMin,searchObject.indexStep);
          }
      });
  }
  function refreshCountBadge(count){    
	$.each(searchAllEngine.searchCount, function(e,v){
		var type=(e=='citoyens')? 'persons' : e; 
		$('#count'+type).text(v);
	});

}
// eslint-disable-next-line no-unused-vars
function calculateAgendaWindow(nbMonth){

	//TODO refaire la fonction et rendre custumisable la date de debut et de fin 
	mylog.log('directory.js calculateAgendaWindow', nbMonth);
      
	if(typeof STARTDATE == 'undefined') return;
      

	var today = new Date();
	var todayMoment = moment().seconds(0).minute(0).hour(0);
	mylog.log('directory.js calculateAgendaWindow today', typeof today, today);
	mylog.log('directory.js calculateAgendaWindow today', typeof todayMoment, todayMoment, todayMoment.valueOf());
	today = new Date(today.setSeconds(0));
	today = new Date(today.setMinutes(0));
	today = new Date(today.setHours(0));
	//var todayUTC0 = moment(todayMoment.format()).utcOffset("+00:00");
	//STARTDATE = moment(todayMoment.format()).valueOf();
	STARTDATE = today.setDate(today.getDate());

	mylog.log('directory.js calculateAgendaWindow today2', typeof today, today);
	mylog.log('directory.js calculateAgendaWindow todayMoment', typeof todayMoment, todayMoment.format(), todayMoment.valueOf());
	/// mylog.log("directory.js calculateAgendaWindow todayUTC0", typeof todayUTC0, todayUTC0, todayUTC0.format(), todayUTC0.valueOf());
	mylog.log('directory.js calculateAgendaWindow STARTDATE', typeof STARTDATE, STARTDATE);
	mylog.log('directory.js calculateAgendaWindow end',STARTDATE);


	searchObject.startDate = Math.floor(STARTDATE / 1000);

	//searchObject.startDate =STARTDATE;

	// 

}

function initBtnAdmin(){ 
	$('.adminToolBar').each(function(){
		if($(this).children('button').length == 0){
			$(this).parent().find('.adminIconDirectory').remove();
			$(this).remove();
		}
	});
	if(directory.viewMode=='block'){
		$('.adminIconDirectory, .container-img-profil').mouseenter(function(){
			$(this).parent().find('.adminToolBar').show();
		});

		$('.adminToolBar').mouseleave(function(){
			$(this).hide();
		});
	}
	mylog.log('initBtnAdmin');

	$('.disconnectConnection').click(function(){
		var $this=$(this); 
		links.disconnect(	contextData.type,
			contextData.id, 
			$this.data('id'), 
			$this.data('type'), 
			$this.data('connection'),
			function() {
				$this.parents().eq($this.data('parent-hide')).fadeOut();   
			});
	});

	$('.acceptAsBtn').off().on('click',function () {
		links.validate(contextData.type, contextData.id, $(this).data('id'), $(this).data('type'), $(this).data('connect-validation'));
	});
}


function initUiCoopDirectory(){
    
	$.each($('.searchEntity.coopPanelHtml .descMD'), function(){
		$(this).html(dataHelper.markdownToHtml($(this).html()) );
	});

	$('.btn-openVoteDetail').off().click(function(){
		var coopId = $(this).data('coop-id');
		$('.all-coop-detail-votes'+coopId).toggleClass('hide');
		$('.all-coop-detail-desc'+coopId).toggleClass('hide');
	});

	$('.btn-coopfilter').off().click(function(){
		$('.coopFilter').addClass('hidden');
		$('.'+$(this).data('filter')+'Filter').removeClass('hidden');
		directory.sortSearch('.coopFilter:not(.hidden)');
	});

	$('.openCoopPanelHtml').off().click(function(){
		mylog.log('HERE .openCoopPanelHtml');
		var coopType = $(this).data('coop-type');
		var coopId = $(this).data('coop-id');
		var idParentRoom = $(this).data('coop-idparentroom');
		var parentId = $(this).data('coop-parentid');
		var parentType = $(this).data('coop-parenttype');
		var afterLoad = null;
		var hashT=location.hash.split('?');
		var getStatus=searchInterface.getUrlSearchParams();       
		var urlHistoric=hashT[0].substring(0)+'?preview='+coopType+'.'+coopId;    
		if(getStatus != '') urlHistoric+='&'+getStatus; 
		history.replaceState({}, null, urlHistoric);
		//onchangeClick=false;
		//location.hash="preview="+coopType+"."+coopId;//+"."+parentId+"."+parentType;
		uiCoop.prepPreview(coopType,coopId,idParentRoom,parentId,parentType,afterLoad);
		if( $(this).data('coop-section') ){
			var coopSection = $(this).data('coop-section');
			if(coopSection == 'amendments' ){
				afterLoad = function() { 
					uiCoop.showAmendement(true);
					/*if($("#form-amendement").hasClass("hidden"))
                    $("#form-amendement").removeClass("hidden");
                else 
                    $("#form-amendement").addClass("hidden");*/
				};
			} else if(coopSection == 'vote' ){
				mylog.log('.openCoopPanelHtml vote');
				afterLoad = function() { 
					setTimeout(function(){
						// $("#coop-container").animate({
						//       scrollTop: $("#podVote").offset().top
						//   }, 1000);
					}, 1000);
				};
			}
			else if(coopSection == 'comments' ){
				afterLoad = function() { 
					setTimeout(function(){
						$('#coop-container').animate({
							scrollTop: $('.btn-select-arg-comment').offset().top
						}, 1000);
					}, 1000);
                
				};
			}
		}
		coopType = coopType == 'actions' ? 'action' : coopType;
		coopType = coopType == 'proposals' ? 'proposal' : coopType;
		coopType = coopType == 'resolutions' ? 'resolution' : coopType;

      

		if(notNull(contextData) && contextData.id == parentId && contextData.type == parentType && typeof isOnepage == 'undefined' && idParentRoom != ''){
			toastr.info(trad['processing']);
			uiCoop.startUI();
			$('#modalCoop').modal('show');
			onchangeClick=false;
			if(coopType == 'rooms'){
				uiCoop.getCoopData(contextData.type, contextData.id, 'room', null, coopId);
			}else{
				setTimeout(function(){
					uiCoop.getCoopData(contextData.type, contextData.id, 'room', null, idParentRoom, 
						function(){
							toastr.info(trad['processing']);
							uiCoop.getCoopData(contextData.type, contextData.id, coopType, null, coopId);
						}, false);
				}, 1000);
			}
		}else{
			if(coopType == 'rooms'){
				var hash = '#page.type.' + parentType + '.id.' + parentId + 
                '.view.coop.room.' + idParentRoom + '.'+coopType+'.' + coopId;
				urlCtrl.loadByHash(hash);
			}else{
				uiCoop.getCoopDataPreview(coopType, coopId, afterLoad);
			}
		}
	});


	$('.timeline-panel .btn-send-vote').off().click(function(){
		var idParentProposal = $(this).data('idparentproposal');
		var voteValue = $(this).data('vote-value');
		var idParentRoom = $(this).data('idparentroom');
		mylog.log('send vote',voteValue);
		uiCoop.sendVote('proposal', idParentProposal, voteValue, idParentRoom, null, true);
	});
    
}

// eslint-disable-next-line no-unused-vars
function setSearchValue(value){
	$('#searchBarText').val(value);
	startSearch(0, indexStepInit);
}

  
  
// eslint-disable-next-line no-unused-vars
function searchCallback(data) { 
	mylog.log('directory.js agenda. searchCallback', data);
	directory.elemClass = '.searchEntityContainer ';
	// directory.filterTags(true);
	//$(".btn-tag").off().on("click",function(){ directory.toggleEmptyParentSection(null,"."+$(this).data("tag-value"), directory.elemClass, 1)});
	$('#searchBarTextJS').off().on('keyup',function() { 
		directory.search ( null, $(this).val() );
	});

	if(typeof agenda != "undefined" && agenda.finishSearch == false){
		if(data.length > 0)
			agenda.nbSearch = 0;
		else
			agenda.nbSearch++;
		mylog.log("directory.js agenda. searchCallback", agenda.nbSearch, agenda.limitSearch, data.length);
		if( data.length == 0 && agenda.limitSearch <= agenda.nbSearch){
			mylog.log("directory.js agenda. searchCallback aucun Resultat");

			agenda.searchNextEvent();
			
		} else {
			if( data.length == 0 &&
		        typeof agenda.noResult != "undefined" && 
		        agenda.noResult === true &&
		        searchObject.types.length == 1 && 
		        $.inArray( "events", searchObject.types) >= 0 ){
				mylog.log("directory.js agenda. noResult");
		        agenda.addDay();
		    } else if(
		        typeof agenda.countsLaunchSesearch != "undefined" && 
		        typeof agenda.nbLaunchSesearch != "undefined" && 
		        agenda.nbLaunchSesearch > 0 &&
		        agenda.nbLaunchSesearch > agenda.countsLaunchSesearch && 
		        $.inArray( "events", searchObject.types) >= 0 ){
		    	mylog.log("directory.js agenda nbLaunchSesearch");
		        agenda.countsLaunchSesearch++;
		        agenda.addDay();
		    }
		}		
	}
}

var directory = {

	elemClass : smallMenu.destination+' .searchEntityContainer ',
	path : 'div'+smallMenu.destination+' div.favSection',
	tagsT : [],
	scopesT :[],
	multiTagsT : [],
	multiScopesT :[],
	appKeyParam : null,
	scrollTop : true,
	viewMode: directoryViewMode,
	costum : ( typeof costum != 'undefined' && costum != null 
      && typeof costum.htmlConstruct != 'undefined'
      && typeof costum.htmlConstruct.directory != 'undefined') ? costum.htmlConstruct.directory : null, 
	colPos: 'left',
	dirLog : false,
	defaultPanelHtml : function(params){
		mylog.log('----------- defaultPanelHtml',params, params.type,params.name, params.url);
		var str = '';  
		str += '<div class=\'col-lg-3 col-md-4 col-sm-6 col-xs-12 searchEntityContainer '+params.type+' '+params.elTagsList+' '+params.elRolesList+' \'>';
		str +=    '<div class=\'searchEntity\' id=\'entity'+params.id+'\'>';

		if(params.itemType!='city' && (params.useMinSize))
			str += '<div class=\'imgHover\'>' + params.imgProfil + '</div>'+
                  '<div class=\'contentMin\'>';

		if(userId != null && userId != '' && params.id != userId){
			var isFollowed=false;
			if(typeof params.isFollowed != 'undefined' ) isFollowed=true;
			if(params.type!='cities' && params.type!='poi' && params.type!='surveys' && params.type!='actions' ){
				var tip = (params.type == 'events') ? 'Participer' : 'Suivre';
				str += '<a href=\'javascript:;\' class=\'btn btn-default btn-sm btn-add-to-directory bg-white tooltips followBtn\'' + 
                  'data-toggle="tooltip" data-placement="left" data-original-title="'+tip+'"'+
                  ' data-ownerlink=\'follow\' data-id=\''+params.id+'\' data-type=\''+params.type+'\' data-name=\''+params.name+'\' data-isFollowed=\''+isFollowed+'\'>'+
                      '<i class=\'fa fa-chain\'></i>'+ //fa-bookmark fa-rotate-270
                    '</a>';
			}
		}

		if(params.updated != null && !params.useMinSize){
			str += '<div class=\'dateUpdated\'><i class=\'fa fa-flash\'></i> <span class=\'hidden-xs\'>'+trad['actif']+' </span>' + params.updated + '</div>';
		}

		if(params.itemType!='city' && (typeof params.size == 'undefined' || params.size == 'max'))
			str += '<a href=\''+params.hash+'\' class=\'container-img-profil lbhp add2fav\'  data-modalshow=\''+params.id+'\'>' + params.imgProfil + '</a>';

		str += '<div class=\'padding-10 informations\'>';


		if(!params.useMinSize){
			if(params.startDate != null)
				str += '<div class=\'entityDate dateFrom bg-'+params.color+' transparent badge\'>' + params.startDate + '</div>';
			if(params.endDate != null)
				str += '<div  class=\'entityDate dateTo  bg-'+params.color+' transparent badge\'>' + params.endDate + '</div>';
            
			if(typeof params.size == 'undefined' || params.size == 'max'){
				str += '<div class=\'entityCenter no-padding\'>';
				str +=    '<a href=\''+params.hash+'\' class=\'lbhp add2fav\'  data-modalshow=\''+params.id+'\'>' + params.htmlIco + '</a>';
				str += '</div>';
			}
		}  
              
		str += '<div class=\'entityRight no-padding\'>';
                             
              
		if(notEmpty(params.parent) && notEmpty(params.parent.name))
			str += '<a href=\''+params.urlParent+'\' class=\'entityName text-'+params.parentColor+' lbhp add2fav   data-modalshow=\''+params.id+'\'text-light-weight margin-bottom-5\'>' +
                        '<i class=\'fa '+params.parentIcon+'\'></i> '
                        + params.parent.name + 
                      '</a>';

		var iconFaReply = notEmpty(params.parent) ? '<i class=\'fa fa-reply fa-rotate-180\'></i> ' : '';
		str += '<a  href=\''+params.hash+'\' class=\''+params.size+' entityName text-dark lbhp add2fav\'  data-modalshow=\''+params.id+'\'>'+
                      iconFaReply + params.name + 
                   '</a>';
            
		var thisLocality = '';
		if(params.fullLocality != '' && params.fullLocality != ' ')
			thisLocality = '<a href=\''+params.hash+'\' data-id=\'' + params.dataId + '\' class=\'entityLocality lbhp add2fav\'  data-modalshow=\''+params.id+'\'>'+
                                  '<i class=\'fa fa-home\'></i> ' + params.fullLocality + 
                                '</a>';
		else thisLocality = '<br>';
            
		if (params.itemType=='city'){
			var citykey = params.country + '_' + params.insee + '-' + params.cp;
			//$city["country"]."_".$city["insee"]."-".$city["cp"];
			
			thisLocality += '<button class=\'btn btn-sm btn-default item-globalscope-checker start-new-communexion\' '+
                                      'data-scope-value=\'' + citykey + '\' ' + 
                                      'data-scope-name=\'' + params.name + '\' ' + 
                                      'data-scope-type=\'city\' ' + 
                                      'data-insee-communexion=\'' + params.insee + '\' '+ 
                                      'data-name-communexion=\'' + params.name + '\' '+ 
                                      'data-cp-communexion=\'' + params.cp + '\' '+ 
                                      'data-region-communexion=\'' + params.regionName + '\' '+ 
                                      'data-country-communexion=\'' + params.country + '\' '+ 
                              '>'+
                                  'Communecter' + 
                              '</button>';

                              
		}

		//debat / actions
		if(notEmpty(params.parentRoom)){
			params.parentUrl = '';
			params.parentIco = '';
			if (params.type == 'surveys'){ params.parentUrl = '#survey.entries.id.'+params.survey; params.parentIco = 'archive'; }
			else if (params.type == 'actions') {params.parentUrl = '#rooms.actions.id.'+params.room;params.parentIco = 'cogs';}
			str += '<div class=\'entityDescription text-dark\'><i class=\'fa fa-' + params.parentIco + '\'></i><a href=\'' + params.parentUrl + '\' class=\'lbhp add2fav\'  data-modalshow=\''+params.id+'\'> ' + params.parentRoom.name + '</a></div>';
			if(notEmpty(params.parentRoom.parentObj)){
				var typeIcoParent = params.parentRoom.parentObj.typeSig;
				//mylog.log("typeIcoParent", params.parentRoom);

				var p = dyFInputs.get(typeIcoParent);
				params.icoParent = p.icon;
				params.colorParent = p.color;

				thisLocality = notEmpty(params.parentRoom) && notEmpty(params.parentRoom.parentObj) && 
                              notEmpty(params.parentRoom.parentObj.address) ? 
					params.parentRoom.parentObj.address : null;

				var postalCode = notEmpty(thisLocality) && notEmpty(thisLocality.postalCode) ? thisLocality.postalCode : '';
				var cityName = notEmpty(thisLocality) && notEmpty(thisLocality.addressLocality) ? thisLocality.addressLocality : '';

				thisLocality = postalCode + ' ' + cityName;
				if(thisLocality != ' ') thisLocality = ', <small> ' + thisLocality + '</small>';
				else thisLocality = '';

				var ctzCouncil = typeIcoParent=='city' ? 'Conseil citoyen de ' : '';
				str += '<div class=\'entityDescription text-'+params.colorParent+'\'> <i class=\'fa '+params.icoParent+'\'></i> <b>' + ctzCouncil + params.parentRoom.parentObj.name + '</b>' + thisLocality+ '</div>';
              

			}
		}else{
			str += thisLocality;
		}
            
		if (params.itemType == 'entry'){
			var vUp   = notEmpty(params.voteUpCount)       ? params.voteUpCount.toString()        : '0';
			var vMore = notEmpty(params.voteMoreInfoCount) ? params.voteMoreInfoCount.toString()  : '0';
			var vAbs  = notEmpty(params.voteAbstainCount)  ? params.voteAbstainCount.toString()   : '0';
			var vUn   = notEmpty(params.voteUnclearCount)  ? params.voteUnclearCount.toString()   : '0';
			var vDown = notEmpty(params.voteDownCount)     ? params.voteDownCount.toString()      : '0';
			str += '<div class=\'pull-left margin-bottom-10 no-padding\'>';
			str += '<span class=\'bg-green lbl-res-vote\'><i class=\'fa fa-thumbs-up\'></i> ' + vUp + '</span>';
			str += ' <span class=\'bg-blue lbl-res-vote\'><i class=\'fa fa-pencil\'></i> ' + vMore + '</span>';
			str += ' <span class=\'bg-dark lbl-res-vote\'><i class=\'fa fa-circle\'></i> ' + vAbs + '</span>';
			str += ' <span class=\'bg-purple lbl-res-vote\'><i class=\'fa fa-question-circle\'></i> ' + vUn + '</span>';
			str += ' <span class=\'bg-red lbl-res-vote\'><i class=\'fa fa-thumbs-down\'></i> ' + vDown + '</span>';
			str += '</div>';
		}

		str += '<div class=\'entityDescription\'>' + params.description + '</div>';
		str += '<div class=\'tagsContainer text-red\'>'+params.tagsLbl+'</div>';

		if(params.useMinSize){
			if(params.startDate != null)
				str += '<div class=\'entityDate dateFrom bg-'+params.color+' transparent badge\'>' + params.startDate + '</div>';
			if(params.endDate != null)
				str += '<div  class=\'entityDate dateTo  bg-'+params.color+' transparent badge\'>' + params.endDate + '</div>';
              
			if(typeof params.size == 'undefined' || params.size == 'max'){
				// console.log("params.size",params.size);
				str += '<div class=\'entityCenter no-padding\'>';
				str +=    '<a href=\''+params.hash+'\' class=\'lbhp add2fav\'  data-modalshow=\''+params.id+'\'>' + params.htmlIco + '</a>';
				str += '</div>';
			}
		}  

		if(params.type!='city' && (params.useMinSize))
			str += '</div>';
		str += '</div>';
		str += '</div>';
		str += '</div>';

		str += '</div>';
		return str;
	},
	// ********************************
	//  ELEMENT DIRECTORY PANEL
	// ********************************
	lightPanelHtml : function(params){
		mylog.log('directory.lightPanelHtml', params);
		var linkAction = ( $.inArray(params.type, ['poi','classifieds','ressources'])>=0 ) ? ' lbh-preview-element' : ' lbh';
    
		params.htmlIco ='<i class=\'fa '+ params.ico +' fa-2x letter-'+params.color+'\'></i>';
    
		var nameAuthor, authorType, authorId;
		if(params.targetIsAuthor){   
			nameAuthor=params.target.name;
			authorType=params.target.type;
			authorId=params.target.id;
		}else if(params.author){
			nameAuthor=params.author.name;  
			authorType='citoyens';
			authorId=params.author.id;
		} 


		if(typeof params.fullLocality != 'undefined' && params.fullLocality != '' && params.fullLocality != ' ')
			params.fullLocality = ' <small class=\'lbh letter-red margin-left-10\'>'+
                                  '<i class=\'fa fa-map-marker\'></i> '+params.fullLocality+
                                '</small> ';

		if(typeof params.scope != 'undefined'){
			if(typeof params.scope.localities != 'undefined'){
				$.each(params.scope.localities, function(key, scope){
					params.fullLocality += ' <small class=\'lbh letter-red margin-left-10\'>'+
                                    '<i class=\'fa fa-bullseye\'></i> '+(notNull(scope.name) ? scope.name : scope.postalCode)+
                                 '</small> ';
				});
			}
		}

		var grayscale = ( ( notNull(params.isInviting) && params.isInviting == true) ? 'grayscale' : '' ) ;
		var tipIsInviting = ( ( notNull(params.isInviting) && params.isInviting == true) ? trad['Wait for confirmation'] : '' ) ;
    
		mylog.log('lightPanel', params);
    
		var str = '';
		str += '<div class=\'col-xs-12 searchEntity entityLight no-padding '+params.elRolesList+' '+grayscale+' tooltips\'  data-toggle=\'tooltip\' data-placement=\'left\' data-original-title=\''+tipIsInviting+'\' '+
            'id=\'entity'+params.id+'\'>';
    
		str += '<div class=\'entityLeft pull-left text-right padding-10 hidden-xs\'>';
		if(typeof params.hash != 'undefined' && typeof params.imgProfil != 'undefined' /*&& location.hash != "#agenda"*/){
			str +=    '<a href=\''+params.hash+'\' class=\'container-img-profil '+linkAction+'\'>' + params.imgProfil + '</a>';
		}//else if(params.type == "events" && location.hash == "#agenda"){
		// str +=    "<a href='"+params.hash+"' class='container-img-profil event "+linkAction+"'>" + params.imgMediumProfil + "</a>";
		//}
		str += '</div>';

		str += '<div class=\'entityCenter col-xs-11 col-sm-8 col-md-7 col-lg-6 no-padding\'>';
    
		if(typeof params.hash != 'undefined' && typeof params.htmlIco != 'undefined')
			str +=    '<a href=\''+params.hash+'\' class=\'margin-top-15 iconType '+linkAction+'\'>' + params.htmlIco + '</a>';
      
		if(typeof params.name != 'undefined' && params.name != ''){
			str += '<a href=\''+params.hash+'\' class=\'margin-top-10 letter-blue title '+linkAction+'\'>'+params.name+'</a>';


			if(typeof params.price != 'undefined' && typeof params.devise != 'undefined')
				str += ' <span class=\'letter-light bold margin-left-10\'>'+ 
                  '<i class=\'fa fa-money\'></i> ' +
                  params.price + ' ' + params.devise +
              '</span>';
      
			if(typeof params.fullLocality != 'undefined' && params.fullLocality != '')
				str += params.fullLocality;
		}

		if(typeof params.text != 'undefined' && typeof params.hash != 'undefined'){
			str += '<span class=\'letter-blue\'>Message de</span> <a href=\'#page.type.'+authorType+'.id.'+authorId+'\' class=\'lbh\'>'+nameAuthor+'</a>';
        
			if(typeof params.fullLocality != 'undefined' && params.fullLocality != '' && params.fullLocality != ' ')
				str += params.fullLocality;

			str += '<br><a href=\''+params.hash+'\' class=\'margin-top-10 lbh textNews\'><i>'+params.text+'</i></a>';
		}

		if(typeof params.hash != 'undefined'){
			var echoLabel= params.hash;
			str += '<br><a href=\''+params.hash+'\' class=\''+linkAction+' letter-green url elipsis\'>'+echoLabel+'</a>';
		}

		if(typeof params.url != 'undefined' && params.url != null && params.url != '')
			str += '<br><a href=\''+params.url+'\' class=\'lbh text-light url bold elipsis\'>'+params.url+'</a>';

		if(typeof params.section != 'undefined'){
			str += '<div class=\'entityType\'>' + tradCategory[params.section];

			if(typeof params.category != 'undefined') {
				var labelType = ( typeof tradCategory[params.category] != 'undefined' ) ? tradCategory[params.category] : params.category+' :: to Trad' ;
				str += ' > ' + labelType;
			}
			if(typeof params.subtype != 'undefined') {
				var subTypeLab = ( typeof tradCategory[params.subtype] != 'undefined' ) ? tradCategory[params.subtype] : params.subtype+' :: to Trad' ;
				str += ' > ' + subTypeLab;
			}
			str += '</div>';
		}

		if(params.type=='events'){
			var dateFormated = directory.getDateFormated(params);
			var countSubEvents = ( params.links && params.links.subEvents ) ? '<br/><i class=\'fa fa-calendar\'></i> '+Object.keys(params.links.subEvents).length+' '+trad['subevent-s']  : '' ;
			str += dateFormated+countSubEvents;
		} 

		if(typeof(params.statusLink)!='undefined'){
			if(typeof(params.statusLink.isAdmin)!='undefined' 
          && typeof(params.statusLink.isAdminPending)=='undefined' 
          && typeof(params.statusLink.isAdminInviting)=='undefined'
          && typeof(params.statusLink.toBeValidated)=='undefined'){
				str+='<br><span class=\'text-red\'>'+trad.administrator+'</span>';
			}
			if(typeof(params.statusLink.isAdminInviting)!='undefined'){
				str+='<br><span class=\'text-red\'>'+trad.invitingToAdmin+'</span>';
			}
			if(typeof(params.statusLink.toBeValidated)!='undefined' || typeof(params.statusLink.isAdminPending)!='undefined')
				str+='<br><span class=\'text-red\'>'+trad.waitingValidation+'</span>';
		}

		if(notNull(params.rolesLbl) && params.rolesLbl != '')
			str += '<br><span class=\'rolesContainer\'>'+params.rolesLbl+'</span>';
 
		if(typeof params.shortDescription != 'undefined' && params.shortDescription != '' && params.shortDescription != null)
			str += '<br><span class=\'description\'>'+params.shortDescription+'</span>';
		else if(typeof params.description != 'undefined' && params.description != '' && params.description != null){
			str += '<br><span class=\'description\'>'+
          ( (params.description.length > 140) ? params.description.substring(0,140)+'...' : params.description )+'</span>';
		}

		if(typeof params.tagsLbl != 'undefined')
			str += '<div class=\'tagsContainer\'>'+params.tagsLbl+'</div>';

		if(typeof params.counts != 'undefined'){
			$.each(params.counts, function (key, count){
				str +=  '<small class=\'pull-left lbh letter-light bg-transparent url elipsis bold countMembers margin-right-10\'>'+
                    '<i class=\'fa fa-link\'></i> '+ count + ' ' + trad[key] +
                  '</small>';
			});
		}

		if(typeof params.edit  != 'undefined' && notNull(params.edit))
			str += this.getAdminToolBar(params);

		if(userId != null && userId != '' && typeof params.id != 'undefined' && typeof params.type != 'undefined') 
			str+=directory.socialToolsHtml(params);

		str += '</div>';

		str += '</div>';

		return str;
	},
	// ********************************
	//  ELEMENT DIRECTORY PANEL
	// ********************************
	elementPanelHtml : function(params){

		//mylog.log('directory.elementPanelHtml',params.type,params.name,params.elTagsList, params);
		var str = '';

		var grayscale = ( ( notNull(params.isInviting) && params.isInviting == true) ? 'grayscale' : '' ) ;
		var tipIsInviting = ( ( notNull(params.isInviting) && params.isInviting == true) ? trad['Wait for confirmation'] : '' ) ;
		var classType=params.type;
		classType =(typeof params.category != 'undefined' && notNull(params.category)) ? ' '+params.category : '';
		if(params.type=='events') classType='';

		str += '<div class=\'col-lg-4 col-md-4 col-sm-6 col-xs-12 searchEntityContainer '+grayscale+' '+classType+' '+params.elTagsList+' '+params.elRolesList+' contain_'+params.type+'_'+params.id+'\'>';
		str +=    '<div class="searchEntity" id="entity'+params.id+'">';

		if(typeof params.edit  != 'undefined' && notNull(params.edit))
			str += this.getAdminToolBar(params);

		if( params.tobeactivated == true ){
			str += '<div class=\'dateUpdated\'><i class=\'fa fa-flash\'></i> <span class=\'hidden-xs\'>'+trad['Wait for confirmation']+' </span></div>';
		}else{
			var timeAction= trad.actif;

			var update = params.updated ? params.updated : null; 


			if (params.type != null && 
            params.type == 'projects' &&
            (  typeof params.properties != 'undefined' &&
               typeof params.properties.avancement != 'undefined') ) {

				if (params.properties.avancement == 'inprogress') {
					str += '<div class=\'dateUpdated\'>'+
                        '<span class=\'status\' style=\'margin-left:23px;\'>'+
                          '<i style=\'position:absolute;margin-left:-24px;font-size:18px;\' class=\'fa fa-play-circle iconesStatus\'></i>'+
                            '<p class=\'textStatus\' style=\'position:absolute;color:black;visibility:hidden\'> En cours</p>'+
                        '</span> <i class=\'fa fa-flash\'></i>'+
                        '<span class=\'hidden-xs\'>'+timeAction+
                        '</span>' + update + 
                      '</div>';
				}
				else if (params.properties.avancement == 'stopped') {
					str += '<div class=\'dateUpdated\'>'+
                        '<span class=\'status\' style=\'margin-left:23px;\'>'+
                          '<i style=\'position:absolute;margin-left:-24px;font-size:18px;\' class=\'fa fa-pause-circle iconesStatus\'></i>'+
                            '<p class=\'textStatus\' style=\'position:absolute;color:black;visibility:hidden\'> Stoppé</p>'+
                        '</span> <i class=\'fa fa-flash\'></i>'+
                        '<span class=\'hidden-xs\'>'+timeAction+
                        ' </span>' + update + 
                      '</div>';
				}
				else if (params.properties.avancement == 'archived') {
					str += '<div class=\'dateUpdated\'>'+
                        '<span class=\'status\' style=\'margin-left:23px;\'>'+
                          '<i style=\'position:absolute;margin-left:-24px;font-size:18px;\' class=\'fa fa-check-circle iconesStatus\'></i>'+
                            '<p class=\'textStatus\' style=\'position:absolute;color:black;visibility:hidden\'> Archivé</p>'+
                        '</span> <i class=\'fa fa-flash\'></i>'+
                        '<span class=\'hidden-xs\'>'+timeAction+
                        '</span>' + update + 
                      '</div>';
				}
				else if (params.properties.avancement == 'upcoming'){
					str += '<div class=\'dateUpdated\'>'+
                        '<span class=\'status\' style=\'margin-left:23px;\'>'+
                          '<i style=\'position:absolute;margin-left:-24px;font-size:18px;\' class=\'fa fa-lightbulb-o iconesStatus\'></i>'+
                            '<p class=\'textStatus\' style=\'position:absolute;color:black;visibility:hidden\'> A venir</p>'+
                        '</span> <i class=\'fa fa-flash\'></i>'+
                        '<span class=\'hidden-xs\'>'+timeAction+
                        '</span>' + update + 
                      '</div>';
				}
			}
			else{
				str += '<div class=\'dateUpdated\'> <i class=\'fa fa-flash\'></i> <span class=\'hidden-xs\'>'+timeAction+' </span>' + update + '</div>';
			}
		}
      
		var linkAction = ( $.inArray(params.type, ['poi','classifieds'])>=0 ) ? ' lbh-preview-element' : ' lbh';

		if( typeof directory.costum != 'undefined' &&
        directory.costum != null &&
        typeof directory.costum.preview != 'undefined' && 
        directory.costum.preview === true ){
			linkAction = ' lbh-preview-element';
		}

		if(typeof params.imgType !='undefined' && params.imgType=='banner'){
			str += '<a href=\''+params.hash+'\' class=\'container-img-banner add2fav '+linkAction+'>' + params.imgBanner + '</a>';
			str += '<div class=\'padding-10 informations tooltips\'  data-toggle=\'tooltip\' data-placement=\'top\' data-original-title=\''+tipIsInviting+'\'>';

			str += '<div class=\'entityRight banner no-padding\'>';

			if(typeof params.size == 'undefined' || params.size == undefined || params.size == 'max'){
				str += '<div class=\'entityCenter no-padding\'>';
				str +=    '<a href=\''+params.hash+'\' class=\'container-thumbnail-profil add2fav '+linkAction+'\'>' + params.imgProfil + '</a>';
				str +=    '<a href=\''+params.hash+'\' class=\'add2fav pull-right margin-top-15 '+linkAction+'\'>' + params.htmlIco + '</a>';
				str += '</div>';
			}
		}else{
			str += '<a href=\''+params.hash+'\' class=\'container-img-profil add2fav '+linkAction+'\'>' + params.imgMediumProfil + '</a>';
			str += '<div class=\'padding-10 informations tooltips\'  data-toggle=\'tooltip\' data-placement=\'top\' data-original-title=\''+tipIsInviting+'\'>';

			str += '<div class=\'entityRight profil no-padding\'>';

			if(typeof params.size == 'undefined' || params.size == undefined || params.size == 'max'){
				str += '<div class=\'entityCenter no-padding\'>';
				str +=    '<a href=\''+params.hash+'\' class=\'add2fav pull-right '+linkAction+'\'>' + params.htmlIco + '</a>';
				str += '</div>';
			}
		}


		var iconFaReply ='';
		str += '<a  href=\''+params.hash+'\' class=\''+params.size+' entityName bold text-dark add2fav '+linkAction+'\'>'+
					iconFaReply + params.name +
				'</a>';  
		
		if(typeof(params.statusLink)!='undefined'){
			if(typeof(params.statusLink.isAdmin)!='undefined'
         && typeof(params.statusLink.isAdminPending)=='undefined'
          && typeof(params.statusLink.isAdminInviting)=='undefined'
            && typeof(params.statusLink.toBeValidated)=='undefined')
				str+='<span class=\'text-red\'>'+trad.administrator+'</span>';
			if(typeof(params.statusLink.isAdminInviting)!='undefined'){
				str+='<span class=\'text-red\'>'+trad.invitingToAdmin+'</span>';
			}
			if(typeof(params.statusLink.toBeValidated)!='undefined' || typeof(params.statusLink.isAdminPending)!='undefined')
				str+='<span class=\'text-red\'>'+trad.waitingValidation+'</span>';
		}

		if(params.rolesLbl != '')
			str += '<div class=\'rolesContainer\'>'+params.rolesLbl+'</div>';
 

	

		var thisLocality = '';
		if(params.fullLocality != '' && params.fullLocality != ' ')
			thisLocality = '<a href=\''+params.hash+'\' data-id=\'' + params.dataId + '\'  class=\'entityLocality add2fav'+linkAction+'\'>'+
							'<i class=\'fa fa-home\'></i> ' + params.fullLocality + '</a>';
		else thisLocality = '';

		str += thisLocality;

		var devise = (typeof params.devise != 'undefined') ? params.devise : '';
		if(typeof params.price != 'undefined' && params.price != '')
			str += '<div class=\'entityPrice text-azure\'><i class=\'fa fa-money\'></i> ' + params.price + ' ' + devise + '</div>';
 
		if($.inArray(params.type, ['classifieds','ressources'])>=0 && typeof params.category != 'undefined'){
			str += '<div class=\'entityType col-xs-12 no-padding\'><span class=\'uppercase bold pull-left\'>' + tradCategory[params.section] + ' </span><span class=\'pull-left\'>';
			if(typeof params.category != 'undefined' && params.type != 'poi') str += ' > ' + tradCategory[params.category];
			if(typeof params.subtype != 'undefined') str += ' > ' + tradCategory[params.subtype];
			str += '</span></div>';
		}
		if(notEmpty(params.typeEvent))
			str += '<div class=\'entityType\'><span class=\'uppercase bold\'>' + tradCategory[params.typeEvent] + '</span></div>';  
    
		if(params.type=='events'){
			var dateFormated = directory.getDateFormated(params, true);
			var countSubEvents = ( params.links && params.links.subEvents ) ? '<br/><i class=\'fa fa-calendar\'></i> '+Object.keys(params.links.subEvents).length+' '+trad['subevent-s']  : '' ;
			str += dateFormated+countSubEvents;
		}
		str += '<div class=\'entityDescription\'>' + ( (params.shortDescription == null ) ? '' : params.shortDescription ) + '</div>';
   
		str += '<div class=\'tagsContainer text-red\'>'+params.tagsLbl+'</div>';
		if(typeof params.counts != 'undefined'){
			str+='<div class=\'col-xs-12 no-padding communityCounts\'>';
			$.each(params.counts, function (key, count){
				var iconLink=(key=='followers') ? 'link' : 'group';
				str +=  '<small class=\'pull-left lbh letter-light bg-transparent url elipsis bold countMembers margin-right-10\'>'+
                      '<i class=\'fa fa-'+iconLink+'\'></i> '+ count + ' ' + trad[key] +
                    '</small>';
			});
			str+='</div>';
		}

		if(userId != null && userId != '' && typeof params.id != 'undefined' && typeof params.type != 'undefined') 
			str+=directory.socialToolsHtml(params);  
		str += '</div>';
		str += '</div>';
		str += '</div>';
		str += '</div>';
		str += '</div>';
		return str;
	},
	socialToolsHtml: function(params){
		var str='<div class=\'col-xs-12 no-padding actionSocialBtn margin-bottom-5\'>';
		if(params.id != userId && $.inArray(params.type, ['events', 'organizations', 'citoyens','projects'])>=0){
			var tip = (params.type == 'events') ? trad['participate'] : trad['Follow'];
			var classBind='followBtn';
			var strLink, actionType, titleMember;
			if(notNull(myContacts) && typeof myContacts[params.type] != 'undefined' && typeof myContacts[params.type][params.id] != 'undefined'){
				if(typeof myContacts[params.type][params.id].isFollowed != 'undefined' || params.type=='events'){
					tip = (params.type == 'events') ? trad['alreadyAttendee'] : trad['alreadyFollow'];
					strLink='<i class=\'fa fa-unlink\'></i> '+tip;
					actionType='unfollow';
					classBind+=' text-green';
				}else{
					classBind='text-green disabled';
					actionType='';
					titleMember=(params.type=='organizations')?trad.alreadyMember : trad.alreadyContributor; 
					strLink='<i class=\'fa fa-user-circle\'></i> '+titleMember;
				}
			}else{
				strLink='<i class=\'fa fa-link fa-rotate-270\'></i> '+tip;
				actionType='follow';
			}
          
			//mylog.log("isFollowed", params.isFollowed, isFollowed);
			str += '<button class=\'pull-left btn btn-link no-padding margin-right-10 btn-element-panel-link '+classBind+'\'' + 
                  ' data-toggle="tooltip" data-placement="left" data-original-title="'+tip+'"'+
                  ' data-ownerlink=\''+actionType+'\' data-id=\''+params.id+'\' data-type=\''+params.type+'\''+
                  ' data-name=\''+params.name+'\'>'+ // data-isFollowed='"+isFollowed+"'
                  '<small>'+strLink+'</small>'+ 
                '</button>';
		}else if($.inArray(params.type, ['poi','classifieds'])>=0){
			var isInFavorite=collection.isFavorites(params.type, params.id);
			tip='<i class=\'fa fa-star-o\'></i> ';
			var classBtn='';
			var actionFav='onclick=\'collection.add2fav("'+params.type+'","'+params.id+'")\'';
			if(isInFavorite){
				if(isInFavorite != 'favorites')
					actionFav='onclick=\'collection.add2fav("'+params.type+'","'+params.id+'", "'+isInFavorite+'")\'';
				classBtn='letter-yellow-k';
				tip='<i class=\'fa fa-star\'></i> ';
			}
			tip+=trad.Favorites;
			str += '<button '+actionFav+' class=\'dirStar star_'+params.type+'_'+params.id+' btn btn-link no-padding margin-right-10 pull-left '+classBtn+'\'' +
                  ' data-id=\''+params.id+'\' data-type=\''+params.type+'\'>'+
                    '<small>'+tip+'</small>'+
                  '</button>';
          
		}
		if(params.type != 'citoyens')
			str += '<button id=\'btn-share-'+params.type+'\' class=\'pull-left btn no-padding  btn-link btn-share-panel\''+
                                      ' data-ownerlink=\'share\' data-id=\''+params.id+'\' data-type=\''+params.type+'\'>'+
                                      '<small><i class=\'fa fa-retweet\'></i> '+trad['share']+'</small></button> ';
		str += '</div>';
		return str;
	},

	//Boutons de partage


	socialBarHtml:function(params){
		var str='';
		if(notEmpty(params.socialBarConfig)&&notEmpty(params.socialBarConfig.btnList)){
			str='<div class=\'socialBar\'>';
			var btnSize=16;
			if(notEmpty(params.socialBarConfig.btnSize))
				btnSize=params.socialBarConfig.btnSize;
			for (var i = 0; i <params.socialBarConfig.btnList.length ; i++) {
				str+=directory.shareBtnHtml(params.socialBarConfig.btnList[i],params,btnSize);
			}
			str+='</div>';
		}
		return str;
	},

	shareBtnHtml: function(media,params,size){
		var url = baseUrl+'/co2/app/page/type/'+params.type+'/id/'+params.id;
		var str='';
		switch(media.type){
		case 'co':
			if(userId != null && userId != '')
				str='<img class="co3BtnShare btn-share" width="'+size+'" data-ownerlink="share" data-id="'+params.id+'"" data-type="'+params.type+'" src="'+parentModuleUrl+'/images/social/co-icon-64.png"/>';
			break;
		case 'facebook': 
			//str='<i class="fa fa-facebook-square" style="float:right;margin-left:0.5em;" onclick="window.open(\'https://www.facebook.com/sharer.php?u='+url+'\',\'_blank\')"></i>';
			str='<img class="co3BtnShare" width="'+size+'" src="'+parentModuleUrl+'/images/social/facebook-icon-64.png" onclick="window.open(\'https://www.facebook.com/sharer.php?u='+url+'\',\'_blank\')"/>';
			break;
		case 'twitter':
			//str='<i class="fa fa-twitter-square" style="float:right;margin-left:0.5em;" onclick="window.open(\'https://twitter.com/intent/tweet?url='+url+'\',\'_blank\')"></i>';
			str='<img class="co3BtnShare" width="'+size+'" src="'+parentModuleUrl+'/images/social/twitter-icon-64.png" onclick="window.open(\'https://twitter.com/intent/tweet?url='+url+'\',\'_blank\')"/>';
			break;
		case 'custom':
			str='<img class="co3BtnShare" width="'+size+'" src="'+media.img+'" onclick="window.open(\''+media.shareLink+url+'\',\'_blank\')"/>';
			break;
		default :
			mylog.log('<!-- Warning : invalid media (directory.shareBtnHtml) -->', media);
		}
		return str;
	},
    


	// ********************************
	// CALCULATE NEXT PREVIOUS 
	// ********************************
	findNextPrev : function  (hash) { 
		mylog.log('----------- findNextPrev', hash);
		var p = 0;
		var n = 0;
		var nid = 0;
		var pid = 0;
		var  found = false;
		var l = $( '.searchEntityContainer .container-img-profil' ).length;
		$.each( $( '.searchEntityContainer .container-img-profil' ), function(i,val){
            
			if( $(val).attr('href') == hash ){
				found = i;
                
				return false;
			}
		});
        
		var prevIx = (found == 0) ? l-1 : found-1;
		p =  $( $('.searchEntityContainer .container-img-profil' )[ prevIx ] ).attr('href');
		pid = urlCtrl.map(p).id;
        
		var nextIx = (found == l-1) ? 0 : found+1;
		n = $( $('.searchEntityContainer .container-img-profil' )[nextIx] ).attr('href');
		nid =  urlCtrl.map(n).id;
        
        
        

		return {
			prev : '<a href=\''+p+'\' data-modalshow=\''+pid+'\' class=\'lbhp text-dark\'><i class=\'fa fa-2x fa-angle-left\'></i> </a> ',
			next : '<a href=\''+n+'\' data-modalshow=\''+nid+'\' class=\'lbhp text-dark\'> <i class=\'fa fa-2x fa-angle-right\'></i></a>'
		};
	},
	// ********************************
	//  DIRECTORY PREVIEW PANEL
	// ********************************
	//TODO : ADD link to seller contact page
	previewedObj : null,
	preview : function(params,hash){

		mylog.log('----------- preview',params,params.name, hash);
		mylog.log('----------- preview id : ',params.id);
		directory.previewedObj = {
			hash : hash,
			params : params
		};
      

		var str = '';

      
		// ********************************
		// NEXT PREVIOUS 
		// ********************************
		str += '<div class=\'col-xs-12 col-sm-1 col-md-2 pull-left text-right\'></div>';

		// ********************************
		// RIGHT SECTION
		// ********************************
		str += '<div class=\'col-xs-12 margin-top-15\'>';

		// ********************************
		// LEFT SECTION
		// ********************************
		str +=  '<div class=\'col-xs-12 text-left\'>';
        
		var devise = (typeof params.devise != 'undefined') ? params.devise : '';
		var price = (typeof params.price != 'undefined' && params.price != '') ? 
			'<br/><i class=\'fa fa-money\'></i> ' + params.price + ' ' + devise : '';
		if(price != '')
			str += '<h4 class=\'text-azure\'>'+price+'</h4>';


		if(typeof params.category != 'undefined'){
			str += '<div class=\'entityType text-dark\'><span class=\'bold uppercase\'>' + tradCategory[ params.section ]+ '</span> > '+tradCategory[ params.category ];
			if(typeof params.subtype != 'undefined') str += ' > ' + tradCategory[ params.subtype ];
			str += '</div><hr>';
		}
		if(typeof params.typePoi != 'undefined'){
			str += '<div class=\'entityType text-dark\'><span class=\'bold uppercase\'>' + trad[params.type] + '</span> > '+tradCategory[params.typePoi];
                
			str += '</div><hr>';
		}

		var nav = directory.findNextPrev(hash);
		if(typeof params.name != 'undefined' && params.name != '')
			str += '<div class=\'bold text-black\' style=\'font-size:20px;\'>'+ 
                        '<div class=\'col-md-8 col-sm-8 col-xs-7 no-padding margin-top-10\'>'+params.name + '</div>';
		if( typeof hash != 'undefined' ){ 
			str +=  '<div class=\'col-md-4 col-sm-4 col-xs-5 no-padding\'>'+ 
                                    nav.next+
                                    nav.prev+
                                  '</div>';
		}
		str +=    '<br>'+
                     '</div>';


		if(typeof params.description != 'undefined' && params.description != '')
			str += '<div class=\'col-xs-12 no-padding pull-left\'><hr>' + params.description + '<hr></div>';

		if( typeof params.medias != 'undefined' && typeof params.medias[0].content.url ){
			str += '<div class=\'col-xs-12 bold text-black\' style=\'font-size:15px;\'>Média (urls, informations)</div>';
			$.each(params.medias, function (ix, mo) {  
				str += '<div class=\'col-xs-12\'><a href=\'' + mo.content.url + '\' target=\'_blank\'>' + mo.content.url + '</a></div>';
			});
			str += '<hr class=\'col-xs-12\'>';
		}


		if(typeof params.contactInfo != 'undefined' && params.contactInfo != ''){
			str += '<div class=\'entityType letter-green bold\' style=\'font-size:17px;\'><i class=\'fa fa-address-card\'></i> Contact : ' + params.contactInfo + '</div>';
			str += '<hr class=\'col-xs-12\'>';
		} else {
			str += '<div class=\'entityType letter-green bold\' style=\'font-size:17px;\'>'+
                      '<i class=\'fa fa-address-card\'></i> '+
                      '<a class=\'lbh btn btn-link btn-azure\' href=\'#page.type.citoyens.id.' + params.creator + '\'>'+
                        'Voir la page de l\'auteur'+
                      '</a>'+
                    '</div>';
			str += '<hr class=\'col-xs-12 no-padding\'>';
		}

		
		if(typeof params.fullLocality != 'undefined' && params.fullLocality != '' && params.fullLocality != ' ')
		{
			str += '<div class=\'col-xs-12 bold text-black\' style=\'font-size:15px;\'>Addresse : ';
			str += '<a href=\''+params.hash+'\' data-id=\'' + params.dataId + '\' class=\'entityLocality  lbhp add2fav letter-red\' data-modalshow=\''+params.id+'\'>'+
                              '<i class=\'fa fa-home\'></i> ' + params.fullLocality + 
                            '</a>';
			str += '</div>';
			str += '<hr class=\'col-xs-12\'>';
		
		}
		
          

       
		if(notEmpty(params.tagsLbl)){
			str += '<div class=\'col-xs-12 bold text-black\' style=\'font-size:15px;\'>Mots clés : '+params.tagsLbl+'</div>';
			str += '<hr class=\'col-xs-12\'>';
		}


		str += '</div>';

		if('undefined' != typeof params.images && params.images.length > 0)
			str+='<div id=\'container-element-accordeon\'></div>';


		str += '</div>';

		return str;
	},
	// ********************************
	// CLASSIFIED DIRECTORY PANEL
	// ********************************    
	newsPanelHtml : function(params){
		if(directory.dirLog) mylog.log('----------- newsPanelHtml',params);
		var str = '';  
		str += '<div class=\'col-lg-3 col-md-4 col-sm-6 col-xs-12 pull-left searchEntityContainer '+params.type+params.id+' '+params.type+' '+params.elTagsList+' \'>';
		str +=    '<div class=\'searchEntity\' id=\'entity'+params.id+'\'>';


		if(params.updated != null )
			str += '<div class=\'dateUpdated\'><i class=\'fa fa-flash\'></i> <span class=\'hidden-xs\'>'+trad.posted+' </span>' + 
                    params.updated + 
                  '</div>';
		var textLength=200;
		var descriptionContainer=false;
		if(typeof params.imgProfil != 'undefined'){
			descriptionContainer=true;
			textLength=100;
		}
		if(params.text.length > 0)
			params.text=checkAndCutLongString(params.text,textLength);
		if(typeof(params.mentions) != 'undefined')
			params.text = mentionsInit.addMentionInText(params.text,params.mentions);
		params.text=linkify(params.text);
 
		if(typeof params.imgProfil != 'undefined' && (typeof params.size == 'undefined' || params.size == 'max'))
			str += '<a href=\''+params.hash+'\' class=\'container-img-profil lbh add2fav\'  data-modalshow=\''+params.id+'\'>' + 
                    params.imgProfil + 
                  '</a>';
		else if(typeof params.text != 'undefined')
			str += '<a href=\''+params.hash+'\' class=\'container-text-profil lbh\'  data-modalshow=\''+params.id+'\'>' + 
                    params.text + 
                  '</a>';
		str += '<div class=\'padding-10 informations\'>';

		str += '<div class=\'entityRight no-padding\'>';

		if(typeof params.size == 'undefined' || params.size == 'max'){
			str += '<div class=\'entityCenter no-padding\'>';
			str +=    '<a href=\''+params.hash+'\' class=\'lbhp\' data-modalshow=\''+params.id+'\'>' + params.htmlIco + '</a>';
			str += '</div>';
		}

		if(descriptionContainer)
			str += '<div class=\'entityDescription\'>' + params.text + '</div>';
            

		var thisLocality = '';
		if(params.fullLocality != '' && params.fullLocality != ' ')
			thisLocality = '<a href=\''+params.hash+'\' data-id=\'' + params.dataId + '\' class=\'entityLocality pull-right lbhp add2fav letter-red\' data-modalshow=\''+params.id+'\'>'+
                                  '<i class=\'fa fa-home\'></i> ' + params.fullLocality + 
                                '</a>';
		
            
		str += thisLocality;

		var nameAuthor, authorType, authorId;
		if(params.targetIsAuthor){   
			nameAuthor=params.target.name;
			authorType=params.target.type;
			authorId=params.target.id;
		}else{
			nameAuthor=params.author.name;  
			authorType='citoyens';
			authorId=params.author.id;
		}    
		var authorActionLbh;

		if (typeof params.media != 'undefined' && params.media.type=='gallery_images'){
			var s=(params.media.images.length >1 ) ? 's' : '';
			authorActionLbh=trad['image'+s]+' '+trad.sharedby;
		}
		else if (typeof params.media != 'undefined' && params.media.type=='gallery_files'){
			s=(params.media.files.length >1 ) ? 's' : '';
			authorActionLbh=trad['file'+s]+' '+trad.sharedby;
		}else if (typeof params.media != 'undefined' && params.media.type=='url_content' && params.text==''){
			authorActionLbh=trad.linksharedby;
		}else{
			authorActionLbh=trad.writenby;
		}
		str += '<div class=\'entityType\'><i class=\'fa fa-pencil\'></i> ' + authorActionLbh + ' <a href=\'#page.type.'+authorType+'.id.'+authorId+'\' class=\'lbh\'>'+nameAuthor+'</a></div>';
         
		str += '<div class=\'tagsContainer text-red\'>'+params.tagsLbl+'</div>';
		str += '</div>';
		str += '</div>';
		str += '</div>';

		str += '</div>';
		return str;
	},
	// ********************************
	// CLASSIFIED DIRECTORY PANEL
	// ********************************    
	classifiedPanelHtml : function(params){
		if(directory.dirLog) mylog.log('----------- classifiedPanelHtml',params,params.name);

		var str = '';  
		str += '<div class=\'col-md-4 col-sm-6 col-xs-12 searchEntityContainer '+params.type+params.id+' '+params.type+' '+params.elTagsList+' contain_'+params.type+'_'+params.id+' \'>';
		str +=    '<div class=\'searchEntity\' id=\'entity'+params.id+'\'>';
      

		if(userId != null && userId != '' && params.id != userId){
			var isFollowed=false;
			if(typeof params.isFollowed != 'undefined' ) isFollowed=true;
			var tip = 'Garder en favoris';
			str += '<a href=\'javascript:collection.add2fav("classifieds","'+params.id+'")\' class=\'dirStar star_classified_'+params.id+' btn btn-default btn-sm btn-add-to-directory bg-white tooltips\'' + 
                  'data-toggle="tooltip" data-placement="left" data-original-title="'+tip+'"'+
                  ' data-ownerlink=\'follow\' data-id=\''+params.id+'\' data-type=\''+params.type+'\'>'+
                    '<i class=\'fa fa star fa-star-o\'></i>'+
                  '</a>';
          
		}

		if(params.updated != null )
			str += '<div class=\'dateUpdated\'><i class=\'fa fa-flash\'></i> <span class=\'hidden-xs\'>publié </span>' + 
                    params.updated + 
                  '</div>';
        
		
		str += '<a href=\''+params.hash+'\' class=\'container-img-profil lbh-preview-element add2fav\'  data-modalshow=\''+params.id+'\'>' + 
                    params.imgMediumProfil + 
                  '</a>';

		str += '<div class=\'padding-10 informations\'>';

		str += '<div class=\'entityRight profil no-padding\'>';

		if(typeof params.size == 'undefined' || params.size == 'max'){
			str += '<div class=\'entityCenter no-padding\'>';
			str +=    '<a href=\''+params.hash+'\' class=\'lbh-preview-element add2fav\' data-modalshow=\''+params.id+'\'>' + params.htmlIco + '</a>';
			str += '</div>';
		}
		var iconFaReply = '';
		str += '<a  href=\''+params.hash+'\' class=\'entityName text-dark lbh-preview-element add2fav\'  data-modalshow=\''+params.id+'\'>'+
                      iconFaReply + params.name + 
                   '</a>';  
		str += '<button id=\'btn-share-event\' class=\'text-dark btn btn-link no-padding margin-left-10 btn-share-panel pull-right\''+
                              ' data-ownerlink=\'share\' data-id=\''+params.id+'\' data-type=\''+params.type+'\'>'+
                              '<i class=\'fa fa-retweet\'></i> Partager</button>';


		var devise = (typeof params.devise != 'undefined') ? params.devise : '';
		if(typeof params.price != 'undefined' && params.price != '')

			str += '<div class=\'entityPrice text-azure\'><i class=\'fa fa-money\'></i> ' + params.price + ' ' + devise + '</div>';
         
		if(typeof params.category != 'undefined' && params.category != null && params.category != ''){
			str += '<div class=\'entityType col-xs-12 no-padding\'><span class=\'uppercase bold pull-left\'>' + tradCategory[params.section] + '</span><span class=\'pull-left\'>';
			if(typeof params.category != 'undefined') str += ' > ' + tradCategory[params.category];
			if(typeof params.subtype != 'undefined') str += ' > ' + tradCategory[params.subtype];
			str += '</span></div>';
		}
            
		if(typeof params.description != 'undefined' && params.description != '')
			str += '<div class=\'entityDescription\'>' + params.description + '</div>';
		if(params.preferences != null && typeof params.preferences.private != 'undefined' && params.preferences.private)
			str += '<div class=\'col-xs-12 text-red no-padding\'> <i class=\'fa fa-lock\'></i> ' + tradDynForm.private + '</div>';
            

		var thisLocality = '';
		if(params.fullLocality != '' && params.fullLocality != ' ')
			thisLocality = '<a href=\''+params.hash+'\' data-id=\'' + params.dataId + '\' class=\'entityLocality pull-right lbhp add2fav letter-red\' data-modalshow=\''+params.id+'\'>'+
                                  '<i class=\'fa fa-home\'></i> ' + params.fullLocality + 
                                '</a>';
		
            
		str += thisLocality;


		if(typeof params.contactInfo != 'undefined' && params.contactInfo != '')
			str += '<div class=\'entityType letter-green\'><i class=\'fa fa-address-card\'></i> ' + params.contactInfo + '</div>';
         
		str += '<div class=\'tagsContainer text-red col-xs-12 no-padding margin-bottom-5\'>'+params.tagsLbl+'</div>';

            
		if(params.startDate != null)
			str += '<div class=\'entityDate dateFrom bg-'+params.color+' transparent badge\'>' + params.startDate + '</div>';
		if(params.endDate != null)
			str += '<div  class=\'entityDate dateTo  bg-'+params.color+' transparent badge\'>' + params.endDate + '</div>';
      
		str += '</div>';
		str += '</div>';
		str += '</div>';

		str += '</div>';
		return str;
	},
	circuitPanelHtml : function(params){
		if(directory.dirLog) mylog.log('----------- classifiedPanelHtml',params,params.name);

		var str = '';  

		str += '<div class=\'col-lg-4 col-md-4 col-sm-6 col-xs-12 searchEntityContainer '+params.type+params.id+' '+params.type+' '+params.elTagsList+' \'>';

		str +=    '<div class=\'searchEntity\' id=\'entity'+params.id+'\'>';
      
		
		if(typeof userId != 'undefined' && params.creator == userId)
			params.hash=params.hash+'.view.show';
		
		str += '<a href=\''+params.hash+'\' class=\'container-img-profil lbhp add2fav\'  data-modalshow=\''+params.id+'\'>' + 
                    params.imgProfil + 
                  '</a>';

		str += '<div class=\'padding-10 informations\'>';

		str += '<div class=\'entityRight padding-5\'>';
       
            
		if(typeof params.name != 'undefined' && params.name != '')
			str += '<div class=\'entityName\'>' + params.name + '</div>';
            
		if(typeof params.description != 'undefined' && params.description != '')
			str += '<div class=\'entityDescription\'>' + params.description + '</div>';
		str += '</div>';
		str += '<div class=\'entityRight no-padding price\'>';
		str += '<hr class=\'margin-bottom-10 margin-top-10\'>';
   
		str += '<a  href=\''+params.hash+'\' class=\'showMore btn bg-orange text-white lbhp\'  data-modalshow=\''+params.id+'\'>'+
                      tradDynForm['show'] + '+' + 
                   '</a>';  
       
		str += '</div>';
		str += '</div>';
		str += '</div>';

		str += '</div>';
		return str;
	},
	storePanelHtml : function(params){
		if(directory.dirLog) mylog.log('----------- storePanelHtml',params,params.name);

		var str = '';  
		str += '<div class=\'col-lg-3 col-md-4 col-sm-4 col-xs-12 searchEntityContainer '+params.type+params.id+' '+params.type+' '+params.elTagsList+' \'>';
		str +=    '<div class=\'searchEntity\' id=\'entity'+params.id+'\'>';
		if(typeof userId != 'undefined' && params.creator == userId)
			params.hash=params.hash+'.view.show';
        
		str += '<a href=\''+params.hash+'\' class=\'container-img-profil lbhp add2fav\'  data-modalshow=\''+params.id+'\'>' + 
                    params.imgProfil + 
                  '</a>';

		str += '<div class=\'padding-10 informations\'>';

		str += '<div class=\'entityRight no-padding\'>';

		if(typeof params.name != 'undefined' && params.name != '')
			str += '<div class=\'entityName\'>' + params.name + '</div>';
            
		if(typeof params.description != 'undefined' && params.description != '')
			str += '<div class=\'entityDescription\'>' + params.description + '</div>';
		str += '</div>';
		str += '<div class=\'entityRight no-padding price\'>';
		str += '<hr class=\'margin-bottom-10 margin-top-10\'>';
		var devise = typeof params.devise != 'undefined' ? params.devise : '€';
		if(typeof params.price != 'undefined' && params.price != '')
			str += '<div class=\'entityPrice col-md-6\'><span class=\'price-trunc\'>'+ Math.trunc(params.price) + '</span> ' + devise + '</div>';
		// TODO il y a tradTerla.show je sais pas je remplace par trad
		str += '<a  href=\''+params.hash+'\' class=\'showMore btn bg-orange text-white lbhp\'  data-modalshow=\''+params.id+'\'>'+
      trad.show+' +'+ 
                   '</a>';  
       
		str += '</div>';
		str += '</div>';
		str += '</div>';

		str += '</div>';
		return str;
	},

	// ********************************
	// EVENT DIRECTORY PANEL
	// ********************************
	eventPanelHtml : function(params){
		if(directory.dirLog) mylog.log('-----------eventPanelHtml', params);
		var str = '';  
		str += '<div class=\'col-xs-12 searchEntityContainer '+params.type+' '+params.elTagsList+' contain_'+params.type+'_'+params.id+' \'>';
		str +=    '<div class=\'searchEntity\' id=\'entity'+params.id+'\'>';

		if(params.updated != null && params.updated.indexOf(trad.ago)>=0)
			params.updated = trad.rightnow;
		if(Date.parse(params.startDate) > Date.now()){
			params.updated='';
		}
		if(params.updated != null && params.updated!= '')
			str += '<div class=\'dateUpdated\'><i class=\'fa fa-flash\'></i> ' + params.updated + '</div>';

		var dateFormated = directory.getDateFormated(params);

		params.attendees = '';
		var cntP = 0;
		var cntIv = 0;

		if(typeof params.links != 'undefined')
			if(typeof params.links.attendees != 'undefined'){
				$.each(params.links.attendees, function(key, val){ 
					if(typeof val.isInviting != 'undefined' && val.isInviting == true)
						cntIv++; 
					else
						cntP++; 
				});
			}

		params.attendees = '<hr class=\'margin-top-10 margin-bottom-10\'>';
        
		var isFollowed=false;
		if(typeof params.isFollowed != 'undefined' ) isFollowed=true;
          
		if(userId != null && userId != '' && params.id != userId){

			var isShared = false;

			params.attendees += '<button id=\'btn-share-event\' class=\'text-dark btn btn-link no-padding margin-left-10 btn-share\''+
                              ' data-ownerlink=\'share\' data-id=\''+params.id+'\' data-type=\''+params.type+'\' '+//data-name='"+params.name+"'"+
                              ' data-isShared=\''+isShared+'\'>'+
                              '<i class=\'fa fa-retweet\'></i> '+trad['share']+'</button>';
		}

		params.attendees += '<small class=\'light margin-left-10 tooltips pull-right\'  '+
                                    'data-toggle=\'tooltip\' data-placement=\'bottom\' data-original-title=\''+trad['attendee-s']+'\'>' + 
                              cntP + ' <i class=\'fa fa-street-view\'></i>'+
                            '</small>';


		params.attendees += '<small class=\'light margin-left-10 tooltips pull-right\'  '+
                                    'data-toggle=\'tooltip\' data-placement=\'bottom\' data-original-title=\''+trad['guest-s']+'\'>' +
                               cntIv + ' <i class=\'fa fa-envelope\'></i>'+
                            '</small>';

		var countSubEvents = ( params.links && params.links.subEvents ) ? '<br/><i class=\'fa fa-calendar\'></i> '+Object.keys(params.links.subEvents).length+' '+trad['subevent-s']  : '' ; 
		str += '<div class="col-xs-12 col-sm-4 col-md-4 no-padding">'+
                  '<a href="'+params.hash+'" class="container-img-profil lbh add2fav block">'+params.imgMediumProfil+'</a>'+  
                '</div>';
        
		if(userId != null && userId != '' && params.id != userId /*&& !inMyContacts(params.typeSig, params.id)*/){
			var tip = trad['interested'];
			var actionConnect='follow';
			var icon='chain';
			var classBtn='';
			if(isFollowed){
				actionConnect='unfollow';
				icon='unlink';
				classBtn='text-green';
			}
			str += '<a href=\'javascript:;\' class=\'btn btn-default btn-sm btn-add-to-directory bg-white tooltips followBtn '+classBtn+'\'' + 
                      'data-toggle="tooltip" data-placement="left" data-original-title="'+tip+'"'+
                      ' data-ownerlink=\''+actionConnect+'\' data-id=\''+params.id+'\' data-type=\''+params.type+'\' data-name=\''+params.name+'\''+
                      ' data-isFollowed=\''+isFollowed+'\'>'+
                      '<i class=\'fa fa-'+icon+'\'></i>'+
                    '</a>';
		}

		str += '<div class=\'col-md-8 col-sm-8 col-xs-12 margin-top-25\'>';
		str += dateFormated+countSubEvents;
		str += '</div>';
        

		str += '<div class=\'col-md-8 col-sm-8 col-xs-12 entityRight padding-top-10 margin-top-10 pull-right\' style=\'border-top: 1px solid rgba(0,0,0,0.2);\'>';

		var thisLocality = '';
		if(params.fullLocality != '' && params.fullLocality != ' ')
			thisLocality = '<small class=\'margin-left-5 letter-red\'><i class=\'fa fa-map-marker\'></i> ' + params.fullLocality + '</small>' ;
		else thisLocality = '';
                
		var typeEvent = (typeof tradCategory[params.typeEvent] != 'undefined') ? tradCategory[params.typeEvent] : trad.event;
        
		str += '<h5 class=\'text-dark lbh add2fav no-margin\'>'+
                  '<i class=\'fa fa-reply fa-rotate-180\'></i> ' + typeEvent + thisLocality +
               '</h5>';

		str += '<a href=\''+params.hash+'\' class=\'entityName text-dark lbh add2fav\'>'+
                  params.name + 
               '</a>';
        
		if('undefined' != typeof params.organizer && params.organizer != null){ 
			var countOrganizer=Object.keys(params.organizer).length;
			str += '<div class=\'col-xs-12 margin-top-10 no-padding\'>';
			str+='<span class=\'bold\'>'+tradDynForm.organizedby+' : </span>';
			$.each(params.organizer, function(e,v){
				var imgIcon = (typeof v.profilThumbImageUrl != 'undefined' && v.profilThumbImageUrl!='' ) ? baseUrl+'/'+v.profilThumbImageUrl: assetPath + '/images/thumb/default_'+v.type+'.png';  
				str+='<a href="#page.type.'+v.type+'.id.'+e+'" class="lbh-preview-element tooltips" ';
				if(countOrganizer>1) str+= 'data-toggle="tooltip" data-placement="top" title="'+v.name+'"';
				str+='>'+
              '<img src="'+imgIcon+'" class="img-circle margin-right-10" width="35" height="35"/>';
				if(countOrganizer==1) str+=v.name;
				str+='</a>';     
			});
			str+='</div>';

		}
		str +=    '<div class=\'entityDescription margin-bottom-10\'>' + 
                    params.description + 
                  '</div>';
		str +=    '<div class=\'margin-bottom-10 col-md-12 no-padding\'>' + 
                    params.attendees + 

                  '</div>';
		str +=    '<div class=\'tagsContainer text-red\'>'+params.tagsLbl+'</div>';
		str += '</div>';
            
		str += '</div>';
		str += '</div>';

		str += '</div>';
		return str;
	},
	// ********************************
	// CITY DIRECTORY PANEL
	// ********************************
	cityPanelHtml : function(params){
		mylog.log('-----------cityPanelHtml', params);
		var domContainer=(notNull(params.input)) ? params.input+' .scopes-container' : '';
		var typeSearchCity='city';
		var levelSearchCity='city';

		
		var str = '';
		str += '<a href="javascript:" class="col-md-12 col-sm-12 col-xs-12 no-padding communecterSearch item-globalscope-checker searchEntity" ';
		str += 'data-scope-value="' + params.key  + '"' + 
					'data-scope-name="' + params.name + '"' +
					'data-scope-level="'+levelSearchCity+'"' +
					'data-scope-type="'+typeSearchCity+'"' +
					'data-scope-notsearch="'+true+'"'+
					'data-append-container="'+domContainer+'"';
		str += '>';
		str += '<div class="col-xs-12 margin-bottom-10 '+params.type+' '+params.elTagsList+' ">';
		str += '<div class="padding-10 informations">';
		str += '<div class="entityRight no-padding">';

		var title =  '<span> ' + params.name + ' ' + (notEmpty(params.postalCode) ? ' - ' +  params.postalCode : '') +'</span>' ;

		var subTitle = '';
		if( notEmpty( params.level4Name ) )
			subTitle +=  (subTitle == '' ? '' : ', ') +  params.level4Name ;
		if( notEmpty( params.level3Name ) )
			subTitle +=  (subTitle == '' ? '' : ', ') +  params.level3Name ;
		if( notEmpty( params.level2Name ) )
			subTitle +=  (subTitle == '' ? '' : ', ') +  params.level2Name ;

		subTitle +=  (subTitle == '' ? '' : ', ') + params.country ;
		str += ' <span class="entityName letter-red ">'+
									'<i class="fa fa-university"></i>' + title + 
									'<br/>'+
									'<span style="color : grey; font-size : 13px">'+subTitle+'</span>'+
								'</span>';


		str += '</div>';
		str += '</div>';              
		str += '</div>';
		str += '</a>';
		return str;
	},
	// ********************************
	// Zone DIRECTORY PANEL
	// ********************************
	zonePanelHtml : function(params){
		mylog.log('-----------zonePanelHtml', params);
		var domContainer=(notNull(params.input)) ? params.input+' .scopes-container' : '';
		var typeSearchCity, levelSearchCity;
		if(params.level.indexOf('1') >= 0){
			typeSearchCity='level1';
			levelSearchCity='1';
		}else if(params.level.indexOf('2') >= 0){
			typeSearchCity='level2';
			levelSearchCity='2';
		}else if(params.level.indexOf('3') >= 0){
			typeSearchCity='level3';
			levelSearchCity='3';
		}else if(params.level.indexOf('4') >= 0){
			typeSearchCity='level4';
			levelSearchCity='4';
		}

		var subTitle = '';

		if( notEmpty( params.level4 ) && params.id != params.level4){
			subTitle +=  (subTitle == '' ? '' : ', ') +  params.level4Name ;
		}
		if( notEmpty( params.level3 ) && params.id != params.level3 ){
			subTitle +=  (subTitle == '' ? '' : ', ') +  params.level3Name ;
		}
		if( notEmpty( params.level2 ) && params.id != params.level2){
			subTitle +=  (subTitle == '' ? '' : ', ') +  params.level2Name ;
		}

		var str = '';
		str += '<a href="javascript:" class="col-md-12 col-sm-12 col-xs-12 no-padding communecterSearch item-globalscope-checker searchEntity" ';
		str += 'data-scope-value="' + params.key  + '" ' + 
					'data-scope-name="' + params.name + '" ' +
					'data-scope-level="'+levelSearchCity+'" ' +
					'data-scope-type="'+typeSearchCity+'" ' +
					//'data-scope-values="'+JSON.stringify(valuesScopes)+'" ' +
					'data-scope-notsearch="'+true+'" '+
					'data-append-container="'+domContainer+'" ';
		str += '>';
		str += '<div class="col-xs-12 margin-bottom-10 '+params.type+' '+params.elTagsList+' ">';
		str += '<div class="padding-10 informations">';
		str += '<div class="entityRight no-padding">';
		// params.hash = '; //'main-col-search';
		// params.onclick = "setScopeValue($(this))"; //'"+params.name.replace('"', '\"')+"');";
		// params.onclickCp = "setScopeValue($(this));";
		// params.target = ';
		// params.dataId = params.name; 

		var title =  '<span>' + params.name + '</span>' ;
		subTitle +=  (subTitle == '' ? '' : ', ') + params.countryCode;
		str += ' <span class="entityName letter-red">'+
										'<i class="fa fa-bullseye bold text-red"></i>'+
										'<i class="fa bold text-dark">'+
											levelSearchCity+
										'</i> '+ 
									title + 
									'<br/>'+
									'<span style="color : grey; font-size : 13px">'+subTitle+'</span>'+
								'</span>';

		str += '</div>';
		str += '</div>';              
		str += '</div>';
		str += '</a>';
		return str;
	},
	// ********************************
	// URL DIRECTORY PANEL
	// ********************************
	urlPanelHtml : function(params, key){
		//if(directory.dirLog) 
		mylog.log('-----------urlPanelHtml', params, key);
		params.title = escapeHtml(params.title);
		if(directory.dirLog) mylog.log('-----------contactPanelHtml', params);
		var str = '';  
		str += '<div class=\'col-lg-4 col-md-6 col-sm-6 col-xs-12 margin-bottom-10 \' style=\'word-wrap: break-word; overflow:hidden;\'\'>';
		str += '<div class=\'searchEntity contactPanelHtml\'>';
		str += '<div class=\'panel-heading border-light col-lg-12 col-xs-12\'>';
		if(params.title.length > 20){
			str += '<h4 class="panel-title text-dark pull-left tooltips"' + 
                        'data-toggle="tooltip" data-placement="bottom" data-original-title="'+params.title+'">'+
                        params.title.substring(0,20)+'...</h4>';
		}else{
			str += '<h4 class="panel-title text-dark pull-left">'+ params.title+'</h4>';
		}
		str += '<br/><a href="'+params.url+'" target="_blank" class="text-dark">';
		str += ((params.url.length > 65) ? params.url.substring(0,65)+'...' : params.url)+'</a>';             
              
		str += '<br/><span class="" style="font-size: 11px !important;">' + params.urlTypes[params.type]+'</span>';
		str += '</div>';
		if ((typeof params.openEdition != 'undefined' && params.openEdition == true) || (typeof params.edit != 'undefined' && params.edit == true) ) {
			str += '<ul class="nav navbar-nav margin-5 col-md-12">';

			str += '<li class="text-red pull-right">';
			str += '<a href="javascript:;"  onclick="removeUrl(\''+key+'\');" class="margin-left-5 bg-white tooltips btn btn-link btn-sm" '+
              'data-toggle="tooltip" data-placement="top" data-original-title="'+trad['delete']+'" >';
			str += '<i class="fa fa-trash"></i>';
			str += '</a>';
			str += '</li>';

			str += '<li class="text-red pull-right">';
			str += '<a href="javascript:;" onclick="updateUrl(\''+key+'\', \''+params.title+'\',  \''+params.url+'\', \''+params.type+'\');" ' +
              'class="bg-white tooltips btn btn-link btn-sm" data-toggle="tooltip" data-placement="top" data-original-title="'+trad['update']+'" >';
			str += '<i class="fa fa-pencil"></i>';
			str += '</a>';
			str += '</li>';
            
			str += '</ul>';
		}
		str += '</div>';  
		str += '</div>';
		return str;
    
	},

	// ********************************
	// URL DIRECTORY PANEL
	// ********************************
	networkPanelHtml : function(params, key){

		mylog.log('-----------networkPanelHtml', params, key);
		params.title = escapeHtml(params.title);
		var str = '';

		str += '<div class=\'col-lg-4 col-md-6 col-sm-6 col-xs-12 searchEntityContainer contain_'+params.type+'_'+params.id+'\'>';
		str += '<div class="searchEntity" id="entity'+params.id+'">';


		if(userId != null && userId != '' && params.id != userId /*&& !inMyContacts(params.typeSig, params.id)*/ && location.hash.indexOf('#page') < 0 && searchObject.initType != 'all'){
			var isFollowed=false;

			if(typeof params.isFollowed != 'undefined' )
				isFollowed=true;
			mylog.log('isFollowed', params.isFollowed, isFollowed);

			str += '<a href=\'javascript:;\' class=\'btn btn-default btn-sm btn-add-to-directory bg-white tooltips followBtn\'' + 
                    ' data-toggle="tooltip" data-placement="left" data-original-title="'+trad['Follow']+'"'+
                    ' data-ownerlink=\'follow\' data-id=\''+params.id+'\' data-type=\''+params.type+'\' data-name=\''+params.skin.title+'\' data-isFollowed=\''+isFollowed+'\'>'+
                    '<i class=\'fa fa-chain\'></i>'+
                  '</a>';
		}


		str += '<div class=\'panel-heading border-light col-xs-12\'>';
		str += '<a href="'+baseUrl+'/network/default/index?src='+baseUrl+'/'+moduleId+'/network/get/id/'+params.id+'" target="_blank" class="text-dark">';
		str += '<h4 class="panel-title text-dark pull-left">'+ params.skin.title+'</h4></a>';
		str += '<span class=\'col-xs-12\'>'+(notNull(params.skin.shortDescription) ? params.skin.shortDescription : '' ) +'</span>';
		str += '</div>';

		if (typeof params.edit != 'undefined' && params.edit == true ) {
			str += '<ul class="nav navbar-nav margin-5 col-md-12">';

			str += '<li class="text-red pull-right">';
			str += '<a href="javascript:;"  onclick="removeNetwork(\''+params.id+'\');" class="margin-left-5 bg-white tooltips btn btn-link btn-sm" '+
              'data-toggle="tooltip" data-placement="top" data-original-title="'+trad['delete']+'" >';
			str += '<i class="fa fa-trash"></i>'+trad['delete'];
			str += '</a>';
			str += '</li>';

			str += '<li class="text-red pull-right">';
			str += '<a href="javascript:;" onclick="updateNetwork(\''+params.id+'\');" ' +
              'class="bg-white tooltips btn btn-link btn-sm" data-toggle="tooltip" data-placement="top" data-original-title="'+trad['update']+'" >';
			str += '<i class="fa fa-pencil"></i>'+trad['update'];
			str += '</a>';
			str += '</li>';

			str += '<li class="text-red pull-right">';
			str += '<a href="javascript:;" onclick="" ' +
              'class="bg-white tooltips btn btn-link btn-sm" data-toggle="tooltip" data-placement="top" data-original-title="'+trad['share']+'" >';
			str += '<i class="fa fa-pencil"></i>'+trad['share'];
			str += '</a>';
			str += '</li>';
            
			str += '</ul>';
		}
		str += '</div>';  
		str += '</div>';
		return str;
	},

	network2PanelHtml : function(params){
		mylog.log('----------- network2PanelHtml',params);
		params.hash = baseUrl+'/network/default/index?src='+baseUrl+'/'+moduleId+'/network/get/id/'+params.id;
		var str = '';
		var classType=params.type;
		str += '<div class=\'col-lg-4 col-md-6 col-sm-6 col-xs-12 searchEntityContainer '+classType+' '+params.elTagsList+' '+params.elRolesList+' contain_'+params.type+'_'+params.id+'\'>';
		str +=    '<div class="searchEntity" id="entity'+params.id+'">';

		if(typeof params.edit  != 'undefined')
			str += this.getAdminToolBar(params);

		if(params.updated != null )
			str += '<div class=\'dateUpdated\'><i class=\'fa fa-flash\'></i> <span class=\'hidden-xs\'>'+trad.actif+' </span>' + params.updated + '</div>';

		str += '<a href=\''+params.hash+'\' target=\'_blank\' class=\'container-img-banner add2fav >' + params.imgBanner + '</a>';
		str += '<div class=\'padding-10 informations\'>';

		str += '<div class=\'entityRight no-padding\'>';

		if(typeof params.size == 'undefined' || params.size == undefined || params.size == 'max'){
			str += '<div class=\'entityCenter no-padding\'>';
			str +=    '<a href=\''+params.hash+'\' target=\'_blank\' class=\'container-img-profil add2fav \'>' + params.imgProfil + '</a>';
			str +=    '<a href=\''+params.hash+'\' target=\'_blank\' class=\'add2fav pull-right margin-top-15 \'>' + params.htmlIco + '</a>';
			str += '</div>';
		}

		str += '<a href=\''+params.hash+'\' target=\'_blank\' class=\''+params.size+' entityName bold text-dark add2fav \'>'+ params.skin.title + '</a>';  

		str += '<div class=\'entityDescription\'>' + ( (params.skin.shortDescription == null ) ? '' : params.skin.shortDescription ) + '</div>';
		str += '<div class=\'tagsContainer\'>';

		if (typeof params.edit != 'undefined' && params.edit == true ) {
			str += '<ul class="nav text-center">';
			str += '<li class="text-red pull-right">';
			str += '<a href="javascript:;"  onclick="removeNetwork(\''+params.id+'\');" class="margin-left-5 bg-white tooltips btn btn-link btn-sm" '+
					'data-toggle="tooltip" data-placement="top" data-original-title="'+trad['delete']+'" >';
			str += '<i class="fa fa-trash"></i>';
			str += '</a>';
			str += '</li>';

			str += '<li class="text-red pull-right">';
			str += '<a href="javascript:;" onclick="updateNetwork(\''+params.id+'\');" ' + 'class="margin-left-5 bg-white tooltips btn btn-link btn-sm" data-toggle="tooltip" data-placement="top" data-original-title="'+trad['update']+'" >';
			str += '<i class="fa fa-pencil"></i>';
			str += '</a>';
			str += '</li>';


			str += '</ul>';
		}
		str += '</div>';

		str += '</div>';
		str += '</div>';
		str += '</div>';
		str += '</div>';
		return str;
	},

	coopPanelHtml : function(params, key,size){
		params.chat = true;
		//params.invite = true;
		mylog.log('-----------proposalPanelHtml', params, key);
		var idParentRoom = typeof params.idParentRoom != 'undefined' ? params.idParentRoom : '';
		if(idParentRoom == '' && params.type == 'rooms') idParentRoom = params.id;
		//mylog.log("-----------idParentRoom", idParentRoom);
      
		var name = (typeof params.title != 'undefined' && params.title != 'undefined') ? params.title : params.name;

		name = escapeHtml(name);
		var thisId = typeof params['_id'] != 'undefined' &&
                   typeof params['_id']['$id'] != 'undefined' ? params['_id']['$id'] : 
			typeof params['id'] != 'undefined' ? params['id'] : '';

		var filterClass = '';
		var sortData = '';
		if(typeof params.votes != 'undefined'){
			if(typeof params.votes.up != 'undefined'){
				filterClass += ' upFilter';
				sortData += ' data-sort=\''+params.votes.up.length+'\' ';
			}
			if(typeof params.votes.down != 'undefined'){
				filterClass += ' downFilter';
				sortData += ' data-sort=\''+params.votes.down.length+'\'';
			}
		}

		var str = '';  
		if(size == 'S')     
			str += '<div class=\'col-lg-4 col-md-4 col-sm-6 col-xs-12 blockCoop'+thisId+' '+filterClass+' coopFilter coop-wraper margin-bottom-10 \' '+sortData+' style=\'word-wrap: break-word; \'>';
		else 
			str += '<div class=\'col-xs-12 coop-wraper margin-bottom-10 coopFilter '+filterClass+' \' '+sortData+' style=\'word-wrap: break-word;\'>';

		var linkParams = ' data-coop-type=\''+ params.type + '\' '+
                    ' data-coop-id=\''+ thisId + '\' '+
                    ' data-coop-idparentroom=\''+ idParentRoom + '\' '+
                    ' data-coop-parentid=\''+ params.parentId + '\' '+'data-coop-parenttype=\''+ params.parentType + '\' ';

		str += '<div class=\'blockCoop'+ params.parentId + ' searchEntity coopPanelHtml\' '+linkParams+'>';
		var diffColor = (params.sourceKey) ? 'border-left:3px solid red;' : '';
		str += '<div class=\'panel-heading border-light col-lg-12 col-xs-12\' style=\''+diffColor+'\'>';


		// IMAGE 
		if(typeof params.imgMediumProfil == 'undefined'){
			//quand on reload apres un vote on ne passe pas forcement par showResultsDirectoryHtml
			if(!params.useMinSize){
				params.imgProfil = '<i class=\'fa fa-image fa-2x\'></i>';
				params.imgMediumProfil = '<i class=\'fa fa-image fa-2x\'></i>';
			}
                
			if('undefined' != typeof params.profilMediumImageUrl && params.profilMediumImageUrl != '')
				params.imgMediumProfil= '<img class=\'img-responsive\' onload=\'directory.checkImage(this);\' src=\''+baseUrl+params.profilMediumImageUrl+'\'/>';
		}
		str += '<a href=\'javascript:;\' '+linkParams+' class=\'margin-bottom-10 all-coop-detail-desc'+ thisId + ' openCoopPanelHtml container-img-profil\'>' + params.imgMediumProfil +'</a>';
            
		// OPEN PANEL BTN
		// str += "<button class='btn btn-sm btn-default pull-right openCoopPanelHtml bold letter-turq' "+linkParams+
		//         "><i class='fa fa-chevron-right'></i> <span class='hidden-xs'>"+trad.Open+"</span></button>";

		// NAME
		if(name != ''){
			str += '<a href="javascript:;" class="openCoopPanelHtml" style="text-decoration:none;" '+linkParams+' >'+
                          '<h4 class="panel-title tooltips letter-turq" data-toggle="tooltip" data-placement="bottom" data-original-title="'+name+'">'+
                          '<i class="fa '+ params.ico + '"></i> '+ 
                          ( (name.length > 100) ? name.substring(0,100)+'...' : name ) + '</h4></a>';
		}                          

		if(	typeof params.producer != 'undefined' && 
				params.producer != null &&
				Object.keys(params.producer).length > 0 ) {
			var count=Object.keys(params.producer).length;
			var htmlAbout='';
			$.each(params.producer, function(e, v){
				var heightImg=(count>1) ? 35 : 25;
				var imgIcon = (typeof v.profilThumbImageUrl != 'undefined' && v.profilThumbImageUrl!='' ) ? baseUrl+'/'+v.profilThumbImageUrl: parentModuleUrl + '/images/thumb/default_'+v.type+'.png';  

				htmlAbout+='<span ';
				if(count>1) htmlAbout+= 'data-toggle="tooltip" data-placement="left" title="'+v.name+'"';
				htmlAbout+='>'+
						'<img src="'+imgIcon+'" class="img-circle margin-right-10" width='+heightImg+' height='+heightImg+' />';
				if(count==1) htmlAbout+=v.name;
				//htmlAbout+="</a>";
				htmlAbout+='</span>';
			});
			var htmlHeader = ((params.type == typeObj.event.col) ? trad['Planned on'] : tradCategory.carriedby ) ;
			htmlHeader += ' : '+htmlAbout;

			str += htmlHeader;
		}

		// STATE OF THE PROPOSAL
          
		if(params.type != 'rooms'){
			str += '<h5 class="col-sm-12">';
			var statusColor = 'red';
			if(params.status =='adopted')
				statusColor = 'green';
			else if(params.status =='refused')
				statusColor = 'red';
			else if(params.status =='amendable')
				statusColor = 'orange';
			str +=  '<small class="text-'+statusColor+'"><i class="fa fa-certificate"></i> '+trad[params.status]+'</small>';

			// YOUR VOTE STATUS
			if(typeof userId != 'undefined' && userId != null && userId != ''){
				if((params.status == 'tovote' || params.status == 'amendementAndVote') && params.hasVote ===false)
					str +=  '<small class="margin-left-15 letter-red"><i class="fa fa-ban"></i> '+trad['You did not vote']+'</small>';
				else if((params.status == 'tovote' || params.status == 'amendementAndVote') && params.hasVote !==false)
					str +=  '<small class="margin-left-15"><i class="fa fa-thumbs-up"></i> '+trad['You did vote']+'</small>';
			}

			str += '</h5>';
            
		}

          
          
		str += '<div class="all-coop-detail">';

		str += '<div class="all-coop-detail-desc'+ thisId + '">';            
                

		if( typeof params.shortDescription != 'undefined' && params.shortDescription != ''){
			str += '<hr>';
			str += '<span class="col-xs-12 no-padding text-dark descMD">'+params.shortDescription+'</span>';
		} 
		var voteCount = '';
		if(params.votes){
			var vc = 0;
			if(params.votes.up)
				vc += Object.keys(params.votes.up).length;
			if(params.votes.down)
				vc += Object.keys(params.votes.down).length;
			if(params.votes.uncomplet)
				vc += Object.keys(params.votes.uncomplet).length;
			if(params.votes.white)
				vc += Object.keys(params.votes.white).length;
			voteCount = ' ('+vc+')';
		}
		//SHOW HIDE VOTE BTNs
		var btnSize;
		if(typeof userId != 'undefined' && userId != null && userId != ''){
			btnSize = (params.status == 'amendementAndVote') ? '6' : '12';
			if( (params.status == 'tovote' || params.status == 'amendementAndVote') && params.hasVote ===false )
				str += '<a href="javascript:;" '+ linkParams + ' data-coop-section="vote"  class="bg-green  openCoopPanelHtml btn col-sm-'+btnSize+' "><i class="fa fa-gavel"></i> '+trad.Vote+voteCount+'</a>';

			else if( (params.status == 'tovote' || params.status == 'amendementAndVote') && params.hasVote !==false )
				str += '<a href="javascript:;" '+ linkParams + ' data-coop-section="vote"  class="openCoopPanelHtml btn btn-default col-sm-'+btnSize+' "><i class="fa fa-eye"></i> '+trad['See votes']+voteCount+'</a>';
		} else {
			btnSize = (params.status == 'amendementAndVote') ? '6' : '12';
			if( (params.status == 'tovote' || params.status == 'amendementAndVote') && params.hasVote ===false )
				str += '<a href="javascript:" data-toggle="modal" data-target="#modalLogin"  class="btn-menu-connect bg-green btn col-sm-'+btnSize+' "><i class="fa fa-gavel"></i> '+trad.Vote+voteCount+'</a>';
		}
                
		if( (params.status == 'amendementAndVote'  || params.status =='amendable') ){
			var amendCount = (params.amendements) ? ' ('+Object.keys(params.amendements).length+')': '';
			str += '<a href="javascript:;" '+ linkParams + ' data-coop-section="amendments"  class="openCoopPanelHtml btn btn-default text-purple col-sm-6 "><i class="fa fa-list"></i> '+trad.Amendements+amendCount+'</a>';
		}

		if(params.votes ){
			str += '<div class=\'col-sm-12 padding-10\'>';
			if(params.votes.up)
				str += '<div class=\'col-sm-3 text-green tooltips text-center vote-counter-entity\' data-toggle=\'tooltip\' data-placement=\'bottom\' data-original-title=\''+trad.Agree+'\'><i class=\'fa fa-thumbs-up\'></i> '+Object.keys(params.votes.up).length+'</div>';
			if(params.votes.down)
				str += '<div class=\'col-sm-3 text-red tooltips text-center vote-counter-entity\' data-toggle=\'tooltip\' data-placement=\'bottom\' data-original-title=\''+trad.Disagree+'\'><i class=\'fa fa-thumbs-down\'></i> '+Object.keys(params.votes.down).length+'</div>';
			if(params.votes.uncomplet)
				str += '<div class=\'col-sm-3 text-orange tooltips text-center vote-counter-entity\' data-toggle=\'tooltip\' data-placement=\'bottom\' data-original-title=\''+trad.Uncomplet+'\'><i class=\'fa fa-hand-grab-o\'></i> '+Object.keys(params.votes.uncomplet).length+'</div>';
			if(params.votes.white)
				str += '<div class=\'col-sm-3 text-dark tooltips text-center vote-counter-entity\' data-toggle=\'tooltip\' data-placement=\'bottom\' data-original-title=\''+trad.Abstain+'\'><i class=\'fa fa-circle-o\'></i> '+Object.keys(params.votes.white).length+'</div>';
			str += '</div>';
		}

                
               

		str += '</div>';

		//VOTE BTNS
		str += '<div class="all-coop-detail-votes'+ thisId + ' hide">';
                
		//BTN toggle description
		str += '<a href="javascript:" data-coop-id="'+ thisId + '"  class="btn-openVoteDetail"><i class="text-dark fa fa-arrow-circle-left"></i></a>';
                
		if((params.auth || typeof params.idParentRoom == 'undefined') 
                    && (params.status == 'tovote' || params.status == 'amendementAndVote') )
		{


			var isMulti = typeof params.answers != 'undefined';
			var answers = isMulti ? params.answers : 
				{ 'up':'up', 'down': 'down', 'white': 'down', 'uncomplet':'uncomplet'};
                  
			$.each(answers, function(key, val){
				var voteRes = ( typeof params.voteRes != 'undefined' &&
                                   typeof params.voteRes[key] != 'undefined' ) ? params.voteRes[key] : false;             
                     
				str += '<div class="col-xs-12 no-padding timeline-panel">';

				if((params.status == 'tovote' || params.status == 'amendementAndVote') && (!params.hasVote  || params.voteCanChange == 'true')){
					str += '<div class="col-lg-1 col-md-1 col-sm-1 col-xs-2 no-padding text-right pull-left margin-top-20">';

					str += '  <button class="btn btn-send-vote btn-link btn-sm bg-vote bg-'+voteRes['bg-color']+'"';
					str += '     title="'+trad.clicktovote+'" ';
					str += '      data-idparentproposal="'+thisId+'"';
					str += '      data-idparentroom="'+ params.idParentRoom +'"';
					str += '      data-vote-value="'+key+'"><i class="fa fa-gavel"></i>';
					str += '  </button>';

					if(params.hasVote  === ''+key)
						str +=  '<br><i class="fa fa-user-circle padding-10" title="'+trad['You voted for this answer']+'"></i> ';
                          
					str += '</div>';
				}

				str += '<div class="col-lg-11 col-md-11 col-sm-11 col-xs-10">';
                      
				var hashAnswer = !isMulti ? trad[voteRes['voteValue']] : (key+1);

				str +=    '<div class="padding-10 margin-top-15 border-vote border-vote-'+key+'">';
				str +=      '<i class="fa fa-hashtag"></i><b>'+hashAnswer+'</b> ';
				if(isMulti) 
					str +=        voteRes['voteValue'];
				str +=    '</div>';

				if(voteRes !== false && voteRes['percent']!=0){
					str +=  '<div class="progress progress-res-vote">';
					str +=       '<div class="progress-bar bg-vote bg-'+voteRes['bg-color']+'" role="progressbar" ';
					str +=       'style="width:'+voteRes['percent']+'%">';
					str +=       voteRes['percent']+'%';
					str +=     '</div>';
					str +=     '<div class="progress-bar bg-transparent" role="progressbar" ';
					str +=       'style="width:'+(100-voteRes['percent'])+'%">';
					str +=      voteRes['votant']+' <i class="fa fa-gavel"></i>';
					str +=     '</div>';
					str +=  '</div>';
				}
				str += '</div>';

				str += '</div>';
			});
		} 
                
		str += '</div>';

		str += '</div>';
		str += '</div>';
		str += '</div>';  
		str += '</div>';
		return str;
    
	},

	// ********************************
	// Contact DIRECTORY PANEL
	// ********************************
	contactPanelHtml : function(params, key){
		if (directory.dirLog) mylog.log('-----------contactPanelHtml', params, params.openEdition);
		mylog.log('-----------contactPanelHtml', params, params.openEdition);
		var str = '';  
		str += '<div class=\'col-lg-4 col-md-6 col-sm-6 col-xs-12 margin-bottom-10 \'>';
		str += '<div class=\'searchEntity contactPanelHtml\'>';
		str += '<div class=\'panel-heading border-light col-lg-12 col-xs-12\'>';
		if(notEmpty(params.idContact)){
			str += '<a href="#page.type.citoyens.id.'+params.idContact+'" class="lbh" >';
			str += (notEmpty(params.name) ? '<h4 class="panel-title text-dark pull-left">'+params.name+'</h4><br/>' : '')+'</a>';
		}
		else
			str += (notEmpty(params.name) ? '<h4 class="panel-title text-dark pull-left">'+params.name+'</h4><br/>' : '');
		str += (notEmpty(params.role) ? '<span class="" style="font-size: 13px !important;">'+params.role+'</span><br/>' : '');
		//str += (notEmpty(params.email) ? '<a href="javascript:;" onclick="dyFObj.openForm(\'formContact\', \'init\')" style="font-size: 11px !important;">'+params.email+'</a><br/>' : '');
		str += (notEmpty(params.email) ? '<span class="" style="font-size: 12px !important;">'+params.email+'</span><br/>' : '');
		str += (notEmpty(params.telephone) ? '<span class="" style="font-size: 12px !important;">'+params.telephone+'</span>' : '');
		str += '</div>';
		if(typeof userId != 'undefined' && userId != ''){
			str += '<ul class="nav navbar-nav margin-5 col-md-12">';
			if(notEmpty(params.email)){
				str += '<li class="text-left pull-left">';
				str += '<a href="javascript:;" class="tooltips btn btn-default btn-sm openFormContact" '+
                               'data-id-receiver="'+key +'" '+
                               'data-email="'+(notEmpty(params.email) ? params.email : '') +'" '+
                               'data-name="'+(notEmpty(params.name) ? params.name : '') +'">';
				str += '<i class="fa fa-envelope"></i> Envoyer un e-mail';
				str += '</a>';
				str += '</li>';
			}
			if ((typeof params.openEdition != 'undefined' && params.openEdition == true) || (typeof params.edit != 'undefined' && params.edit == true) ) {
				str += '<li class="text-red pull-right">';
				str += '<a href="javascript:;" onclick="removeContact(\''+key+'\');" '+
                          'class="margin-left-5 bg-white tooltips btn btn-link btn-sm" '+
                          'data-toggle="tooltip" data-placement="top" data-original-title="'+trad['delete']+'" >';
				str += '<i class="fa fa-trash"></i>';
				str += '</a>';
				str += '</li>';
				str += '<li class="text-left pull-right">';
            
				str += '<a href="javascript:" ' +
                          'class="bg-white tooltips btn btn-link btn-sm btn-update-contact" '+
                          
                          'data-contact-key="'+key+'" data-contact-name="'+params.name+'" '+
                          'data-contact-email="'+params.email+'" data-contact-role="'+params.role+'" '+
                          'data-contact-telephone="'+params.telephone+'"'+
                          
                          'data-toggle="tooltip" data-placement="top" '+
                          'data-original-title="'+trad['update']+'" >';
				str += '<i class="fa fa-pencil"></i>';
				str += '</a>';
				str += '</li>';
			}
            
			str += '</ul>';
		}
		str += '</div>';  
		str += '</div>';
		return str;
	},
	// ********************************
	// ROOMS DIRECTORY PANEL
	// ********************************
	roomsPanelHtml : function(params, itemType){
		if(directory.dirLog) mylog.log('-----------roomsPanelHtml');
		mylog.log('-----------roomsPanelHtml :'+itemType);
		if(itemType == 'surveys') params.hash = '#page.type.surveys.id.'+params.id;
		else if(itemType == 'vote') params.hash = '#page.type.'+params.parentType+'.id.'+params.parentId+'.view.dda.dir.vote.idda.'+ params.id;
		else if(itemType == 'discuss') params.hash = '#page.type.'+params.parentType+'.id.'+params.parentId+'.view.dda.dir.discuss.idda.'+ params.id;
		else if(itemType == 'actions') params.hash = '#page.type.'+params.parentType+'.id.'+params.parentId+'.view.dda.dir.actions.idda.'+ params.id;
		//else if(params.type == "actions") params.hash = "#rooms.action.id."+params.id;
   
		var str = '';  
		str += '<div class=\'col-xs-12 col-sm-8 col-md-6 col-lg-4 searchEntityContainer '+itemType+' '+params.type+' '+params.elTagsList+' \'>';
		str +=    '<div class=\'searchEntity\'>';

		str += '<a href=\''+params.hash+'\' class=\'container-img-profil add2fav\'>' + params.imgProfil + '</a>';

		if(params.updated != null && !params.useMinSize)
			str += '<div class=\'dateUpdated\'><i class=\'fa fa-flash\'></i> <span class=\'hidden-xs\'>'+trad['actif']+' </span>' + params.updated + '</div>';

		params.startDay = notEmpty(params.startDate) ? moment(params.startDate).local().locale('fr').format('DD/MM') : '';
		params.startTime = notEmpty(params.startDate) ? moment(params.startDate).local().locale('fr').format('HH:mm') : '';
		params.startDate = notEmpty(params.startDate) ? moment(params.startDate).local().locale('fr').format('DD MMMM YYYY - HH:mm') : null;
		params.endDay = notEmpty(params.endDate) ? moment(params.endDate).local().locale('fr').format('DD/MM') : '';
		params.endTime = notEmpty(params.endDate) ? moment(params.endDate).local().locale('fr').format('HH:mm') : '';
		params.endDate   = notEmpty(params.endDate) ? moment(params.endDate).local().locale('fr').format('DD MMMM YYYY - HH:mm') : null;
        
		str += '<div class="col-xs-5">';
		if(params.startDate != null){
			str += '<div class="col-xs-4">';
			if(params.startDate != null)
				str += '<div class="bg-'+params.color+' text-white padding-5 text-bold" style="border: 2px solid #328a00; font-size:27px;margin-top:5px;">'+params.startDay+'</div>'+ params.startTime;
			if(params.endDate != null)
				str += '<div class="bg-'+params.color+' text-white padding-5 text-bold" style="border: 2px solid #328a00; font-size:27px;margin-top:5px;">'+params.endDay+'</div>'+ params.endTime;
			str += '</div>';
		}
		var w = (params.startDate != null) ? '8' : '12';
            
       
		str +=  '</div>';
        
		str += '<div class=\'padding-10 informations\'>';

		str += '<div class=\'entityRight no-padding\'>';
               
		if(notEmpty(params.parent) && notEmpty(params.parent.name))
			str += '<a href=\''+params.urlParent+'\' class=\'entityName text-'+params.parentColor+' lbh add2fav text-light-weight margin-bottom-5\'>' +
                        '<i class=\'fa '+params.parentIcon+'\'></i> '
                        + params.parent.name + 
                      '</a>';

		var iconFaReply = notEmpty(params.parent) ? '<i class=\'fa fa-reply fa-rotate-180\'></i> ' : '<i class=\'fa fa-inbox\'></i> ';
		str += '<a  href=\''+params.hash+'\' class=\''+params.size+' entityName text-dark lbh add2fav margin-top-25\'>'+
                      iconFaReply + params.name + 
                   '</a>';
            
		var thisLocality = '';
		if(params.fullLocality != '' && params.fullLocality != ' ')
			thisLocality = '<a href=\''+params.hash+'\' data-id="' + params.dataId + '"' + '  class=\'entityLocality lbh add2fav\'>'+
                                  '<i class=\'fa fa-home\'></i> ' + params.fullLocality + 
                                '</a>';
		else thisLocality = '<br>';
            
            
		if(notEmpty(params.parentRoom)){
			params.parentUrl = params.hash;
			params.parentIco = '';
			if(itemType == 'surveys')
				params.parentIco = 'archive'; 
			else if(itemType == 'actions') 
				params.parentIco = 'cogs';
			else if(itemType == 'vote') 
				params.parentIco = 'gavel';

			str += '<div class=\'text-dark\'>'+

                        '<i class=\'fa fa-' + params.parentIco + '\'></i><a href=\'' + params.parentUrl + '\' class=\'lbh add2fav\'> ' + params.parentRoom.name + '</a>'+
                    '</div>';
			if(notEmpty(params.parentRoom.parentObj)){
				var typeIcoParent = params.parentRoom.parentObj.typeSig;

				var p = dyFInputs.get(typeIcoParent);
				params.icoParent = p.icon;
				params.colorParent = p.color;

				thisLocality = notEmpty(params.parentRoom) && notEmpty(params.parentRoom.parentObj) && 
                              notEmpty(params.parentRoom.parentObj.address) ? 
					params.parentRoom.parentObj.address : null;

				var postalCode = notEmpty(thisLocality) && notEmpty(thisLocality.postalCode) ? thisLocality.postalCode : '';
				var cityName = notEmpty(thisLocality) && notEmpty(thisLocality.addressLocality) ? thisLocality.addressLocality : '';

				thisLocality = postalCode + ' ' + cityName;
				if(thisLocality != ' ') thisLocality = ', <small> ' + thisLocality + '</small>';
				else thisLocality = '';

				var ctzCouncil = typeIcoParent=='city' ? 'Conseil citoyen de ' : '';
				str += '<div class=\' text-'+params.colorParent+'\'> <i class=\'fa '+params.icoParent+'\'></i> <b>' + ctzCouncil + params.parentRoom.parentObj.name + '</b>' + thisLocality+ '</div>';
              

			}
		}else{
			str += thisLocality;
		}
            
		if(itemType == 'entry'){
			var vUp   = notEmpty(params.voteUpCount)       ? params.voteUpCount.toString()        : '0';
			var vMore = notEmpty(params.voteMoreInfoCount) ? params.voteMoreInfoCount.toString()  : '0';
			var vAbs  = notEmpty(params.voteAbstainCount)  ? params.voteAbstainCount.toString()   : '0';
			var vUn   = notEmpty(params.voteUnclearCount)  ? params.voteUnclearCount.toString()   : '0';
			var vDown = notEmpty(params.voteDownCount)     ? params.voteDownCount.toString()      : '0';
			str += '<div class=\'margin-bottom-10 no-padding\'>';
			str += '<span class=\'bg-green lbl-res-vote\'><i class=\'fa fa-thumbs-up\'></i> ' + vUp + '</span>';
			str += ' <span class=\'bg-blue lbl-res-vote\'><i class=\'fa fa-pencil\'></i> ' + vMore + '</span>';
			str += ' <span class=\'bg-dark lbl-res-vote\'><i class=\'fa fa-circle\'></i> ' + vAbs + '</span>';
			str += ' <span class=\'bg-purple lbl-res-vote\'><i class=\'fa fa-question-circle\'></i> ' + vUn + '</span>';
			str += ' <span class=\'bg-red lbl-res-vote\'><i class=\'fa fa-thumbs-down\'></i> ' + vDown + '</span>';
			str += '</div>';
		}

		str += '<div>' + params.description + '</div>';
         
		str += '<div class=\'tagsContainer text-red\'>'+params.tagsLbl+'</div>';


		str += '</div>';
		str += '</div>';
		str += '</div>';

		str += '</div>';
		return str;
	},
	searchTypeHtml : function(){
		var spanType='';

		$.each( searchObject.types, function(key, val){
			mylog.log('directory.searchTypeHtml typeHeader', val, typeObj[val]);
			var params = (searchInterface.isForced('types, category') && typeObj.isDefined(searchObject.forced.category)) ?
				typeObj.get(searchObject.forced.category) : typeObj.get(val);
			spanType += '<span class=\'text-'+params.color+'\'>'+
                          '<i class=\'fa fa-'+params.icon+' hidden-sm hidden-md hidden-lg padding-5\'></i> <span class=\'hidden-xs\'>'+params.name+'</span>'+
                        '</span>';

		});
		return spanType;
	},
	endOfResult : function(noResult){
		mylog.log('directory.endOfResult', noResult);
		var str='';
		//Event scroll and all searching
		$('#btnShowMoreResult').remove();
		if($.inArray( 'events', searchObject.types) == -1)
			scrollEnd=true;
		if(!notNull(directory.costum)){
			//msg specific for end search
			var match= (searchObject.text != '') ? 'match' : '';
			var msg= (notNull(noResult) && noResult) ? trad['noresult'+match] : trad['nomoreresult'+match];
			var contributeMsg='<span class=\'italic\'><small>'+trad.contributecommunecterslogan+'</small><br/></span>';
			if(userId !=''){
				var contributeAction = ';" data-target="#dash-create-modal" data-toggle="modal" data-toggle="tooltip" data-placement="top"';
				if(location.hash == '#ressources')
					contributeAction = 'dyFObj.openForm(\'ressources\');"';
				else if(location.hash == '#annonces')
					contributeAction = 'dyFObj.openForm(\'classifieds\');"';
				else if(location.hash == '#agenda')
					contributeAction = 'dyFObj.openForm(\'event\');"';

				contributeMsg+='<a href="javascript:'+contributeAction+' class="text-green-k tooltips" > '+
                '<i class="fa fa-plus-circle"></i> '+trad.sharesomething+
            '</a>';
			} else {
				contributeMsg+='<a href="javascript:;" class="letter-green margin-left-10" data-toggle="modal" data-target="#modalLogin">'+
                                      '<i class="fa fa-sign-in"></i> '+trad.connectyou+
                              '</a>';
			}
			str = '<div class="pull-left col-md-12 text-left" id="footerDropdown" style="width:100%;">';
			str += '<h5 style=\'margin-bottom:10px; margin-left:15px;border-left: 2px solid lightgray;\' class=\'text-dark padding-20\'>'+msg+'<br/>'+contributeMsg+'</h5><br/>';
			str += '</div>';
		}else{
			$('#btnShowMoreResult').remove();
			match= (searchObject.text != '') ? 'match' : '';
			msg= (notNull(noResult) && noResult) ? trad['noresult'+match] : trad['nomoreresult'+match];
			str = '<div class="pull-left col-xs-12 col-sm-8 col-sm-offset-2 col-md-offset-3 col-md-6 text-center shadow2" id="footerDropdown" style="border-radius: 50px;">';
			str += '<h5 class=\'letter-blue padding-10\'><i class=\'fa fa-ban\'></i> '+msg+'</h5>';
			str += '</div>';
		}
		return str;
	},
	createBtnHtml : function(){
		var btn='';
		if(searchObject.types.length > 1){
			btn+='<a href="javascript:;" class="btn margin-left-5 btn-add pull-right text-green-k" '+
            'data-target="#dash-create-modal" '+
            'data-toggle="modal">'+
              '<i class="fa fa-plus-circle"></i> '+trad.createpage+
          '</a>';
		}else if(searchObject.types[0]=='persons'){
			btn+='<a href="#element.invite" class="btn text-yellow lbhp btn-add pull-right">'+
                    '<i class="fa fa-plus-circle"></i> '+trad.invitesomeone+
              '</a>';
		}else{
			var elt=directory.getTypeObj(searchObject.types[0]);
			var subData='';
			if(typeof elt.formSubType != 'undefined')
				subData='data-form-subtype=\''+elt.formSubType+'\'';
			btn+='<button class="btn btn-open-form btn-add pull-right text-white bg-'+elt.color+'" '+
            'data-form-type="'+elt.formType+'" '+
            subData+'>'+
              '<i class="fa fa-plus-circle"></i> '+elt.createLabel+
          '</button>';
		}
		return btn;
	},
	isCostum : function(section, label){
		var res = false;
		if(notNull(directory.costum) && typeof directory.costum[section] != 'undefined'){
			res = true;
			if(notNull(label)){
				if(directory.costum[section][label] != 'undefined')
					res = true;
				else 
					res=false;
			}
		}
		return res;
	},
	getCostumValue : function(section, label){
		var res='';
		if(directory.isCostum(section, label)){
			res = directory.costum[section];
			if(notNull(label))
				res = directory.costum[label];
		}
		return res;
	},
	headerHtml : function(){
		mylog.log('-----------headerHtml ');
		var headerStr = '';
		if(!directory.isCostum('header') || directory.getCostumValue('header')){
			mylog.log('-----------headerHtml :',searchObject.count);
			if((typeof searchObject.count != 'undefined' && searchObject.count) || searchObject.indexMin==0 ){
				var countHeader=0;
				mylog.log('-----------headerHtml countHeader:',countHeader);
				if(searchObject.countType.length > 1 && typeof searchObject.ranges != 'undefined'){
					$.each(searchAllEngine.searchCount, function(e, v){
						countHeader+=v;
					});
					mylog.log('-----------headerHtml countHeader2:',countHeader);

				} else {
					var typeCount = (searchObject.types[0]=='persons') ? 'citoyens' : searchObject.types[0];
					if(typeof searchAllEngine.searchCount[typeCount] != 'undefined')
						countHeader=searchAllEngine.searchCount[typeCount];
					mylog.log('-----------headerHtml countHeader3:',countHeader);

				}


				mylog.log('-----------headerHtml :'+countHeader);
				var resultsStr = (countHeader > 1) ? trad.results : trad.result;
				var titleSize=(searchObject.initType=='classifieds') ? 'col-md-7 col-sm-6' : 'col-md-8 col-sm-8';
				var toolsSize=(searchObject.initType=='classifieds') ? 'col-md-5 col-sm-6' : 'col-md-4 col-sm-4';

				var headerFilters = '';
				if( typeof costum != 'undefined' && costum != null && 
					typeof costum.filters != 'undefined' && 
					typeof costum.filters.searchFilters != 'undefined'){
					$.each(costum.filters.searchFilters,function(k,p) { 
						var colorClass = (typeof p.colorClass != 'undefined') ? p.colorClass : '';
						headerFilters += '<a href=\'javascript:;\' data-filter=\''+k+'\' class=\'btn-coopfilter btn btn-xs '+colorClass+'\'> '+p.label+'</a> ';
					});
				}

				headerStr +='<div class="col-xs-12 margin-bottom-10">'+
						'<h4 class="elipsis '+titleSize+' col-xs-10 no-padding">'+
							'<i class=\'fa fa-angle-down\'></i> ' + countHeader + ' '+resultsStr+' ';
				if( !notNull(directory.costum) 
				|| typeof directory.costum.header == 'undefined'
				|| typeof directory.costum.header.searchTypeHtml == 'undefined'
				|| directory.costum.header.searchTypeHtml === false){
					headerStr += '<small>'+directory.searchTypeHtml()+'</small>';
				}
				headerStr += headerFilters+'</h4>'+ 
							'<div class="'+toolsSize+' col-xs-2 pull-right no-padding text-right headerSearchTools" style="padding-top:3px !important;">';
				// TODO CLEM :: ADD SURVEY	
				// if(searchObject.types.length == 1 && searchObject.types[0]=="vote"){
				// 	headerStr +='<a href="javascript:;" data-form-type="survey" class="addBtnFoot btn-open-form btn btn-default addBtnAll letter-turq margin-bottom-10">'+ 
				// 					'<i class="fa fa-plus"></i>'+
				// 					'<span>'+tradDynForm.createsurvey+'</span>'+
				// 				'</a>';
				// }
				mylog.log('headerStr');
				if( notNull(costum) 
              && typeof costum.app != 'undefined'
             && typeof directory.appKeyParam != 'undefined'
              && typeof costum.app[directory.appKeyParam] != 'undefined'
              && typeof costum.app[directory.appKeyParam].map != 'undefined'
             && typeof costum.app[directory.appKeyParam].map.hash != 'undefined'){
					mylog.log('headerStr if');
					headerStr+= '<button class="lbh-menu-app hidden-xs" data-hash="'+costum.app[directory.appKeyParam].directory.map.hash+'" style="" title="'+trad.showmap+'" alt="'+trad.showmap+'">'+
                          '<i class="fa fa-map-marker"></i> '+trad.map;
					'</button>';
				} else if( !notNull(directory.costum) 
							|| typeof directory.costum.header == 'undefined'
							|| typeof directory.costum.header.map== 'undefined'
							|| directory.costum.header.map) {
					mylog.log('headerStr else');
					headerStr+=       '<button class="btn-show-map hidden-xs" style="" title="'+trad.showmap+'" alt="'+trad.showmap+'">'+
		'<i class="fa fa-map-marker"></i> '+trad.map;
					'</button>';
				}

				if(userId!='' && searchObject.initType=='classifieds'){
					headerStr+='<button class="btn btn-default letter-blue addToAlert margin-right-5 tooltips" data-toggle="tooltip" data-placement="bottom" title="'+trad.bealertofnewitems+'" data-value="list" onclick="directory.addToAlert();"><i class="fa fa-bell"></i> <span class="hidden-xs">'+trad.alert+'</span></button>';
				}

				if(userId != "" 
				&& notNull(directory.costum) 
				&& typeof directory.costum.header != 'undefined' 
				&& typeof directory.costum.header.add != 'undefined'
				// && typeof directory.costum.header.add[searchObject.initType] != "undefined"
				&& typeof directory.costum.header.add[searchObject.types] != 'undefined'
				){
					headerStr+='<div class=\'pull-right\'>';
					headerStr+=directory.createBtnHtml();
					headerStr+='</div>';
				}
				if(!notNull(directory.costum) 
				|| typeof directory.costum.header == 'undefined'
				|| typeof directory.costum.header.viewMode == 'undefined'
				|| directory.costum.header.viewMode)  {
					headerStr+='<button class="btn switchDirectoryView ';
					if(directory.viewMode=='list') headerStr+='active ';
					headerStr+=     'margin-right-5" data-value="list"><i class="fa fa-bars"></i></button>'+
								'<button class="btn switchDirectoryView ';
					if(directory.viewMode=='block') headerStr+='active ';
					headerStr+=     '" data-value="block"><i class="fa fa-th-large"></i></button>';
				}


				headerStr+= '</div>';      
			}
		}

		mylog.log('-----------headerHtml headerStr', headerStr);
		return headerStr;
	},
	sortSearch : function(what) {
		var $wrapper = $('#dropdown_search');

		$wrapper.find(what).sort(function (a, b) {
			return +a.dataset.sort - +b.dataset.sort;
		})
			.appendTo( $wrapper );
	},
	checkImage : function($this){
		var imgH = $this.scrollHeight;
		if(imgH > 240){
			var marginTop= imgH/3;
			$($this).css({'margin-top':'-'+marginTop+'px'});
		}
	},
	deleteElement : function(type, id, btnCLick){
		var urlToSend = baseUrl+'/'+moduleId+'/element/delete/type/'+type+'/id/'+id;
		bootbox.confirm(trad.areyousuretodelete,
			function(result) 
			{
				if (!result) {
					if(typeof btnClick !='undefined' && btnClick != null)
						btnClick.empty().html('<i class="fa fa-trash"></i>');
					return;
				} else {
					$.ajax({
						type: 'POST',
						url: urlToSend,
						dataType : 'json'
					})
						.done(function (data) {
							if ( data && data.result ) {
								toastr.info('élément effacé');
								if( $('#'+type+id).length > 0)
									$('#'+type+id).remove();
								else if( $('.contain_'+type+'_'+id).length > 0 )
									$('.contain_'+type+'_'+id).remove();
								else
									urlCtrl.loadByHash( location.hash );
								if($('#openModal').hasClass('in'))
									$('#openModal').modal('hide');
							} else {
								toastr.error('something went wrong!! please try again.');
							}
						});
				}
			});
	},
	bindBtnElement: function(){ mylog.log('directory.bindBtnElement');
		coInterface.bindLBHLinks();

		$('.btn-update-contact').click(function(){ 
			updateContact($(this).data('contact-key'),$(this).data('contact-name'), $(this).data('contact-email'), 
				$(this).data('contact-role'),$(this).data('contact-telephone'));
		});
		$('.btn-edit-preview, .btn-edit-element').off().on('click', function(){
			$('#modal-preview-coop').hide(300);
			$('#modal-preview-coop').html('');
			dyFObj.editElement($(this).data('type'), $(this).data('id'), $(this).data('subtype'));
		});
		$('.deleteThisBtn').off().on('click',function (){
			mylog.log('deleteThisBtn click');
			$(this).empty().html('<i class="fa fa-spinner fa-spin"></i>');
			directory.deleteElement($(this).data('type'), $(this).data('id'), $(this));
		});
		$('.tooltips').tooltip();
		initUiCoopDirectory();
		directory.bindMediaSharingElt();
		directory.bindShareElt();
		//Specific case of link in directory
		$('.followBtn').off().on('click', function(){
			mylog.log('.followBtn');
			var parentId = $(this).attr('data-id');
			var parentType=$(this).attr('data-type');
			var childId = userId;
			var childType = 'citoyens';
			//traduction du type pour le floopDrawer
			var thiselement = $(this);
			$(this).html('<i class=\'fa fa-spin fa-circle-o-notch text-azure\'></i>');
			var connectType = (parentType == 'events') ? 'connect' : 'follow';
			var callback, labelLink;
			if ($(this).attr('data-ownerlink')=='follow'){
				callback=function(){
					//toastr.success(data.msg); 
					labelLink=(parentType == 'events') ? trad.alreadyAttendee : trad.alreadyFollow;
					if(thiselement.hasClass('btn-add-to-directory'))
						labelLink='';
					thiselement.html('<small><i class=\'fa fa-unlink\'></i> '+labelLink+'</small>');
					thiselement.addClass('text-green');
					thiselement.attr('data-ownerlink','unfollow');
					thiselement.attr('data-original-title', labelLink);
				};
				if(parentType=='events')
					links.connectAjax(parentType, parentId, childId, childType, connectType, null, callback);
				else
					links.follow(parentType, parentId, childId, childType, callback);
			}
			else if($(this).attr('data-ownerlink')=='unfollow'){
				connectType = (parentType == 'events') ? 'attendees' : 'followers';
				callback=function(){
					labelLink=(parentType == 'events') ? trad.participate : trad.Follow;
					if(thiselement.hasClass('btn-add-to-directory'))
						labelLink='';
					$(thiselement).html('<small><i class=\'fa fa-chain\'></i> '+labelLink+'</small>');
					$(thiselement).attr('data-ownerlink','follow');
					$(thiselement).attr('data-original-title', labelLink);
					$(thiselement).removeClass('text-green');
				};
				links.disconnectAjax(parentType, parentId, childId,childType,connectType, null, callback);
			}
		});
	},
	addToAlert : function(){
		var message = '<span class=\'text-dark\'>'+tradDynForm.saveresearchandalert+'</span><br>';
		var searchName = (searchObject.text != '' ) ? searchObject.text : '';   
		bootbox.dialog({
			title: message,
			message: '<div class="row">  ' +
                      '<div class="col-md-12"> ' +
                        '<form class="form-horizontal"> ' +
                          '<label class="col-md-12 no-padding" for="awesomeness">'+tradDynForm.searchtosave+' : </label><br> ' +
                          '<span>'+location.hash+'</span><br><br>'+
                          '<label class="col-md-12 no-padding" for="awesomeness">'+tradDynForm.nameofsearch+' : </label><br> ' +                        
                          '<input type="text" id="nameFavorite" class="nameSearch wysiwygInput form-control" value="'+searchName+'" style="width: 100%" placeholder="'+tradDynForm.addname+'..."></input>'+
                          '<br>'+
            
                        '<label class="col-md-6 col-sm-6 no-padding" for="awesomeness">'+tradDynForm.bealertbymailofnewclassified+' ?</label> ' +
                        '<div class="col-md-4 col-sm-4"> <div class="radio no-padding"> <label for="awesomeness-0"> ' +
                          '<input type="radio" name="awesomeness" id="awesomeness-0" value="true"  checked="checked"> ' +
                          tradDynForm.yes+' </label> ' +
                          '</div><div class="radio no-padding"> <label for="awesomeness-1"> ' +
                          '<input type="radio" name="awesomeness" id="awesomeness-1" value="false"> '+tradDynForm.no+' </label> ' +
                          '</div> ' +
                    '</div> </div>' +
                    '</form></div></div>',
			buttons: {
				success: {
					label: trad.save,
					className: 'btn-primary',
					callback: function () {
						var formData={
							'parentId':userId,
							'parentType':'citoyens',
							'url':location.hash,
							'searchUrl':searchInterface.getParamsUrlForAlert(),
							'alert':$('input[name=\'awesomeness\']:checked').val(),
							'name':$('#nameFavorite').val(),
							'type':'research'
						};
						if(typeof costum != 'undefined'){
							formData.mailParams={
								logo : costum.logo,
								title : costum.title,
								url : costum.url
							};
						}
						mylog.log(formData);
						$.ajax({
							type: 'POST',
							url: baseUrl+'/'+moduleId+'/bookmark/save',
							data: formData,
							dataType: 'json',
							success: function(data) {
								if(data.result){
									toastr.success(data.msg); 
								}
								else{
									if(typeof(data.type)!='undefined' && data.type=='info')
										toastr.info(data.msg);
									else
										toastr.error(data.msg);
								}
							},
						});  
					}
				},
				cancel: {
					label: trad.cancel,
					className: 'btn-secondary',
					callback: function() {
					}
				}
			}
 
		});
	},
	bindMediaSharingElt: function(){
		$('.btn-share-panel').off().click(function(){
			directory.showShareModal($(this).attr('data-type'), $(this).attr('data-id'));	
		});
	},
	showShareModal : function(shareEltType, shareEltId){
		mylog.log('init btn-share ');
		var url = baseUrl+'/co2/app/page/type/'+shareEltType+'/id/'+shareEltId;
		var str='<div class=\'socialBar col-xs-12\'>'+
'<label class=\'title text-dark margin-top-5\'>'+trad.copylink+' :</label>'+
'<input type=\'text\' class=\'form-control margin-bottom-10\' value=\''+url+'\'/>';
		if(userId != null && userId != ''){
			str+='<button class="share-co-btn btn-share co-btn" data-id="'+shareEltId+'"" data-type="'+shareEltType+'">'+
'<img class="" width="40" data-ownerlink="share"  src="'+parentModuleUrl+'/images/CO.png"/> Communecter'+
'</button>';
		}
		str+=	'<button class="share-co-btn facebook-btn">'+
'<img class="" width="25" src="'+parentModuleUrl+'/images/social/facebook-icon-64.png" onclick="window.open(\'https://www.facebook.com/sharer.php?u='+url+'\',\'_blank\')"/> Facebook'+
'</button>';
		//str='<i class="fa fa-twitter-square" style="float:right;margin-left:0.5em;" onclick="window.open(\'https://twitter.com/intent/tweet?url='+url+'\',\'_blank\')"></i>';
		str+= '<button class="share-co-btn twitter-btn">'+
				'<img class="" width="25" src="'+parentModuleUrl+'/images/social/twitter-icon-64.png" onclick="window.open(\'https://twitter.com/intent/tweet?url='+url+'\',\'_blank\')"/> Twitter'+
			'</button>'+
		'</div>';
		$('#modal-share').modal('show');
		$('#modal-share #htmlElementToShare').html(str);
		$('#modal-share #btn-share-it, #modal-share .title-share-modal, #modal-share #msg-share').hide();
		directory.bindShareElt();
	},
	bindShareElt : function(){
		$('.btn-share').off().click(function(){
			var thiselement = this;
			var type = $(thiselement).attr('data-type');
			var id = $(thiselement).attr('data-id');
        
			$('#modal-share').modal('show');
        
			mylog.log('directory.bindBtnShareElt '+type+' - '+id);
			var html = '';
        
			if($('#news-list li#'+type+id + ' .timeline-panel').length > 0)
				html = $('#news-list li#'+type+id + ' .timeline-panel').html();
        
			if($('.timeline-body .newsActivityStream'+id).length > 0)
				html = $('.timeline-body .newsActivityStream'+id).html();
        
			if(html == '' && $('.searchEntity#entity'+id).length > 0) {
				var light = $('.searchEntity#entity'+id).hasClass('entityLight') ? 'entityLight' : '';
				html = '<div class=\'searchEntity '+light+'\'>'+$('.searchEntity#entity'+id).html()+'</div>';
			}

			if(html == '' && type !='news' && type!='activityStream' && typeof contextData != 'undefined'){
				mylog.log('HERE', contextData);
				html = directory.showResultsDirectoryHtml(new Array(contextData), type);
			} 
        
			$('#modal-share #htmlElementToShare').html(html);
			$('#modal-share #btn-share-it, #modal-share .title-share-modal, #modal-share #msg-share').show();
			$('#modal-share #btn-share-it').attr('data-id', id);
			$('#modal-share #btn-share-it').attr('data-type', type);
			$('#modal-share #btn-share-it').off().click(function(){
				directory.shareIt('#modal-share #btn-share-it');
			});
		});
	},
	shareIt: function(thiselement){
		var formData = new Object();
		formData.parentId = $(thiselement).attr('data-id');
		formData.childId = userId;
		formData.childType = 'citoyens';
		formData.connectType =  'share';
		var type = $(thiselement).attr('data-type');
      
		var comment = $('#msg-share').val();
		formData.comment = comment;
		$('#msg-share').val('');
      
		//traduction du type pour le floopDrawer
		var typeOrigine = dyFInputs.get(type).col;
		if(typeOrigine == 'persons'){ typeOrigine = personCOLLECTION;}
		formData.parentType = typeOrigine;
		if(type == 'person') type = 'people';
		else type = dyFInputs.get(type).col;

		$.ajax({
			type: 'POST',
			url: baseUrl+'/news/co/share',
			data : formData,
			dataType: 'json',
			success: function(data){
				if(data.result){
            
					$('#modal-share #htmlElementToShare').html('');
					$(thiselement).attr('data-original-title', 'Vous avez partagé cette page avec votre réseau');
					toastr.success(data.msg);
				}  
			}
		});
	},
	switcherViewer : function(results, dom){
		$('.switchDirectoryView').off().on('click',function(){
			$('.switchDirectoryView').removeClass('active');
			$(this).addClass('active');
			directory.viewMode=$(this).data('value');
			var str=directory.showResultsDirectoryHtml(results);
			var domId=(notNull(dom))? dom : '#dropdown_search';
			$(domId).html(str);
			//active les link lbh
			coInterface.bindLBHLinks();
			initBtnAdmin();
			directory.bindBtnElement();
			if(userId != ''){
				var param={
					typeEntity : 'citoyens',
					value : directory.viewMode,
					name : 'directoryView'
				};
				$.ajax({
					type: 'POST',
					url: baseUrl+'/'+moduleId+'/element/updatesettings',
					data: param,
					dataType: 'json',
					success: function(data){}
				});
			}
		});
	},
	footerHtml : function(){
		var footerStr = '';
		// GET PAGINATION STRUCTURE
     
		if(typeof searchObject.ranges == 'undefined' && pageCount === true){
			footerStr += '<div class="pageTable col-md-12 col-sm-12 col-xs-12 text-center"></div>';
			if(userId != ''){
				// eslint-disable-next-line no-empty
				if(typeof searchObject.ranges == 'undefined'){

				} 
			}
			else{
				'<div class="col-md-12 col-sm-12 col-xs-12 padding-5 text-center">'+
              '<small>'+
                '<span>Connect you to share your knowledge</span>'+ 
              '</small>'+
            '</h5>'+
          '</div>';  
			}
		}else if(searchObject.indexStep > 0){
			footerStr='<div id=\'btnShowMoreResult\' class=\'col-xs-12\'>'+coInterface.showLoader(null, '')+'</div>';
		}
		return footerStr;
	},
	showResultsDirectoryHtml : function ( data, contentType, size, edit, viewMode){

		mylog.log('directory.showResultsDirectoryHtml', data, contentType, size, edit, viewMode);

		var str = '';

		directory.colPos = 'left';

         

		if(typeof data == 'object' && data!=null){
			$.each(data, function(i, params) {
				if(i!='count'){
              
					if ((typeof(params.id) == 'undefined') && (typeof(params['_id']) !== 'undefined')) {
						params.id = params['_id'];
					} else if (typeof(params.id) == 'undefined' && location.hash.indexOf('#interoperability') >= 0) {
						params.id = Math.random();
						params.type = 'poi';
					}
					//mylog.log(params.sorting);
              

					if(notNull(params['_id']) || notNull(params.id)){

						var itemType=(contentType) ? contentType : params.type;
						params.itemType = itemType;
						//mylog.log('directory.showResultsDirectoryHtml itemType', itemType);
						if( itemType ){ 
    
							var typeIco = i;
							params.size = size;
							params.id = getObjectId(params);
							//mylog.log(params.id);
							params.name = notEmpty(params.name) ? params.name : '';
							params.description = notEmpty(params.shortDescription) ? params.shortDescription : 
								(notEmpty(params.message)) ? params.message : 
									(notEmpty(params.description)) ? params.description : 
										'';

							if(typeof edit != 'undefined' && edit != false)
								params.edit = edit;
							//mylog.log('directory.showResultsDirectoryHtml edit', edit);
							if ( params.type && typeof typeObj.classifieds != 'undefined' && $.inArray(params.type, typeObj.classifieds.subTypes )>=0  ) {
								itemType = 'classifieds';
							} else if(typeof( typeObj[itemType] ) == 'undefined') {
								itemType='poi';
							}

							if( dyFInputs.get( itemType ) == null){
								itemType='poi';
							}

							typeIco = itemType;
							if(directory.dirLog) mylog.warn('itemType',itemType,'typeIco',typeIco);

							if(typeof params.typeOrga != 'undefined')
								typeIco = params.typeOrga;
							if(typeof params.typeClassified != 'undefined')
								typeIco = params.typeClassified;
							var obj = (dyFInputs.get(typeIco)) ? dyFInputs.get(typeIco) : typeObj['default'] ;
							params.ico =  'fa-'+obj.icon;
							params.color = obj.color;
							if(params.parentType){
								if(directory.dirLog) mylog.log('params.parentType',params.parentType);
								var parentObj = (dyFInputs.get(params.parentType)) ? dyFInputs.get(params.parentType) : typeObj['default'] ;
								params.parentIcon = 'fa-'+parentObj.icon;
								params.parentColor = parentObj.color;
							}
							if((typeof searchObject.countType != 'undefined' && searchObject.countType.length==1) && params.type == 'classifieds' && typeof params.category != 'undefined' && typeof modules[params.typeClassified] != 'undefined'){
								var getIcoInModules=modules[params.typeClassified].categories;
								params.ico = (typeof getIcoInModules.filters != 'undefined' && typeof getIcoInModules.filters[params.category] != 'undefined') ?
									'fa-' + getIcoInModules.filters[params.category]['icon'] : 'fa-bullhorn';
							}
							if(params.type=='poi' 
                      && typeof modules.poi != 'undefined' 
                      && typeof modules.poi.categories != 'undefined' 
                      && typeof modules.poi.categories.filters != 'undefined'
                      && typeof modules.poi.categories.filters[params.typePoi] != 'undefined'
                      && typeof modules.poi.categories.filters[params.typePoi].icon != 'undefined')
								params.ico='fa-'+modules.poi.categories.filters[params.typePoi].icon;
							params.htmlIco ='<i class=\'fa '+ params.ico +' fa-2x bg-'+params.color+'\'></i>';

							params.useMinSize = typeof size != 'undefined' && size == 'min';

							params.imgProfil = ''; 
							if(!params.useMinSize){
								params.imgProfil = '<i class=\'fa fa-image fa-2x\'></i>';
								params.imgMediumProfil = '<i class=\'fa fa-image fa-2x\'></i>';
							}
							if('undefined' != typeof directory.costum && notNull(directory.costum)  
                      && typeof directory.costum.results != 'undefined' 
                      && typeof directory.costum.results[params.type] != 'undefined' 
                      && typeof directory.costum.results[params.type].defaultImg != 'undefined')

								params.imgMediumProfil= '<img class=\'img-responsive\' onload=\'directory.checkImage(this);\' src=\''+assetPath+directory.costum.results[params.type].defaultImg+'\'/>';
       
							if('undefined' != typeof params.profilMediumImageUrl && params.profilMediumImageUrl != '')
								params.imgMediumProfil= '<img class=\'img-responsive\' onload=\'directory.checkImage(this);\' src=\''+baseUrl+params.profilMediumImageUrl+'\'/>';
                    
							if('undefined' != typeof params.profilThumbImageUrl && params.profilThumbImageUrl != '')
								params.imgProfil= '<img class=\'shadow2\' src=\''+baseUrl+params.profilThumbImageUrl+'\'/>';


							params.imgBanner = ''; 
							if(!params.useMinSize)
								params.imgBanner = '<i class=\'fa fa-image fa-2x\'></i>';

							params.type = dyFInputs.get(itemType).col;
							params.urlParent = (notEmpty(params.parentType) && notEmpty(params.parentId)) ? 
								'#page.type.'+params.parentType+'.id.' + params.parentId : '';

							params.insee = params.insee ? params.insee : '';
							params.postalCode = '', params.city='',params.cityName='';
							if (params.address != null) {
								params.city = params.address.addressLocality;
								params.postalCode = params.cp ? params.cp : params.address.postalCode ? params.address.postalCode : '';
								params.cityName = params.address.addressLocality ? params.address.addressLocality : '';
							}
							params.fullLocality = params.postalCode + ' ' + params.cityName;

							params.hash = '#page.type.'+params.type+'.id.' + params.id;

							if(typeof networkJson != 'undefined' && typeof networkJson.dataSrc != 'undefined')
								params.hash = params.source;

							params.onclick = 'urlCtrl.loadByHash("' + params.url + '");';
							if(params.type=='circuits')
								params.hash = '#circuit.index.id.' + params.id;
							params.onclick = 'urlCtrl.loadByHash("' + params.url + '");';

							if( params.type == 'poi' && params.source  && ( notNull(params.source.key) && params.source.key.substring(0,7) == 'convert')) {
								var interop_type = getTypeInteropData(params.source.key);
								params.type = 'poi.interop.'+interop_type;
							}

							params.elTagsList = '';
							var thisTags = '';
							if(typeof params.tags != 'undefined' && params.tags != null){
								$.each(params.tags, function(key, value){
									if(typeof value != 'undefined' && value != '' && value != 'undefined'){
										var tagTrad = typeof tradCategory[value] != 'undefined' ? tradCategory[value] : value;
										thisTags += '<span class=\'badge bg-transparent text-red btn-tag tag\' data-tag-value=\''+slugify(value, true)+'\' data-tag-label=\''+tagTrad+'\'>#' + tagTrad + '</span> ';

										params.elTagsList += slugify(value, true)+' ';
									}
								});
								params.tagsLbl = thisTags;
							}else{
								params.tagsLbl = '';
							}
							params.elRolesList = '';
							var thisRoles = '';
							params.rolesLbl = '';
							if(typeof params.rolesLink != 'undefined' && params.rolesLink != null){
								thisRoles += '<small class=\'letter-blue\'><b>'+trad.roleroles+' :</b> ';
								thisRoles += params.rolesLink.join(', ');
								$.each(params.rolesLink, function(key, value){
									if(typeof value != 'undefined' && value != '' && value != 'undefined')
										params.elRolesList += slugify(value)+' ';
								});
								thisRoles += '</small>';
								params.rolesLbl = thisRoles;
							}
							params.updated   = notEmpty(params.updatedLbl) ? params.updatedLbl : null;
							if(notNull(params.tobeactivated) && params.tobeactivated == true){
								params.isInviting = true ;
							}
                    
							// if(directory.dirLog) 
							// 	mylog.log('template principal',params,params.type, itemType);
							// mylog.log('directory.js params.type', params.type);
							if( typeof domainName != 'undefined' && domainName=='terla'){
								if(params.type=='circuits')
									str += directory.circuitPanelHtml(params);
								else
									str += directory.storePanelHtml(params);
								//template principal
							}else{
								//mylog.log('template principal',params,params.type, itemType);
								if((((typeof directory.viewMode != 'undefined' && directory.viewMode=='list' && !notNull(viewMode))) || (notNull(viewMode) && viewMode=='list'))  && $.inArray(params.type, ['citoyens','organizations','projects','events','poi','news','places','ressources','classifieds'] )>=0) 
									str += directory.lightPanelHtml(params);  
								else{ 
									if(params.type == 'cities')
										str += directory.cityPanelHtml(params);  
                      
									else if( $.inArray(params.type, ['citoyens','organizations','projects','poi','places','ressources'] )>=0) 
										str += directory.elementPanelHtml(params);  
                      
									else if(params.type == 'events'){
										if(typeof searchObject.countType != 'undefined' && searchObject.countType.length > 1)
											str += directory.elementPanelHtml(params);
										else
											str += directory.eventPanelHtml(params);  
									}
                        
									else if (params.type == 'news')
										str += directory.newsPanelHtml(params);

									else if(params.type == 'classifieds'){
										if(typeof searchObject.countType != 'undefined' && searchObject.countType.length > 1)
											str += directory.elementPanelHtml(params);  
										else
											str += directory.classifiedPanelHtml(params);
									}
									else if(params.type == 'proposals' || 
                                params.type == 'actions' || 
                                params.type == 'resolutions' || 
                                params.type == 'rooms'){

										if(location.hash.indexOf('#dda') == 0)
											str += directory.coopPanelHtml(params,null,'S');
										else   
											str += directory.coopPanelHtml(params);
									}
									else if(typeof params.type == 'string' && params.type.substring(0,11) == 'poi.interop')
										str += directory.interopPanelHtml(params);
									else if(params.type == 'network')
										str += directory.network2PanelHtml(params);
									
									else
										str += directory.defaultPanelHtml(params);
								}
							}
						}
					}else{
						//mylog.log('pas d\'id');
						if(contentType == 'urls')
							str += directory.urlPanelHtml(params, i);
						if(contentType == 'contacts')
							str += directory.contactPanelHtml(params, i);
					}
               
				}
			});
		} //end each
		mylog.log('END -----------showResultsDirectoryHtml ('+str.length+' html caracters generated)');
		return str;
	},
	getAdminToolBar : function(data){
		mylog.log('directory.getAdminToolBar', data);
		var countBtn=0;
		var html ='';
		if(directory.viewMode=='block'){
			html += '<a href=\'javascript:;\' class=\'btn btn-default btn-sm btn-add-to-directory bg-white tooltips adminIconDirectory\'>'+
               '<i class=\'fa fa-cog\'></i>'+ //fa-bookmark fa-rotate-270
               '</a>';
		}
		html+='<div class=\'adminToolBar';
		if(directory.viewMode=='block')
			html+=  ' entityProfil';
		html+='\'>';
		if(data.edit=='follows'){
			html +='<button class=\'btn btn-default btn-xs disconnectConnection\''+ 
            ' data-type=\''+data.type+'\' data-id=\''+data.id+'\' data-connection=\''+data.edit+'\' data-parent-hide=\'2\''+
            ' style=\'bottom:'+(30*countBtn)+'px\'>'+
            '<i class=\'fa fa-unlink\'></i> '+trad['unfollow']+
          '</button> ';
			countBtn++;
		}
		if(data.edit=='organizations' || data.edit=='projects' || data.edit=='networks'){
			html +='<button class=\'btn btn-default btn-xs disconnectConnection\''+ 
            ' data-type=\''+data.type+'\' data-id=\''+data.id+'\' data-connection=\''+data.edit+'\' data-parent-hide=\'3\''+
            ' style=\'bottom:'+(30*countBtn)+'px\'>'+
            '<i class=\'fa fa-unlink\'></i> '+trad['cancellink']+
          '</button> ';
			countBtn++;
		}
     
		if(data.edit=='members' || data.edit=='contributors' || data.edit=='attendees'){
			if(data.type=='organizations' || ( typeof data.statusLink != 'undefined' && (typeof data.statusLink['isAdmin'] == 'undefined' || typeof data.statusLink['isAdminPending'] != 'undefined') ) ){
				html +='<button class=\'btn btn-default btn-xs disconnectConnection\''+ 
            ' data-type=\''+data.type+'\' data-id=\''+data.id+'\' data-connection=\''+data.edit+'\' data-parent-hide=\'2\''+
            ' style=\'bottom:'+(30*countBtn)+'px\'>'+
            '<i class=\'fa fa-unlink\'></i>!! '+trad['delete'+data.edit]+
          '</button> ';
				countBtn++;
			}
			if(data.type!='organizations' && typeof data.statusLink != 'undefined' && typeof data.statusLink['isAdmin'] == 'undefined'){
				html +='<a href="javascript:links.connect(\''+contextData.type+'\',\''+contextData.id+'\', \''+data.id+'\', \''+data.type+'\', \'admin\',\'\',\'true\')" class="btn btn-default btn-xs" '+
                   ' style=\'bottom:'+(30*countBtn)+'px\'>'+
                            '<i class="fa fa-user-plus"></i> '+trad['addasadmin']+
                          '</a>';
				countBtn++;
			}
			if(data.type!='organizations'  && typeof data.statusLink != 'undefined'&& typeof data.statusLink['toBeValidated'] != 'undefined' && typeof data.statusLink['isAdminPending'] == 'undefined'){
				html +='<button class=\'btn btn-default btn-xs acceptAsBtn\''+ 
            ' data-type=\''+data.type+'\' data-id=\''+data.id+'\' data-connect-validation=\'toBeValidated\' data-parent-hide=\'2\''+
            ' style=\'bottom:'+(30*countBtn)+'px\'>'+
            '<i class=\'fa fa-user\'></i> '+trad['acceptas'+data.edit]+
          '</button> ';
				countBtn++;
			}else if(data.type!='organizations'  && typeof data.statusLink != 'undefined' && typeof data.statusLink['isAdminPending'] != 'undefined'){
				html +='<button class=\'btn btn-default btn-xs acceptAsBtn\''+ 
            ' data-type=\''+data.type+'\' data-id=\''+data.id+'\' data-connect-validation=\'isAdminPending\' data-parent-hide=\'2\''+
            ' style=\'bottom:'+(30*countBtn)+'px\'>'+
            '<i class=\'fa fa-user-plus\'></i> '+trad['acceptasadmin']+
          '</button> ';
				countBtn++;
			}
			if(data.edit=='members' || data.edit=='contributors' || data.edit=='attendees'){
				var roles='';
				if(typeof data.rolesLink != 'undefined')
					roles+=addslashes(data.rolesLink.join(', '));
				html +='<button class=\'btn btn-default btn-xs\''+ 
            ' onclick="updateRoles(\''+data.id+'\', \''+data.type+'\', \''+addslashes(data.name)+'\', \''+data.edit+'\',\''+roles+'\')"'+
            ' style=\'bottom:'+(30*countBtn)+'px\'>'+
            '<i class=\'fa fa-pencil\'></i> '+trad.addmodifyroles;
				'</button> ';
				countBtn++;
			}

			if(typeof data.statusLink != 'undefined' && 
				( ( typeof data.statusLink['isAdminPending'] != 'undefined' && data.statusLink['isAdminPending'] == true )  || 
					( typeof data.statusLink['isAdmin'] != 'undefined' && data.statusLink['isAdmin'] == true ) ) ) {
				html +='<button class=\'btn btn-default btn-xs disconnectConnection\''+ 
	            ' data-type=\''+data.type+'\' data-id=\''+data.id+'\' data-connection=\''+data.edit+'\' data-parent-hide=\'3\''+
	            ' style=\'bottom:'+(30*countBtn)+'px\'>'+
	            '<i class=\'fa fa-unlink\'></i> Supprimer admin '+
	          '</button> ';
				countBtn++;
			}
		}

    
		html+='</div>';
		//mylog.log('directory.getAdminToolBar html end', html);
		return html;
	},
	getTypeObj: function(e){
		var elt={};
		if(typeof typeObj[e] != 'undefined'){
			if(typeof typeObj[e].name != 'undefined')
				elt.name=typeObj[e].name; 
			else if(typeof typeObj[e].sameAs != 'undefined')
				elt.name=typeObj[typeObj[e].sameAs].name;
        
			if(typeof typeObj[e].icon != 'undefined')
				elt.icon=typeObj[e].icon; 
			else if(typeof typeObj[e].sameAs != 'undefined')
				elt.icon=typeObj[typeObj[e].sameAs].icon;
        
			if(typeof typeObj[e].color != 'undefined')
				elt.color=typeObj[e].color; 
			else if(typeof typeObj[e].sameAs != 'undefined')
				elt.color=typeObj[typeObj[e].sameAs].color;
        
			if(typeof typeObj[e].formType != 'undefined')
				elt.formType=typeObj[e].formType; 
			else if(typeof typeObj[e].sameAs != 'undefined')
				elt.formType=typeObj[typeObj[e].sameAs].formType;
        
			if(typeof typeObj[e].formSubType != 'undefined')
				elt.formSubType=typeObj[e].formSubType; 
			else if(typeof typeObj[e].sameAs != 'undefined' && typeof typeObj[typeObj[e].sameAs].formSubType != 'undefined')
				elt.formSubType=typeObj[typeObj[e].sameAs].formSubType;
        
			if(typeof typeObj[e].createLabel != 'undefined')
				elt.createLabel=typeObj[e].createLabel; 
			else if(typeof typeObj[e].sameAs != 'undefined')
				elt.createLabel=typeObj[typeObj[e].sameAs].createLabel;

			if(typeof typeObj[e].col != 'undefined')
				elt.col=typeObj[e].col; 
			else if(typeof typeObj[e].sameAs != 'undefined')
				elt.col=typeObj[typeObj[e].sameAs].col;
			if(typeof typeObj[e].ctrl != 'undefined')
				elt.ctrl=typeObj[e].ctrl; 
			else if(typeof typeObj[e].sameAs != 'undefined')
				elt.ctrl=typeObj[typeObj[e].sameAs].ctrl;
        
			if(typeof typeObj[e].formParent != 'undefined')
				elt.formParent=typeObj[e].formParent;
        
		}
		return elt;
	},
	//builds a small sized list
	buildList : function(list) {
		$('.favSectionBtnNew,.favSection').remove();
		mylog.warn('START >>>>>> buildList',smallMenu.destination + ' #listDirectory');
      
		$.each( list, function(key,slist)
		{
			var subContent = directory.showResultsDirectoryHtml ( slist, key);
			if( notEmpty(subContent) ){
				favTypes.push(typeObj[key].col);
          
				var color = (typeObj[key] && typeObj[key].color) ? typeObj[key].color : 'dark';
				var icon = (typeObj[key] && typeObj[key].icon) ? typeObj[key].icon : 'circle';
				$(smallMenu.destination + ' #listDirectory').append('<div class=\''+typeObj[key].col+'fav favSection \'>'+
                                            '<div class=\' col-xs-12 col-sm-12\'>'+
                                            '<h4 class=\'text-left text-'+color+'\'><i class=\'fa fa-angle-down\'></i> '+trad[key]+'</h4><hr>'+
                                            subContent+
                                            '</div>');
				$('.sectionFilters').append(' <a class=\'text-black btn btn-default favSectionBtn favSectionBtnNew  bg-'+color+'\''+
                                      ' href=\'javascript:directory.showAll(".favSection",directory.elemClass);toggle(".'+typeObj[key].col+'fav",".favSection",1)\'> '+
                                          '<i class=\'fa fa-'+icon+' fa-2x\'></i><br>'+trad[key]+
                                        '</a>');
			}
		});

		directory.bindBtnElement();
		directory.filterList();
		$(directory.elemClass).show();

	},

	getWeekDayName : function(numWeek){
		var wdays = new Array(trad['sunday'],trad['monday'],trad['tuesday'],trad['wednesday'],trad['thursday'],trad['friday'],trad['saturday'],trad['sunday']);
		if(typeof wdays[numWeek] != 'undefined') return wdays[numWeek];
		else return '';
	},
	getMonthName : function(numMonth){
		numMonth = parseInt(numMonth);
		var wdays = new Array('', trad['january'],trad['february'],trad['march'],trad['april'],trad['may'],trad['june'],trad['july'],trad['august'],trad['september'],trad['october'],trad['november'],trad['december']);
		if(typeof wdays[numMonth] != 'undefined') return wdays[numMonth];
		else return '';
	},

	//build list of unique tags based on a directory structure
	//on click hides empty parent sections
	filterList : function  (elClass,dest) { 
		directory.tagsT = [];
		directory.scopesT = [];
		$('#listTags').html('');
		$('#listScopes').html('<h4><i class=\'fa fa-angle-down\'></i> Où</h4>');
		mylog.log('tagg', directory.elemClass);
		$.each($(directory.elemClass),function(k,o){
          
			var oScope = $(o).find('.entityLocality').text();
			
			$.each($(o).find('.btn-tag'),function(i,oT){
				var oTag = $(oT).data('tag-value');
				if( notEmpty( oTag ) && !inArray( oTag,directory.tagsT ) ){
					directory.tagsT.push(oTag);
					$('#listTags').append('<a class=\'btn btn-xs btn-link text-red text-left w100p favElBtn '+slugify(oTag)+'Btn\' data-tag=\''+slugify(oTag)+'\' href=\'javascript:directory.toggleEmptyParentSection(".favSection",".'+slugify(oTag)+'",directory.elemClass,1)\'><i class=\'fa fa-tag\'></i> '+oTag+'</a><br/>');
				}
			});
			if( notEmpty( oScope ) && !inArray( oScope,directory.scopesT ) ){
				directory.scopesT.push(oScope);
				$('#listScopes').append('<a class=\'btn btn-xs btn-link text-red text-left w100p favElBtn '+slugify(oScope)+'Btn\' href=\'javascript:directory.searchFor("'+oScope+'")\'><i class=\'fa fa-map-marker\'></i> '+oScope+'</a><br/>');
			}
		});
		
	},

	//todo add count on each tag
	filterTags : function (withSearch,open) { 
		directory.tagsT = [];
		$('#listTags').html('');
		if(withSearch){
			$('#listTags').append('<h5 class=\'\'><i class=\'fa fa-search\'></i> '+trad['filtertags']+'</h5>');
			$('#listTags').append('<input id="searchBarTextJS" data-searchPage="true" type="text" class="input-search form-control">');
		}

		$('#listTags').append('<a class=\'btn btn-link text-red favElBtn favAllBtn\' '+
            'href=\'javascript:directory.toggleEmptyParentSection(".favSection",null,directory.elemClass,1)\'>'+
            ' <i class=\'fa fa-refresh\'></i> <b>'+trad['seeall']+'</b></a><br/>');
        
		$.each( $(directory.elemClass),function(k,o){
			$.each($(o).find('.btn-tag'),function(i,oT){
				var realTag = $(oT).data('tag-label');


				var oTag = $(oT).data('tag-value').toLowerCase();
				if( notEmpty( oTag ) && !inArray( oTag,directory.tagsT ) ){
					directory.tagsT.push(oTag);

					$('#listTags').append('<a class=\'btn btn-link favElBtn text-red elipsis '+slugify(oTag)+'Btn\' '+
                                            'data-tag=\''+slugify(oTag)+'\' '+
                                            'href=\'javascript:directory.toggleEmptyParentSection(".favSection",".'+slugify(oTag)+'",directory.elemClass,1)\'>'+
                                              '#'+realTag+
                                        '</a><br> ');
				}
			});
		});
		if( directory.tagsT.length && open ){
			directory.showFilters();
		}

	},
    
	sectionFilter : function (objJson, dest, what, type ) { 
		mylog.log('sectionFilter',objJson,what,dest);
		if(typeof objJson.sources != 'undefined'){
			var str='';
			$('.sourcesInterrop').show(800);
			$.each(objJson.sources, function(e,v){
				str+='<button class="btn btn-default col-xs-12 padding-10 bold text-dark elipsis btn-select-source dropDesign" '+
  'data-source="'+v.label+'" data-key="'+v.key+'">'+
  '<div class="checkbox-filter pull-left"><label>'+
  '<input type="checkbox" class="checkbox-info">'+
  '<span class="cr"><i class="cr-icon fa fa-circle"></i></span>'+
                '</label></div>'+
                '<span class="pull-left" ><img src="'+parentModuleUrl+v.path+'" width="20" height="20" class=""/></span>'+ 
  '<span class="pull-left" >'+v.label+'</span>'+
  '</button>';
			});
			$('.dropdown-sources').show();
			$('.dropdown-sources .dropdown-menu').html(str);
		}else{
			$('.dropdown-sources').hide(800);
		}
		if(typeof objJson.sections != 'undefined'){
			str='';
			$.each(objJson.sections, function(e,v){
				str+='<button class="btn btn-default col-xs-12 padding-10 bold text-dark elipsis btn-select-section dropDesign" '+
                'data-section-anc="'+v.label+'" data-key="'+v.key+'" '+ 
                'data-section="classifieds">'+
                '<div class="checkbox-filter pull-left"><label>'+
                          '<input type="checkbox" class="checkbox-info">'+
                          '<span class="cr"><i class="cr-icon fa fa-circle"></i></span>'+
                      '</label></div>'+
                  '<i class="fa fa-'+v.icon+' hidden-xs"></i> '+ 
                  tradCategory[v.labelFront]+
            '</button>';
			});
			$('.dropdown-section .dropdown-menu').html(str);
		}
		if(typeof objJson.filters != 'undefined'){
			str='';
			$.each( objJson.filters,function(k,o){
				if( type == 'btn' ){
					str += '<div class="col-md-4 padding-5 categoryBtnC '+k+'">'+
                          '<a class="btn tagListEl btn-select-category elipsis categoryBtn '+k+'Btn " data-tag="'+k+'" '+
                              'data-key="'+k+'" href="javascript:;">'+
                            '<i class="fa fa-'+o.icon+'"></i> <br>'+tradCategory[k]+
                          '</a>'+
                        '</div>';
				}
				else {
					str += '<button class="btn btn-default col-xs-12 text-dark btn-select-category margin-bottom-5 elipsis dropDesign" style="margin-left:-5px;" data-keycat="'+k+'">'+
                        '<div class="checkbox-filter pull-left"><label>'+
                            '<input type="checkbox" class="checkbox-info">'+
                            '<span class="cr"><i class="cr-icon fa fa-circle"></i></span>'+
                        '</label></div>'+
                        '<i class="fa fa-'+o.icon+' hidden-xs"></i> '+tradCategory[k]+'</button><br>';
				}
				if(typeof o.subcat != 'undefined' && type != 'btn' )
				{
					$.each( o.subcat ,function(i,oT){
						str += '<button class="btn btn-default col-xs-12 text-dark margin-bottom-5 margin-left-15 hidden keycat keycat-'+k+' dropDesign" data-categ="'+k+'" data-keycat="'+oT.key+'">'+
                                '<div class="checkbox-filter pull-left"><label>'+
                                  '<input type="checkbox" class="checkbox-info">'+
                                  '<span class="cr"><i class="cr-icon fa fa-circle"></i></span>'+
                                '</label></div>'+
                                tradCategory[i]+'</button><br class="hidden">';
					});
				}else if(typeof objJson.subcat != 'undefined'){
					$.each( objJson.subcat ,function(i,oT){
						var icon=(typeof oT.icon != 'undefined') ? oT.icon : 'angle-right';
						str += '<button class="btn btn-default text-dark col-xs-12 margin-bottom-5 margin-left-15 elipsis hidden keycat keycat-'+k+' dropDesign" data-categ="'+k+'" data-keycat="'+oT.key+'">'+
                                '<div class="checkbox-filter pull-left"><label>'+
                                  '<input type="checkbox" class="checkbox-info">'+
                                  '<span class="cr"><i class="cr-icon fa fa-circle"></i></span>'+
                                '</label></div>'+
                              '<i class="fa fa-'+icon+'"></i> '+tradCategory[i]+'</button><br class="hidden">';
					});
				}
				$('.dropdown-category .dropdown-menu').html(str);
			});
		}
	},
	showFilters : function () { 
		if($('#listTags').hasClass('hide')){
			$('#listTags').removeClass('hide');
			$('#dropdown_search').removeClass('col-md-offset-1');
		}else{
			$('#listTags').addClass('hide');
			$('#dropdown_search').addClass('col-md-offset-1');
		}
		$('#listTags').removeClass('hide');
		$('#dropdown_search').removeClass('col-md-offset-1');
	},

	addMultiTagsAndScope : function() { 
		directory.multiTagsT = [];
		directory.multiScopesT = [];
		$.each(myMultiTags,function(oTag,oT){
			if( notEmpty( oTag ) && !inArray( oTag,directory.multiTagsT ) ){
				directory.multiTagsT.push(oTag);
				//mylog.log(oTag);
				$('#listTags').append('<a class=\'btn btn-xs btn-link btn-anc-color-blue  text-left w100p favElBtn '+slugify(oTag)+'Btn\' data-tag=\''+slugify(oTag)+'\' href=\'javascript:directory.searchFor("#'+oTag+'")\'><i class=\'fa fa-tag\'></i> '+oTag+'</a><br/>');
			}
		});
		$.each(myScopes.multiscopes,function(oScope,oT){
			var oScope = oT.name;
			if( notEmpty( oScope ) && !inArray( oScope,directory.multiScopesT ) ){
				directory.multiScopesT.push(oScope);
				$('#listScopes').append('<a class=\'btn btn-xs btn-link text-white text-left w100p favElBtn '+slugify(oScope)+'Btn\' data-tag=\''+slugify(oScope)+'\' href=\'javascript:directory.searchFor("'+oScope+'")\'><i class=\'fa fa-tag\'></i> '+oScope+'</a><br/>');
			}
		});
	},

	//show hide parents when empty
	toggleEmptyParentSection : function ( parents ,tag ,children ) { 
		mylog.log('toggleEmptyParentSection(\''+parents+'\',\''+tag+'\',\''+children+'\')');
		var showAll = true;
		if(tag){
			$('.favAllBtn').removeClass('active');
			//apply tag filtering
			$(tag+'Btn').toggleClass('btn-link text-white').toggleClass('active text-white');

			if( $( '.favElBtn.active' ).length > 0 ) 
			{
				showAll = false;
				var tags = '';
				$.each( $( '.favElBtn.active' ) ,function( i,o ) { 
					tags += '.'+$(o).data('tag')+',';
				});
				tags = tags.replace(/,\s*$/, '');
				mylog.log(tags);
				toggle(tags,children,1);
            
				directory.toggleParents(directory.elemClass);
			}
		}
        
		if( showAll )
			directory.showAll(parents,children);

		$('.my-main-container').scrollTop(0);
	},

	showAll: function(parents,children,path,color) {
		//show all
		if(!color)
			color = 'text-white';
		$('.favElBtn').removeClass('active btn-dark-blue').addClass('btn-link ');
		$('.favAllBtn').addClass('active');
		$(parents).removeClass('hide');
		$(children).removeClass('hide');
	},
	//be carefull with trailing spaces on elemClass
	//they break togglePArents and breaks everything
	toggleParents : function (path) { 
		//mylog.log("toggleParents",parents,children);
		$.each( favTypes, function(i,k)
		{
			if( $(path.trim()+'.'+k).length == $(path.trim()+'.'+k+'.hide ').length )
				$('.'+k+'fav').addClass('hide');
			else
				$('.'+k+'fav').removeClass('hide');
		});
	},

	//fait de la recherche client dans les champs demandé
	search : function(parentClass, searchVal) { 
		mylog.log('searchDir searchVal',searchVal);           
		if(searchVal.length>2 ){
			$.each( $(directory.elemClass) ,function (i,k) { 
				var found = null;
				if( $(this).find('.entityName').text().search( new RegExp( searchVal, 'i' ) ) >= 0 || 
                  $(this).find('.entityLocality').text().search( new RegExp( searchVal, 'i' ) ) >= 0 || 
                  $(this).find('.tagsContainer').text().search( new RegExp( searchVal, 'i' ) ) >= 0 )
				{
				
					found = 1;
				}
                
				if(found)
					$(this).removeClass('hide');
				else
					$(this).addClass('hide');
			});

			directory.toggleParents(directory.elemClass);
		} else
			directory.toggleEmptyParentSection(parentClass,null, directory.elemClass ,1);
	},

	searchFor : function (str) { 
		$('.searchSmallMenu').val(str).trigger('keyup');
	},

	get_time_zone_offset : function( ) {
		var current_date = new Date();
		return -current_date.getTimezoneOffset() / 60;
	},
	getDateFormated: function(params, onlyStr, allInfos){
		if(typeof params.recurrency != 'undefined' && params.recurrency){
			return directory.initOpeningHours(params, allInfos);
		}else{
			params.startDateDB = notEmpty(params.startDate) ? params.startDate : null;
			params.startDay = notEmpty(params.startDate) ? moment(params.startDate/*,"YYYY-MM-DD HH:mm"*/).local().locale('fr').format('DD') : '';
			params.startMonth = notEmpty(params.startDate) ? moment(params.startDate/*,"YYYY-MM-DD HH:mm"*/).local().locale('fr').format('MM') : '';
			params.startYear = notEmpty(params.startDate) ? moment(params.startDate/*,"YYYY-MM-DD HH:mm"*/).local().locale('fr').format('YYYY') : '';
			params.startDayNum = notEmpty(params.startDate) ? moment(params.startDate/*,"YYYY-MM-DD HH:mm"*/).local().locale('fr').format('d') : '';
			params.startTime = notEmpty(params.startDate) ? moment(params.startDate/*,"YYYY-MM-DD HH:mm"*/).local().locale('fr').format('HH:mm') : '';
        
			params.endDateDB = notEmpty(params.endDate) ? params.endDate: null;
			params.endDay = notEmpty(params.endDate) ? moment(params.endDate/*,"YYYY-MM-DD HH:mm"*/).local().locale('fr').format('DD') : '';
			params.endMonth = notEmpty(params.endDate) ? moment(params.endDate/*,"YYYY-MM-DD HH:mm"*/).local().locale('fr').format('MM') : '';
			params.endYear = notEmpty(params.startDate) ? moment(params.endDate/*,"YYYY-MM-DD HH:mm"*/).local().locale('fr').format('YYYY') : '';
			params.endDayNum = notEmpty(params.startDate) ? moment(params.endDate/*,"YYYY-MM-DD HH:mm"*/).format('d') : '';
			params.endTime = notEmpty(params.endDate) ? moment(params.endDate/*,"YYYY-MM-DD HH:mm"*/).local().locale('fr').format('HH:mm') : '';
			params.startDayNum = directory.getWeekDayName(params.startDayNum);
			params.endDayNum = directory.getWeekDayName(params.endDayNum);

			params.startMonth = directory.getMonthName(params.startMonth);
			params.endMonth = directory.getMonthName(params.endMonth);
			params.color='orange';
        

			var startLbl = (params.endDay != params.startDay) ? trad['fromdate'] : '';
			var endTime = ( params.endDay == params.startDay && params.endTime != params.startTime) ? ' - ' + params.endTime : '';
			mylog.log('params.allDay', !notEmpty(params.allDay), params.allDay);
       
        
			var str = '';
			var dStart = params.startDay + params.startMonth + params.startYear;
			var dEnd = params.endDay + params.endMonth + params.endYear;
			mylog.log('DATEE', dStart, dEnd);

			if(params.startDate != null){
				if(notNull(onlyStr)){
					str+='<h4 class=\'text-bold letter-orange no-margin\'>';
					if(params.endDate != null && dStart != dEnd)
						str +=  '<small class="">'+trad.fromdate+'</small> ';
					str += '<span class="letter-'+params.color+'">'+params.startDay+'</span>';
					if(params.endDate == null || dStart == dEnd || (params.startMonth != params.endMonth || params.startYear != params.endYear))
						str += ' <small class="">'+ params.startMonth+'</small>';
					if(params.endDate == null || dStart == dEnd || params.startYear != params.endYear)
						str += ' <small class="">'+ params.startYear+'</small>';
					if(params.endDate != null && dStart != dEnd)
						str += ' <small class="">'+trad.todate+'</small> <span class="letter-'+params.color+'">'+params.endDay +'</span> <small class="">'+ params.endMonth +' '+ params.endYear+'</small>';
					str+='</h4>';

					str +=  '<small class="margin-top-5"><b><i class="fa fa-clock-o"></i> '+
                                params.startTime+endTime+'</b></small>';
				
				}else{ 
					str += '<h3 class="letter-'+params.color+' text-bold no-margin" style="font-size:20px;">'+
                    '<small>'+startLbl+' </small>'+
                    '<small class="letter-'+params.color+'">'+params.startDayNum+'</small> '+
                    params.startDay + ' ' + params.startMonth + 
                    ' <small class="letter-'+params.color+'">' + params.startYear + '</small>';
					
					str +=  ' <small class="pull-right margin-top-5"><b><i class="fa fa-clock-o margin-left-10"></i> '+
                                params.startTime+endTime+'</b></small>';
         
					str +=  '</h3>';
				}
			}    
        
			if(params.endDate != null && dStart != dEnd && !notNull(onlyStr)){
				str += '<h3 class="letter-'+params.color+' text-bold no-margin" style="font-size:20px;">'+
                        '<small>'+trad['todate']+' </small>'+
                        '<small class="letter-'+params.color+'">'+params.endDayNum+'</small> '+
                        params.endDay + ' ' + params.endMonth + 
                        ' <small class="letter-'+params.color+'">' + params.endYear + '</small>';
				
				str += ' <small class="pull-right margin-top-5"><b><i class="fa fa-clock-o margin-left-10"></i> ' + 
                                  params.endTime+'</b></small>';
				
				str +=  '</h3>';
			}   
			return str;
		}
	},
	initOpeningHours : function(params, allInfos) {
		mylog.log('initOpeningHours', params, allInfos);
		var html = (notNull(allInfos)) ? '<span class=\'uppercase bold col-xs-12 no-padding\'>'+trad.eachWeeks+'</span>' : '<span class=\'uppercase bold no-padding\'>'+trad.each+' </span>' ;
		mylog.log('initOpeningHours contextData.openingHours', params.openingHours);
		if(notNull(params.openingHours) ){
			var count=0;

			var openHour = '';
			var closesHour = '';
			$.each(params.openingHours, function(i,data){
				mylog.log('initOpeningHours data', data, data.allDay, notNull(data));
				mylog.log('initOpeningHours notNull data', notNull(data), typeof data, data.length);
				if( (typeof data == 'object' && notNull(data) ) || (typeof data == 'string' && data.length > 0) ) {
					var day = '' ;
					var dayNum=(i==6) ? 0 : (i+1);
					if(notNull(allInfos)){
						mylog.log('initOpeningHours data.hours', data.hours);
						day = '<b class=\'day-recurrency-show text-orange uppercase\'>'+moment().day(dayNum).local().locale(mainLanguage).format('dddd')+'</b><span class=\'increment-hours-by-day\'> : </span><br/>';
						day += '<ul class=\'hours-recurrency-show\' style=\'list-style: none;margin-bottom:0px;\'>';
						$.each(data.hours, function(i,hours){
							mylog.log('initOpeningHours hours', hours);
							day += '<li style=\'font-size:13px;\'><span class=\'increment-hours-by-day\'>- </span>'+hours.opens+' : '+hours.closes+'</li>';
						});
						day += '</ul>';
						if( moment().format('dd') == data.dayOfWeek )
							html += day;
						else
							html += day;
					}else{
						if(count > 0) html+=', ';

					
						var color = 'text-orange';
					
						if( typeof agenda != 'undefined' && agenda != null && 
            (moment(agenda.getStartMoment(agenda.dayCount)).isoWeekday() - 1 ) == i &&
            typeof data.hours[i] != 'undefined' && 
            typeof data.hours[i].opens != 'undefined' && 
            typeof data.hours[i].closes != 'undefined' ) {

							color = 'text-red';
							openHour = data.hours[i].opens;
							closesHour = data.hours[i].closes;
						}
	

						html+= '<b style=\'font-variant: small-caps;\' class=\''+color+'\'>'+moment().day(dayNum).local().locale(mainLanguage).format('dddd')+'</b>';
					
					}
					count++;
				}
        
			});

			html +=  '<small class="pull-right margin-top-5"><b><i class="fa fa-clock-o"></i> '+openHour+'-'+closesHour+'</b></small>';
		} else 
			html = '<i>'+trad.notSpecified+'</i>'; 

		return html;
	}
};

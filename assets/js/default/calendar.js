/* eslint-disable no-global-assign */
/* eslint-disable no-mixed-spaces-and-tabs */
/* eslint-disable quotes */
/* eslint-disable indent */
/* global $ moment toastr searchObject startSearch searchCallback scrollEnd loadingData isMapEnd searchAllEngine notNull mainLanguage onchangeClick searchInterface urlCtrl trad mylog STARTDATE baseUrl moduleId coInterface directory pageCount assetPath tradDynForm */
var agenda = {
	dayCount : 0,
	noResult : true,
	nbLaunchSesearch : 0,
	countsLaunchSesearch : 0,
	startDate : null,
	heightScroll : 200,
	limitSearch : 7,
	nbSearch : 0,
	finishSearch: false,
	init : function(pInit){
		//mylog.log("agenda.init", pInit);
		this.dayCount = ( (typeof pInit != "undefined" && typeof pInit.dayCount != "undefined") ? pInit.dayCount :  0 )
		this.finishSearch = false;
	},
	getDateHtml : function(startMoment){
		var strdate = '<div class="col-xs-12">' +
			'<div class="dayEvent col-xs-12">' +
			'<h2 class="text-white date" style="text-transform:inherit;padding-left:15px;">' +
			'<i class="fa fa-angle-down"></i> <span class="date-label">' +
			moment(startMoment).local().locale("fr").format('dddd DD MMMM') +
			'</span></h2>' +
			'</div>' +
			'</div>';
		return strdate;
	},
	appendDateHtml : function(startMoment, container){
		// mylog.log("agenda.appendDateHtml", startMoment, container);
		var strdate = this.getDateHtml(startMoment);
        var cont = ( (typeof container != "undefined" && container != null ) ? container : "#dropdown_search" );
        $(cont).append(strdate);
	},
	getStartMoment : function(day){
		// mylog.log("agenda.getStartMoment", day);
		var startMoment = moment(agenda.getStartDate()).local().set('date', moment(agenda.getStartDate()).get('date') + day).format();
		return startMoment;
	},
	searchDay : function(day){
		// mylog.log("agenda.searchDay", day);
		var startMoment = this.getStartMoment(day);
		// mylog.log("agenda.searchDay startMoment", startMoment);
        var startDDDD = new Date(startMoment);
        // mylog.log("agenda.searchDay startDDDD", startDDDD);
        searchObject.startDate = Math.floor(startDDDD / 1000);
        startSearch(10, 30, searchCallback, true);
	},
	addDay : function(){
		// mylog.log("agenda.addDay", this.dayCount);
		this.dayCount++;
		// mylog.log("agenda.addDay end", this.dayCount);
		this.searchDay(this.dayCount);

	},
	getStartDate : function(){
      var today = new Date();
      var todayMoment = moment().seconds(0).minute(0).hour(0);
      // mylog.log("agenda.getStartDate today", typeof today, today);
      // mylog.log("agenda.getStartDate today", typeof todayMoment, todayMoment, todayMoment.valueOf());
      today = new Date(today.setSeconds(0));
      today = new Date(today.setMinutes(0));
      today = new Date(today.setHours(0));
      //var todayUTC0 = moment(todayMoment.format()).utcOffset("+00:00");
      //STARTDATE = moment(todayMoment.format()).valueOf();
      STARTDATE = today.setDate(today.getDate());

      return STARTDATE ;
	},
	scroll : function(){
		// mylog.log("agenda.scroll");
		$(window).bind("scroll",function(){ 
            if(!loadingData && !scrollEnd && !isMapEnd && !searchAllEngine.searchInProgress){
                var heightWindow = $("html").height();
                if($(window).scrollTop() + $(window).height() > $(document).height() - agenda.heightScroll) {
                    // mylog.log("agenda.scroll scroll", loadingData, scrollEnd, isMapEnd, typeof searchObject.ranges ); 
                    if(typeof agenda.dayCount != "undefined" && searchObject.types.length == 1 && $.inArray( "events", searchObject.types) >= 0){
                        agenda.addDay();
                    }
                }
            }
        });
	},
	searchNextEvent: function(){
		mylog.log("agenda.searchNextEvent");
		// var startMoment = this.getStartMoment(agenda.day);
  //       var startDDDD = new Date(startMoment);
  //       searchObject.startDate = Math.floor(startDDDD / 1000);
        var params = searchInterface.constructObjectAndUrl(true);

        if(typeof params.startDate != "undefined" && params.startDate != null){
			params.startDateUTC = moment(params.startDate*1000).local().format();
		}

		$.ajax({
			type: "POST",
			url: baseUrl+"/co2/search/searchnextevent",
			data: params,
			dataType: "json",
			error: function (data){
				mylog.log("agenda.searchNextEvent error", data);
			},
			success: function(data){
				mylog.log("agenda.searchNextEvent success", data);
				if(Object.keys(data).length){
					//$("#dropdown_search .processingLoader").remove();
					$.each(data, function(keyE, valE){

						// mylog.log("agenda.searchNextEvent success each", valE.startDate );
						// mylog.log("agenda.searchNextEvent success each", valE.startDate.sec );
						var datestart = valE.startDate.sec*1000 ;
						var datestartM =  moment(datestart).format() ;
						// mylog.log("agenda.searchNextEvent success each", datestart, datestartM );

						// mylog.log("agenda.searchNextEvent success each", params.startDateUTC, datestartM );
						var duration = moment.duration(moment(datestartM).diff(moment(params.startDateUTC)));
						var days = Math.floor(duration.asDays());
						// mylog.log("agenda.searchNextEvent success duration days", duration, days );

						agenda.dayCount += days;
						agenda.nbSearch = 0;
						agenda.searchDay(agenda.dayCount);

					});
				} else {
					$(".search-loader").remove();
					$("#dropdown_search .processingLoader").remove();
					$("#dropdown_search").append(directory.endOfResult(true));
					agenda.finishSearch = true;
				}
				
			}
		});
	}
};

var calendar = {
	domTarget : "#calendar",
	viewMode : "month",
	eventsList : {},
	options : {
		"popupRender" : false
	},
	init : function(domCal, initDate, initMode){
		// mylog.log("calendar.init ", domCal);
		if(notNull(domCal))
			calendar.domTarget=domCal;
		//INIT DATE IF NOT CURRENT
		var dateToShow; 
		if(typeof initDate != "undefined" && notNull(initDate) ){
			var splitInit=initDate.split("-");
			dateToShow = new Date(splitInit[0], splitInit[1]-1, splitInit[2]);
		}
		else{
			initDate=null;
			dateToShow = new Date();
		}
		// INIT MODE CALENDAR (MONTH , WEEK, DAY)
		if(notNull(initMode))
			calendar.viewMode=initMode;
		$(calendar.domTarget).fullCalendar('destroy');
		calendar.eventsList = {};
		$(calendar.domTarget).fullCalendar({
			header : {
				left : 'prev,next',
				center : 'title',
				right : 'today, month'
			},
			fixedWeekCount:false,
			height: 'auto',
			lang : mainLanguage,
			year : dateToShow.getFullYear(),
			month : dateToShow.getMonth(),
			date : dateToShow.getDate(),
			//gotoDate:moment(initDate),
			editable : false,
			eventBackgroundColor: '#FFA200',
			textColor: '#fff',
			defaultView: calendar.viewMode,
			events : [],
			eventLimit: 3,
			timezone : 'local',
			eventClick: function (calEvent, jsEvent, view) {
				// mylog.log("calendar.eventClick", calEvent, jsEvent, view);
				onchangeClick=false;
				var link = "#page.type.events.id."+calEvent.id;
				// previewHash=link.split(".");
				var hashT=location.hash.split("?");
				var getStatus=searchInterface.getUrlSearchParams();       
				var urlHistoric=hashT[0]+"?preview=events."+calEvent.id;
				//   if($("#entity"+previewHash[4]).length > 0) setTimeout(function(){$("#entity"+previewHash[4]).addClass("active");},200); 
				if(getStatus != "") urlHistoric+="&"+getStatus; 
				history.replaceState({}, null, urlHistoric);
				urlCtrl.openPreview(link);
				//closePopovers();
				// calendar.showPopup=true;
				//popoverElement = $(jsEvent.currentTarget);
			},
			eventLimitClick : function(info){
				// mylog.log(info.date, "here bidi");
				var dateMore=info.date._d;
				var datestring = dateMore.getFullYear()+"-"+("0"+(dateMore.getMonth()+1)).slice(-2)+"-"+("0" + dateMore.getDate()).slice(-2) ;
				$(calendar.domTarget+" .fc-day").removeClass("activeDay");
				$(calendar.domTarget+".fc-day[data-date='"+datestring+"']").addClass("activeDay");
				calendar.searchInAgenda(datestring, "day");
			},
			eventRender:function(event, element, view) {
				//mylog.log("calendar.eventRender event",event,"element",element, "element", view);
				if(calendar.options.popupRender){
					var popupHtml=calendar.popupHtml(event);
					element.popover({
						html:true,
						animation: true,
						container:'body',
						title: event.name,
						template:calendar.popupTemplate(),
						placement: 'top',
						trigger: 'focus',
						content: popupHtml,
					});
					element.attr('tabindex', -1);
				}
			},
			dayClick: function(date, jsEvent, view) {
				// mylog.log("calendar.init.dayClick", date, jsEvent, view);
				// change the day's background color just for fun
				$(calendar.domTarget+" .fc-day").removeClass("activeDay");
				$(this).addClass("activeDay");
				calendar.searchInAgenda(date.format(), "day");
				agenda.dayCount = 0;
				STARTDATE = moment(date.format()).valueOf();
	            searchObject.startDate = STARTDATE;
				// alert('nextis clicked, do something');
			}
		});
		$(calendar.domTarget).fullCalendar("gotoDate", moment(Date.now()));
		calendar.bindEventCalendar();
		$("#showHideCalendar").click(function(){
			if($(this).data("hidden")){
				$(this).html("<i class='fa fa-caret-up'></i> "+trad.showcalendar+"</a>");
				$(this).data("hidden",0);  
				$(domCal).show(700);
			}else{
				$(this).html("<i class='fa fa-caret-down'></i> "+trad.showcalendar+"</a>");
				$(this).data("hidden",1);
				$(domCal).hide(700);
			}

		});  
	},
	bindEventCalendar : function(){
		$(".fc-today").addClass(".activeDay");
		// mylog.log("calendar.bindEventCalendar");
		//var popoverElement;
		$(calendar.domTarget+' .fc-prev-button').click(function(){
			var now = $(calendar.domTarget).fullCalendar('getDate');
			agenda.dayCount = 0;
			mylog.log("calendar.bindEventCalendar .fc-next-button now", now, now.format());
			//var startDDDD = new Date(now);
			STARTDATE = moment(now.format()).valueOf();
            searchObject.startDate = STARTDATE;
			calendar.searchInAgenda(now.format(), "month");
		});

		$(calendar.domTarget+' .fc-next-button').click(function(){
			var now = $(calendar.domTarget).fullCalendar('getDate');
			agenda.dayCount = 0;
			// mylog.log("calendar.bindEventCalendar .fc-next-button now", now, now.format());
			//var startDDDD = new Date(now);
			STARTDATE = moment(now.format()).valueOf();
            searchObject.startDate = STARTDATE;
			calendar.searchInAgenda(now.format(), "month");
			// alert('nextis clicked, do something');
		});
		$('.popover').mouseenter(function(){
			// alert();
			$(this).hide();
			//   calendar.showPopup=false;
		});
		$('body').off().on('click', function (e) {
			// close the popover if: click outside of the popover || click on the close button of the popover
			//alert(e.target);
			if (typeof popoverElement != "undefined" && popoverElement 
			&& ((!popoverElement.is(e.target) && popoverElement.has(e.target).length === 0 && $('.popover').has(e.target).length === 0) 
			|| (popoverElement.has(e.target) && e.target.id === 'closepopover'))) {    
				calendar.closePopovers();
			}
		});
	},
	searchInCalendar : function(startDate, endDate){
		var paramsSearchCalendar= searchInterface.constructObjectAndUrl(true);
		$.ajax({
			type: "POST",
			url: baseUrl+"/" + moduleId + "/search/geteventsforcalendar/startDate/" + startDate + "/endDate/" + endDate,
			data: paramsSearchCalendar,
			dataType: "json",
			error: function (data){
				mylog.log("calendar.php globalautocomplete error", data);
			},
			success: function(data){
				mylog.log("calendar.php globalautocomplete success", data);
				if(!data){ 
					toastr.error(data.content); 
				} else {
					if(typeof calendar != "undefined" && searchObject.initType == "events" && searchObject.text=="")
						calendar.addEvents(data.events);
				}
			}
		});
	},
	searchInAgenda : function(date, format){
		var today = new Date();
		var start = new Date(); 
		//     toTimestamp(strDate){
		var stringDate=new Date(date);
		var labelStr;
		if(format=="day"){
			searchObject.startDate=(Date.parse(date)/1000);
			searchObject.endDate=(Date.parse(date+" 23:59:59")/1000);

			if(date==today){
				labelStr=trad.today;
			}else{
				labelStr=directory.getWeekDayName(new Date(date).getDay())+" "+stringDate.getDate()+' '+directory.getMonthName(stringDate.getMonth()+1);
			}
		} else {
			labelStr=directory.getMonthName(stringDate.getMonth()+1)+" "+stringDate.getFullYear();
			searchObject.startDate=(Date.parse(date)/1000);
			var endDate = new Date(stringDate.getFullYear(), stringDate.getMonth() + 1, 0);
			searchObject.endDate=(Date.parse(endDate)/1000);

			calendar.searchInCalendar(searchObject.startDate, searchObject.endDate);
		}
		//$("#situate-day .date-label").text(labelStr);
		coInterface.scrollTo(".dayEvent");
		searchObject.nbPage=0;
		pageCount=false;
		searchObject.count=true;
		directory.scrollTop = false;
		startSearch(0);
	},
	closePopovers : function() {
		mylog.log("calendar.closePopovers ");
		calendar.showPopup=false;
		$('.popover').not(this).popover('hide');
	},
	templateRef : {
		"competition":"#ed553b",
		"concert" :"#b45f04",
		"contest":"#ed553b",
		"exhibition":"#b45f04",
		"festival":"#b45f04",
		"getTogether":"#eb4124",
		"market":"#df01a5",
		"meeting":"#eb4124",
		"course":"#df01a5",
		"workshop":"#eb4124",
		"conference":"#0073b0",
		"debate":"#0073b0",
		"film":"#2e2e2e",
		"crowdfunding":"#93be3d",
		"others":"#93be3d",
	},
	events : [],
	//dateToShow, calendar, $eventDetail, eventClass, eventCategory,
	widgetNotes : $('#notes .e-slider'), 
	sliderNotes : $('#readNote .e-slider'), 
	//oTable, 
	//contributors,
	//subViewElement, subViewContent, subViewIndex,
	tabOrganiser : [],
	showPopup : false,
	//creates fullCalendar
	buildCalObj: function(eventObj) {
		//mylog.log("calendar.buildCalObj ", eventObj);
		//entries for the calendar
		var organiser = "";
		if("undefined" != typeof eventObj["links"] && "undefined" != typeof eventObj.links["organizer"]){
			$.each(eventObj.links["organizer"], function(k, v){
				if($.inArray(k, calendar.tabOrganiser)==-1)
					calendar.tabOrganiser.push(k);
				organiser = k;
			});
		}
		var organizerName = eventObj.name;
		if(eventObj.organizer != "")
			organizerName = eventObj.organizer +" : "+ eventObj.name;
		var taskCal = null;

		if(eventObj.startDate && eventObj.startDate != "") {
			// if(typeof eventObj.startDateCal != "undefined")
			// 	var startDate = moment(eventObj.startDateCal).local();
			// else if(typeof eventObj.startDateDB != "undefined")
			// 	var startDate = moment(eventObj.startDateDB).local();
			// else
				var startDate = moment(eventObj.startDate).local().format();
			var endDate = null;
			if(eventObj.endDate && eventObj.endDate != "" ){
				// if(typeof eventObj.endDateCal != "undefined")
				// 	endDate = moment(eventObj.endDateCal).local();
				// if(typeof eventObj.endDateDB != "undefined")
				// 	endDate = moment(eventObj.endDateDB).local();
				// else
					endDate = moment(eventObj.endDate).local().format();
			}
			//mylog.log("Start Date = "+startDate+" // End Date = "+endDate);

			
			//mylog.log("calendar.eventObj", eventObj, startDate);
			taskCal = {
				"id" : (typeof eventObj.id != "undefined") ? eventObj.id : eventObj._id.$id ,
				"title" : eventObj.name,
				"content" : (eventObj.description && eventObj.description != "" ) ? eventObj.description : "",
				"start" : startDate,
				"end" : ( endDate ) ? endDate : startDate,
				"startDate" : eventObj.startDate,
				"endDate" : eventObj.endDate,
				// "startDateDB" : eventObj.startDateDB,
				// "endDateDB" : eventObj.endDateDB,
				"allDay" : eventObj.allDay,
				"className": organiser,
				"category": organiser,
				"type": eventObj.typeEvent,
				"description":eventObj.description,
				"shortDescription": eventObj.shortDescription,
				"profilMediumImageUrl": eventObj.profilMediumImageUrl,
				"adresse": eventObj.cityName,
				"links":eventObj.links,
			};

			if(eventObj.allDay )
				taskCal.allDay = eventObj.allDay;

			if(	typeof costum != "undefined" && 
            	costum != null &&
            	typeof costum.calendar != "undefined" && 
            	costum.calendar != null &&
            	typeof costum.calendar.buildCalObj == "function")
            	taskCal = costum.calendar.buildCalObj(eventObj, taskCal);
		}
		//mylog.log("calendar.buildCalObj taskCal", taskCal);
		return taskCal;
	},
	showInCalendar : function (events) {
		/*calendarObject = [];
		if(events){
		$.each(events,function(eventId,eventObj){
		eventCal = calendar.buildCalObj(eventObj);
		if(eventCal)
		calendarObject.push( eventCal );
		});
		}

		if(typeof initDate != "undefined" && notNull(initDate) ){
		splitInit=initDate.split("-");
		dateToShow = new Date(splitInit[0], splitInit[1]-1, splitInit[2]);
		}
		else{
		initDate=null;
		dateToShow = new Date();
		}*/

		//calendar.addEvents(events);
		//  calendar.bindEventCalendar();
	},
	addEvents : function(events){
		//mylog.log("calendar.addEvents", events);
		if(events){
			$.each(events,function(eventId,eventObj){
				//mylog.log("calendar.addEvents eventCal", eventCal);
				if(typeof calendar.eventsList[eventId]=="undefined"){
					var eventCal = calendar.buildCalObj(eventObj);
					//mylog.log("calendar.addEvents eventCal", eventCal);
					if(eventCal){
						calendar.eventsList[eventId]=eventCal ;
						$(calendar.domTarget).fullCalendar('renderEvent', eventCal, true);
					}
				}
			});
		}
	},
	setCategoryColor : function(tab){
		/*mylog.log("calendar.setCategoryColor ", tab);
		$(".fc-content").css("color", "white");
		for(var i =0; i<tab.length; i++){
		$("."+tab[i]+" .fc-content").css("color", "white");
		//$("."+tab[i]+" .fc-content").css("background-color", calendar.templateColor[i]);
		}*/
	},
	getRandomColor : function() {
		/*mylog.log("calendar.getRandomColor ");
		var letters = '0123456789ABCDEF'.split('');
		var color = '#';
		for (var i = 0; i < 6; i++ ) {
		color += letters[Math.floor(Math.random() * 16)];
		}
		return color;*/
	},
	popupTemplate : function(){
			//mylog.log("calendar.popupTemplate ");
			var template='<div class="popover" style="max-width:300px; no-padding" >'+
						'<div class="arrow"></div>'+
						'<div class="popover-header" style="background-color:red;">'+
						'<button id="closepopover" type="button" class="close margin-right-5" aria-hidden="true">&times;</button>'+
						'<h3 class="popover-title"></h3>'+
						'</div>'+
						'<div class="popover-content no-padding"></div>'+
						'</div>';
			return template;
	},
	popupHtml : function(data){
		//mylog.log("calendar.popupHtml ", data);
		var popupContent = "<div class='popup-calendar'>";

		var color = "orange";
		var ico = 'calendar';
		var imgProfilPath =  assetPath + "/images/thumb/default_events.png";
		if(typeof data.profilMediumImageUrl !== "undefined" && data.profilMediumImageUrl != "") 
			imgProfilPath =  baseUrl + data.profilMediumImageUrl;
		var icons = '<i class="fa fa-'+ ico + ' text-'+ color +'"></i>!!!';

		var typeElement = "events";
		var icon = 'fa-calendar';

		var onclick = "";
		var url = '#page.type.'+typeElement+'.id.'+data.id;
		//onclick = 'calendar.closePopovers();urlCtrl.loadByHash("'+url+'");';

		popupContent += "<div class='' id='popup"+data.id+"'>";
		popupContent += "<div class='main-panel'>"
						+   "<div class='col-md-12 col-sm-12 col-xs-12 no-padding'>"
						+      "<div class='thumbnail-profil' style='max-height: 200px;text-align: -webkit-center; overflow-y: hidden;background-color: #cccccc;'><img src='" + imgProfilPath + "' class='popup-info-profil-thumb img-responsive'></div>"      
						+   "</div>"
						+   "<div class='col-md-12 col-sm-12 col-xs-12 padding-5'>";

		if("undefined" != typeof data.title)
			popupContent  +=  "<div class='' style='text-transform:uppercase;'>" + data.title + "</div>";

		if(data.start != null){
			popupContent +="<div style='color:#777'>";
			var startLbl="<i class='fa fa-calendar-o'></i> ";
			var startDate=moment(data.start).format("DD MMMM YYYY"); 
			var endDate="";
			var hoursStr="<br/>";
			if(data.allDay)
				hoursStr+=tradDynForm.allday;
			else
				hoursStr+="<i class='fa fa-clock-o'></i> "+moment(data.start).format("H:mm");
			if(data.end != null){
				if(startDate != moment(data.end).format("DD MMMM YYYY")){
					startLbl+=trad.fromdate+" ";
					endDate=" "+trad.todatemin+" "+moment(data.end).format("DD MMMM YYYY");
				}
				if(!data.allDay)
					hoursStr+= " - "+moment(data.end).format("H:mm");
			}
			popupContent += startLbl+startDate+endDate+hoursStr;
			popupContent +="</div>";
		}
		popupContent += "</div>";
		//Short description
		if ("undefined" != typeof data['shortDescription'] && data['shortDescription'] != "" && data['shortDescription'] != null) {
			popupContent += "<div id='pop-description' class='popup-section'>"
			  + "<div class='popup-info-profil'>" + data['shortDescription'] + "</div>"
			+ "</div>";
		}
		popupContent += '</div>';

		popupContent += "<a href='"+url+"' target='_blank' onclick='"+onclick+"' class=''>";
		popupContent += '<div class="btn btn-sm btn-more col-md-12 col-sm-12 col-xs-12"><i class="fa fa-hand-pointer-o"></i> en savoir +</div>';
		popupContent += '</a>';

		return popupContent;
	}
};
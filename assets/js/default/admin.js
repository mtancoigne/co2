/* eslint-disable no-global-assign */
/* eslint-disable no-mixed-spaces-and-tabs */
/* eslint-disable quotes */
/* eslint-disable indent */
/* global $ jQuery toastr bootbox searchInterface smallMenu urlCtrl trad tradCategory tradDynForm rolesList directory dataHelper dyFObj dyFInputs links typeObj paramsAdmin coInterface notNull onchangeClick notEmpty baseUrl moduleId ajaxPost costum mylog filterObj initType delay searchObject globalCtx getAjax*/

var adminPanel = {
	params:{},
	init : function(){
		if(typeof paramsAdmin.add != "undefined")
			adminPanel.initAddButton(".contain-admin-add");
		adminPanel.bindButtonMenu();
		adminPanel.initView();	
	},
	initAddButton : function(domContain){
		var menuButtonCreate="";
	    $.each(typeObj, function(e,v){
	        if(typeof v.add != "undefined" && v.add){
                var hash=(typeof v.hash != "undefined") ? v.hash : "javascript:;";
                var formType=(typeof v.formType != "undefined") ? 'data-form-type="'+v.formType+'" ' : "";
                var subFormType= (typeof v.subFormType != "undefined") ? 'data-form-subtype="'+v.subFormType+'" ' : "";
                var addClass = (typeof v.class != "undefined") ? v.class : "";
                var nameLabel=(typeof v.addLabel!= "undefined") ? v.addLabel : v.name;
                menuButtonCreate+='<button '+ 
                    formType+
                    subFormType+ 
                    'class="btn btn-link btn-open-form col-xs-6 col-sm-6 col-md-4 col-lg-4 text-'+v.color+' margin-bottom-10">'+ 
                        '<h6><i class="fa fa-'+v.icon+'"></i><br/>'+
                        nameLabel+'</h6>'+
                    '</button>';
	        }
	    });
	    $(domContain).html(menuButtonCreate);
	},
	initView: function(){
		adminPanel.initNav();
		if(adminPanel.params.view!=""){
			adminPanel.views[adminPanel.params.view]();
		}else{
			adminPanel.views.index();
		}
	},
	initNav:function(){
		coInterface.showLoader("#content-view-admin");
		if(adminPanel.params.view=="index" || adminPanel.params.view==""){
			$("#goBackToHome, #content-view-admin").hide(700);
			$("#navigationAdmin").show(700);
			//$("#goBackToHome .addServices, #goBackToHome .show-form-new-circuit").hide(700);
		} else {
			$("#navigationAdmin").hide(700);
			$("#goBackToHome, #content-view-admin").show(700);
		}
	},
	bindViewActionEvent:function(){
		$(".btnNavAdmin").off().on("click", function(){
			adminPanel.params.view = $(this).data("view");
			adminPanel.params.dir = $(this).data("dir");	
			if(!notNull($(this).data("action"))){
				onchangeClick=false;
				adminPanel.initNav();
				var hashAdmin=adminPanel.params.hashUrl;
				var indexView=(notEmpty(adminPanel.params.subView)) ? "subview" : "view";
				if(notEmpty(adminPanel.params.view) && adminPanel.params.view!="index") hashAdmin+="."+indexView+"."+$(this).data("view");
				location.hash=hashAdmin;
			}
			//Get view admin
			adminPanel.views[adminPanel.params.view]();
		});
	},
	bindButtonMenu : function(){
		adminPanel.bindViewActionEvent();
		coInterface.bindButtonOpenForm();
	},
	views : {
		index: function(){
			adminPanel.initNav();
		},
		directory : function(){ 
			var searchAdminType=["citoyens","projects"];
			var data={initType:searchAdminType};
			ajaxPost('#content-view-admin', baseUrl+'/'+moduleId+'/admin/directory', data, function(){},"html");
		
		},
		customCommunity:function(){
			ajaxPost('#content-view-admin', baseUrl+'/'+moduleId+'/admin/community/id/'+costum.contextId+'/type/'+costum.contextType, null, function(){},"html");
		},
		community:function(){
			ajaxPost('#content-view-admin', baseUrl+'/'+moduleId+'/admin/community/id/'+costum.contextId+'/type/'+costum.contextType, null, function(){},"html");
		},
		reference : function(){
			var searchAdminType=(typeof paramsAdmin != "undefined" 
			&& typeof paramsAdmin["reference"] != "undefined"
			&& typeof paramsAdmin["reference"]["initType"] != "undefined") ? paramsAdmin["reference"]["initType"]: ["organizations", "events", "projects"];
			var data={initType:searchAdminType};
			ajaxPost('#content-view-admin', baseUrl+'/'+moduleId+'/admin/reference', data, function(){},"html");
		},
		log : function(){
			ajaxPost('#content-view-admin', baseUrl+'/'+moduleId+'/log/monitoring', null, function(){},"html");
		},
		moderate : function(){
			ajaxPost('#content-view-admin', baseUrl+'/'+moduleId+'/admin/moderate/one', null, function(){},"html");
		},
		addData:function(){
			ajaxPost('#content-view-admin', baseUrl+'/'+moduleId+'/adminpublic/adddata', null, function(){},"html");
		},
		import: function(){
			ajaxPost('#content-view-admin', baseUrl+'/'+moduleId+'/adminpublic/createfile', null, function(){},"html");
		},
		mailerror : function(){
			ajaxPost('#content-view-admin', baseUrl+'/'+moduleId+'/admin/mailerrordashboard', null, function(){},"html");
		},
		notsendmail : function(){
			ajaxPost('#content-view-admin', baseUrl+'/'+moduleId+'/admin/notsendmail', null, function(){},"html");
		},
		mailslist : function(){ 
			ajaxPost('#content-view-admin', baseUrl+'/'+moduleId+'/adminpublic/mailslist', null, function(){},"html");
		},
		statistic : function(){
				ajaxPost('#content-view-admin', baseUrl+'/'+moduleId+'/app/info/p/stats', null, function(){},"html");
		}
	}
};

var adminDirectory = {
	container : "adminDirectory",
	context: {},
	timeoutAddCity : null,
	paramsSearch : {
		text:null,
		page:"",
		types:[]
	},
	panelAdmin : {},
	results : {},
	countTotal : 0,
	modals : {},
	actionsStr : false,
	url : null,
	setType:[],
	filtersObj : null,
	init : function(pInit){
		var pInit = (typeof pInit !== 'undefined') ? pInit : null;
		mylog.log("adminDirectory.init ",pInit);
		//Init variable
		var copyAdmin = jQuery.extend(true, {}, adminDirectory);
		copyAdmin.initVar(pInit);
		return copyAdmin;

	},
	initVar : function(pInit){
		var pInit = (typeof pInit !== 'undefined') ? pInit : null;
		mylog.log("adminDirectory.initVar", pInit);
		var aObj = this;
		aObj.container = ( ( pInit != null && typeof pInit.container != "undefined" ) ? pInit.container : "adminDirectory" );
		aObj.results = ( ( pInit != null && typeof pInit.results != "undefined" ) ? pInit.results : {} );
		aObj.panelAdmin = ( ( pInit != null && typeof pInit.panelAdmin != "undefined" ) ? pInit.panelAdmin : {} );
		aObj.context = ( ( typeof aObj.panelAdmin.context != "undefined" ) ? aObj.panelAdmin.context : {} );
		aObj.paramsSearch = aObj.panelAdmin;
		if( typeof pInit.panelAdmin.types != "undefined" ){
			aObj.paramsSearch.types =  pInit.panelAdmin.types;
			aObj.setType =  pInit.panelAdmin.types;
		}
		if(notEmpty(pInit.panelAdmin.forced)){
			$.each(pInit.panelAdmin.forced, function(e, v){
				aObj.paramsSearch[e]=v;
			});
		}

		aObj.countTotal = 0 ;
		$.each(aObj.results.count, function(key, values){
			aObj.countTotal += values;
		});
		

		aObj.initView();
		aObj.initViewTable();
		aObj.initPageTable();
		aObj.initTable();
		aObj.initModals();
		aObj.initFilters(aObj);
		if(typeof aObj.bindCostum == "function")
			aObj.bindCostum(aObj);

		aObj.filtersObj = ( ( typeof aObj.panelAdmin.paramsFilter != "undefined" ) ? filterObj.init(aObj.panelAdmin.paramsFilter) : null );
		if(typeof aObj.filtersObj != "undefined" && aObj.filtersObj != null){
			aObj.filtersObj.search = function(){
				aObj.search(0);
			};
		}
		
	},
	initView : function(){
		mylog.log("adminDirectory.initView");
		var aObj = this;
		var str = "";
		if(typeof aObj.panelAdmin.title != "undefined" && aObj.panelAdmin.title != null)
			str += '<div class="col-xs-12 padding-10 text-center"><h2>'+aObj.panelAdmin.title+'</h2></div>';
		str += '<div class="panel-heading border-light padding-10">';
		if(typeof initType != "undefined" && initType != null && initType.length > 1 ){
			str += 	'<h4 class="panel-title padding-10">Filtered by types : </h4>'+
					'<div class="panel-heading border-light padding-10">'+
						'<a href="javascript:;" class="btn btn-xs btn-default filterByType" data-type="">Tous</a>';
				$.each(initType, function(kIT, valIT){
					str += '<a href="javascript:;" class="filter'+valIT+' filterByType btn btn-xs btn-default" data-type="'+valIT+'"><span class="badge badge-warning countPeople" id="count'+valIT+'">'+valIT+' '+aObj.results['count'][valIT]+'</span></a>';
				});
			str += '</div>';
		}
		if(typeof aObj.panelAdmin.headerFilters != "undefined"){
			if(typeof aObj.panelAdmin.headerFilters.private != "undefined"){
				str +='<div class="col-xs-12 padding-10 margin-bottom-10">'+
					'<a href="javascript:;" class="privateFilter btn btn-xs btn-default" data-value="">Tous</span></a>'+
					'<a href="javascript:;" class="privateFilter btn btn-xs btn-default" data-value="true">Ajouts non validés (privé)</span></a>'+
				'</div>';
			}
		}
			
		str += 	'<div class="panel-heading border-light padding-10">'+
					'<input type="text" class="form-control col-xs-12" id="search" placeholder="'+trad.searchbyname+'">'+
				'</div>'+
			'</div>';
		if(typeof aObj.panelAdmin != "undefined" && aObj.panelAdmin != null &&
			typeof aObj.panelAdmin.paramsFilter != "undefined" && aObj.panelAdmin.paramsFilter != null ){
			str += 	'<div class="col-xs-12 " id="filterContainer"></div>';
		}
		
		str += 	'<div class="col-xs-12 " style="padding-left : 15px; padding-top: 15px">';
			str += 	'<h4><span class="no-padding pull-left" id="countTotal">'+aObj.countTotal+' </span>  <span class="no-padding pull-left margin-left-10"> Résultats</span> </h4>';
		if(typeof aObj.panelAdmin.csv != "undefined" && aObj.panelAdmin.csv != null){
			$.each(aObj.panelAdmin.csv, function(kCSV, valCSV){
				mylog.log("adminDirectory.initView each csv", kCSV, valCSV);

					if(kCSV == "urls"){
						$.each(valCSV, function(kURL, valURL){
							if(typeof valURL.url != "undefined")
								str += '<a class="pull-right" href="'+valURL.url+'" target="_blank"><i class="fa fa-2x fa-table text-green"></i></a>';
						});
					}else{
						var urlCsv = baseUrl+'/api/'+kCSV+'/get/format/csv';
						if(typeof valCSV == "object" ){
							$.each(valCSV, function(kvalCSV, valvalCSV){
								urlCsv += '/'+kvalCSV+'/'+valvalCSV;
							});
						}
						str += '<a class="pull-right" href="'+urlCsv+'" target="_blank"><i class="fa fa-2x fa-table text-green"></i></a>';
				
					}
					
			});
		}

		if(typeof aObj.panelAdmin.pdf != "undefined" && aObj.panelAdmin.pdf != null){
			$.each(aObj.panelAdmin.pdf, function(kPDF, valPDF){
				mylog.log("adminDirectory.initView each pdf", kPDF, valPDF);

					if(kPDF == "urls"){
						$.each(valPDF, function(kURL, valURL){
							mylog.log("adminDirectory.initView each valPDF", kURL, valURL);
							if(typeof valURL.url != "undefined")
								str += '<a class="pull-right" href="'+valURL.url+'" target="_blank"><i class="fa fa-2x fa-file text-red"></i></a>';
						});
					}
					
			});
		}

		if(typeof aObj.panelAdmin.invite != "undefined" && aObj.panelAdmin.invite != null && aObj.panelAdmin.invite == "true"){
			//str += '<a href="" class="btn btn-success btn-xs pull-right lbhp"><i class="fa fa-user-plus"></i> Ajouter des membres</a>';
			str +='<button id="btnInvite" data-type="'+costum.contextType+'" data-id="'+costum.contextId+'" class="btn btn-success btn-xs pull-right lbhp"><i class="fa fa-user-plus"></i> Ajouter des membres</button>';

		}
		str +='</div>';
		str += 	'<div class="pageTable col-xs-12 padding-10"></div>';
		
		str +=	'<div class="panel-body">'+
			'<div>'+
				'<table style="table-layout: fixed; word-wrap: break-word;" class="table table-striped table-bordered table-hover directoryTable" id="panelAdmin">'+
					'<thead>'+
						'<tr id="headerTable">'+
						'</tr>'+
					'</thead>'+
					'<tbody class="directoryLines">'+
						
					'</tbody>'+
				'</table>'+
			'</div>'+
		'</div>'+
		'<div class="pageTable col-md-12 col-sm-12 col-xs-12 padding-20"></div>';

		$("#"+this.container).html(str);
	},
	initTable : function(e){
		mylog.log("adminDirectory.initTable ", e);
		var aObj = this ;
		var str = "" ;
		if(	typeof aObj.panelAdmin.table != "undefined" && 
			aObj.panelAdmin.table != null ){
			//mylog.log("adminDirectory.initTable table ", aObj.panelAdmin.table);
			$.each(aObj.panelAdmin.table, function(key, value){ 
				mylog.log("adminDirectory.initTable each ", key, value, (key != "actions"));
				if(key != "actions"){
					mylog.log("adminDirectory.initTable if ");
					if(value === "true")
						str +="<th>"+key+"</th>";
					else if(typeof value != "undefined" && typeof value.name != "undefined"){
						var classCol = ((typeof value.class != "undefined") ? value.class : " text-center ");
						str +="<th class='"+classCol+"'>"+value.name+"</th>";
					}
				}
				
			});
			
		}

		if(	typeof aObj.panelAdmin.table != "undefined" && 
			aObj.panelAdmin.actions != null && 
			aObj.actionsStr === true) {
			var classCol = 'col-xs-1 text-center';
			if(typeof aObj.panelAdmin.table.actions != "undefined" ){
				if(typeof aObj.panelAdmin.table.actions.class != "undefined")
					classCol = aObj.panelAdmin.table.actions.class ;
			}
			str +="<th class='"+classCol+"'>Actions</th>";
		}
		$("#"+aObj.container+" #headerTable").append(str) ;
		coInterface.bindLBHLinks();
	},
	columTable : function(e, id, collection){
		mylog.log("adminDirectory.columTable ", e, id, collection);
		var str = "" ;
		var aObj = this ;
		if(	typeof aObj.panelAdmin.table != "undefined" && 
			aObj.panelAdmin.table != null ){

			$.each(aObj.panelAdmin.table, function(key, value){
				if(key != "actions"){
					str += '<td class="center '+key+'">';
					// if(key == "pdf" && value === "true"){
					// 	
					// } else if(key == "validated" && value === "true") {
					// 	str += aObj.values.validated(e, id, type, aObj);
					// } else if(key == "status" && value === "true") {
					// 	str += aObj.values.status(e, id, type, aObj);
					// } else 
					if(typeof aObj.values[key] != "undefined" && value === "true") {
						str += aObj.values[key](e, id, collection, aObj);
					} else if(typeof aObj.values[key] != "undefined") {
						str += aObj.values[key](e, id, collection, aObj, value);
					}
					str += '</td>';
				}
			});
		}
		//mylog.log("adminDirectory.columTable ", str);
		return str ;
	},
	initModals : function(){
		mylog.log("adminDirectory.initModals");
		var aObj = this;
		var str = "";
		if(	typeof aObj.modals != "undefined" && 
			aObj.modals != null && 
			Object.keys( aObj.modals ).length > 0 ) {

			$.each(aObj.modals, function(kM, valM){
				str += aObj.modals[kM](aObj) ;
			});
		}
		$("#"+this.container).after(str);
	},
	initFilters : function(aObj){
			if($("#"+aObj.container+" .privateFilter").length > 0){
				$("#"+aObj.container+" .privateFilter").off().on("click",function(){
					$("#"+aObj.container+" .privateFilter").removeClass("active");
					$(this).addClass("active");
					if(notNull($(this).data("value")) && notEmpty($(this).data("value")))
						aObj.paramsSearch.private=true;
					else
						delete aObj.paramsSearch.private;
					aObj.search();
				});

			}
			if($("#"+aObj.container+" #search").length > 0){
				$("#"+aObj.container+" #search").off().on("keyup", delay(function (e) {
					searchObject.text = $(this).val().toLowerCase();
					if(notNull(aObj.timeoutAddCity)) 
						clearTimeout(aObj.timeoutAddCity);
					aObj.timeoutAddCity = setTimeout(function(){
						aObj.paramsSearch.page = 1;
						aObj.search();
					}, 500);
					
				}, 750));
			}
			if($("#"+aObj.container+" .filterByType").length > 0){
				$("#"+aObj.container+" .filterByType").off().on('click', function(){
					$("#"+aObj.container+" .filterByType").removeClass("active");
					$(this).addClass("active");
					if(notNull($(this).data("type")) && notEmpty($(this).data("type")))
						aObj.paramsSearch.types=[$(this).data("type")];
					else
						aObj.paramsSearch.types=aObj.setType;
					aObj.search();
				});
			}
	},
	getElt : function(id, type){
		mylog.log("adminDirectory.getElt", id, type);
		return this.results[type][id] ;
	},
	setElt : function(elt, id, type){
		mylog.log("adminDirectory.setElt", id, type);
		this.results[type][id] = elt;
	},
	values : {
		type : function(e, id, type, aObj, params){
			mylog.log("adminDirectory.values.type", e, id, type);
			var img = "";
			if (e && typeof e.profilThumbImageUrl != "undefined" && e.profilThumbImageUrl!="")
				img = '<img width="50" height="50" alt="image" class="img-circle" src="'+baseUrl+e.profilThumbImageUrl+'"><br/><span class="uppercase bold">'+type+"</span>";
			else 
				img = '<i class="fa '+icon+' fa-2x"></i> <br/><span class="uppercase bold">'+type+"</span>";
			var lbhN = "lbh" ;
			var urlH='#page.type.'+type+'.id.'+id;
			var targetLink="target='_blank'";
			if(typeof params.preview !="undefined" && params.preview){
				lbhN =  "lbh-preview-element";
				targetLink="";
			}
			if(typeof params.notLink !="undefined" && params.notLink){
				lbhN="";
				urlH="javascript:;";
			}
			var str = 	'<a href="'+urlH+'" class="col-xs-12 no-padding text-center '+lbhN+'" '+targetLink+'>'+
							img +
						'</a>';
			return str;
		},
		name : function(e, id, type, aObj, params){
			mylog.log("adminDirectory.values.name", e, id, type);
			var title;
			if(typeof e.name != "undefined")
				title=e.name;
			else if(typeof e.text != "undefined")
				title=e.text;
			else if(typeof e.title != "undefined")
				title=e.title;
			
			//var lbhN = "lbh" ;
			var lbhN = "" ;
			var urlH='#page.type.'+type+'.id.'+id;
			var targetLink="target='_blank'";
			if(typeof params.preview !="undefined" && params.preview){
				targetLink="";
				lbhN =  "lbh-preview-element";
			}
			if(typeof params.notLink !="undefined" && params.notLink){
				lbhN="";
				urlH="javascript:;";
			}
			
			var str = '<a href="'+urlH+'" class="'+lbhN+'" '+targetLink+'>'+title+'</a>';
			return str;
		},
		category : function(e, id, type, aObj){
			mylog.log("adminDirectory.values.name", e, id, type);
			var category="";
			if(typeof e.category != "undefined")
				category=(typeof tradCategory[e.category] != "undefined") ? tradCategory[e.category] : e.category;
			var str="<span class=''>"+category+"</span>";
			return str;
		},
		creator : function(e, id, type, aObj){
			mylog.log("adminDirectory.values.name", e, id, type);
			var str = '';
			if(typeof e.creator != "undefined"){
				var idCreator, nameCreator;
				if(typeof e.creator == "string"){
					idCreator=e.creator;
					nameCreator=e.creator;
				}else{
					idCreator=e.creator.id;
					nameCreator=e.creator.name;
				}
				str='<a href="#page.type.citoyens.id.'+idCreator+'" class="lbh-preview-element">'+nameCreator+'</a>';	
			}
			else
				str="<span class=''></span>";
			return str;
		},
		tags : function(e, id, type, aObj){
			mylog.log("adminDirectory.values.name", e, id, type);
			var tagsStr="";
			if(notEmpty(e.tags)){
				$.each(e.tags, function(e,v){
					tagsStr+="<span class='badge bg-white text-red shadow2'>"+v+"</span>";
				});
			}
			var str = '<div>'+tagsStr+'</div>';
			return str;
		},
		description : function(e, id, type, aObj){
			mylog.log("adminDirectory.values.name", e, id, type);
			var description="";
			if(typeof e.description != "undefined")
				description=e.description;
			var str = '<span>'+description+'</span>';
			return str;
		},
		email : function(e, id, type, aObj){
			mylog.log("adminDirectory.values.email", e, id, type);
			var str = ( ( typeof e.email != "undefined" ) ? e.email : "" );
			return str;
		},
		pdf : function(e, id, type, aObj){
			return '<a href="'+baseUrl+'/co2/export/pdfelement/id/'+id+'/type/'+type+'/" data-id="'+id+'" data-type="'+type+'" class="margin-right-5" target="_blank"> <i class="fa fa-2x fa-file-pdf-o text-red" ></i> </a> ';
		},
		tobeactivated : function(e, id, type, aObj){
			mylog.log("adminDirectory.values.tobeactivated", e, id, type);
			var tobeactivated=( typeof e.roles != "undefined" && typeof e.roles.tobeactivated != "undefined" && e.roles.tobeactivated == true) ? true : false;
			var str = "";
			if(tobeactivated)
				str = "<span class='badge bg-orange'><i class='fa fa-check'></i> En attente de Validation  </span>";
			else
				str = "<span class='badge bg-green-k'><i class='fa fa-check'></i> Compte validé</span>";
			return str;
		},
		validated : function(e, id, type, aObj){
			//mylog.log("adminDirectory.values.validated", e, id, type);
			var isValidated=( typeof e.source != "undefined" && typeof e.source.toBeValidated != "undefined" && typeof e.source.toBeValidated[costum.slug] != "undefined" && e.source.toBeValidated[costum.slug] == true) ? false : true;
			var str = "";
			if(!isValidated)
				str = "<span class='badge bg-orange'><i class='fa fa-check'></i> En attente de validation</span>";
			else
				str = "<span class='badge bg-green-k'><i class='fa fa-check'></i> Groupe validé</span>";
			return str;
		},
		private : function(e, id, type, aObj){
			mylog.log("adminDirectory.values.private", e, id, type);
			var str = "";
			if( typeof e.preferences != "undefined" && 
				typeof e.preferences.private != "undefined" && 
				e.preferences.private === true ){
				str = '<span id="private'+id+'" class="label label-danger"> Privé </span>';
			}else{
				str = '<span id="private'+id+'" class="label label-success"> Public </span>';
			}
			return str;
		},
		status : function(e, id, type, aObj){
			mylog.log("adminDirectory.values.status", e, id, type);
			var value=( typeof e.source != "undefined" && typeof e.source.status != "undefined" && typeof e.source.status[costum.slug] != "undefined" && e.source.status[costum.slug] != null) ? e.source.status[costum.slug] : "";
			mylog.log("adminDirectory.values.value", value);
			var str = "";
			str = "<span class=''>"+value+"</span>";
			return str;
		},
		roles : function(e, id, type, aObj){
			mylog.log("adminDirectory.values.roles", e, id, type, aObj);
			var value= "";
			if( typeof aObj.context != "undefined" &&
				typeof aObj.context.id != "undefined" &&
				typeof aObj.context.collection != "undefined" &&
				typeof e.links != "undefined" && 
				typeof e.links[ links.getConnect( type, aObj.context.collection ) ] != "undefined" && 
				typeof e.links[ links.getConnect( type, aObj.context.collection ) ][ aObj.context.id ] != "undefined" && 
				typeof e.links[ links.getConnect( type, aObj.context.collection ) ][ aObj.context.id ].roles != "undefined"){
				var i = 0;
				$.each(e.links[ links.getConnect( type, aObj.context.collection ) ][aObj.context.id].roles, function(kR, valR){
					if(i > 0)
						value += "<br>";
					value += valR;
					i++;
				});
			}
				
			mylog.log("adminDirectory.values.roles value", value);
			var str = "";
			str = "<span class=''>"+value+"</span>";
			return str;
		},
		isInviting : function(e, id, type, aObj){
			mylog.log("adminDirectory.values.isInviting", e, id, type, aObj);
			var value= trad.yes;
			if( typeof aObj.context != "undefined" &&
				typeof aObj.context.id != "undefined" &&
				typeof aObj.context.collection != "undefined" &&
				typeof e.links != "undefined" &&
				typeof links != "undefined" && 
				typeof links.linksTypes != "undefined" &&
				typeof links.linksTypes["citoyens"] != "undefined" &&
				typeof links.linksTypes["citoyens"][aObj.context.collection] != "undefined" &&
				typeof e.links[links.linksTypes["citoyens"][aObj.context.collection] ] != "undefined" && 
				typeof e.links[links.linksTypes["citoyens"][aObj.context.collection] ][aObj.context.id] != "undefined" && 
				typeof e.links[links.linksTypes["citoyens"][aObj.context.collection] ][aObj.context.id].isInviting != "undefined"&& 
				e.links[links.linksTypes["citoyens"][aObj.context.collection] ][aObj.context.id].isInviting === true ) 
				value = trad.no ;
			mylog.log("adminDirectory.values.value", value);
			var str = "";
			str = "<span class=''>"+value+"</span>";
			return str;
		},
		admin : function(e, id, type, aObj){
			mylog.log("adminDirectory.values.admin", e, id, type, aObj);
			var value= trad.no;
			if( typeof aObj.context != "undefined" &&
				typeof aObj.context.id != "undefined" &&
				typeof aObj.context.collection != "undefined" &&
				typeof e.links != "undefined" &&
				typeof links != "undefined" && 
				typeof links.linksTypes != "undefined" &&
				typeof links.linksTypes["citoyens"] != "undefined" &&
				typeof links.linksTypes["citoyens"][aObj.context.collection] != "undefined" &&
				typeof e.links[links.linksTypes["citoyens"][aObj.context.collection] ] != "undefined" && 
				typeof e.links[links.linksTypes["citoyens"][aObj.context.collection] ][aObj.context.id] != "undefined" && 
				typeof e.links[links.linksTypes["citoyens"][aObj.context.collection] ][aObj.context.id].isAdmin != "undefined"&& 
				e.links[links.linksTypes["citoyens"][aObj.context.collection] ][aObj.context.id].isAdmin === true) 
				value = trad.yes ;
			mylog.log("adminDirectory.values.value", value);
			var str = "";
			str = "<span class=''>"+value+"</span>";
			return str;
		},
		project : function(e, id, type, aObj){
			var str = "";
			if(typeof e.project != "undefined"){
				str = '<a href="#page.type.'+e.project.type+'.id.'+e.project.id+'" class="" target="_blank">'+e.project.name+'</a>';
			} 
			return str;		
		},
		organization : function(e, id, type, aObj){
			var str = "";
			if(typeof e.organization != "undefined"){
				str = '<a href="#page.type.'+e.organization.type+'.id.'+e.organization.id+'" class="lbh" target="_blank">'+e.organization.name+'</a>';
			} 
			return str;		
		},
		comment : function(e, id, type, aObj){
			var str = "";
			str += 	'<center><a href="javascript:;" class="btn btn-primary openAnswersComment commentBtn" data-id="'+id+'" data-type="'+type+'" >'+
						e.countComment+' <i class="fa fa-commenting"></i>'+
						//'<?php echo PHDB::count(Comment::COLLECTION, array("contextId"=>(string)$v['_id'],"contextType"=>Form::ANSWER_COLLECTION)); ?> <i class="fa fa-commenting"></i>'+
					'</a></center>';
			return str; 
		}
	},
	actions : {
		init : function(e, id, type, aObj){
			mylog.log("adminDirectory.actions.init", e, id, type, aObj);
			var str = "";
			if( typeof aObj.panelAdmin.actions != "undefined" && 
				aObj.panelAdmin.actions != null &&
				Object.keys(aObj.panelAdmin.actions).length == 1 ) {
				mylog.log("adminDirectory.actions.init 1");
				 str = aObj.actions.get(e, id, type, false, aObj);
			}else{
				mylog.log("adminDirectory.actions.init 2");
				var actionsStr = aObj.actions.get(e, id, type, true, aObj);
				if(actionsStr != ""){
					str = '<a href="#" data-toggle="dropdown" class="btn btn-danger dropdown-toggle btn-sm">'+
							'<i class="fa fa-cog"></i> <span class="caret"></span>'+
						'</a>'+
						'<ul class="dropdown-menu pull-right dropdown-dark" role="menu">'+
							actionsStr +
						'</ul>';
				}
				
			}
			mylog.log("adminDirectory.actions.init end ", str);
			return str ;
		},
		get : function(e, id, type, multiple, aObj){
			mylog.log("adminDirectory.actions.get", e, id, type, multiple, aObj);
			var str = "";

			if( typeof aObj.panelAdmin.actions != "undefined" && 
				aObj.panelAdmin.actions != null &&
				Object.keys( aObj.panelAdmin.actions ).length > 0 ){
				$.each(aObj.panelAdmin.actions, function(key, value){

					if(multiple === "true")
						str += "<li>";
					mylog.log("adminDirectory.actions.get key value", key, value);
					if(key == "validated" && value === "true"){
						str += aObj.actions.validated(e, id, type, aObj);	
					} else if(key == "status" && value === "true"){
						str += aObj.actions.status(e, id, type, aObj);	
					} else if(typeof aObj.actions[key] != "undefined") {
						str += aObj.actions[key](e, id, type, aObj);
					}

					if(multiple === "true")
						str += "</li>";
				});
			}
			
			mylog.log("adminDirectory.actions.get str ", str);
			return str ;
		},
		pdf : function(e, id, type, aObj){
			mylog.log("adminDirectory.actions.pdf ");
			return '<a href="'+baseUrl+'/co2/export/pdfelement/id/'+id+'/type/'+type+'/" data-id="'+id+'" data-type="'+type+'" class="col-xs-12 btn bg-green-k" target="_blank"> <i class="fa fa-file-pdf-o text-red" ></i> PDF </a> ';
		},
		validated : function(e, id, type, aObj){
			var str = "" ;
			var isValidated=( typeof e.source != "undefined" && typeof e.source.toBeValidated != "undefined" && typeof e.source.toBeValidated[costum.slug] != "undefined" && e.source.toBeValidated[costum.slug] == true) ? false : true;
			if(!isValidated){
				str ='<button data-id="'+id+'" data-type="'+type+'" data-valid="true" class="col-xs-12 validateSourceBtn btn bg-green-k text-white"><i class="fa fa-check"></i> Valider le groupe</button>';
			} else {
				str ='<button data-id="'+id+'" data-type="'+type+'" data-valid="false" class="col-xs-12 validateSourceBtn btn bg-red-k text-white"><i class="fa fa-trash"></i> Enlever la validation du groupe</button>';
			}
			return str ;
		},
		status : function(e, id, type, aObj){
			mylog.log("adminDirectory.actions.status", e, id, type);
			var str ='<button data-id="'+id+'" data-type="'+type+'" class="col-xs-12 statusBtn btn bg-green-k text-white">Modifier le statut</button>';
			
			return str ;
		},
		private : function(e, id, type, aObj){
			mylog.log("adminDirectory.actions.private", e, id, type);
			var val = false ;
			if( typeof e.preferences != "undefined" && 
				typeof e.preferences.private != "undefined" && 
				e.preferences.private === true ){
				val = true;
			}

			var str ='<button data-id="'+id+'" data-type="'+type+'" data-private="'+val+'" data-path="preferences.private" class="col-xs-12 privateBtn btn bg-green-k text-white">Rendre public</button>';
			
			return str ;
		},
		roles : function(e, id, type, aObj){
			mylog.log("adminDirectory.actions.roles", e, id, type);
			var str ='<button data-id="'+id+'" data-type="'+type+'" class="col-xs-12 rolesBtn btn bg-green-k text-white">Modifier les roles</button>';
			
			return str ;
		},
		admin : function(e, id, type, aObj){
			mylog.log("adminDirectory.actions.admin", e, id, type);

			var isAdmin = false;
			var val = "Ajouté en tant qu'admin";

			if( typeof aObj.context != "undefined" &&
				typeof aObj.context.id != "undefined" &&
				typeof aObj.context.collection != "undefined" &&
				typeof e.links != "undefined" &&
				typeof links != "undefined" && 
				typeof links.linksTypes != "undefined" &&
				typeof links.linksTypes["citoyens"] != "undefined" &&
				typeof links.linksTypes["citoyens"][aObj.context.collection] != "undefined" &&
				typeof e.links[links.linksTypes["citoyens"][aObj.context.collection] ] != "undefined" && 
				typeof e.links[links.linksTypes["citoyens"][aObj.context.collection] ][aObj.context.id] != "undefined" && 
				typeof e.links[links.linksTypes["citoyens"][aObj.context.collection] ][aObj.context.id].isAdmin != "undefined"&& 
				e.links[links.linksTypes["citoyens"][aObj.context.collection] ][aObj.context.id].isAdmin === true){
				isAdmin = true ;
				val = "Supprimé en tant qu'admin";
			}
			var str ='<button data-id="'+id+'" data-type="'+type+'"  data-isadmin="'+isAdmin+'" class="adminBtn btn bg-green-k text-white">'+val+'</button>';
			
			return str ;
		},
		update : function(e, id, type, aObj){
			mylog.log("adminDirectory.actions.update", e, id, type, aObj);
			var subtype = "";
			if( typeof aObj.panelAdmin != "undefined" && 
				typeof aObj.panelAdmin.actions != "undefined" && 
				typeof aObj.panelAdmin.actions.update != "undefined" && 
				typeof aObj.panelAdmin.actions.update.subType != "undefined" )
				subtype =  ' data-subtype="'+aObj.panelAdmin.actions.update.subType+'" ';
			mylog.log("adminDirectory.actions.update type", type);
			var str ='<button data-id="'+id+'" data-type="'+type+'" '+subtype+' class="col-xs-12 updateBtn btn bg-green-k text-white"><i class="fa fa-pencil"></i> Modifier</button>';
			
			return str ;
		},
		delete : function(e, id, type, aObj){
			mylog.log("adminDirectory.actions.update", e, id, type, aObj);

			var str ='<button data-id="'+id+'" data-type="'+type+'" class="col-xs-12 deleteBtn btn bg-red text-white"><i class="fa fa-trash"></i> Supprimer</button>';
			
			return str ;
		},
		deleteUser : function(e, id, type, aObj){
			mylog.log("adminDirectory.actions.deleteUser", e, id, type, aObj);

			var str ='<button data-id="'+id+'" data-type="'+type+'" class="col-xs-12 deleteUserBtn btn bg-red text-white"><i class="fa fa-trash"></i> Supprimer</button>';
			
			return str ;
		},
		banUser : function(e, id, type, aObj){
			mylog.log("adminDirectory.actions.banUser", e, id, type, aObj);
			var label = "Bannir l'utilisateur";
			if( typeof e.roles != "undefined" && typeof e.roles.isBanned != "undefined" )
				label = "Enlèver le bannissement";
			var str = '<button data-id="'+id+'" data-type="'+type+'" class="col-xs-12 banUserBtn btn bg-red text-white"><i class="fa fa-trash"></i> '+label+'</button>';
			return str ;
		},
		switchToUser : function(e, id, type, aObj){
			mylog.log("adminDirectory.actions.switch2User", e, id, type, aObj);
			var str='<button data-id="'+id+'" data-type="'+type+'" class="switch2UserThisBtn"><span class="fa-stack"><i class="fa fa-user fa-stack-1x"></i><i class="fa fa-eye fa-stack-2x stack-right-bottom text-danger"></i></span> Switch to this user</button>';
			return str ;
		},
		disconnect : function(e, id, type, aObj){
			mylog.log("adminDirectory.actions.disconnect", e, id, type, aObj);
			var str='<button data-id="'+id+'" data-type="'+type+'" class="btnDisconnect"><span class="fa fa-stack"></span> Supprimer le lien</button>';
			return str ;
		},
		relaunchInviation : function(e, id, type, aObj){
			mylog.log("adminDirectory.actions.disconnect", e, id, type, aObj);
			var tobeactivated=( typeof e.roles != "undefined" && typeof e.roles.tobeactivated != "undefined" && e.roles.tobeactivated == true) ? true : false;
			var str="";
			if(tobeactivated == true)
				str='<button data-id="'+id+'" data-type="'+type+'" class="btnRelaunchInviation"><span class="fa fa-stack"></span> Relancer l\'invitation</button>';
			return str ;
		},
		creatorMailing : function(e, id, type, aObj){
			mylog.log("adminDirectory.actions.disconnect", e, id, type, aObj);
			var str='<button data-id="'+id+'" data-type="'+type+'" class="btnCreatorMailing btn btn-default"><span class="fa fa-send"></span> Contacter le créateur</button>';
			return str ;
		} 
	},
	events : {
		update: function(aObj){
			$("#"+aObj.container+" .updateBtn").off().on("click", function(){
				mylog.log("adminDirectory..updateBtn ", $(this).data("id"), $(this).data("type"));
				dyFObj.editElement($(this).data("type"), $(this).data("id"), $(this).data("subtype"));
			});
		},
		delete : function(aObj){
			$("#"+aObj.container+" .deleteBtn").off().on("click", function(){
				mylog.log("adminDirectory..delete ", $(this).data("id"), $(this).data("type"));
				directory.deleteElement($(this).data("type"), $(this).data("id"), $(this));
			});
		},
		private : function(aObj){
			$("#"+aObj.container+" .privateBtn").off().on("click", function(){
				mylog.log("adminDirectory..privateBtn ", $(this).data("id"), $(this).data("type"));
				var thisObj=$(this);
				var id = $(this).data("id");
				var type = $(this).data("type");
				
				mylog.log("privatePublicThisBtn click",$(this).data("private"));
				//if private doesn't exist then == not private == fasle > switch to true
				var value = ($(this).data("private") && $(this).data("private") != "undefined") ? null : true;
				var params = {
					collection  : dyFInputs.get( type ).col,
					id    		: id, 
					type  		: type,
					path  		: $(this).data("path"), 
					value 		: value
				};
				globalCtx = params;

				dataHelper.path2Value( params, function(data) {
					if(typeof data.elt.preferences != "undefined"){
						elt.preferences = data.elt.preferences ;
					}
					aObj.setElt(elt, id, type) ;
					$("#"+type+id+" .private").html( aObj.values.private(elt, id, type, aObj));
					thisObj.replaceWith( aObj.actions.private(elt, id, type, aObj) );
					aObj.bindAdminBtnEvents(aObj);
				} );
			});
		},
		disconnect:function(aObj){
			if($("#"+aObj.container+" .btnDisconnect").length > 0){
				$("#"+aObj.container+" .btnDisconnect").off().on("click", function(){
					var childId = $(this).data("id");
					var childType = $(this).data("type");
					var connectType = links.getConnect( aObj.context.collection, childType);
					links.disconnect(childType,childId,aObj.context.id, aObj.context.collection,connectType, function(){
						aObj.search(0);
					});
				});
			}
		},
		creatorMailing : function(aObj){
			$("#"+aObj.container+" .btnCreatorMailing").off().on("click", function(){
				var id = $(this).data("id");
				var type = $(this).data("type");
				var elt = aObj.getElt(id, type) ;
				aObj.mailTo.bootbox(aObj, elt, type, id);
			});
		},
		roles : function(aObj){
			$("#"+aObj.container+" .rolesBtn").off().on("click", function(){
				var id = $(this).data("id");
				var type = $(this).data("type");
				var elt = aObj.getElt(id, type) ;
				//var connectType = ( (typeof aObj.context != "undefined" && typeof aObj.context.collection != "undefined") ? "contributors" : "members" ) ;
				var connectType = links.linksTypes[aObj.context.collection][type] ; 
				var roles= [];
				if( typeof aObj.context != "undefined" &&
					typeof aObj.context.id != "undefined" &&
					typeof aObj.context.collection != "undefined" &&
					typeof elt.links != "undefined" && 
					typeof elt.links[aObj.context.collection] != "undefined" && 
					typeof elt.links[aObj.context.collection][aObj.context.id] != "undefined" && 
					typeof elt.links[aObj.context.collection][aObj.context.id].roles != "undefined"){
					roles = elt.links[aObj.context.collection][aObj.context.id].roles ;
				}
				var form = {
					saveUrl : baseUrl+"/"+moduleId+"/link/removerole/",
					dynForm : {
						jsonSchema : {
							title : tradDynForm.modifyoraddroles+"<br/>"+elt.name,// trad["Update network"],
							icon : "fa-key",
							onLoads : {
								sub : function(){
									$("#ajax-modal .modal-header").removeClass("bg-dark bg-purple bg-red bg-azure bg-green bg-green-poi bg-orange bg-yellow bg-blue bg-turq bg-url")
												  				  .addClass("bg-dark");
								}
							},
							beforeSave : function(){
								mylog.log("beforeSave");
						    },
							afterSave : function(data){
								mylog.log(data);
								if( typeof aObj.context != "undefined" &&
									typeof aObj.context.id != "undefined" &&
									typeof aObj.context.collection != "undefined" &&
									typeof elt.links != "undefined" && 
									typeof elt.links[aObj.context.collection] != "undefined" && 
									typeof elt.links[aObj.context.collection][aObj.context.id] != "undefined" && 
									typeof elt.links[aObj.context.collection][aObj.context.id].roles != "undefined"){
									elt.links[aObj.context.collection][aObj.context.id].roles = data.roles ;
								}
								aObj.setElt(elt, id, type) ;
								$("#"+type+id+" .roles").html( aObj.values.roles(elt, id, type, aObj));

								dyFObj.closeForm();
							},
							properties : {
								contextId : dyFInputs.inputHidden(),
								contextType : dyFInputs.inputHidden(), 
								roles : dyFInputs.tags(rolesList, tradDynForm["addroles"] , tradDynForm["addroles"]),
								childId : dyFInputs.inputHidden(), 
								childType : dyFInputs.inputHidden(),
								connectType : dyFInputs.inputHidden()
							}
						}
					}
				};

				var dataUpdate = {
			        contextId : aObj.context.id,
			        contextType : aObj.context.collection,
			        childId : id,
			        childType : type,
			        connectType : connectType,
			        roles : roles
				};

				dyFObj.openForm(form, "sub", dataUpdate);
			});
		},
		admin : function(aObj){
			$("#"+aObj.container+" .adminBtn").off().click(function(e){
				var thisObj=$(this);
				var id = $(this).data("id");
				var type = $(this).data("type");
				var elt = aObj.getElt(id, type) ;
				var connectType = ( (typeof aObj.context != "undefined" && typeof aObj.context.collection != "undefined") ? links.linksTypes["citoyens"][aObj.context.collection] : "members" ) ;
				var isAdmin = $(this).data("isadmin");
				mylog.log("isAdmin", isAdmin);
				var params = {
			    	parentId : aObj.context.id,
			    	parentType : aObj.context.collection,
			   		childId : id,
					childType : type,
					connect : connectType,
					isAdmin : (isAdmin === true ? false : true )
				};

			    $.ajax({ 
			        type: "POST",
			        url: baseUrl+'/'+moduleId+"/link/updateadminlink/",
			        data: params,
			        dataType: "json",
			        success:function(data) {
			        	mylog.log("success data", data);			        
						if(isAdmin === true)
							isAdmin = false ;
						else
							isAdmin = true ;

						if( typeof aObj.context != "undefined" &&
							typeof aObj.context.id != "undefined" &&
							typeof aObj.context.collection != "undefined" &&
							typeof elt.links != "undefined" && 
							typeof links != "undefined" && 
							typeof links.linksTypes != "undefined" &&
							typeof links.linksTypes["citoyens"] != "undefined" &&
							typeof links.linksTypes["citoyens"][aObj.context.collection] != "undefined" &&
							typeof elt.links[links.linksTypes["citoyens"][aObj.context.collection]] != "undefined" && 
							typeof elt.links[links.linksTypes["citoyens"][aObj.context.collection]][aObj.context.id] != "undefined" && 
							typeof elt.links[links.linksTypes["citoyens"][aObj.context.collection]][aObj.context.id]){
							elt.links[links.linksTypes["citoyens"][aObj.context.collection]][aObj.context.id].isAdmin = isAdmin ;
						}

				        aObj.setElt(elt, id, type) ;
						$("#"+type+id+" .admin").html( aObj.values.admin(elt, id, type, aObj));
						thisObj.replaceWith( aObj.actions.admin(elt, id, type, aObj) );
						aObj.bindAdminBtnEvents(aObj);
			        },
			        error:function(xhr, status, error){
			            $("#searchResults").html("erreur");
			        },
			        statusCode:{
			                404: function(){
			                    $("#searchResults").html("not found");
			            }
			        }
			    });
		    });
		},
		status : function(aObj){
			$("#"+aObj.container+" .statusBtn").off().on("click", function(){
				mylog.log("adminDirectory..statusBtn ", $(this).data("id"), $(this).data("type"));
				var id = $(this).data("id");
				var type = $(this).data("type");
				var elt = aObj.getElt(id, type) ;
				var listStatus = {};
				var statusElt = "" ;

				if( typeof aObj.panelAdmin.actions.status.list != "undefined" ){
					$.each(aObj.panelAdmin.actions.status.list, function(key, value){
						listStatus[value] = value;
					});
				}

				if(	typeof elt != "undefined" && 
					typeof elt.source != "undefined" && 
					typeof elt.source.status != "undefined" &&
					typeof costum != "undefined" && 
					typeof costum.slug != "undefined" &&
					typeof elt.source.status[costum.slug] != "undefined" &&
					elt.source.status[costum.slug] ){
					statusElt = elt.source.status[costum.slug];
				}
					

				var form = {
					saveUrl : baseUrl+"/"+moduleId+"/element/updatepathvalue",
					dynForm : {
						jsonSchema : {
							title : "Modifier les Status",
							icon : "fa-key",
							onLoads : {
								sub : function(){
									$("#ajax-modal #collection").val(type);
								}
							},
							afterSave : function(data){
								mylog.dir(data);
								if(typeof data.elt.source != "undefined"){
									elt.source = data.elt.source ;
								}
								aObj.setElt(elt, id, type) ;
								$("#"+type+id+" .status").html( aObj.values.status(elt, id, type, aObj));
								dyFObj.closeForm();
							},
							properties : {
								//collection : dyFInputs.inputHidden(),
								id : dyFInputs.inputHidden(),
								path : dyFInputs.inputHidden(""),
								value : dyFInputs.inputSelect("Choisir un status", "Choisir un status", listStatus, {required : true}),
							}
						}
					}
				};
				var dataUpdate = {
					value : statusElt,
					collection : type,
					id : id,
					path : "source.status."+costum.slug
				};
				mylog.log("adminDirectory..statusBtn form", form);
				dyFObj.openForm(form, "sub", dataUpdate);
			});
		},
		relaunchInviation : function(aObj){
			if($("#"+aObj.container+" .btnRelaunchInviation").length > 0){
				$("#"+aObj.container+" .btnRelaunchInviation").off().on("click", function(){
					$.ajax({
						type: "POST",
						url: baseUrl+"/co2/mailmanagement/relaunchinvitation",
						data : {
							id : $(this).data("id")
						}
					})
					.done(function (data){
						if ( data && data.result ) {
							//mylog.log("validateSourceBtn data", data, params.id, params.type);

							var elt = aObj.getElt(params.id, params.type) ;
							if(typeof data.elt.source != "undefined"){
								elt.source = data.elt.source ;
							}
							aObj.setElt(elt, params.id, params.type) ;

							thisObj.replaceWith( aObj.actions.validated( elt, params.id, params.type, aObj) );
							$("#"+params.type+params.id+" .validated").html( aObj.values.validated( elt, params.id, params.type, aObj));
							aObj.bindAdminBtnEvents(aObj);
							if(params.valid === "true")
								toastr.success("L'élément est validée");
							else 
								toastr.success("L'élément n'est plus validée");
							
						} else {
							toastr.error("Un problème est survenu lors de la validation");
						}
					});
				});
			}
		},
		switchToUser : function(aObj){
			$("#"+aObj.container+" .switch2UserThisBtn").off().on("click",function () 
			{
				//$(this).empty().html('<i class="fa fa-spinner fa-spin"></i>');
		        var btnClick = $(this);
		        var id = $(this).data("id");
		        var urlToSend = baseUrl+"/"+moduleId+"/admin/switchto/uid/"+id;
		        
		        bootbox.confirm("confirm please !!",
	        	function(result) 
	        	{
					if (!result) {
						btnClick.empty().html('<i class="fa fa-thumbs-down"></i>');
						return;
					} else {
						$.ajax({
					        type: "POST",
					        url: urlToSend,
					        dataType : "json"
					    })
					    .done(function (data)
					    {
					        if ( data && data.result ) {
					        	toastr.info("Switched user!!");
					        	location.hash='#page.type.citoyens.id.'+data.id;
							        				window.location.reload();
					        	//window.location.href = baseUrl+"/"+moduleId;
					        } else {
					           toastr.error("something went wrong!! please try again.");
					        }
					    });
					}
				});

			});
		},
		banUser: function(aObj){
			$("#"+aObj.container+" .banUserBtn").off().on("click",function (){
				mylog.log("adminDirectory..banUserBtn ", $(this).data("id"), $(this).data("type"));
				var id = $(this).data("id");
				var type = $(this).data("type");
				var elt = aObj.getElt(id, type) ;
				var action = "addBannedUser";
				var label = "confirm please !!";
				if( typeof elt.roles != "undefined" && typeof elt.roles.isBanned != "undefined" ){
					action = "revokeBannedUser";
					label = "confirm to accept this user again !!";
				}
				var btnClick = $(this);
					bootbox.confirm(label, function(result) {
						if (result) {
							aObj.changeRole(btnClick, action, "banUser");
						}
				});
			});
		},
		deleteUser : function(aObj){
			$("#"+aObj.container+" .deleteUserBtn").off().on("click",function () {
				mylog.log("deleteThisBtn click");
				var id = $(this).data("id");
				var type = $(this).data("type");
				var url = baseUrl+"/"+moduleId+"/element/delete/id/"+id+"/type/"+type;
				bootbox.confirm("confirm please !!",
		    	function(result){
					if (!result) {
						btnClick.empty().html('<i class="fa fa-thumbs-down"></i>');
						return;
					} else {
						mylog.log("deleteElement", url);
					 	var param = new Object;
						$.ajax({
					        type: "POST",
					        url: url,
					        data: param,
					       	dataType: "json",
					    	success: function(data){
						    	if(data.result){
									toastr.success(data.msg);
									mylog.log("Retour de delete : "+data.status);
									urlCtrl.loadByHash(location.hash);
						    	}else{
						    		toastr.error(data.msg);
						    	}
						    },
						    error: function(data){
						    	toastr.error("Something went really bad ! Please contact the administrator.");
						    }
						});
					}
				});
			});
		},
		validated : function(aObj){
			$("#"+aObj.container+" .validateSourceBtn").off().on("click", function(){
				$(this).find("i").removeClass("fa-check").addClass("fa-spinner fa-spin");
				var thisObj=$(this);
				var params={
					id:$(this).data("id"),
					type:$(this).data("type"),
					valid:$(this).data("valid")
				};
				mylog.log("adminDirectory.params", params);

				$.ajax({
					type: "POST",
					url: baseUrl+"/costum/default/validategroup",
					data : params
				})
				.done(function (data){
					if ( data && data.result ) {
						//mylog.log("validateSourceBtn data", data, params.id, params.type);

						var elt = aObj.getElt(params.id, params.type) ;
						if(typeof data.elt.source != "undefined"){
							elt.source = data.elt.source ;
						}
						aObj.setElt(elt, params.id, params.type) ;

						thisObj.replaceWith( aObj.actions.validated( elt, params.id, params.type, aObj) );
						$("#"+params.type+params.id+" .validated").html( aObj.values.validated( elt, params.id, params.type, aObj));
						aObj.bindAdminBtnEvents(aObj);
						if(params.valid === "true")
							toastr.success("L'élément est validée");
						else 
							toastr.success("L'élément n'est plus validée");
						
					} else {
						toastr.error("Un problème est survenu lors de la validation");
					}
				});
			});
		},
		generateAAP : function(aObj){
			$("#"+aObj.container+" .generateAAPBtn").off().on("click", function(){
				mylog.log("adminDirectory..generateAAPBtn ", $(this).data("slug") );
				var slug = $(this).data("slug");
				ajaxPost(null, baseUrl+"/survey/co/index/id/"+slug+"/copy/ficheAction", null, function(){toastr.success("Le territoire peut désormais ajouter des actions");}, "html");
			});
		},
		privateAnswer: function(aObj){
			$("#"+aObj.container+" .privateAnswerBtn").off().on("click", function(){
				mylog.log("adminDirectory..privateAnswerBtn ", $(this).data("id"), $(this).data("type"));
				var thisObj=$(this);
				var id = $(this).data("id");
				var type = $(this).data("type");
				var parentid = $(this).data("parentid");
				var parenttype = $(this).data("parenttype");
				var elt = aObj.getElt(parentid, parenttype) ;
				mylog.log("adminDirectory..privateAnswerBtn elt", elt);
				mylog.log("privatePublicThisBtn click",$(this).data("private"));
				//if private doesn't exist then == not private == fasle > switch to true
				var value = ($(this).data("private") && $(this).data("private") != "undefined") ? null : true;
				var params = {
					collection  : dyFInputs.get( type ).col,
					id    		: id, 
					type  		: type,
					path  		: $(this).data("path"), 
					value 		: value
				};
				globalCtx = params;
				dataHelper.path2Value( params, function(data) { 
					if(typeof data.elt.preferences != "undefined"){
						elt.project.preferences = data.elt.preferences ;
					}
					aObj.setElt(elt, parentid, parenttype) ;
					$("#"+parenttype+parentid+" .privateanswer").html( aObj.values.privateanswer(elt, parentid, parenttype, aObj));
					thisObj.replaceWith( aObj.actions.privateanswer(elt, parentid, parenttype, aObj) );
					aObj.bindAdminBtnEvents(aObj);
				} );
			});
		}
	},
	bindAdminBtnEvents : function(aObj){
		coInterface.bindLBHLinks();
		if( typeof aObj.panelAdmin.actions != "undefined" && 
				aObj.panelAdmin.actions != null &&
				Object.keys( aObj.panelAdmin.actions ).length > 0 ){
				$.each(aObj.panelAdmin.actions, function(key, value){
					if(typeof aObj.events[key] != "undefined")
						aObj.events[key](aObj);
				});
		}
		mylog.log("adminDirectory.bindAdminBtnEvents");
		if($("#"+aObj.container+" #btnInvite").length > 0){
			$("#"+aObj.container+" #btnInvite").off().on("click", function(){
				var id = $(this).data("id");
				var type = $(this).data("type");
				smallMenu.openAjaxHTML(baseUrl+'/co2/element/invite/type/'+type+'/id/'+id);
			});
		}

		if($("#"+aObj.container+" .commentBtn").length > 0){
			$("#"+aObj.container+" .commentBtn").off().on("click", function(){
				mylog.log("adminDirectory..updateBtn ", $(this).data("id"), $(this).data("type"));
				var id = $(this).data("id");
				var type = $(this).data("type");
				var elt = aObj.getElt(id, type) ;
				
				var modal = bootbox.dialog({
			        message: '<div class="content-risk-comment-tree"></div>',
			        title: "Fil de commentaire du projet",
			        buttons: [
			        
			          {
			            label: "Annuler",
			            className: "btn btn-default pull-left",
			            callback: function() {
			              
			            }
			          }
			        ],
			        onEscape: function() {
			          modal.modal("hide");
			        }
			    });
				modal.on("shown.bs.modal", function() {
				  $.unblockUI();
				  	getAjax(".content-risk-comment-tree",baseUrl+"/"+moduleId+"/comment/index/type/"+type+"/id/"+id,
					function(){  //$(".commentCount").html( $(".nbComments").html() ); 
					},"html");
				});
			});
		}

	},
	changeRole : function(button, action, fAction) {
		mylog.log(button," click");
		var aObj = this ;
	    var id = button.data("id");
		var type = button.data("type");
		var elt = aObj.getElt(id, type) ;
	    var params ={
	    	type:type,
	    	id:id,
	    	action:action
	    };

	    var urlToSend = baseUrl+"/"+moduleId+"/element/updatestatus";
	    var res = false;

		$.ajax({
	        type: "POST",
	        url: urlToSend,
	        data: params,
	        dataType : "json"
	    })
	    .done(function (data) {
	        if ( data && data.result === true ) {
	        	toastr.success("Change has been done !!");
	        	if( typeof data.elt != "undefined" &&
					typeof data.elt.roles != "undefined"){
					elt.roles = data.elt.roles ;
				}
		        aObj.setElt(elt, id, type) ;
				//$("#"+type+id+" .admin").html( aObj.values.admin(elt, id, type, aObj));
				button.replaceWith( aObj.actions[fAction](elt, id, type, aObj) );
	        } else {
	           toastr.error("Something went wrong!! please try again. " + data.msg);
	        }
	    });
	},
	search : function(initPage){
		mylog.log("adminDirectory.search", initPage);
		var aObj = this ;
		$("#"+aObj.container+" #panelAdmin .directoryLines").html("Recherche en cours. Merci de patienter quelques instants...");
		var urlS = baseUrl+"/costum/default/groupadmin/tpl/json";
		if(typeof aObj.panelAdmin != "undefined" && aObj.panelAdmin != null &&
			typeof aObj.panelAdmin.url != "undefined" && aObj.panelAdmin.url != null)
			urlS = aObj.panelAdmin.url+"/tpl/json" ;
		var paramsSearch = searchInterface.constructObjectAndUrl();
		paramsSearch = jQuery.extend(paramsSearch, aObj.paramsSearch);
		$.ajax({ 
			type: "POST",
			url: urlS,
			data: paramsSearch,
			dataType: "json",
			success:function(data) {
				mylog.log("adminDirectory.search ajax.success", data);
				aObj.results = data.results;
				aObj.initViewTable();
				aObj.bindAdminBtnEvents(aObj);
				if(typeof aObj.bindCostum == "function")
					aObj.bindCostum(aObj);

				
				//if(initPage){
					aObj.countTotal = 0 ;
					$.each(aObj.results.count, function(key, values){
						aObj.countTotal += values;
					});
					$("#"+aObj.container+" #countTotal").html(aObj.countTotal);
					aObj.initPageTable();
				//}
			},
			error:function(xhr, status, error){
				$("#"+aObj.container+" #searchResults").html("erreur");
			}
		});
	},
	initPageTable : function(){
		mylog.log("adminDirectory.initPageTable ");
		var aObj = this ;
		var numberPage=(aObj.countTotal/100);
		mylog.log("adminDirectory.initPageTable numberPage", numberPage);
		//if(numberPage > 1){
			$('.pageTable').pagination({
				items: numberPage,
				itemOnPage: 5,
				currentPage: aObj.paramsSearch.page,
				hrefTextPrefix:"?page=",
				cssStyle: 'light-theme',
				onInit: function () {
				},
				onPageClick: function (page, evt) {
					//aObj.paramsSearch.page=(page-1);
					aObj.paramsSearch.page= page;
					//startAdminSearch();
					aObj.search();
				}
			});
		//}
		
	},
	initViewTable : function(){
		var aObj = this ;
		$("#"+aObj.container+" #panelAdmin .directoryLines").html("");
		mylog.log("valuesInit",aObj , aObj.results);
		if(Object.keys(aObj.results).length > 0){
			$.each(aObj.results ,function(type,list){
				$.each(list, function(key, values){
					var entry = aObj.buildDirectoryLine( values, type, type, typeObj.get(type).icon);
					$("#"+aObj.container+" #panelAdmin .directoryLines").append(entry);
				});
			});
		}
		aObj.bindAdminBtnEvents(aObj);
	},
	buildDirectoryLine : function( e, collection, icon ){
		var aObj = this;
		var strHTML="";
		if(typeof e._id =="undefined" || ((typeof e.name == "undefined" || e.name == "") && (e.text == "undefined" || e.text == "")) )
			return strHTML;
		var actions = "";
		var classes = "";
		var id = e._id.$id;
		var status=[];
		strHTML += '<tr id="'+collection+id+'" class="'+collection+' line">';
			strHTML += aObj.columTable(e, id, collection);
			var actionsStr = aObj.actions.init(e, id, collection, aObj);
			if(actionsStr != ""){
				aObj.actionsStr = true;
				strHTML += 	'<td class="center">'+ 
								'<div class="btn-group">'+
								actionsStr +
								'</div>'+
							'</td>';
			}
		strHTML += '</tr>';
		return strHTML;
	},
	mailTo : {
		initMessage : true,
		initObject : true,
		initMail: function(elt){
			if(typeof elt.creator != "undefined" && elt.creator.email)
				return elt.creator.email;
			else
				return "";
		},
		defaultObject : function(elt){
			return "[co] Modération avant validation de "+elt.name;
		},
		defaultRedirect : function(elt, type, id){
			return "<a href='"+baseUrl+"/#page.type."+type+".id."+id+"' target='_blank'>Retrouvez la page "+elt.name+" en cliquant sur ce lien</a>";
		},
		defaultMessage : function(elt, type, id){
			var nameContact=(typeof elt.creator != "undefined" && elt.creator.name) ? elt.creator.name : "";
			var str="Bonjour"+((notEmpty(nameContact)) ? " "+nameContact : "")+",\n"+
				"Le contenu que vous avez publié demande d'être approfindi : \n"+
				" - Référencer l'adresse\n"+
				" - Ajouter la description, des mots clés, une image\n"+
				" - Les dates ne sont pas cohérentes\n"+
				"Encore un petit effort et vos points seront attribués";
			return str;
			//$("#send-mail-admin #message-email").text(str);
		},
		bootbox : function(aObj, elt, type, id){
			bootbox.dialog({
		        onEscape: function() {},
		        message: '<div id="send-mail-admin" class="row">  ' +
		            '<div class="col-xs-12"> ' +
		            	'<span>Email</span> ' +
		            	'<input type="text" id="contact-email" class="col-xs-12" value="'+aObj.mailTo.initMail(elt)+'"/>'+
		            '</div>'+
		            '<div class="col-xs-12"> ' +
		            	'<span>Object</span> ' +
		            	'<input type="text" id="object-email" class="col-xs-12" value="'+aObj.mailTo.defaultObject(elt)+'"/>'+
		            '</div>'+
		            '<div class="col-xs-12"> ' +
		            	'<span>Message</span> ' +
		            	'<textarea id="message-email" class="col-xs-12 text-dark" style="min-height:250px;">'+aObj.mailTo.defaultMessage(elt, type, id)+'</textarea>'+
		            '</div>'+
		            '</div>',
		        buttons: {
		            success: {
		                label: "Ok",
		                className: "btn-primary",
		                callback: function () {
		                	aObj.mailTo.sendMail(aObj, elt, type, id);
		                }
		            },
		            cancel: {
		            	label: trad["cancel"],
		            	className: "btn-secondary",
		            	callback: function() {}
		            }
		        }
		    });
		},
		sendMail : function(aObj, elt, type, id){
			var msg="<span style='white-space: pre-line;'>"+$("#send-mail-admin #message-email").text()+"<br/></span>"+
				aObj.mailTo.defaultRedirect(elt, type, id);
				
			var params={
				tplMail : $("#send-mail-admin #contact-email").val(),
				tpl : "basic",
				tplObject : $("#send-mail-admin #object-email").val(),
				html: msg
			};
			$.ajax({ 
				type: "POST",
				url: baseUrl+"/co2/mailmanagement/createandsend",
				data: params,
				dataType: "json",
				success:function(data) {
					toastr.success("Le mail a été envoyé avec succès");
				}
			});
		}
	}
};
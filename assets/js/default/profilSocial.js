function pushListRoles(links){
	//Members
	if( typeof links.members != "undefined" ){
		$.each(links.members, function(e,v){
			if(typeof v.roles != "undefined"){
				$.each(v.roles, function(i,data){
					if(data != "" && !rolesList.includes(data)){
						rolesList.push(data);
					}
				});
			}
		});
	}

	//Contributors
	if(typeof links.contributors != "undefined"){
		$.each(links.contributors, function(e,v){
			if(typeof v.roles != "undefined"){
				$.each(v.roles, function(i,data){
					if(data != "" && !rolesList.includes(data)){
						rolesList.push(data);
					}
				});
			}
		});
	}

	//Attendees
	if(typeof links.attendees != "undefined"){
		$.each(links.attendees, function(e,v){
			if(typeof v.roles != "undefined"){
				$.each(v.roles, function(i,data){
					if(data != "" && !rolesList.includes(data)){
						rolesList.push(data);
					}
				});
			}
		});
	}
}
var pageProfil = {
	params:{},
	init : function(){
		mylog.log("pageProfil.init");
		coInterface.showLoader("#central-container");
		pageProfil.initDateHeaderPage();
		pageProfil.initView();
		pageProfil.initMetaPage(contextData.name,contextData.shortDescription,contextData.profilImageUrl);
		if(typeof contextData.name !="undefined")
			setTitle("", "", contextData.name);
		pageProfil.bindButtonMenu();
		if(typeof pageProfil.initCallB == "function")
			pageProfil.initCallB();
		
	},
	initDateHeaderPage : function(){
		mylog.log("pageProfil.initDateHeaderPage");
		var str = directory.getDateFormated(contextData);
		$(".event-infos-header").html(str);
	},
	initView(){
		mylog.log("pageProfil.initView");
		if(pageProfil.params.view!=""){
			if(!notEmpty(pageProfil.params.dir))
				$(".ssmla[data-view='"+pageProfil.params.view+"']").addClass("active");
			else
				$(".ssmla[data-view='"+pageProfil.params.view+"'][data-dir='"+pageProfil.params.dir+"']").addClass("active");
			//if(pageProfil.params.view=="coop"){
			//	pageProfil.views.newspaper(false);
			//	pageProfil.actions.cospace();
			//}
			//else	
				pageProfil.views[pageProfil.params.view]();
			$("#central-container").attr("data-active-view", pageProfil.params.view);
		}else{
			$(".ssmla[data-view='newspaper']").addClass("active");
			$("#central-container").attr("data-active-view", "newspaper");
			pageProfil.views.newspaper(false);
		}
	},
	initMetaPage(title, description, image){
		mylog.log("pageProfil.initMetaPage", title, description, image);
		if(title != ""){
			$("meta[name='title']").attr("content",title);
			$("meta[property='og:title']").attr("content",title);
		}
		if(description != ""){
			$("meta[name='description']").attr("content",description);
			$("meta[property='og:description']").attr("content",description);
		}
		if(image != ""){
			$("meta[name='image']").attr("content",baseUrl+image);
			$("meta[property='og:image']").attr("content",baseUrl+image);
		}
	},
	bindViewActionEvent:function(){
		mylog.log("pageProfil.bindViewActionEvent");
		$(".ssmla").off().on("click", function(){
			pageProfil.responsiveMenuLeft();
			pageProfil.params.action = $(this).data("action");
			$("#div-select-create").hide(200);
			if(notEmpty($(this).data("view"))){
				pageProfil.params.view = $(this).data("view");
				pageProfil.params.dir = $(this).data("dir");
				coInterface.showLoader("#central-container");
				$("#central-container").attr("data-active-view", pageProfil.params.view);
				$(".ssmla").removeClass("active");
				$(this).addClass("active");
				onchangeClick=false;
				url=hashUrlPage+".view."+$(this).data("view");
				url+=(notNull(pageProfil.params.dir)) ? ".dir."+pageProfil.params.dir : "";
				if(pageProfil.params.view=="directory" && !notEmpty(pageProfil.params.dir)){
					url+=".dir."+links.connectType[contextData.type];
				}
				location.hash=url;
				pageProfil.views[pageProfil.params.view]($(this));
			}else if(notEmpty(pageProfil.params.action)){
				pageProfil.actions[pageProfil.params.action]();
			}
		});
	},
	bindButtonMenu : function(){
		mylog.log("pageProfil.bindButtonMenu");
		pageProfil.bindViewActionEvent();
		$("#paramsMenu").click(function(){
	        $(".dropdown-profil-menu-params").addClass("open"); 
	    });
	    $(".dropdown-profil-menu-params .dropdown-menu").mouseleave(function(){ //alert("dropdown-user mouseleave");
	    	setTimeout(function(){ 
	    		//if(!$(".dropdown-profil-menu-params").is(":hover"))
	    			$(".dropdown-profil-menu-params").removeClass("open");
	    	}, 200);
	    });
	    $("#btnHeaderEditInfos").click(function(){
			$("#btn-start-detail").trigger("click");
		});
		
		$("#btn-close-select-create").click(function(){
	    	$("#div-select-create").hide(200);    	
	    });

		coInterface.bindButtonOpenForm();
		
	},
	responsiveMenuLeft: function(menuTop){
		mylog.log("pageProfil.responsiveMenuLeft", menuTop);
		if($(window).width()<768)
			pageProfil.menuLeftShow();
		if(menuTop){
			if($(window).width()>768)
				$(".ssmla").removeClass('active');
		}
	},
	menuLeftShow : function(){
		mylog.log("pageProfil.menuLeftShow");
		if($("#menu-left-container").hasClass("hidden-xs"))
			$("#menu-left-container").removeClass("hidden-xs");
		else
			$("#menu-left-container").addClass("hidden-xs");
	},
	views : {
		newspaper: function(){
			mylog.log("pageProfil.views.newspaper");
			//coInterface.scrollTo("#profil_imgPreview");
			setTimeout(function(){ //attend que le scroll retourn en haut (coInterface.scrollTo)
				ajaxPost('#central-container', baseUrl+"/news/co/index/type/"+typeItem+"/id/"+contextData.id, 
					{nbCol:1},
					function(){},"html");
			}, 700);
		},
		preferences: function(){
			mylog.log("pageProfil.views.preferences");
			var url = "pod/preferences";
			ajaxPost('#central-container', baseUrl+'/'+moduleId+'/'+url, 
				null,
				function(){},"html");
		},
		gallery: function(){
			mylog.log("pageProfil.views.gallery");
			var url = "gallery/index/type/"+typeItem+"/id/"+contextData.id;
			if(notNull(pageProfil.params.dir))
				url+="/docType/"+pageProfil.params.dir;

			if(notNull(pageProfil.params.key))
				url+="/contentKey/"+pageProfil.params.key;

			if(notNull(pageProfil.params.folderId))
				url+="/folderId/"+pageProfil.params.folderId;
			ajaxPost('#central-container', baseUrl+'/'+moduleId+'/'+url, 
				null,
				function(){},"html");
		},
		coop : function(){
			mylog.log("pageProfil.views.coop");
			$("#modalCoop").modal("show");
			pageProfil.actions.cospace();
			pageProfil.views.newspaper(false);
		},
		photos : function(){
			mylog.log("pageProfil.views.photos");
			var url = "gallery/index/type/"+typeItem+"/id/"+contextData.id+"/docType/image";	
			ajaxPost('#central-container', baseUrl+'/'+moduleId+'/'+url, 
				null,
				function(){},"html");
		},
		bookmarks : function(){
			mylog.log("pageProfil.views.bookmarks");
			var url = "gallery/index/type/"+typeItem+"/id/"+contextData.id+"/docType/bookmark";	
			ajaxPost('#central-container', baseUrl+'/'+moduleId+'/'+url, 
				null,
				function(){},"html");
		},
		library: function(){
			mylog.log("pageProfil.views.library");
			var url = "gallery/index/type/"+typeItem+"/id/"+contextData.id+"/docType/file";	
			ajaxPost('#central-container', baseUrl+'/'+moduleId+'/'+url, 
				null,
				function(){},"html");
		},
		notifications : function(){
			mylog.log("pageProfil.views.notifications");
			var url = "element/notifications/type/"+typeItem+"/id/"+contextData.id;
			ajaxPost('#central-container', baseUrl+'/'+moduleId+'/'+url, 
				null,
				function(){},"html");
		},
		history : function (){
			mylog.log("pageProfil.views.history");
			var url = "pod/activitylist/type/"+typeItem+"/id/"+contextData.id;
			ajaxPost('#central-container', baseUrl+'/'+moduleId+'/'+url, 
				null,
				function(){},"html");
		},
		community : function(){
			mylog.log("pageProfil.views.community");
			pageProfil.views.directory();
		},
		directory : function(callBack){
			mylog.log("pageProfil.views.directory");
			var dataIcon = (!notEmpty(pageProfil.params.dir)) ? "users" : $(".smma[data-type-dir="+pageProfil.params.dir+"]").data("icon");
			pageProfil.params.dir=(!notEmpty(pageProfil.params.dir)) ? links.connectType[contextData.type] : pageProfil.params.dir;
			var sub=(!notEmpty(pageProfil.params.sub)) ? "" : "/sub/"+pageProfil.params.sub;

			getAjax('', baseUrl+'/'+moduleId+'/element/getdatadetail/type/'+contextData.type+
						'/id/'+contextData.id+'/dataName/'+pageProfil.params.dir+sub+'?tpl=json',
						function(data){ 
							var type = ($.inArray(pageProfil.params.dir, ["poi","ressources","vote","actions","discuss"]) >=0) ? pageProfil.params.dir : null;
							mylog.log("pageProfil.views.directory canEdit" , canEdit);
							if(typeof canEdit != "undefined" && canEdit)
								canEdit=pageProfil.params.dir;
							mylog.log("pageProfil.views.directory edit" , canEdit);
							displayInTheContainer(data, pageProfil.params.dir, dataIcon, type, canEdit);
							if(typeof mapCO != "undefined"){
								mapCO.clearMap();
					            mapCO.addElts(data);
					            mapCO.map.invalidateSize();
							}
							coInterface.bindButtonOpenForm();

							if(typeof callBack != "undefined" && callBack != null)
								callBack();
						}
			,"html");
		},
		chart: function(){
			mylog.log("pageProfil.views.chart");
			var url = "chart/header/type/"+typeItem+"/id/"+contextData.id;
			ajaxPost('#central-container', baseUrl+'/'+moduleId+'/'+url, 
				null,
				function(){},"html");
		},
		editChart: function(){
			mylog.log("pageProfil.views.editChart");
			var url = "chart/addchartsv/type/"+contextData.type+"/id/"+contextData.id;
			ajaxPost('#central-container', baseUrl+'/'+moduleId+'/'+url, 
				null,
			function(){},"html");
		},
		detail : function(){
			mylog.log("pageProfil.views.detail");
			var url = "element/about/type/"+contextData.type+"/id/"+contextData.id;
			ajaxPost('#central-container', baseUrl+'/'+moduleId+'/'+url+'?tpl=ficheInfoElement', null, function(){
				
			},"html");
		},
		urls : function(){
			mylog.log("pageProfil.views.urls");
			getAjax('', baseUrl+'/'+moduleId+'/element/geturls/type/'+contextData.type+
						'/id/'+contextData.id,
						function(data){ 
							displayInTheContainer(data, "urls", "external-link", "urls");
						}
			,"html");
		},
		networks: function(){
			mylog.log("pageProfil.views.networks");
			getAjax('', baseUrl+'/'+moduleId+'/element/getnetworks/type/'+contextData.type+
						'/id/'+contextData.id,
						function(data){
							mylog.log("loadNetworks success", data, canEdit);
							displayInTheContainer(data, "networks", "external-link", "networks",canEdit);
						}
			,"html");
		},
		curiculum: function(){
			mylog.log("pageProfil.views.curiculum");
			getAjax('', baseUrl+'/'+moduleId+'/element/getcuriculum/type/'+contextData.type+
						'/id/'+contextData.id,
						function(html){
							mylog.log("loadCuriculum success", html);
							$("#central-container").html(html);
						}
			,"html");
		},
		settings: function(){
			mylog.log("pageProfil.views.settings");
			getAjax('', baseUrl+'/'+moduleId+'/settings/index',
				function(html){
					$("#central-container").html(html);
				}
			,"html");//urlCtrl.loadByHash("#settings.page.myAccount");
		},
		settingsCommunity : function(){
			mylog.log("pageProfil.views.settingsCommunity");
			getAjax('', baseUrl+'/'+moduleId+'/settings/confidentiality/type/'+contextData.type+'/id/'+contextData.id,
				function(html){
					$("#central-container").html("<div class='col-xs-12 bg-white'>"+html+"</div>");
				}
			,"html");
		},
		contacts: function(){
			mylog.log("pageProfil.views.contacts");
			getAjax('', baseUrl+'/'+moduleId+'/element/getcontacts/type/'+contextData.type+
						'/id/'+contextData.id,
						function(data){ 
							mylog.log("loadContacts", data);
							displayInTheContainer(data, "contacts", "envelope", "contacts");
							$(".openFormContact").click(function(){
					    		var idReceiver = $(this).data("id-receiver");
					    		var idReceiverParent = contextData.id;
					    		var typeReceiverParent = contextData.type;
					    		
					    		var contactMail = $(this).data("email");
					    		var contactName = $(this).data("name");
					    		//mylog.log('contactMail', contactMail);
					    		$("#formContact .contact-email").html(contactMail);
					    		$("#formContact #contact-name").html(contactName);

					    		$("#formContact #emailSender").val(userConnected.email);
					    		$("#formContact #name").val(userConnected.name);
					    		
					    		$("#formContact #form-control").val("");
					    		
					    		$("#formContact #idReceiver").val(idReceiver);
					    		$("#formContact #idReceiverParent").val(idReceiverParent);
					    		$("#formContact #typeReceiverParent").val(typeReceiverParent);
					    		
					    		$("#conf-fail-mail, #conf-send-mail, #form-fail").addClass("hidden");
		        				$("#form-group-contact").removeClass("hidden");
					    		$("#formContact").modal("show");
					    	});
						}
			,"html");
		},
		md : function(){
			mylog.log("pageProfil.views.md");
			getAjax('', baseUrl+'/api/person/get/id/'+contextData.id+"/format/md",
						function(data){ 
							descHtml = dataHelper.markdownToHtml(data) ; 
							$('#central-container').html(descHtml);
							coInterface.bindLBHLinks();
						}
			,"html");
		}
	},
	actions : {
		delete: function(){
			mylog.log("Delete Element");
    		$("#modal-delete-element").modal("show");
		},
		share : function(){
			//directory.bindBtnShareElt();
			//$(".btn-share").trigger("click");
			//if(html == "" && type !="news" && type!="activityStream" && typeof contextData != "undefined"){
			directory.showShareModal(contextData.type, contextData.id);
			/*$("#modal-share").modal("show");
	         html = directory.showResultsDirectoryHtml(new Array(contextData), contextData.type);
	        //} 
	        
	        $("#modal-share #htmlElementToShare").html(html);
	        $("#modal-share #btn-share-it").attr("data-id", contextData.id);
	        $("#modal-share #btn-share-it").attr("data-type", contextData.type);
	        $("#modal-share #btn-share-it").off().click(function(){
	          	directory.shareIt("#modal-share #btn-share-it");
	        });*/
		},
		cospace : function(){
			onchangeClick=false;
			location.hash=hashUrlPage+".view.coop";
			uiCoop.startUI();
		},
		create : function(){
			pageProfil.responsiveMenuLeft(true);
			$("#div-select-create").show(200);
			setTimeout(function(){
				$('html, body').stop().animate({
			    	scrollTop: $("#div-select-create").offset().top - 300
			    }, 300, '');
			}, 500);
		},
		chat : function(){
			hasRc=(typeof contextData.hasRC != "undefined" || contextData.type=="citoyens" ) ? true : false;
			rcObj.loadChat(contextData.slug,contextData.type,canEdit,hasRc, contextData);
		},
		mindmap : function(){
			co.mind();				
		},	
		graph:function(){
			window.location.href = baseUrl+"/"+moduleId+"/graph/d3/id/585bdfdaf6ca47b6118b4583/type/citoyen";
		},
		chatSettings : function(){
			rcObj.settings();
		},
		qrCode : function(){
			showDefinition('qrCodeContainerCl',true);
		},
		updateSlug : function(){
			updateSlug();
		},
		settingsAccount: function(){
			urlCtrl.loadByHash("#settings.page.myAccount");
		},
		confidentialityCommunity : function(){
			urlCtrl.loadByHash("#settings.page.confidentialityCommunity?slug="+contextData.slug);
		},
		notificationsCommunity:function(){
			urlCtrl.loadByHash("#settings.page.notificationsCommunity?slug="+contextData.slug);
		},
		downloadData : function(){
			$.ajax({
				url: baseUrl+"/"+moduleId+"/data/get/type/"+contextData.type+"/id/"+contextData.id ,
				type: 'POST',
				dataType: 'json',
				async:false,
				crossDomain:true,
				complete: function () {},
				success: function (obj){
					mylog.log("obj", obj);
					$("<a/>", {
					    "download": "profil.json",
					    "href" : "data:application/json," + encodeURIComponent(JSON.stringify(obj))
					  }).appendTo("body")
					  .click(function() {
					    $(this).remove()
					  })[0].click() ;
				},
				error: function (error) {
					
				}
			});
		},
		printOut : function(){
			javascript:window.print();
		}
	}
}
function bindButtonMenu(){
	$("#btn-survey").click(function(){
		//$(".ssmla").removeClass('active');
		responsiveMenuLeft(true);
		location.hash=hashUrlPage+".view.survey";
		loadAnswers(false);
		uiCoop.closeUI(false);
	});
	//$("#btn-survey-projects").click(function(e){
	//	e.preventDefault();
	//	alert("btn-survey-projects"+contextData.slug+"/pId/"+contextData.id+"/pType/"+contextData.type );
		// if(jsonHelper.notNull("costum.htmlConstruct.element.menuLeft.projects.lbh") && costum.htmlConstruct.element.menuLeft.projects.lbh )
		// 	getAjax('#central-container', baseUrl+"/survey/co/index/id/"+contextData.slug+"/pId/"+contextData.id+"/pType/"+contextData.type,function(data){ 
		// 		name = costum.htmlConstruct.element.menuLeft.projects.label;
		// 		icon = costum.htmlConstruct.element.menuLeft.projects.icon;
		// 		alert("btn-survey-projects"+name);
		// 		displayInTheContainer( data, name, icon.substring(3), "projects"); }, "html");
		// else 
	//		window.open(baseUrl+"/survey/co/index/id/"+contextData.slug+"/pId/"+contextData.id+"/pType/"+contextData.type);
	//});
	//$("#btn-survey-ficheAction").click(function(e){
	//	e.preventDefault();
	//	alert("btn-survey-ficheAction"+contextData.slug+"/pId/"+contextData.id+"/pType/"+contextData.type );
		// if(jsonHelper.notNull("costum.htmlConstruct.element.menuLeft.ficheAction.lbh") && costum.htmlConstruct.element.menuLeft.ficheAction.lbh ){
		// 	getAjax('#central-container', baseUrl+"/survey/co/tags/id/cte/session/1/",
		// 		function(data){
		// 			name = costum.htmlConstruct.element.menuLeft.ficheAction.label;
		// 			icon = costum.htmlConstruct.element.menuLeft.ficheAction.icon;
		// 			displayInTheContainer(data, name, icon.substring(3), "ficheAction"); } ,"html");
		// } else
	//		window.open(baseUrl+"/survey/co/tags/id/"+contextData.slug+"/pId/"+contextData.id+"/pType/"+contextData.type);
	//});
}

function getLabelTitleDir(dataName, dataIcon, countData, n){
	mylog.log("getLabelTitleDir", dataName, dataIcon, countData, n, trad);
	var elementName = "<span class='Montserrat' id='name-lbl-title'>"+contextData.name+"</span>";
	
	var s = (n>1) ? "s" : "";

	//if(countData=='Aucun')
	//	countData=tradException.no;
	var html = "<i class='fa fa-"+dataIcon+" fa-2x margin-right-10'></i> <i class='fa fa-angle-down'></i> ";
	if(dataName == "follows")	{ html += elementName + " "+trad.isfollowing+" " + countData + " "+trad["page"+s]+""; }
	else if(dataName == "followers")	{ html += countData + " <b>"+trad["follower"+s]+"</b> "+trad.to+" "+ elementName; }
	else if(dataName == "members")		{ html += elementName + " "+trad.iscomposedof+" " + countData + " <b>"+trad["member"+s]+"</b>"; }
	else if(dataName == "attendees")	{ html += countData + " <b>"+trad["attendee"+s]+"</b> "+trad.toevent+" " + elementName; }
	else if(dataName == "guests")		{ html += countData + " <b>"+trad["guest"+s]+"</b> "+trad.on+" " + elementName; }
	else if(dataName == "contributors")	{ html += countData + " <b>"+trad["contributor"+s]+"</b> "+trad.toproject+" " + elementName; }
	
	else if(dataName == "events"){ 
		if(type == "events"){
			html += elementName + " "+trad.iscomposedof+" " + countData+" <b> "+trad["subevent"+s]; 
		}else{
			html += elementName + " "+trad.takepart+" " + countData+" <b> "+trad["event"+s]; 
		}
	}
	else if(dataName == "organizations"){ html += elementName + " "+trad.ismemberof+" "+ countData+" <b>"+trad["organization"+s]; }
	else if(dataName == "projects")		{ html += elementName + " "+trad.contributeto+" " + countData+" <b>"+trad["project"+s] }

	else if(dataName == "collections")	{ html += elementName+" "+trad.hasgot+" "+countData+" <b>"+trad["collection"+s]+"</b>"; }
	else if(dataName == "poi")			{ html += countData+" <b>"+trad["point"+s+"interest"+s]+"</b> "+trad['createdby'+s]+" " + elementName; }
	else if(dataName == "classifieds")	{ html += countData+" <b>"+trad["classified"+s]+"</b> "+trad['createdby'+s]+" " + elementName; }
	else if(dataName == "ressources")	{ html += countData+" <b>ressource"+s+"</b> "+trad['createdby'+s]+" " + elementName; }
	else if(dataName == "jobs")	{ html += countData+" <b>"+trad.job+s+"</b> "+trad['createdby'+s]+" " + elementName; }

	else if(dataName == "needs")	{ html += countData+" <b>"+trad[need+s]+"</b> "+trad.of+" " + elementName; }

	else if(dataName == "vote")		{ html += countData+" <b>"+trad[proposal+s]+"</b> "+trad.of+" " + elementName; }
	else if(dataName == "discuss")	{ html += countData+" <b>"+trad.discussion+s+"</b> "+trad.of+" " + elementName; }
	else if(dataName == "actions")	{ html += countData+" <b>"+trad.action+s+"</b> "+trad.of+" " + elementName; }

	else if(dataName == "surveys")	{ html += countData+" <b>"+trad.survey+s+"</b> "+trad['createdby'+s]+" " + elementName; }

	else if(dataName == "actionRooms")	{ html += countData+" <b>espace de décision"+s+"</b> de " + elementName; }
	else if(dataName == "networks")		{ html += countData+" <b>"+trad.map+s+"</b> "+trad.of+" " + elementName;
		if( (typeof openEdition != "undefined" && openEdition == true) || (typeof canEdit != "undefined" && canEdit == true) ){
			html += '<a class="btn btn-sm btn-link bg-green-k pull-right " href="javascript:;" onclick="dyFObj.openForm ( \'network\', \'sub\')">';
	    	html +=	'<i class="fa fa-plus"></i> '+tradDynForm["Add map"]+'</a>' ;
	    }
	 }

	else if(dataName == "urls"){ 
		var str = " a " + countData;
		if(countData == "Aucun")
			str = " n'a aucun";
		html += elementName + str+" <b> lien"+s;
		if( (typeof openEdition != "undefined" && openEdition == true) || (typeof canEdit != "undefined" && canEdit == true) ){
			html += '<a class="btn btn-sm btn-link bg-green-k pull-right " href="javascript:;" onclick="dyFObj.openForm ( \'url\',\'sub\')">';
    		html +=	'<i class="fa fa-plus"></i> '+trad["Add link"]+'</a>' ;
		}
		  
	}

	else if(dataName == "contacts"){
		var str = " a " + countData;
		if(countData == "Aucun")
			str = " n'a aucun";
		html += elementName + " a " + countData+" <b> point de contact"+s;
		if( (typeof openEdition != "undefined" && openEdition == true) || (typeof canEdit != "undefined" && canEdit == true) ){
			html += '<a class="btn btn-sm btn-link bg-green-k pull-right " href="javascript:;" onclick="dyFObj.openForm ( \'contactPoint\',\'sub\')">';
	    	html +=	'<i class="fa fa-plus"></i> '+trad["Add contact"]+'</a>' ;
	    }
	}

	if( openEdition || canEdit ){
		if( $.inArray( dataName, ["events","projects","organizations","poi","classifieds", "jobs","collections","actionRooms", "ressources"] ) >= 0 ){
			if(dataName == "collections"){
				html += '<a class="btn btn-sm btn-link bg-green-k pull-right " href="javascript:;" onclick="collection.crud()">';
		    	html +=	'<i class="fa fa-plus"></i> '+trad.createcollection+'</a>' ; 
			}
			else {
				var elemSpec = dyFInputs.get(dataName);
				var formInputBtn='data-form-type="'+elemSpec.ctrl+'"';
				var labelCreate=trad["create"+elemSpec.ctrl];
				if($.inArray( dataName , ["ressources","classifieds","jobs"] ) >= 0){
					formInputBtn='data-form-type="'+dataName+'"';
					labelCreate=trad["create"+dataName];
				}
				

				html += '<button class="btn btn-sm btn-link bg-green-k pull-right btn-open-form" '+formInputBtn+' data-dismiss="modal">';
		    	html +=	'<i class="fa fa-plus"></i> '+labelCreate+'</button>' ;  
		    }
		}
	}
	if(dataName != "contacts" && dataName != "collections" &&
		( costum == null ||
			typeof costum.htmlConstruct.element.viewMode == "undefined" || 
			costum.htmlConstruct.element.viewMode == true ) ){
		html+='<div class="col-xs-12 text-right no-padding margin-top-5">'+
                        '<button class="btn switchDirectoryView ';
                          if(directory.viewMode=="list") html+='active ';
         html+=     'margin-right-5" data-value="list"><i class="fa fa-bars"></i></button>'+
                        '<button class="btn switchDirectoryView ';
                          if(directory.viewMode=="block") html+='active ';
         html+=     '" data-value="block"><i class="fa fa-th-large"></i></button>'+
                    '</div>';
    }
	return html;
}

/*function loadAnswers(){
	toogleNotif(false);
	var url = "element/about/type/"+contextData.type+"/id/"+contextData.id+"/view/answers";
	showLoader('#central-container');
	ajaxPost('#central-container', baseUrl+'/'+moduleId+'/'+url, null, function(){},"html");
}*/


//todo add count on each tag
    function getfilterRoles(roles) { 
    	mylog.log("getfilterRoles roles",roles);
    	if(typeof roles == "undefined") {
    		$("#listRoles").hide();
    		return;
		}

		var nRole = 0;
    	$.each( roles,function(k,o){ nRole++; } );
    	if(nRole == 0){
    		$("#listRoles").hide();
    		return;
		}
		$("#listRoles").show(300);
        $("#listRoles").html("<i class='fa fa-filter'></i> "+trad.sortbyrole+": ");
        $("#listRoles").append("<a class='btn btn-link btn-sm letter-blue favElBtn favAllBtn' "+
            "href='javascript:directory.toggleEmptyParentSection(\".favSection\",null,\".searchEntityContainer, .entityLight\",1)'>"+
            " <i class='fa fa-refresh'></i> <b>"+trad["seeall"]+"</b></a>");
        	$.each( roles,function(k,o){
                $("#listRoles").append("<a class='btn btn-link btn-sm favElBtn letter-blue "+slugify(k)+"Btn' "+
                                "data-tag='"+slugify(k)+"' "+
                                "href='javascript:directory.toggleEmptyParentSection(\".favSection\",\"."+slugify(k)+"\",\".searchEntityContainer, .entityLight\",1)'>"+
                                  k+" <span class='badge'>"+o.count+"</span>"+
                            "</a>");
        	});
    }
function displayInTheContainer(data, dataName, dataIcon, contextType, edit){ 
	mylog.log("displayInTheContainer",data, dataName, dataIcon, contextType, edit)
	var n=0;
	listRoles={};
	
	$.each(data, function(key, val){ 
		mylog.log("rolesShox",key, val);
		if(typeof key != "undefined" && ( (typeof val.id != "undefined" || typeof val["_id"] != "undefined") || contextType == "contacts") || dataName=="collections" ) n++; 
		if(typeof val.rolesLink != "undefined"){
			mylog.log(val.rolesLink);
			$.each(val.rolesLink, function(i,v){
				//Push new roles in rolesList
				if(v != "" && !rolesList.includes(v))
					rolesList.push(v);
				//Incrément and push roles in filter array
				if(v != ""){
					if(typeof listRoles[v] != "undefined")
						listRoles[v].count++;
					else
						listRoles[v]={"count": 1}
				}
			});
		}
	});
	var communityStr="";
	if($.inArray( dataName , ["follows","followers","organizations","members","guests","attendees","contributors"] ) >= 0){
		communityStr+="<div id='menuCommunity' class='col-md-12 col-sm-12 col-xs-12 padding-20'>";
		if(contextData.type == "citoyens" ) {
		communityStr+='<a href="javascript:" class="ssmla uppercase load-coummunity';
			if(dataName=="follows")
				communityStr+=' active';		
		communityStr+='" data-type-dir="follows" data-icon="link">'+
				'<i class="fa fa-link"></i> <span class="hidden-xs">'+trad.follows+'</span>';
				if(typeof contextData.links != "undefined" && typeof contextData.links.follows != "undefined")
		communityStr += "<span class='badge'>"+Object.keys(contextData.links.follows).length+"</span>";
		communityStr +=	'</a>';
		}
		countHtml="";
		if(typeof contextData.links != "undefined" && typeof contextData.links[connectTypeElement] != "undefined"){
			countLinks=0;
			countGuests=0;
			$.each(contextData.links[connectTypeElement], function(e,v){
				if(typeof v.isInviting == "undefined")
					countLinks++;
				else
					countGuests++;
			});
			countHtml += "<span class='badge'>"+countLinks+"</span>";
		}
		communityStr+=	'<a href="javascript:" class="ssmla uppercase load-coummunity';
			if(dataName==connectTypeElement)
				communityStr+=' active';		
		communityStr+='" data-type-dir="'+connectTypeElement+'" data-icon="users">'+
				'<i class="fa fa-users"></i> <span class="hidden-xs">'+trad[connectTypeElement]+"</span>"+
				countHtml+
			'</a>';
		
		if(contextData.type != "citoyens" && contextData.type != "events") { 
			communityStr+='<a href="javascript:" class="ssmla uppercase load-coummunity';
			if(dataName=="followers")
				communityStr+=' active';		
		communityStr+='" data-type-dir="followers" data-icon="link">'+
				'<i class="fa fa-link"></i> <span class="hidden-xs">'+trad.followers+'</span>';
				if(typeof contextData.links != "undefined" && typeof contextData.links.followers != "undefined")
		communityStr += "<span class='badge'>"+Object.keys(contextData.links.followers).length+"</span>";
		communityStr +=	'</a>';
		}
		if(contextData.type != "citoyens") {
			communityStr+='<a href="javascript:" class="ssmla uppercase load-coummunity';
			if(dataName=="guests")
				communityStr+=' active';		
		communityStr+='" data-type-dir="guests" data-icon="send">'+
				'<i class="fa fa-send"></i> <span class="hidden-xs">'+trad.guests+'</span>';
				if(typeof countGuests != "undefined")
		communityStr += "<span class='badge'>"+countGuests+"</span>";
		communityStr +=	'</a>';
		}
		if (contextData.type=="citoyens" || contextData.type=="places" ){
			communityStr+='<a href="javascript:" class="ssmla uppercase load-coummunity';
			if(dataName=="organizations")
				communityStr+=' active';		
		communityStr+='" data-type-dir="organizations" data-icon="group"> '+
					'<i class="fa fa-group"></i> <span class="hidden-xs">'+trad.organizations+'</span>';
					if(typeof contextData.links != "undefined" && typeof contextData.links.memberOf != "undefined")
		communityStr += "<span class='badge'>"+Object.keys(contextData.links.memberOf).length+"</span>";
		communityStr += '</a>';
			
		}
		communityStr+="</div>"; 
	}/* TODO REFACTOR COMMUNITY AND MUTUALIZE ALL CLASSIFIEDS
	else if($.inArray( dataName , ["classifieds", "jobs", "ressources"])>=0){
		communityStr+="<div id='menuCommunity' class='col-md-12 col-sm-12 col-xs-12 padding-20'>";
		if(contextData.type == "citoyens" ) {
		communityStr+='<a href="javascript:" class="ssmla uppercase load-coummunity';
			if(dataName=="follows")
				communityStr+=' active';		
		communityStr+='" data-type-dir="follows" data-icon="link">'+
				'<i class="fa fa-link"></i> <span class="hidden-xs">'+trad.follows+'</span>';
				if(typeof contextData.links != "undefined" && typeof contextData.links.follows != "undefined")
		communityStr += "<span class='badge'></span>";
		communityStr +=	'</a>';
		}
		communityStr +='</div>';
	}*/

	mylog.log("displayInTheContainer communityStr", n, communityStr);

	if(n>0){
		var thisTitle = getLabelTitleDir(dataName, dataIcon, parseInt(n), n);
		
		var html = "";
		html+=communityStr;
		var btnMap = '<button class="btn btn-default btn-sm hidden-xs btn-show-onmap inline" id="btn-show-links-onmap">'+
				            '<i class="fa fa-map-marker"></i>'+
				        '</button>';

		if(dataName == "networks" || dataName == "surveys") btnMap = "";

		html += "<div class='col-md-12 col-xs-12 margin-bottom-15 labelTitleDir'>";
		
		mylog.log("eztrzeter", dataName);
		if(dataName != "urls" && dataName != "contacts")
			html += btnMap;

		html +=	thisTitle;
		html += "<div id='listRoles' class='shadow2 col-xs-12'></div>"+
			 "<hr>";
		html +=	"</div>";
		
		if(dataName=="events")
			html += "<div class='col-md-12 col-sm-12 col-xs-12 margin-bottom-10'>"+
						"<a href='javascript:;' id='showHideCalendar' class='text-azure' data-hidden='0'><i class='fa fa-caret-up'></i> Hide calendar</a>"+
					"</div>"+
					"<div id='profil-content-calendar' class='col-md-12 col-sm-12 col-xs-12 margin-bottom-20'></div>";
		mapElements = [];
		
		mylog.log("listRoles",listRoles);
		if(dataName != "collections"){
			if(mapElements.length==0) mapElements = data;
        	else $.extend(mapElements, data);
        	mylog.log("edit2", edit);
			html +="<div id='content-results-profil'>";
			html += 	directory.showResultsDirectoryHtml(data, contextType, null, edit);
			html +="</div>";
		}else{
			$.each(data, function(col, val){
				colName=col;
				if(col=="favorites")
					colName="favoris";
				html += "<a class='btn btn-default col-xs-12 shadow2 padding-10 margin-bottom-20' onclick='$(\"."+colName+"\").toggleClass(\"hide\")' ><h2><i class='fa fa-star'></i> "+colName+" ("+Object.keys(val.list).length+")</h2></a>"+
						"<div class='"+colName+" col-sm-12 col-xs-12  hide'>";
				mylog.log("list", val);
				if(val.count==0)
					html +="<span class='col-xs-12 text-dark margin-bottom-20'>"+trad.noelementinthiscollection+"</span>";
				else{
					$.each(val.list, function(key, elements){ 
						if(mapElements.length==0) mapElements = elements;
        				else $.extend(mapElements, elements);
						html += directory.showResultsDirectoryHtml(elements, key);
					});
				}
				html += "</div>";
			});
		}
		toogleNotif(false);
		mylog.log("displayInTheContainer html",html);
		$("#central-container").html(html);
		//if(dataName != "collections" && directory.viewMode=="block")
          //  setTimeout(function(){ directory.checkImage(data);}, 300);
		if(dataName == "events"){
			//init calendar view
			calendar.init("#profil-content-calendar");
			mylog.log("calendar data", data);
			calendar.addEvents(data);
     		//$(window).on('resize', function(){
  			//	$("#profil-content-calendar").fullCalendar('destroy');
  			//	calendar.showCalendar("#profil-content-calendar", data, "month");
  			//});
	     	/*$(".fc-button").on("click", function(e){
	      		calendar.setCategoryColor();
	     	})*/
		}
		directory.bindBtnElement();
		initBtnAdmin();
		
		getfilterRoles(listRoles);
		var dataToMap = data;
		if(dataName == "collections"){
			dataToMap = new Array();
			$.each(data, function(key, val){
				$.each(val.list, function(type, list){
					mylog.log("collection", type, list);
					$.each(list, function(id, el){
						dataToMap.push(el);
					});
				});
			});
		}
		mylog.log("displayInTheContainer here");
		mylog.log("dataToMap", dataToMap);
		$("#btn-show-links-onmap").off().click(function(){
			//Sig.showMapElements(Sig.map, dataToMap, "", thisTitle);
			//mapCO.addElts(dataToMap);
			if(typeof mapCO != "undefined")
				showMap();
		});
    
	}else{
		//var nothing = tradException.no;
		//if(dataName == "organizations" || dataName == "collections" || dataName == "follows")
		//	nothing = tradException.nofem;
		var html =  communityStr+"<div class='col-md-12 col-sm-12 col-xs-12 labelTitleDir margin-bottom-15'>"+
						getLabelTitleDir(dataName, dataIcon, parseInt(n), n)+
					"</div>";
		mylog.log("displayInTheContainer html2", html);
		$("#central-container").html(html + "<span class='col-md-12 alert bold bg-white'>"+
												"<i class='fa fa-ban'></i> "+trad.nodata+
											"</span>");
		toogleNotif(false);
	}
	coInterface.bindButtonOpenForm();
	if(communityStr != ""){
		$(".load-coummunity").off().on("click",function(){ 
			//responsiveMenuLeft();
			$(".load-coummunity").removeClass("active");
			$(this).addClass("active");
			pageProfil.params.dir = $(this).data("type-dir");
			//history.pushState remplace le location.hash car il recharge la page .
			//location.hash=hashUrlPage+".view.directory.dir."+pageProfil.params.dir;
			history.pushState(null, null, hashUrlPage+".view.directory.dir."+pageProfil.params.dir);
			pageProfil.views.directory();
		});
	}	
	directory.switcherViewer(data, "#content-results-profil");
}


var colNotifOpen = true;
function toogleNotif(open){
	if(typeof open == "undefined") open = false;
	
	if(open==false){
		$('#notif-column').removeClass("col-md-3 col-sm-3 col-lg-3").addClass("hidden");
		$('#central-container').removeClass("col-md-9 col-lg-9").addClass("col-md-12 col-lg-12");
	}else{
		$('#notif-column').addClass("col-md-3 col-sm-3 col-lg-3").removeClass("hidden");
		$('#central-container').addClass("col-sm-12 col-md-9 col-lg-9").removeClass("col-md-12 col-lg-12");
	}

	colNotifOpen = open;
}

function descHtmlToMarkdown() {
	mylog.log("htmlToMarkdown");
	if(typeof contextData.descriptionHTML != "undefined" && contextData.descriptionHTML == true) {
		mylog.log("htmlToMarkdown");
		if( $("#descriptionAbout").html() != "" ){
			var paramSpan = {
			  filter: ['span'],
			  replacement: function(innerHTML, node) {
			    return innerHTML;
			  }
			}
			var paramDiv = {
			  filter: ['div'],
			  replacement: function(innerHTML, node) {
			    return innerHTML;
			  }
			}
			mylog.log("htmlToMarkdown2");
			var converters = { converters: [paramSpan, paramDiv] };
			var descToMarkdown = toMarkdown( $("#descriptionMarkdown").html(), converters ) ;
			mylog.log("descToMarkdown", descToMarkdown);
			$("descriptionMarkdown").html(descToMarkdown);
			var param = new Object;
			param.name = "description";
			param.value = descToMarkdown;
			param.id = contextData.id;
			param.typeElement = contextData.type;
			param.block = "toMarkdown";
			$.ajax({
		        type: "POST",
		       	url : baseUrl+"/"+moduleId+"/element/updateblock/",
		        data: param,
		       	dataType: "json",
		    	success: function(data){
			    	//toastr.success(data.msg);
			    }
			});
			mylog.log("param", param);
		}
	}
}


function removeAddress(form){
	var msg = trad.suredeletelocality ;
		if(!form && contextData.type == personCOLLECTION)
			msg = trad.suredeletepersonlocality ;

		bootbox.confirm({
			message: msg + "<span class='text-red'></span>",
			buttons: {
				confirm: {
					label: trad.yes,
					className: 'btn-success'
				},
				cancel: {
					label: trad.no,
					className: 'btn-danger'
				}
			},
			callback: function (result) {
				if (!result) {
					return;
				} else {
					param = new Object;
			    	param.name = "locality";
			    	param.value = "";
			    	param.pk = contextData.id;
					$.ajax({
				        type: "POST",
				        url: baseUrl+"/"+moduleId+"/element/updatefields/type/"+contextData.type,
				        data: param,
				       	dataType: "json",
				    	success: function(data){
					    	//
					    	if(data.result && !form){
								if(contextData.type == personCOLLECTION) {
									//Menu Left
									$("#btn-geoloc-auto-menu").attr("href", "javascript:");
									$('#btn-geoloc-auto-menu > span.lbl-btn-menu').html("Communectez-vous");
									$("#btn-geoloc-auto-menu").attr("onclick", "communecterUser()");
									$("#btn-geoloc-auto-menu").off().removeClass("lbh");
									//Dashbord
									$("#btn-menuSmall-mycity").attr("href", "javascript:");
									$("#btn-menuSmall-citizenCouncil").attr("href", "javascript:");
									//Multiscope
									$(".msg-scope-co").html("<i class='fa fa-cogs'></i> Paramétrer mon code postal</a>");
									//MenuSmall
									$(".hide-communected").show();
									$(".visible-communected").hide();

									$(".communecter-btn").removeClass("hidden");
								}
								myScopes.communexion={};
								localStorage.setItem("myScopes",JSON.stringify(myScopes));
								toastr.success(data.msg);
								urlCtrl.loadByHash("#page.type."+contextData.type+".id."+contextData.id+".view.detail");
					    	}
					    }
					});
				}
			}
		});
}

/*function createSlugBeforeChat(type,canEdit,hasRc){
	dynForm.openForm("slug","sub");
	rcObj.loadChat(loadChat,type,canEdit,hasRC);
}*/

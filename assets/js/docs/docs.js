function initDocJs(faIcon, title){
  setTitle(" La doc : <span class='text-red'><i class='fa fa-"+faIcon+"'></i> "+title+"</span>", "binoculars",title);
  
  $(".carousel-control").click(function(){
    var top = $("#docCarousel").position().top-30;
    $(".my-main-container").animate({ scrollTop: top, }, 100 );
  });

  $(".btn-carousel-previous").click(function(){ //toastr.success('success!'); mylog.log("CAROUSEL CLICK");
      var top = $("#docCarousel").position().top-30;
      $(".my-main-container").animate({ scrollTop: top, }, 100 );
      setTimeout(function(){ $(".carousel-control.left").click(); }, 500);
    });
   $(".btn-carousel-next").click(function(){ //toastr.success('success!'); mylog.log("CAROUSEL CLICK");
      var top = $("#docCarousel").position().top-30;
      $(".my-main-container").animate({ scrollTop: top, }, 100 );
      setTimeout(function(){ $(".carousel-control.right").click(); }, 500);
    });
}


var documentation = {
  addLinkToDocsPage : false,
  openDoc : function() {
    $('#openModal').modal("hide");
    urlCtrl.loadByHash("#documentation");
  },
  getBuildPoiDoc : function (poiId,container,linkToDoc) {
    mylog.log("documentation","getBuildPoiDoc",poiId,container);
    if(linkToDoc)
      documentation.addLinkToDocsPage = true;
    if(!notEmpty(container) )
      container = '#container-docs'; 
    var url = baseUrl+'/co2/element/get/type/poi/id/'+poiId+"/edit/true";
    //alert(url);
    ajaxPost(null ,url, null,function(data){
      descHtml = documentation.renderDocHTML(data);
        // $('#container-docs').removeClass('col-xs-9').removeClass('col-xs-12');
        // $('#menu-left').removeClass('hide');
        $(container).html(descHtml);
      });
  },
  renderDocHTML : function(data){
    mylog.log("documentation","renderDocHTML",data);

    descHtml = dataHelper.markdownToHtml("#"+data.map.name); 
    if(documentation.addLinkToDocsPage)
      descHtml = "<h4><a onclick='documentation.openDoc()' href='javascript:;'>DOCUMENTATION</a></h4>"+descHtml;
    descHtml += dataHelper.markdownToHtml(data.map.description); 
    mylog.log("doooc", data.map.documents);
    if(data.map.documents){
      mylog.log("building","poi doc associated documents",data.map.documents);
      descHtml += "<br/><h4>Documents</h4>";
      $.each(data.map.documents, function(k,d) { 
        if(d.doctype=="file"){
        var dicon = "fa-file";
        var fileType = d.name.split(".")[1];
        mylog.log("documents",d);
        if( fileType == "png" || fileType == "jpg" || fileType == "jpeg" || fileType == "gif" )
          dicon = "fa-file-image-o";
        else if( fileType == "pdf" )
          dicon = "fa-file-pdf-o";
        else if( fileType == "xls" || fileType == "xlsx" || fileType == "csv" )
          dicon = "fa-file-excel-o";
        else if( fileType == "doc" || fileType == "docx" || fileType == "odt" || fileType == "ods" || fileType == "odp" )
          dicon = "fa-file-text-o";
        else if( fileType == "ppt" || fileType == "pptx" )
          dicon = "fa-file-text-o";
        else 
          dicon = "fa-file";
        descHtml += "<a href='"+d.path+"' target='_blanck'><i class='text-red fa "+dicon+"'></i> "+d.name+"</a><br/>" 
        }
      });
    }
    
    descHtml += data.map.editBtn;
    return descHtml;
  }
}
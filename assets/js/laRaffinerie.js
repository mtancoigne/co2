
$(document).ready(function() {

	//mylog.log("elt", elt);
	//mylog.log("elt", type,idElt);
	//mylog.log("links", links);

	setTitle("La Raffinerie");
	toggleProjects();
	toggleAllProjects();
	toggleNavMobile();
	customTabs();
	changeMobileBtnMenu();
	selectProject();
	selectProjectChildren();
	selectProjectStart()
	toggleDescription();
	initDescs();
	//isContributor();
	//loadMyStream(type, idElt);


	//getFileImageContainer();

	calendar.init("#profil-content-calendar");
	//loadCalendar(events);
	
	$("#outils2").click(function(){
		uiCoop.startUI();
		$("#modalCoop").modal("show");
	});

	$('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
		var target = $(e.target).attr("href") // activated tab
		mylog.log("HERE journal", target, initNews);
		if(target == "#agenda"){
			if(initEvent === false){
				initEvent = true;
				loadCalendar('projects', idElt);
				$(".fc-month-button").trigger("click");


			}
			
		}

		if(target == "#documents"){
			if(initDoc === false){
				initDoc = true;
				
				loadGallery("", "", "");
			}
			
			
		}

		if(target == "#journal"){
			if(initNews === false){
				initNews = true;
				loadMyStream('projects', idElt);
			}
			
		}

		
 	
 	
	});

	// $('a[data-toggle="tab"]').click(function(){
	// 	mylog.log("agenda");
	// 	alert("here");
	// 	$(".fc-month-button").trigger("click");
	// 	// setTimeout(200, function(){
	// 	// 	$(".fc-month-button").trigger("click");
	// 	// });
		
	// });

	$('#follows').click(function(){
		var id = $(this).data("id");
		var isco = $(this).data("isco");
		if(isco == false){
			links.connectAjax('projects',id,userId,'citoyens','contributors', null, function(){
				$('#follows').html("Désinscrire");
				$('#follows').data("isco", true);
			});
		}else{
			links.disconnectAjax('projects',id,userId,'citoyens','contributors', null, function(){
				$('#follows').html("S'inscrire");
				$('#follows').data("isco", false);
			});
		}
		
	});

});

function loadCalendar(typeElt, id) {
	mylog.log("loadCalendar",typeElt, id);

	$.ajax({
		type: "POST",
		url: baseUrl+'/co2/element/getdatadetail/type/'+typeElt+'/id/'+id+'/dataName/events/sub/2',
		data: {},
		dataType: "json"
	}).done(function(data){
		mylog.log("loadCalendar", data);
		$("#profil-content-calendar").fullCalendar('destroy');
		calendar.showCalendar("#profil-content-calendar", data, "month");
		$("#profil-content-calendar").fullCalendar("gotoDate", moment(Date.now()));
		$(window).on('resize', function(){
			$("#profil-content-calendar").fullCalendar('destroy');
			calendar.showCalendar("#profil-content-calendar", data, "month");
		});

		var str = "";

		var str = directory.showResultsDirectoryHtml( data, null, null, false, "list");
		// $.each(data, function(key, value){
		// 	value.hash = "#@" + params.slug;
		// 	str += directory.lightPanelHtml(value);  
		// });
		$("#list-calendar").html(str);
		$(".fc-month-button").trigger("click");
		directory.bindBtnElement();

	}).fail(function(error) {
		$('.ret').html(error);
	});

	
}

function isContributor() {
	mylog.log("isContributor", userId,  elt);
	if( typeof userId != "undefined" &&
		userId != null &&
		userId != "" &&
		typeof elt != "undefined" && 
		typeof elt.links != "undefined" && 
		typeof elt.links.contributors != "undefined" && 
		typeof elt.links.contributors[userId] != "undefined" ) {
		$('#follows').html("Désinscrire");
		$('#follows').data("isco", true);
	} else {
		$('#follows').html("S'inscrire");
		$('#follows').data("isco", false);
	}
}

function initDescs() {
	mylog.log("inintDescs");
	var descHtml = "<i>"+trad.notSpecified+"</i>";
	if($("#descriptionMarkdown").html().length > 0){
		descHtml = dataHelper.markdownToHtml($("#descriptionMarkdown").html()) ;
	}
	$("#descriptionAbout").html(descHtml);
	mylog.log("descHtml", descHtml);
}

function toggleDescription(){
	$('#projectDescription button.customBtnTrigger').click(function(){
		if($(this).hasClass('open')){
			$('#projectDescription #descriptionAbout').css({'max-height': '148px', 'overflow': 'hidden'});
			$(this).removeClass('open').html('Lire la suite <i class="fa fa-angle-down"></i>');
		}
		else{
			$('#projectDescription #descriptionAbout').css({'max-height': 'none', 'overflow': 'auto'});
			$(this).addClass('open').html('Réduire <i class="fa fa-angle-up"></i>');
		}
	});
}
function toggleAllProjects(){
	$('.toggleAllProjects').click(function(){
		if( $(this).hasClass('open') ) {
			$('.projectNavThirdLvl').hide();
			$(this).removeClass('open');
			$('.toggleProjects').removeClass('fa-minus').addClass('fa-plus');
		}
		else{
			$('.projectNavThirdLvl').show();
			$(this).addClass('open');
			$('.toggleProjects').removeClass('fa-plus').addClass('fa-minus');
		}
	});
}
function toggleProjects(){
	$('.toggleProjects').click(function(){
		if( $(this).hasClass('fa-plus') ){
			$(this).removeClass('fa-plus').addClass('fa-minus');
		}
		else{
			$(this).removeClass('fa-minus').addClass('fa-plus');
		}

		$(this).parents('li').next('.projectNavThirdLvl').toggle();
	});
}
function toggleNavMobile(){
	$('.projectNavTriggerMobile').click(function(e){
		e.preventDefault();
		if(!$('.projectNav').hasClass('isAnimated')){

			$('.projectNav').addClass('isAnimated');
			if($(this).hasClass('active')){
				projectCloseMobileNav();
			}
			else{
				projectOpenMobileNav();
			}
		}
	});
}
function projectCloseMobileNav(){
	$('.projectNav').animate({left: '-100%'}, 200, function(){
		$('.projectNavTriggerMobile').removeClass('active');
		$('.projectNav').removeClass('isAnimated');
	});
}
function projectOpenMobileNav(){
	$('.projectNav').animate({left: '0'}, 300, function(){
		$('.projectNavTriggerMobile').addClass('active');
		$('.projectNav').removeClass('isAnimated');
	});
}
function showProjectMask(){
	$('.projectMask').show();
}
function hideProjectMask(){
	$('.projectMask').hide();
}
function customTabs(){
	$('.customTab').hide();
	$('.customTabTrigger').click(function(e){
		e.preventDefault();

		var $this = $(this);
		var href = $this.attr('href');
		var $tab = $(href);

		if( $tab.hasClass('active') ){
			$tab.hide().removeClass('active');
			$this.removeClass('active');
		}
		else{
			$('.customTab').hide().removeClass('active');
			$('.customTabTrigger').removeClass('active');
			$tab.show().addClass('active');
			$this.addClass('active');
		}
	});
	$('.closeCustomTab').click(function(e){
		e.preventDefault();
		var $tab = $(this).parents('.customTab');
		$tab.hide().removeClass('active');
	});
}
function selectProjectStart(){
	var hash = window.location.hash;
	if(hash){
		$(".projectNav .linkMenu[data-slug='" + hash.substring(1) + "']").click();
	}
	else{
		$('.projectNav .projectNavFirstLvl .linkOrganization').click();
	}
}
function selectProjectChildren(){
	$("#projectChildren").on('click','.linkMenu',function(){
		var key  = $(this).data("key");
		$(".projectNav .linkMenu[data-key='" + key + "']").click();
	});
}

function selectProject(){
	$(".projectNav").on('click','.linkMenu',function(e){
		//e.preventDefault();
		$this = $(this);
		var key  = $this.data("key");
		var col  = $this.data("col");
		var color = $this.data("color");
		var countMembers = $this.data('countmembers');
		var countEvents = $this.data('countevents');
		var countProjects = $this.data('countprojects');
		var toggleBtn = $this.prev('i');
		if(toggleBtn.hasClass('fa-plus'))
			toggleBtn.click();

		$(".linkMenu").removeClass('active');
		$this.addClass('active');

		var colorHover = color.replace("1)",".8)" );
				colorHover = colorHover.replace(".82)",".6)");

		$('#tempStyle').remove();

		$('<style id="tempStyle">.projectBorderColorHover:hover{border-color:'+colorHover+' !important;}.projectBgColorHover:not(.active):hover{background-color:'+colorHover+' !important;}.projectBgColorActive.active{background-color:'+color+';}.projectNav .linkMenu.active::after{background-color:'+color+'}</style>').appendTo('head');

		$('.projectColorBorder').css('border-color', color);
		$('.projectBgColor').css('background-color', color);

		$('.customTab').hide().removeClass('active');
		$('.customTabTrigger').removeClass('active');

	 	$.ajax({
			type: "POST",
			url: baseUrl+'/co2/element/get/id/'+key+'/type/'+col,
			data: {},
			dataType: "json",
			beforeSend: showProjectMask()
		}).done(function(data){
			feedProject(data, countMembers, countEvents, countProjects);
    	//mylog.log( "Data Ajax : " + data.toSource() );
			//loadMyStream(col, key);
		}).fail(function(error) {
    	//mylog.log( "error : " + error.toSource() );
    	$('.ret').html(error);
	  }).always(function(){
	  	hideProjectMask();
	  	if(isMobile()) 
	  		projectCloseMobileNav();
	  });
	});
}
function feedProject(el, countMembers, countEvents, countProjects){

 	mylog.log('feedProject : ');
	
	// name
	elt = el.map;
	var id = el.map._id.$id ;
	idElt = id;
	//$(".projectName, .projectNameParent").html(el.map.name);

	$("#follows").data( "id", id);

	// shortdecription
 	var shortDescription = el.map.shortDescription ? el.map.shortDescription : "";
 	if(el.map.shortDescription){
 		$(".projectShortDescription").show().html(el.map.shortDescription);
 	}
 	else{
 		$(".projectShortDescription").hide();
 	}

 	// description
 	if(el.map.description){
 		$(".customTabTriggerDescr").show();
 		if(el.map.description.length > 0){
			descHtml = dataHelper.markdownToHtml(el.map.description) ;
		}
 		$("#descriptionAbout").html(descHtml);
 	}
 	else{
 		$(".customTabTriggerDescr").hide();
 	}

 	// banner image
 	var bannerImgUrl = el.map.profilBannerUrl ? baseUrl + el.map.profilBannerUrl : defaultBannerUrl;
 	$(".projectBanner").css('background-image', 'url(' + bannerImgUrl + ')');
 	//alert(getBaseUrl() + el.map.profilBannerUrl);

 	// thumb image
 	var thumbImgUrl = el.map.profilMediumImageUrl ? baseUrl + el.map.profilMediumImageUrl : defaultBannerUrl;
 	$(".projectThumb").css('background-image', 'url(' + thumbImgUrl + ')');

 	// Members count

 	$('.projectNbMembers').text(countMembers);
 	$('.projectNbProjects').text(countProjects);
 	$('.projectNbEvents').text(countEvents);

 	// Project List
 	
 	$('#projectChildren .row').html('');

 	if( el.map.links.projects != null ){

 		$('.customTabTriggerProj').show();

	  $(el.map.links.projects).each(function(){
	 		//mylog.log("Project : " + this.toSource() );
	 		for (key in this) {

			  mylog.log("ProjectKey : " + key );

			  var $link = $('.linkMenu[data-key="' + key + '"]');
	 			var projectName = $link.text();
	 			var projectCollection = $link.attr('data-col');
	 			var projectImage = $link.attr('data-img');
	 			var htmlProjects = "<div class='col col-xs-6 col-md-3'>" +
	 			"<a href='#" + el.map.slug + "' class='linkMenu' data-col='" + projectCollection + "'" + 
	 			"data-key='" + key + "' style='background-image: url(" + projectImage + ");'>" + 
	 			"<span class='projectBorderColorHover'><h4>" + projectName+ "</h4></span></a></div>";

	 			$(htmlProjects).appendTo('#projectChildren .row');

			}
	 	});
 	}
 	else{
 		$('.customTabTriggerProj').hide();
 	}

 	// CONTACTS

 	$('#projectContacts .row').html('');

 	if( el.map.contacts != null ){
 		$('.customTabTriggerContacts').show();

 		// $(el.map.contacts).each(function(index){
 		$.each(el.map.contacts, function (key,value){
	 		//mylog.log("Project : " + this.toSource() );
	 			var htmlContacts = "<div class='col col-xs-6 col-md-3'>";
	 			htmlContacts += "<h4>" + (typeof value['name'] != "undefined" ? value['name'] : "") + "</h4>";
	 			if(typeof value['email'] != "undefined")
	 				htmlContacts += "<a href='mailto:"+value['email']+"'>"+value['email']+"</a>" ;
	 			if(typeof value['telephone'] != "undefined" && typeof value['telephone'][0] != "undefined")
	 				htmlContacts += "<br><a href='tel:"+value['telephone'][0]+"'>"+value['telephone'][0]+"</a>";
	 			htmlContacts += "</div>";
	 			$(htmlContacts).appendTo('#projectContacts .row');
	 
	 	});
 	} else{
 		$('.customTabTriggerContacts').hide();
 	}

 	initEvent = false;
	initDoc = false;
	initNews = true;

 	isContributor();
 	// loadMyStream('projects', id);
 	// loadCalendar('projects', id);
 	// loadGallery("", "", "");

 	contextData = el.map;
 	contextData.type = 'projects';
 	contextData.id = id;


	$("#btnChatRaffinerie").attr("href","https://chat.communecter.org/channel/"+el.map.slug);

 	if(notNull(el.map.links) && 
 		notNull(el.map.links.contributors) && 
 		notNull(el.map.links.contributors[userId]) &&
 		notNull(el.map.links.contributors[userId].isAdmin) &&
 		el.map.links.contributors[userId].isAdmin == true){
 		$(".projectAdmin").attr("href",baseUrl+"/#@"+el.map.slug);
 		$(".projectAdmin").show()
	} else {
		$(".projectAdmin").hide();
	}
	$('a[href="#journal"]').trigger("click");
	loadMyStream('projects', idElt);
 	
}

function loadGallery(dir, key, folderId){
	toogleNotif(false);
	var url = "gallery/index/type/"+type+"/id/"+idElt;
	showLoader('#documents');
	ajaxPost('#documents', baseUrl+'/'+moduleId+'/'+url, 
		null,
		function(){
			$("#header-gallery .nav-pills").hide();

		},"html");
}

function loadMyStream(typeElt, id){
	mylog.log("loadMyStream", typeElt, id);
	$('#journalTimeline').html("");
    var url = "news/co/index/type/"+typeElt+"/id/"+id;///date/"+dateLimit;
    setTimeout(function(){ //attend que le scroll retourn en haut (coInterface.scrollTo)
        showLoader('#journalTimeline');
        ajaxPost('#journalTimeline', baseUrl+'/'+url, 
            {
                nbCol: 1,
                inline : true
            },
            function(){ 
               // loadLiveNow();
            },"html");
    }, 700);
}
function changeMobileBtnMenu(){
	$('.btn-show-mainmenu i').removeClass('fa-bars').addClass('fa-cog'); 
}
function isMobile(){
	return ( $(window).outerWidth() < 997 ) ? true : false ;
}


$('#journalFilters').on('click', '.journalFilter', function(){
	
});
var importObj = {
	container : "#adminContainer" ,
	typeSource : "",
	nameFile : "",
	typeFile : "",
	typeElement : "",
	pInit : null,
	file : [],
	csvFile : {},
	extensions : ["csv", "json", "js", "geojson","xml"],
	urlStep3 : '',
	init : function(pInit = null){
		mylog.log("importObj init",pInit);
		//Init variable
		var copyFilters = jQuery.extend(true, {}, importObj);
		copyFilters.initVar(pInit);
		return copyFilters;

	},
	initVar : function(pInit){
		mylog.log("importObj initVar",pInit);
		var iObj = this;
		iObj.pInit = pInit ;
		iObj.container =  ( (pInit != null && typeof pInit.container != "undefined") ? pInit.container : "#adminContainer" );
		iObj.urlStep3 =  ( (pInit != null && typeof pInit.urlStep3 != "undefined") ? pInit.urlStep3 : baseUrl+'/'+moduleId+'/adminpublic/previewData/' );
		iObj.initViews(iObj, pInit);
		iObj.bind.init(iObj);
		iObj.initStep1(iObj, pInit);
		iObj.initStep2(iObj, pInit);
		iObj.initStep3(iObj, pInit);
	},
	initStep1 : function(iObj, pInit){
		mylog.log("importObj initStep1",pInit);
		if( typeof pInit != "undefined" && pInit != null &&
			typeof pInit.step1 != "undefined"){

			if(typeof pInit.step1.typeElt != "undefined")
				$(iObj.container+ " #typeElt").val(pInit.step1.typeElt);

			if(typeof pInit.step1.typeSource != "undefined")
				$(iObj.container+ " #typeSource").val(pInit.step1.typeSource).change();


			if(typeof pInit.step1.hide != "undefined"){
				$.each(pInit.step1.hide, function(key, value){
					if(value === true)
						$(iObj.container+ " #div"+key).hide();
				});
			}
		}
	},
	initStep2 : function(iObj, pInit){
		mylog.log("importObj initStep2",pInit);
		if( typeof pInit != "undefined" && pInit != null &&
			typeof pInit.step2 != "undefined"){

			if(typeof pInit.step2.inputKey != "undefined")
					$(iObj.container+ " #inputKey").val(pInit.step2.inputKey);
			mylog.log("importObj initStep2 isTest", typeof pInit.step2.isTest);
			if(typeof pInit.step2.isTest == "boolean" )
				//$(iObj.container+ " #isTest").val(pInit.step2.isTest);
				$(iObj.container+ " #checkboxTest").bootstrapSwitch('state', pInit.step2.isTest);

			if(typeof pInit.step2.mapping != "undefined"){
				var nbLigneMapping = $(iObj.container+ " #nbLigneMapping").val();
				var i = 0 ;
				var ligne = "";
				$.each( pInit.step2.mapping, function(key, value){
					ligne = '<tr id="lineMapping'+nbLigneMapping+'" class="lineMapping"> ';
			  		ligne =	 ligne + '<td id="valueSource'+nbLigneMapping+'">' + key + '</td>';
					ligne =	 ligne + '<td id="valueAttributeElt'+nbLigneMapping+'">' + value + '</td>';
					ligne =	 ligne + '<td><input type="hidden" id="idHeadCSV'+nbLigneMapping+'" value="'+ key +'"/><a href="javascript:;" class="deleteLineMapping btn btn-danger">X</a></td></tr>';
			  		nbLigneMapping++;
			  		$(iObj.container+ " #LineAddMapping").before(ligne);
			  		i++;
				});
				$(iObj.container+ " #nbLigneMapping").val(nbLigneMapping);
			}

			if(typeof pInit.step2.hide != "undefined"){
				$.each(pInit.step2.hide, function(key, value){
					if(value === true)
						$(iObj.container+ " #div"+key).hide();
				});
			}

		}
	},
	initStep3 : function(iObj, pInit){
		mylog.log("importObj initStep3",pInit);
		if( typeof pInit != "undefined" && 
			pInit != null &&
			typeof pInit.step3 != "undefined"){

			// if(typeof pInit.step3.inputKey != "undefined")
			// 		$(iObj.container+ " #inputKey").val(pInit.step3.inputKey);
			// mylog.log("importObj initStep2 isTest", typeof pInit.step3.isTest);
			
			if(typeof pInit.step3.hide != "undefined"){
				$.each(pInit.step3.hide, function(key, value){
					if(value === true)
						$(iObj.container+ " #div"+key).hide();
				});
			}

		}
	},
	loaderImport(show){
		if(typeof show !== "undefined" && show !== null && show === true){
			coInterface.showLoader(".importLoader", trad.currentlyloading+"<br>It may take several minutes if you have a lot of data to process");
			$(".importLoader").show(700);
			$(".divImport").hide(700);
		}else{
			$(".importLoader").hide(700);
			$(".divImport").show(700);
			$(".importLoader").html("");
		}
	},
	initViews : function(iObj, pInit){
		mylog.log("importObj initViews",pInit);
		var str = '';
		str = '<div class="importLoader col-xs-12 no-padding" style="display: none"></div>'+
			'<div class="divImport col-xs-12 no-padding">'+
				'<div class="col-xs-12 no-padding">';
				//HEADER
			str += '<center>'+
						'<div class="col-md-12 center bg-azure-light-3 menu-step-tsr section-tsr center">'+
							'<div class="homestead text-white selected" id="menu-step-1">'+
								'<i class="fa fa-2x fa-circle"></i><br/>'+tradAdmin.source+
							'</div>'+
							'<div class="homestead text-white" id="menu-step-2">'+
								'<i class="fa fa-2x fa-circle-o"></i><br/>'+tradAdmin.link+
								
							'</div>'+
							'<div class="homestead text-white" id="menu-step-3">'+
								'<i class="fa fa-2x fa-circle-o"></i><br/>'+tradAdmin.result+
							'</div>'+
						'</div>'+
					'</center>';


			str += '<div class="col-xs-12 block-step-tsr section-tsr" id="menu-step-source" >'+
					'<div id="divtypeElt" class="col-sm-4 col-xs-12">'+
						'<label for="typeElt">Element : </label>'+
						'<select id="typeElt" name="typeElt" class="">'+
							'<option value="-1">Choose</option>'+
							'<option value="'+typeObj.organization.col+'">Organization</option>'+
							'<option value="'+typeObj.project.col+'">Project</option>'+
							'<option value="'+typeObj.event.col+'">Event</option>'+
							'<option value="'+typeObj.person.col+'">Person</option>'+
							'<option value="'+typeObj.poi.col+'">Poi</option>'+
						'</select>'+
					'</div>'+
					'<div id="divtypeSource" class="col-sm-4 col-xs-12">'+
						'<label for="typeSource">Source : </label>'+
						'<select id="typeSource" name="typeSource" class="">'+
							'<option value="-1">Choose</option>'+
							'<option value="url">URL</option>'+
							'<option value="file">File</option>'+
						'</select>'+
					'</div>'+
					'<div id="divchooseMapping" class="col-sm-4 col-xs-12">'+
						'<label for="chooseMapping">Link : </label>'+
						'<select id="chooseMapping" name="chooseMapping" class="">'+
							'<option value="-1">Not link</option>'+
						// <?php
						// 	if(!empty($allMappings)){
						// 		foreach ($allMappings as $key => $value){
						// 			if(empty($value["userId"]) || ( $userId == $value["userId"] || $value["userId"] == "0") ) {
						// 				echo '<option value="'.$key .'">'.$value["name"].'</option>';
						// 			}
						// 		}
						// 	}
						// ?>
						'</select>'+
					
					'</div>'+
					
					'<div id="divFile" style="display : none" class="col-sm-8 col-xs-12">'+
						'<div class="col-sm-2 col-xs-12">'+
							'<label for="fileImport">'+tradAdmin.fileType+'</label>'+
						'</div>'+
						'<div class="col-sm-4 col-xs-12" id="divInputFile">'+
							'<input type="file" id="fileImport" name="fileImport" accept=".csv,.json,.js,.geojson,.xml">'+
						'</div>'+
					'</div>'+
					'<div id="divUrl" style="display : none" class="col-sm-6 col-xs-12">'+
						'<label for="textUrl">URL (JSON) :</label>'+
						'<input type="text" id="textUrl" name="textUrl" value="">'+
					'</div>'+
					'<div id="divPathElement" style="display : none" class="col-sm-4 col-xs-12">'+
						'<label for="pathElement">Path Elements :</label>'+
						'<input type="text" id="pathElement" name="pathElement" value="">'+
					'</div>'+
					'<div id="divCsv" style="display : none" class="col-sm-12 col-xs-12">'+
						// '<div class="col-sm-4 col-xs-12">'+
						// 	'<label for="selectSeparateur">Séparateur : </label>'+
						// 	'<select id="selectSeparateur" name="selectSeparateur" class="">'+
						// 		'<option value=";">Semicolon</option>'+
						// 		'<option value=",">Comma</option>'+
						// 		'<option value=" ">Space</option>'+
						// 	'</select>'+
						// '</div>'+
						// '<div class="col-sm-4 col-xs-12">'+
						// 	'<label for="selectSeparateurText">Separateur de Text : </label>'+
						// 	'<select id="selectSeparateurText" name="selectSeparateur" class="">'+
						// 		'<option value="">Nothing</option>'+
						// 		'<option value=\'"\'>Quotation marks</option>'+
						// 		'<option value="\'">Quotes</option>'+
						// 	'</select>'+
						// '</div>'+
					'</div>'+
					'<div class="col-sm-12 col-xs-12">'+
						'<a href="javascript:;" id="btnNextStep" class="btn btn-success margin-top-15">'+tradAdmin.nextStep+'</a>'+
					'</div>'+
				'</div>' ;

				str += '<div class="col-md-12 mapping-step-tsr section-tsr" id="menu-step-mapping" style="display : none">'+
							'<input type="hidden" id="nbLigneMapping" value="0"/>'+
							'<div class="col-md-12 nbFile text-dark" >'+
								'There is <span id="nbFileMapping" class="text-red"> <span>'+
							'</div>'+
							'<div id="divInputHidden"></div>'+
							'<table id="tabcreatemapping" class="table table-striped table-bordered table-hover">'+
					    		'<thead>'+
						    		'<tr>'+
						    			'<th class="col-sm-5">Source</th>'+
						    			'<th class="col-sm-5">Communecter <a href="https://hackmd.co.tools/KwUwTAbADMDMDGBaA7BCBDRAWAZuqi6EYAnIgCblgBGAjBABzIMli1A"  target="_blank" title="Data sheet referenced" class="homestead text-red"><i class="fa fa-info-circle"></i></a></th>'+
						    			'<th class="col-sm-2">Add/Remove</th>'+
						    		'</tr>'+
					    		'</thead>'+
						    	'<tbody class="directoryLines" id="bodyCreateMapping">'+
							    	'<tr id="LineAddMapping">'+
						    			'<td>'+
						    				'<input type="hidden" name="hiddenSwitch" id="hiddenSwitch" value="noHidden">'+
						    				'<input type="text" id="selectSourceTxt" style="display : none" class="col-sm-12" placeholder="Grap manually my mapping" maxlength="40" title="Maximum 40 caractères">'+
											'<select id="selectSource" class="col-sm-12"></select>'+
						    			'</td>'+
						    			'<td>'+
						    				'<select id="selectAttributesElt" class="col-sm-12"></select>'+
						    			'</td>'+
						    			'<td>'+
						    				'<input type="submit" id="addMapping" class="btn btn-primary col-sm-12" value="Add"/>'+
										'</td>'+
									'</tr>'+
								'</tbody>'+
							'</table>'+
							'<div id="divinputKey" class="col-sm-12 col-xs-12">'+
								'<div  class="col-sm-6 col-xs-12">'+
								'<i>Fields mandatory (*)</i><br />'+
									'<label for="inputKey">Key : </label>'+
									'<input class="" placeholder="Key assigned to all data import" id="inputKey" name="inputKey" value="">'+
								'</div>'+
							'</div>'+
							'<div id="divisTest" class="col-sm-12 col-xs-12">'+
								'<div class="col-sm-6 col-xs-12">'+
									'<label>'+
									'Test : <input type="hidden" id="isTest" name="isTest"/>'+
									'<input id="checkboxTest" name="checkboxTest" type="checkbox" data-on-text="Yes" data-off-text="No" checked/></input></label>'+
								'</div>'+
								'<div class="col-sm-6 col-xs-12" id="divNbTest">'+
									'<div id="divNbTestAff"><label for="inputNbTest">Number of entites to test (max 900) :  </label>'+
									'<input class="" placeholder="" id="inputNbTest" name="inputNbTest" value="5"></div>'+
									'<center>'+
										'<div id="divAjout">'+
											'<a id="btnAjoutMapping" class="btn btn-primary col-sm-12" data-toggle="modal" data-target="#modal-ajout-element">Add my mapping</a>'+
										'</div>'+
										'<div id="divUpdate">'+
											'<a id="btnUpdateMapping" class="btn btn-warning" data-toggle="modal" data-target="#modal-update-element"><strong>Update my mapping</strong></a>'+
											'<a id="btnDeleteMapping" class="btn btn-danger" data-toggle="modal" data-target="#modal-delete-element"><strong>Delete my mapping</strong></a>'+
										'</div>'+
									'</center>'+
								'</div>'+
							'</div>'+
							// '<div class="col-sm-2 col-xs-12"  id="divInvite">'+
							// 	'<div class="col-sm-12 col-xs-12" id="divAuthor">'+
							// 		'<label for="nameInvitor">Author Invite: </label>'+
							// 		'<input class="" placeholder="" id="nameInvitor" name="nameInvitor" value="">'+
							// 	'</div>'+
							// 	'<div class="col-sm-12 col-xs-12" id="divMessage">'+
							// 		'<textarea id="msgInvite" class="" rows="3">Message Invite</textarea>'+
							// 	'</div>'+
							// '</div>'+
							'<div class="col-sm-12 col-xs-12">'+
								'<a href="javascript:;" id="btnPreviousStep" class="btn btn-danger margin-top-15">'+tradAdmin.previousStep+'</a>'+
								'<a href="javascript:;" id="btnNextStep2" class="btn btn-success margin-top-15">'+tradAdmin.nextStep+'</a>'+
							'</div>'+
							'<div class="testAffichageXML"></div>'+
						'</div>';


				str += '<div style="display : none" class="col-md-12 mapping-step-tsr section-tsr" id="menu-step-visualisation">'+
							'<div id="divrepresentation" class="panel-scroll row-fluid" style="height: 300px; overflow-y: scroll;">'+
								'<label class="nbFile text-dark">List of elements :</label>'+
								'<table id="representation" class="table table-striped table-hover"></table>'+
							'</div>'+
							'<br/>'+
							'<div class="panel-scroll row-fluid height-300">'+
								'<label class="nbFile text-dark">List of cities has add :</label>'+
								'<table id="saveCitiesTab" class="table table-striped table-hover"></table>'+
								'<input type="hidden" id="jsonCities" value="">'+
							'</div>'+
							'<br/>'+
							'<div class="col-xs-12 col-sm-6">'+
								'<label class="nbFile text-dark">'+
									'Imported data : <span id="nbFileImport" class="text-red"> <span> '+
								'</label>'+
								'<div class="panel panel-default">'+
									'<div class="panel-body">'+
											'<input type="hidden" id="jsonImport" value="">'+
										    '<div class="col-sm-12" style="max-height : 300px ;overflow-y: auto" id="divJsonImportView"></div>'+
									'</div>'+
								'</div>'+
								'<div class="col-sm-12 center"></div>'+
							'</div>'+
							'<div class="col-xs-12 col-xs-12 col-sm-6">'+
								'<label class="nbFile text-dark">'+
									'Data rejected : <span id="nbFileError" class="text-red"> <span>'+
								'</label>'+
								'<div class="panel panel-default">'+
									'<div class="panel-body">'+
										'<input type="hidden" id="jsonError" value="">'+
										   '<div class="col-sm-12" id="divJsonErrorView" style="max-height : 300px ;overflow-y: auto"></div>'+
									'</div>'+
								'</div>'+
								'<div class="col-sm-12 col-xs-12 center"></div>'+
							'</div>'+
							'<div class="col-xs-12 col-sm-12 margin-top-15">'+
								'<button class="btn btn-danger col-sm-2 col-md-offset-4 " onclick="returnStep2()">Return'+
									'<i class="fa fa-reply"></i>'+
								'</button>'+
								//'<a href="javascript:;" class="btn btn-success col-sm-3 col-md-offset-2 lbh" onclick="location.hash='#admin.view.adddata';loadAdddata();" type="submit" id="btnBDD">Page add of datas</a>
								// '<a href="javascript:;" class="btn btn-primary col-sm-3 col-md-offset-2" type="submit" id="btnImport">Save</a'+
								// '<a href="javascript:;" class="btn btn-primary col-sm-3 col-md-offset-2" type="submit" id="btnError">Save</a'+
								
							'</div>'+
						'</div>';	
				// str+='<div id="modal-ajout-element" class="modal fade" role="dialog" style="z-index: 100000;">'+
				// 		'<div class="modal-dialog">'+
				// 			'<div class="modal-content">'+
				// 				'<div class="modal-header">'+
				// 					'<button type="button" class="close" data-dismiss="modal">&times;</button>'+
				// 					'<h4 class="modal-title text-dark">add my mapping.</h4>'+
				// 				'</div>'+
				// 				'<div class="modal-body text-dark">'+
				// 					'<p><h2></h2>'+
				// 						'<i>Add the mapping, this allow you to reuse </i>'+
				// 					'</p>'+
				// 						'<strong>Grab the name your mapping : </strong>'+
				// 						'<input type="text" id="nameMapping" name="nameMapping">'+
				// 					'</div>'+
				// 					'<div class="modal-footer">'+
				// 						'<a href="javascript:;" id="btnconfirmInsertMapping" type="button" class="btn btn-success margin-top-15">Add my mapping</a>'+
				// 						'<button type="button" class="btn btn-danger margin-top-15" data-dismiss="modal">No</button>'+
				// 					'</div>'+
				// 				'</div>'+
				// 			'</div>'+
				// 		'</div>'+
				// 	'</div>';

		str += '</div>'+
			'</div>';
		
		$(iObj.container).html(str);
	},
	stepTwo : function(iObj){
		mylog.log("importObj stepTwo",iObj);
		var params = {
			typeElement : iObj.typeElement, // Organisation, personne...
			typeFile : iObj.typeFile, //Si JSON or CSV
			idMapping : $(iObj.container+ " #chooseMapping").val(), //Les liens 
			path : $(iObj.container+ " #pathElement").val()
		};

		mylog.log("importObj stepTwo params", params);

		if(iObj.typeFile == "json" || iObj.typeFile == "js" || iObj.typeFile == "geojson" || iObj.typeFile == "xml")
			params["file"] = file ;
		else if(typeFile == "csv")
			iObj.file = dataHelper.csvToArray(iObj.csvFile, $(iObj.container+ " #selectSeparateur").val(), $(iObj.container+ " #selectSeparateurText").val());

		$.ajax({
	        type: 'POST',
	        data: params,
	        url: baseUrl+'/'+moduleId+'/adminpublic/assigndata/',
	        dataType : 'json',
	        async : false,
	        success: function(data)
	        {
	        	mylog.log("importObj stepTwo data",data);
	        	if(data.result == true){
	        		iObj.createStepTwo(iObj,data);
	        	}else{
					toastr.error(data.msg);
				}

	        }
		});
	},
	createStepTwo : function(iObj, data){
		mylog.log("importObj createStepTwo ",data);
		//Gestion du select côté source
		var chaineSelectCSVHidden = "" ;
		//var i = 0;
		if(data.typeFile == "csv"){ //Cas CSV
			$(iObj.container+ " #nbFileMapping").html(iObj.file.length - 1 + "element(s)"); //Compte le nb d'élèment
			$.each(iObj.file[0], function(key, value){
				// chaineSelectCSVHidden += (key == data.arrayMapping[i]?'<option value="'+value+'" disabled>'+value+'</option>':'<option value="'+value+'">'+value+'</option>'); //Pour l'utilisateur puisse rajouté un élèment en cas s'il lui manque
				chaineSelectCSVHidden += '<option value="'+value+'">'+value+'</option>';
				// i++;
			});
		}
		else if(data.typeFile == "json" || data.typeFile == "geojson" || data.typeFile == "js"){ //Cas JSON
			$(iObj.container+ " #nbFileMapping").html(data.nbElement  + "element(s)"); //Compte le nb d'élèment
			$.each(data.arbre, function(key, value){
				chaineSelectCSVHidden += '<option value="'+value+'">'+value+'</option>'; //Pour l'utilisateur puisse rajouté un élèment en cas s'il lui manque
			});
		}

		else if(data.typeFile == "xml"){
			$(iObj.container+ " #nbFileMapping").html(data.nbElement + "element(s)");
			$.each(data.arbre, function(key, value){
				chaineSelectCSVHidden += '<option value="'+value+'">'+value+'</option>';
			});

			iObj.file[0] = data.json;
			mylog.log("importObj createStepTwo file", iObj.file);
		}	

		$(iObj.container+ " #selectSource").html(chaineSelectCSVHidden); //Le select de la partie "Lien" côté Source
		

		//On fait de même pour le select côté communecter
		chaineAttributesElt = "" ;
		$.each(data.attributesElt, function(key, value){
			var valueadd = value.replace("address.","");
			if(value == listeObligatoire[value] || valueadd == listeObligatoire[valueadd]){
				chaineAttributesElt += '<option name="optionAttributesElt" value="'+value+'">'+value+' (*) </option>';
			}
			else{
				chaineAttributesElt += '<option name="optionAttributesElt" value="'+value+'">'+value+'</option>';
			}
		});

		$(iObj.container+ " #selectAttributesElt").html(chaineAttributesElt); //Le select de la partie "Lien" côté Communecter

		//Partie HTML a était mise en commentaire
		if(iObj.typeElement != "organisation")
			$(iObj.container+ " #divCheckboxWarnings").hide();

		if(iObj.typeElement != "citoyens")
			$(iObj.container+ " #divInvite").hide();
		
		//Si la chaîne renvoyé est différent de "undefined"
		if(typeof data.arrayMapping != "undefined"){
			var nbLigneMapping = $(iObj.container+ " #nbLigneMapping").val();
			var i = 0 ;
			var ligne = "";
			$.each(data.arrayMapping, function(key, value){
				ligne = '<tr id="lineMapping'+nbLigneMapping+'" class="lineMapping"> ';
		  		ligne =	 ligne + '<td id="valueSource'+nbLigneMapping+'">' + key + '</td>';
				ligne =	 ligne + '<td id="valueAttributeElt'+nbLigneMapping+'">' + value + '</td>';
				ligne =	 ligne + '<td><input type="hidden" id="idHeadCSV'+nbLigneMapping+'" value="'+ key +'"/><a href="javascript:;" class="deleteLineMapping btn btn-danger">X</a></td></tr>';
		  		nbLigneMapping++;

		  		$(iObj.container+ " #LineAddMapping").before(ligne);
		  		i++;
			});
			$(iObj.container+ " #nbLigneMapping").val(nbLigneMapping);
		}

		if(typeof data.idMapping == "undefined" || data.idMapping == "-1" ){
			$(iObj.container+ " #divAjout").show();
			$(iObj.container+ " #divUpdate").hide();
		}
		else if(data.idMapping != ""){
			$(iObj.container+ " #divAjout").hide();
			$(iObj.container+ " #divUpdate").show();
		}
		else{
			$(iObj.container+ " #divAjout").hide();
			$(iObj.container+ " #divUpdate").hide();
		}

		if(typeof data.mapping != "undefined" && typeof data.mapping.name != "undefined"){
			var nameUpdate = '<input type="text" name="nameMappingUpdate" id="nameMappingUpdate" value='+data.mapping.name+'>';
			$(iObj.container+ " #divSaisirNameUpdate").html(nameUpdate);
		}
		
		//bindUpdate();
		iObj.displayStepTwo(iObj);
	},
	displayStepTwo : function (iObj){
		mylog.log("importObj displayStepTwo ");
		$(iObj.container+ " #menu-step-2 i.fa").removeClass("fa-circle-o").addClass("fa-circle");
		$(iObj.container+ " #menu-step-1 i.fa").removeClass("fa-circle").addClass("fa-check-circle");
		$(iObj.container+ " #menu-step-1").removeClass("selected");
		$(iObj.container+ " #menu-step-2").addClass("selected");
		$(iObj.container+ " #menu-step-mapping").show(400);
		$(iObj.container+ " #menu-step-source").hide(400);
		$(iObj.container+ " #menu-step-visualisation").hide(400);
	},
	verifNameSelected : function(arrayName){
		mylog.log("importObj verifNameSelected", arrayName);
		var find = false ; 
		$.each(arrayName, function(key, value){
			var beInt = parseInt(value);
			if(!isNaN(beInt)){
				find = true ;
			}
		});
		return find ;
	},
	addNewMappingForSelecte : function (iObj, arrayMap, subArray){
		mylog.log("importObj getOptionHTML", arrayMap, subArray);
		var firstElt = arrayMap[0] ;
		arrayMap.shift(); //Supprime le premier élèment du tableau.
		var beInt = parseInt(firstElt);
		var newSelect = {} ;
		if(!isNaN(beInt)){
			beInt++;
			if(subArray){	
				if(arrayMap.length >= 1){
					var newArrayMap = jQuery.extend([], arrayMap);
					newSelect[firstElt] = addNewMappingForSelecte(arrayMap, subArray);
					newSelect[beInt.toString()] = iObj.addNewMappingForSelecte(newArrayMap, subArray);
				}
				else{
					newSelect[firstElt] = "";
					newSelect[beInt.toString()] = "";
				}
			}
			else{
				if(arrayMap.length >= 1){
					subArray = true ;
					newSelect[beInt.toString()] = aiObj.ddNewMappingForSelecte(arrayMap, subArray);
				}
				else{
					newSelect[beInt.toString()] = "";
				}
			}
		}
		else{
			if(arrayMap.length >=1){
				newSelect[firstElt] = iObj.addNewMappingForSelecte(arrayMap, true);
			}
			else{
				newSelect[firstElt] = "";
			}
		}
		return newSelect ;
	},
	getOptionHTML :function (iObj, arrayOption, objectOption, father){
		mylog.log("importObj getOptionHTML", arrayOption, objectOption, father);
		if(!jQuery.isPlainObject(objectOption)){
			arrayOption.push(father);
		}
		else{
			$.each(objectOption, function(key, values){
				if(father != "")
					var newfather = father +"."+ key
				else
					var newfather = key
				iObj.getOptionHTML(arrayOption, values, newfather);
			});
		}
	},
	verifBeforeAddSelect : function (iObj, arrayMap){
		mylog.log("importObj verifBeforeAddSelect", arrayMap);
		$(iObj.container+' [name=optionAttributesElt]').each(function() {
		  	var option = $(this).val() ;
		  	var position = jQuery.inArray( option, arrayMap);
		  	if(position != -1)
		  		arrayMap.splice(position, 1);
			//mylog.log("option", option);
		});
	},
	switchChamp : function (iObj){
		mylog.log("importObj switchChamp");
		var selectSource = $(iObj.container+ " #selectSource option:selected").val();
		var hiddenSwitch = $(iObj.container+ " #hiddenSwitch").val();
		if(hiddenSwitch === "noHidden" && selectSource === "Autre"){
			$(iObj.container+ " #selectSource").hide();
			$(iObj.container+ " #selectSourceTxt").show();
			$(iObj.container+ " #hiddenSwitch").val('yesHidden');
			return true;
		} else {
			$(iObj.container+ " #selectSource").show();
			$(iObj.container+ " #selectSourceTxt").hide();
			return false;
		}
	},
	bind : {
		init : function(iObj){
			mylog.log("importObj bind init",iObj);
			$(iObj.container+ " #typeSource").change( function (){
				mylog.log("importObj bind init #typeSource");
				iObj.typeSource = $(iObj.container+ " #typeSource").val();
				//Cache les elements de l'url
				if(iObj.typeSource == "url"){
					$(iObj.container+ " #divUrl").show();
					$(iObj.container+ " #divPathElement").show();
					$(iObj.container+ " #divCsv").hide();
					$(iObj.container+ " #divFile").hide();
				}	
				//Cache du file
				else if(iObj.typeSource == "file"){
					$(iObj.container+ " #divUrl").hide();
					$(iObj.container+ " #divFile").show();
					$(iObj.container+ " #divPathElement").hide();
				}else{
				//CACHE TOUT
					$(iObj.container+ " #divUrl").hide();
					$(iObj.container+ " #divFile").hide();
					$(iObj.container+ " #divPathElement").hide();
				}	
			});

			$(iObj.container+ " #fileImport").change(function(e) {
				mylog.log("importObj bind init #fileImport");
				var fileSplit = $(iObj.container+ " #fileImport").val().split("."); 
				
				if(iObj.extensions.indexOf(fileSplit[fileSplit.length-1]) == -1){
					toastr.error("You will need to select a file CSV, JSON or XML");
					return false ;
				}
				
				//Affiche les fichiers qu'on compte upload sur le serveur
				var nameFileSplit = fileSplit[0].split('\\');
				mylog.log("importObj bind init #fileImport nameFileSplit", nameFileSplit);
				iObj.nameFile = nameFileSplit[nameFileSplit.length-1];
				iObj.typeFile = fileSplit[fileSplit.length-1];

				//Si le format ne correspond pas
				if(iObj.extensions.indexOf(iObj.typeFile) == -1) {
					alert("Upload CSV, JSON or XML");
					return false;
				}

				iObj.file = [];		//Tableau vide
				if (e.target.files != undefined) {
					mylog.log("importObj bind init #fileImport nameFileSplit", e.target.files);
					var reader = new FileReader();
					reader.onload = function(e) {
						mylog.log("importObj bind init #fileImport reader.onload");
						if(iObj.typeFile == "csv"){
							iObj.csvFile = e.target.result;
							var resPapa = Papa.parse(iObj.csvFile);
							mylog.log("importObj bind init #fileImport resPapa", resPapa);
							if(typeof resPapa.data != "undefined" && resPapa.data.length > 1){
								iObj.file = resPapa.data;
							}
							$(iObj.container+ " #divCsv").show();
						} else if(iObj.typeFile == "json" || iObj.typeFile == "js" || iObj.typeFile == "geojson") {
							$(iObj.container+ " #divCsv").hide();
							$(iObj.container+ " #divPathElement").show();
							iObj.file.push(e.target.result);
						} else if(iObj.typeFile == "xml"){
							iObj.file.push(e.target.result);
							$(iObj.container+ " #divCsv").hide();
							$(iObj.container+ " #divPathElement").hide();
						}	
					};
					reader.readAsText(e.target.files.item(0));
				}
				return false;
			});
			

			$(iObj.container+ " #btnNextStep").off().on('click', function(e){
				mylog.log("importObj #btnNextStep", $(iObj.container+ " #typeSource").val(), iObj.file.length);
		  		if($(iObj.container+ " #typeElt").val() == "-1"){
		  			toastr.error("You will need to select an element type");
		  			return false ;
		  		}
		  		else if($(iObj.container+ " #typeSource").val() == "-1"){
		  			toastr.error( "You will need to select an source");
		  			return false ;
		  		}else if($(iObj.container+ " #typeSource").val() == "file"){
		  			if(iObj.file.length == 0 && iObj.csvFile.length == 0){
			  			toastr.error("You will need to select an file");
			  			return false ;
			  		}
		  		}
		  		var typeSource = $(iObj.container+ " #typeSource").val();
		  		iObj.typeElement = $(iObj.container+ " #typeElt").val();

				//Si on choisir de transmettre un lien
		  		if(typeSource == "url"){
					iObj.nameFile = "JSON_URL";
		  			iObj.typeFile = "json";			
					$.ajax({
						url: baseUrl+'/'+moduleId+'/adminpublic/getdatabyurl/',
						type: 'POST',
						dataType: 'json', 
						data:{ url : $(iObj.container+ " #textUrl").val() },
						async : false,
						success: function (obj){
							mylog.log('importObj #btnNextStep success' , obj);
							file.push(obj.data) ;
							iObj.stepTwo(iObj);
						},
						error: function (error) {
							mylog.log('importObj #btnNextStep error', error);
						}
					});
				}	
				//Si on choisi d'uplod un fichier
				else if(typeSource == "file"){
					iObj.stepTwo(iObj);
				}	
				
				return false;
		  		
		  	});


		  	$(iObj.container+ " #checkboxTest").bootstrapSwitch();
			$(iObj.container+ " #checkboxTest").on("switchChange.bootstrapSwitch", function (event, state) {
				mylog.log("importObj #checkboxTest", state);
				$(iObj.container+ " #isTest").val(state);
				if(state == true){
					$(iObj.container+ " #divNbTestAff").removeClass("hide");
				}else{
					$(iObj.container+ " #divNbTestAff").addClass("hide");
				}
			});

		  	$(iObj.container+ " #addMapping").off().on('click', function(){
		  		mylog.log("importObj #addMapping");
		  		var nbLigneMapping = parseInt($(iObj.container+ " #nbLigneMapping").val()) + 1;
		  		var error = false ;
		  		var msgError = "" ;

		  		//var selectValueHeadCSV = $(iObj.container+ " #selectHeadCSV option:selected").text() ;
		  		var selectSource = $(iObj.container+ " #selectSource option:selected").val() ;
		  		//var selectSource = $(iObj.container+ " #selectSource").val() ;
		  		var selectSourceTxt = $(iObj.container+ " #selectSourceTxt").val();
		  		var selectAttributesElt = $(iObj.container+ " #selectAttributesElt option:selected").val() ;


		  		var inc = 0;
		  		while(error == false && inc <= nbLigneMapping){
		  			if($(iObj.container+ " #valueSource"+inc).text() == selectSource ){
		  				error = true;
		  				msgError = "You have already added this elements in the CSV column";
		  			}

		  			if($(iObj.container+ " #valueAttributeElt"+inc).text() == selectAttributesElt){
		  				error = true;
		  				msgError = "You have already add this elements in the mapping column";
		  			}
		  			inc++;
		  		}

		  		if(error == false){

		  			var attributeEltSplit = selectAttributesElt.split(".");
		  			if(iObj.verifNameSelected(attributeEltSplit)){
		  				var newOptionSelect = iObj.addNewMappingForSelecte(iObj, attributeEltSplit, false);
			  			var arrayOption = [];
			  			iObj.getOptionHTML(iObj, arrayOption, newOptionSelect, "");
			  			iObj.verifBeforeAddSelect(iObj, arrayOption);
			  			chaine = "" ;
			  			$.each(arrayOption, function(key, value){
			  				chaine = chaine + '<option name="optionAttributesElt" values="'+value+'">'+value+'</option>'
			  			});

			  			$(iObj.container+ " #selectAttributesElt").append(chaine);
		  			}
		  			 			
			  		var ligne = '<tr id="lineMapping'+nbLigneMapping+'" class="lineMapping"> ';
			  		//ligne = ligne + ($(iObj.container+ " #hiddenSwitch").val() === "yesHidden" ? '<td id="valueSource'+nbLigneMapping+'">' + selectSourceTxt + '</td>' : '<td id="valueSource'+nbLigneMapping+'">' + selectSource + '</td>');
			  		ligne = ligne +'<td id="valueSource'+nbLigneMapping+'">' + selectSource + '</td>';
			  		ligne =	 ligne + '<td id="valueAttributeElt'+nbLigneMapping+'">' + selectAttributesElt + '</td>';
			  		//ligne = ligne + ($(iObj.container+ " #hiddenSwitch").val() === "yesHidden" ? '<td><input type="hidden" id="idHeadCSV'+nbLigneMapping+'" value="'+ selectSourceTxt +'"/><a href="javascript:;" class="deleteLineMapping btn btn-danger">X</a></td></tr>' : '<td><input type="hidden" id="idHeadCSV'+nbLigneMapping+'" value="'+ selectSource +'"/><a href="javascript:;" class="deleteLineMapping btn btn-danger">X</a></td></tr>');
			  		ligne = ligne + '<td><input type="hidden" id="idHeadCSV'+nbLigneMapping+'" value="'+ selectSource +'"/><a href="javascript:;" class="deleteLineMapping btn btn-danger">X</a></td></tr>';

			  		$(iObj.container+ " #nbLigneMapping").val(nbLigneMapping);
			  		$(iObj.container+ " #LineAddMapping").before(ligne);

			  		$(iObj.container+ " #selectSourceTxt").val('');
			  		iObj.switchChamp(iObj);
			  		
			  	} else {
			  		toastr.error(msgError);
			  	}

			  	//bindUpdate();
		  		return false;
		  	});

		  	$(iObj.container+ " .deleteLineMapping").off().on('click', function(){
		  		$(this).parent().parent().remove();
		  	});

		  	$(iObj.container+ " #btnNextStep2").off().on('click', function(){
		  		themeObj.blockUi.show();
				setTimeout(function(){ iObj.preStep2(iObj); }, 2000);
		  		return false;
		  	});
		}
	},
	cleanVisualisation : function (iObj){
		mylog.log("importObj cleanVisualisation");
		$(iObj.container+ " #representation").html("");
		$(iObj.container+ " #jsonImport").val("");
	    $(iObj.container+ " #jsonError").val("");
	    $(iObj.container+ " #jsonCities").val("");
	},
	preStep2 : function (iObj){
		mylog.log("importObj preStep2");
		iObj.cleanVisualisation(iObj);
		var nbLigneMapping = $(iObj.container+ " #nbLigneMapping").val();
		var inputKey = $(iObj.container+ " #inputKey").val().trim();
		var infoCreateData = [] ;

		//Je sais pas à quoi sa cela correspond
		if(nbLigneMapping == 0){
			toastr.error("You must make at least one data assignment"); 
			$.unblockUI();
  			return false ;
		}else if(inputKey.length == 0){ //Il est n'a pas de clé
			toastr.error("You will need to add the Key");
			$.unblockUI();
  			return false ;
		}
		else{
			//Pour i allant de 0 à nbLigneMapping
			for (i = 0; i <= nbLigneMapping; i++){
				//si lineMapping.lenght+i
	  			if($('#lineMapping'+i).length){
					  // création d'un tableau vide
	  				var valuesCreateData = {};
					valuesCreateData['valueAttributeElt'] = $(iObj.container+ " #valueAttributeElt"+i).text(); //Récupère les informations du tableau Etape "Lien" côté communecter
					valuesCreateData['idHeadCSV'] = $(iObj.container+ " #idHeadCSV"+i).val(); //Récupère les informations du tableau Etape "Lien" partie "Source"
					infoCreateData.push(valuesCreateData);
	  			}	
	  		}
	  		if(infoCreateData != []){	
	  			
				//Renseigne les informations importants.
	  			var params = {
	        		infoCreateData : infoCreateData, 
	        		typeElement : iObj.typeElement,
	        		nameFile : iObj.nameFile,
	        		typeFile : iObj.typeFile,
	        		pathObject : $('#pathObject').val(),
			        key : inputKey,
			        warnings : $(iObj.container+ " #checkboxWarnings").is(':checked')
			    }

				//Si le typeElement concerne les personnes
			    if(iObj.typeElement == typeObj.person.col){
			    	params["msgInvite"] = $(iObj.container+ " #msgInvite").val();
					params["nameInvitor"] = $(iObj.container+ " #nameInvitor").val();
				}

				//Si on a coché la partie "test"
	  			if($(iObj.container+ " #checkboxTest").is(':checked')){
					//Si c'est un fichier de type "csv"
	  				if(iObj.typeFile == "csv"){
	  					var subFile = iObj.file.slice(0,parseInt($(iObj.container+ " #inputNbTest").val())+1);  // Je sais pas à quoi sert cette ligne.
	  					params["file"] = Papa.unparse(subFile);
	  				}
					// Si c'est un fichier de type JSON
			  		else if(iObj.typeFile == "json" || iObj.typeFile == "js" || iObj.typeFile == "geojson"){
			  			params["file"] = iObj.file;
			  			params["nbTest"] = $(iObj.container+ " #inputNbTest").val();
			  		}
			  		else if(iObj.typeFile == "xml"){
			  			params["pathObject"] = "elements";
			  			params["file"] = iObj.file;
			  			params["nbTest"] = $(iObj.container+ " #inputNbTest").val();	
			  		}
	  				iObj.stepThree(iObj, params, params["file"]);
		  			iObj.showStep3(iObj);

	  			}else{
	  				//Si c'est un fichier csv
	  				if(iObj.typeFile == "csv"){
	  					iObj.stepThree(iObj, params, iObj.csvFile);
		  				iObj.showStep3(iObj);
	  				// 	var fin = false ;
				  	// 	var indexStart = 1 ;
				  	// 	var limit = 10 ; //On ne charge pas le fichier d'un block, mais peu par peu
				  	// 	if(typeof iObj.pInit != "undefined" && iObj.pInit != null &&
				  	// 		typeof iObj.pInit.step2 != "undefined" && iObj.pInit.step2 != null &&
				  	// 		typeof iObj.pInit.step2.limit != "undefined" && iObj.pInit.step2.limit != null)
				  	// 		limit = iObj.pInit.step2.limit;

				  	// 	var indexEnd = limit;
				  	// 	var head = iObj.file.slice(0,1);

				  	// 	while(fin == false){
				  	// 		params["indexStart"] = indexStart;
				  	// 		subFile = head.concat(iObj.file.slice(indexStart,indexEnd));
				  	// 		mylog.log("subFile", subFile.length);
				  	// 		params["file"] = subFile;

				  	// 		iObj.stepThree(iObj, params);

							// indexStart = indexEnd ;
				  	// 		indexEnd = indexEnd + limit;
				  	// 		if(indexStart > iObj.file.length)
				  	// 			fin = true ;
				  	// 	}
				  	// 	iObj.showStep3(iObj);

	  				}
					//Si c'est un fichier JSON
			  		else if(iObj.typeFile == "json" || iObj.typeFile == "js" || iObj.typeFile== "geojson"){
			  			params["file"] = iObj.file;
				  		iObj.stepThree(iObj, params, iObj.file);
				  		iObj.showStep3(iObj);
			  		}
			  		else if(iObj.typeFile == "xml"){
			  			params["pathObject"] = "elements";
			  			params["file"] = iObj.file;
			  			iObj.stepThree(iObj, params, iObj.file);
			  			iObj.showStep3(iObj);
			  		}
	  			}
	  		}else{
				$.unblockUI();
				toastr.error("You will need to add the elements in the mapping");
			}
		}
	},
	stepThree : function(iObj, params, fileSend){
		mylog.log("importObj stepThree", params);
		//var blob = new Blob(iObj.file, { type: "text/csv"});
		iObj.loaderImport(true);
		var formData = new FormData();
		$.each(params, function(keyP, valP){
			if(keyP === "infoCreateData")
				formData.append(keyP,JSON.stringify(valP));
			else if(keyP !== "file")
				formData.append(keyP, valP);
		});
		formData.append("file", fileSend);
		var request = new XMLHttpRequest();
		request.onreadystatechange = function(){
			mylog.log("request.responseText", typeof request.responseText, request.readyState, request.status);
		    if (request.readyState == 4 && request.status == 200 ){
		        				//Affiche dans le console
		        mylog.log("request.responseText", typeof request.responseText, request.responseText.length);
		        var data = JSON.parse(request.responseText);
	        	mylog.log("importObj stepThree data",data);
	        	if(data.result){
	        		var importD = "" ;
	        		var errorD = "" ;
	        		var saveCities = "" ;

					if($(iObj.container+ " #jsonImport").val() == "") //Si le tableau d'import est vide
						importD = data.elements; //Import les données elements
					else{
						if(data.elements == "[]") //S'il n'y a pu rien a importé
							importD = $(iObj.container+ " #jsonImport").val(); //Import dans le tableau affichage "Donnée importés"
						else{
							var elt1 = jQuery.parseJSON($(iObj.container+ " #jsonImport").val()); //Transforme un String en Objet
							var elt2 = jQuery.parseJSON(data.elements); //Transforme un String en Objet
							$.each(elt2, function(key, val){ //Boucle
								elt1.push(val); //Ajout dans l'element 1 les informations que continent elt2
							});
							importD = JSON.stringify(elt1); //Convertis un Objet en String
						}

					}
	        		
	        		if($(iObj.container+ " #jsonError").val() == "") //Si le tableau d'erreur est vide
	        			errorD = data.elementsWarnings; //Récupère les donnees d'élèments Warnings.
	        		else{
	        			if(data.elementsWarnings == "[]") //S'il n'y a pu rien à importé
	        				errorD = $(iObj.container+ " #jsonError").val(); //Import dans le tableau affichage "Donnée rejetées"
	        			else{
	        				var elt1E = jQuery.parseJSON($(iObj.container+ " #jsonError").val()); //Transforme un String en Objet
	        				var elt2E = jQuery.parseJSON(data.elementsWarnings); //Transforme un String en Objet
	        				$.each(elt2E, function(key, val){
			        			elt1E.push(val); //Ajout dans un élèment 1 E les informations que contient elt2E
			        		});
	        				errorD = JSON.stringify(elt1E); //Convertis un objet en string
	        			}
	        		}

					//Même étape pour les villes (voir les commentaires en haut)
	        		if($(iObj.container+ " #jsonCities").val() == "")
						saveCities = data.saveCities;
					else{
						if(data.elements == "[]")
							saveCities = $(iObj.container+ " #jsonCities").val();
						else{
							var elt1 = jQuery.parseJSON($(iObj.container+ " #jsonCities").val());
							var elt2 = jQuery.parseJSON(data.saveCities);
							$.each(elt2, function(key, val){
								elt1.push(val);
							});
							saveCities = JSON.stringify(elt1);
						}

					}

					
					mylog.log("importObj stepThree importD",typeof importD); //Affiche un string		
					mylog.log("importObj stepThree errorD",typeof errorD); //Affiche un string

					$(iObj.container+ " #jsonImport").val(importD);
					$(iObj.container+ " #jsonCities").val(saveCities);
					$(iObj.container+ " #jsonError").val(errorD);
					$(iObj.container+ " #divJsonImportView").JSONView(importD); //Affiche le contenu de importD
					$(iObj.container+ " #divJsonErrorView").JSONView(errorD); //Affiche le contenu de errorD

					//Affichage au niveau de "Liste d'élèment"
					var chaine = "" ;
					$.each(data.listEntite, function(keyListEntite, valueListEntite){
						nbFinal++;
						chaine += "<tr>" ;
						if(keyListEntite == 0)
						{
							chaine += "<th>N°</th>";
							$.each(valueListEntite, function(key, value){
								chaine += "<th>"+value+"</th>";
							});
						}else{
							chaine += "<td>"+keyListEntite+"</td>";
							$.each(valueListEntite, function(key, value){
								chaine += "<td>"+value+"</td>";
							});
						}
						chaine += "</tr>" ;
					});
					$(iObj.container+ " #representation").append(chaine); //Affiche les infos


					$(iObj.container+ " #nbFileImport").html(jQuery.parseJSON(importD).length); //Convertis un String en Objet (chiffre)
					$(iObj.container+ " #nbFileError").html(jQuery.parseJSON(errorD).length); //Convertis un String en Objet (chiffre)
					
					if(data.elements != "[]" && data.elementsWarnings == "[]")
					{
						$(iObj.container+ " #btnImport").show();
					}
					else if (data.elementsWarnings != "[]")
					{
						$(iObj.container+ " #btnError").show();
					}
					iObj.loaderImport();
					//$(iObj.container+ " #verifBeforeImport").show();
				}else
					iObj.loaderImport();
			}
		};
		request.open("POST", iObj.urlStep3);
		request.send(formData);


		// $.ajax({
	 //        type: 'POST',
	 //        data: params,
	 //        url: iObj.urlStep3,
	 //        dataType : 'json',
	 //        async : false,
	 //        success: function(data){
		// 		//Affiche dans le console
	 //        	mylog.log("importObj stepThree data",data);
	 //        	if(data.result){
	        		
	 //        		var importD = "" ;
	 //        		var errorD = "" ;
	 //        		var saveCities = "" ;

		// 			if($(iObj.container+ " #jsonImport").val() == "") //Si le tableau d'import est vide
		// 				importD = data.elements; //Import les données elements
		// 			else{
		// 				if(data.elements == "[]") //S'il n'y a pu rien a importé
		// 					importD = $(iObj.container+ " #jsonImport").val(); //Import dans le tableau affichage "Donnée importés"
		// 				else{
		// 					var elt1 = jQuery.parseJSON($(iObj.container+ " #jsonImport").val()); //Transforme un String en Objet
		// 					var elt2 = jQuery.parseJSON(data.elements); //Transforme un String en Objet
		// 					$.each(elt2, function(key, val){ //Boucle
		// 						elt1.push(val); //Ajout dans l'element 1 les informations que continent elt2
		// 					});
		// 					importD = JSON.stringify(elt1); //Convertis un Objet en String
		// 				}

		// 			}
	        		
	 //        		if($(iObj.container+ " #jsonError").val() == "") //Si le tableau d'erreur est vide
	 //        			errorD = data.elementsWarnings; //Récupère les donnees d'élèments Warnings.
	 //        		else{
	 //        			if(data.elementsWarnings == "[]") //S'il n'y a pu rien à importé
	 //        				errorD = $(iObj.container+ " #jsonError").val(); //Import dans le tableau affichage "Donnée rejetées"
	 //        			else{
	 //        				var elt1E = jQuery.parseJSON($(iObj.container+ " #jsonError").val()); //Transforme un String en Objet
	 //        				var elt2E = jQuery.parseJSON(data.elementsWarnings); //Transforme un String en Objet
	 //        				$.each(elt2E, function(key, val){
		// 	        			elt1E.push(val); //Ajout dans un élèment 1 E les informations que contient elt2E
		// 	        		});
	 //        				errorD = JSON.stringify(elt1E); //Convertis un objet en string
	 //        			}
	 //        		}

		// 			//Même étape pour les villes (voir les commentaires en haut)
	 //        		if($(iObj.container+ " #jsonCities").val() == "")
		// 				saveCities = data.saveCities;
		// 			else{
		// 				if(data.elements == "[]")
		// 					saveCities = $(iObj.container+ " #jsonCities").val();
		// 				else{
		// 					var elt1 = jQuery.parseJSON($(iObj.container+ " #jsonCities").val());
		// 					var elt2 = jQuery.parseJSON(data.saveCities);
		// 					$.each(elt2, function(key, val){
		// 						elt1.push(val);
		// 					});
		// 					saveCities = JSON.stringify(elt1);
		// 				}

		// 			}

					
		// 			mylog.log("importObj stepThree importD",typeof importD); //Affiche un string		
		// 			mylog.log("importObj stepThree errorD",typeof errorD); //Affiche un string

		// 			$(iObj.container+ " #jsonImport").val(importD);
		// 			$(iObj.container+ " #jsonCities").val(saveCities);
		// 			$(iObj.container+ " #jsonError").val(errorD);
		// 			$(iObj.container+ " #divJsonImportView").JSONView(importD); //Affiche le contenu de importD
		// 			$(iObj.container+ " #divJsonErrorView").JSONView(errorD); //Affiche le contenu de errorD

		// 			//Affichage au niveau de "Liste d'élèment"
		// 			var chaine = "" ;
		// 			$.each(data.listEntite, function(keyListEntite, valueListEntite){
		// 				nbFinal++;
		// 				chaine += "<tr>" ;
		// 				if(keyListEntite == 0)
		// 				{
		// 					chaine += "<th>N°</th>";
		// 					$.each(valueListEntite, function(key, value){
		// 						chaine += "<th>"+value+"</th>";
		// 					});
		// 				}else{
		// 					chaine += "<td>"+keyListEntite+"</td>";
		// 					$.each(valueListEntite, function(key, value){
		// 						chaine += "<td>"+value+"</td>";
		// 					});
		// 				}
		// 				chaine += "</tr>" ;
		// 			});
		// 			$(iObj.container+ " #representation").append(chaine); //Affiche les infos


		// 			$(iObj.container+ " #nbFileImport").html(jQuery.parseJSON(importD).length); //Convertis un String en Objet (chiffre)
		// 			$(iObj.container+ " #nbFileError").html(jQuery.parseJSON(errorD).length); //Convertis un String en Objet (chiffre)
					
		// 			if(data.elements != "[]" && data.elementsWarnings == "[]")
		// 			{
		// 				$(iObj.container+ " #btnImport").show();
		// 			}
		// 			else if (data.elementsWarnings != "[]")
		// 			{
		// 				$(iObj.container+ " #btnError").show();
		// 			}

		// 			//$(iObj.container+ " #verifBeforeImport").show();
		// 		}
		// 	}
	 //    });
	},
	showStep3 : function (iObj){
		mylog.log("importObj showStep3");
		$(iObj.container+ " #menu-step-3 i.fa").removeClass("fa-circle-o").addClass("fa-circle");
		$(iObj.container+ " #menu-step-2 i.fa").removeClass("fa-circle").addClass("fa-check-circle");
		$(iObj.container+ " #menu-step-2").removeClass("selected");
		$(iObj.container+ " #menu-step-3").addClass("selected");
		$(iObj.container+ " #menu-step-mapping").hide(400);
		$(iObj.container+ " #menu-step-source").hide(400);
		$(iObj.container+ " #menu-step-visualisation").show(400);
		//alert("hello");
		$.unblockUI();
	}
}
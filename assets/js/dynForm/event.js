dynForm = {
    jsonSchema : {
	    title : trad.addEvent,
	    icon : "calendar",
	    type : "object",
	    onLoads : {
	    	onload : function(){
	    		if($("#ajaxFormModal #recurrency").is(':checked')){
	    			$("#ajaxFormModal .allDaycheckbox").fadeOut("slow");
    				$("#ajaxFormModal .startDatedatetime").fadeOut("slow");
    				$("#ajaxFormModal .endDatedatetime").fadeOut("slow");
    				$("#ajaxFormModal .openingHoursopeningHours").fadeIn("slow");
	    		}else{
	    			$("#ajaxFormModal .openingHoursopeningHours").fadeOut("slow");
    				$("#ajaxFormModal .allDaycheckbox").fadeIn("slow");
    				$("#ajaxFormModal .startDatedatetime").fadeIn("slow");
    				$("#ajaxFormModal .endDatedatetime").fadeIn("slow");
	    		}
    	   	},
	    	//pour creer un subevnt depuis un event existant
	    	sub : function(){
	    		
    			$("#ajax-modal .modal-header").removeClass("bg-dark bg-purple bg-red bg-azure bg-green bg-green-poi bg-orange bg-yellow bg-blue bg-turq bg-url")
						  					  .addClass("bg-orange");
	    		if(contextData && contextData.type == "events"){
	    			if(contextData.startDateDB && contextData.endDateDB){
	    				$("#ajaxFormModal").after("<input type='hidden' id='startDateParent' value='"+contextData.startDateDB+"'/>"+
	    										  "<input type='hidden' id='endDateParent' value='"+contextData.endDateDB+"'/>");
	    				$("#ajaxFormModal #startDate").after("<span id='parentstartDate'><i class='fa fa-warning'></i> "+tradDynForm["parentStartDate"]+" : "+ moment( contextData.startDateDB /*,"YYYY-MM-DD HH:mm"*/).format('DD/MM/YYYY HH:mm')+"</span>");
	    				$("#ajaxFormModal #endDate").after("<span id='parentendDate'><i class='fa fa-warning'></i> "+tradDynForm["parentEndDate"]+" : "+ moment( contextData.endDateDB /*,"YYYY-MM-DD HH:mm"*/).format('DD/MM/YYYY HH:mm')+"</span>");
	    			}
	    		}
	    	}
	    },
	    beforeBuild : function(){
	    	dyFObj.setMongoId('events',function(){
	    		uploadObj.gotoUrl = '#page.type.events.id.'+uploadObj.id;
	    	});
	    },
	    afterSave : function(){
			dyFObj.commonAfterSave();
		},
	    beforeSave : function(){
	    	if( !$("#ajaxFormModal #allDay").val())
	    		$("#ajaxFormModal #allDay").val(false);
	    	if( typeof $("#ajaxFormModal #description").code === 'function' )
	    		$("#ajaxFormModal #description").val( $("#ajaxFormModal #description").code() );
	    	
	    	//Transform datetime before sending
	    	//var allDay = $("#ajaxFormModal #allDay").is(':checked');
	    	/*var dateformat = "DD/MM/YYYY";
	    	var outputFormat="YYYY-MM-DD";
	    	if (! allDay) {
	    		var dateformat = "DD/MM/YYYY HH:mm";
	    		var outputFormat="YYYY-MM-DD HH::mm";
	    	}*/
	  	
			mylog.log( "HERE", $("#ajaxFormModal #startDate").val(), moment( $("#ajaxFormModal #startDate").val(), "DD/MM/YYYY HH:mm").format() ) ; 
			$("#ajaxFormModal #startDate").val( moment( $("#ajaxFormModal #startDate").val(), "DD/MM/YYYY HH:mm").format() );
			$("#ajaxFormModal #endDate").val( moment(   $("#ajaxFormModal #endDate").val(), "DD/MM/YYYY HH:mm").format() );
	    },
	    properties : {
	    	info : {
                inputType : "custom",
                html:"<p><i class='fa fa-info-circle'></i> "+tradDynForm["infocreateevent"]+"...</p>",
            },
            name : dyFInputs.name("event"),
	        similarLink : dyFInputs.similarLink,
	        organizer : {
	            inputType : "finder",
	            id : "organizer",
	            label : tradDynForm.whoorganizedevent,
	           	initType: ["projects", "organizations"],
    			multiple : true,
    			initMe : false,
    			openSearch :true,
    		},
	        parent : {
	            inputType : "finder",
	            label : tradDynForm.ispartofevent,
	           	multiple : false,
    			initType: ["events"],
    			openSearch :true,
    			init: function(){
    				mylog.log("finder init2");
    				var finderParams = {
	        			id : "parent",
	        			multiple : false,
	        			initType : ["events"],
	        			values : null,
	        			update : null
	        		};

    				finder.init(finderParams, 
    					function(){
    						$.each(finder.selectedItems, function(e, v){
    							startDateParent=v.startDate;
    							endDateParent=v.endDate;
    						});
		            		$("#startDateParent").val(startDateParent);
		            		$("#endDateParent").val(endDateParent);
		            		if($("#parentstartDate").length <= 0){
			            		$("#ajaxFormModal #startDate").after("<span id='parentstartDate'><i class='fa fa-warning'></i> "+tradDynForm.eventparentStartDate+" : "+ moment( startDateParent /*,"YYYY-MM-DD HH:mm"*/).format('DD/MM/YYYY HH:mm')+"</span>");
		    					$("#ajaxFormModal #endDate").after("<span id='parentendDate'><i class='fa fa-warning'></i> "+tradDynForm.eventparentEndDate+" : "+ moment( endDateParent /*,"YYYY-MM-DD HH:mm"*/).format('DD/MM/YYYY HH:mm')+"</span>");
		            		}
		            		$("#parentstartDate").html("<i class='fa fa-warning'></i> "+tradDynForm.eventparentStartDate+" : "+moment( startDateParent ).format('DD/MM/YYYY HH:mm'));
			    			$("#parentendDate").html("<i class='fa fa-warning'></i> "+tradDynForm.eventparentEndDate+" : "+moment( endDateParent ).format('DD/MM/YYYY HH:mm'));	
    					}
    				);
    			}
			},
	        type : dyFInputs.inputSelect(tradDynForm.eventTypes,null,eventTypes, { required : true }),
	        public : dyFInputs.checkboxSimple("true", "public", 
				{ "onText" : trad.yes,
				  "offText": trad.no,
				  "onLabel" : tradDynForm.public,
				  "offLabel": tradDynForm.private,
				  "labelText": tradDynForm.makeeventvisible+" ?",
				  "labelInformation": tradDynForm.explainvisibleevent
    		}),
	        image : dyFInputs.image(),
            recurrency : dyFInputs.recurrency(false),
            openingHours : dyFInputs.openingHours(false, null, null, false),
            //allDay : dyFInputs.allDay(),
            startDate : dyFInputs.startDateInput("datetime"),
            endDate : dyFInputs.endDateInput("datetime"),
            formLocality : dyFInputs.formLocality(tradDynForm.addLocality, tradDynForm.addLocality),
			location : dyFInputs.location,
            email : dyFInputs.text(),
            tags : dyFInputs.tags(),
            shortDescription : dyFInputs.textarea(tradDynForm.shortDescription, "...",{ maxlength: 140 }),
            url : dyFInputs.inputUrl(),
            "preferences[publicFields]" :  dyFInputs.inputHidden([]),
            "preferences[privateFields]" : dyFInputs.inputHidden([]),
            "preferences[isOpenData]" :  dyFInputs.inputHidden(true),
            "preferences[isOpenEdition]" :  dyFInputs.inputHidden(true)
	    }
	}
};
dynForm = {
    jsonSchema : {
	    title : "Add a badge",
	    icon : " bookmark-o",
	    type : "object",
	    onLoads : {
	    	onload : function(){
    		 	dyFInputs.setSub("bg-azure");
    		}
	    },
        beforeBuild : function(){
            //alert("before Build orga");
            dyFObj.setMongoId('badges', function(){
                //uploadObj.gotoUrl = '/co2/badges';
            });
        },

        beforeSave : function(data,callB){
            if(!notEmpty($("#ajaxFormModal #tags").val())){
                strTags=$("#ajaxFormModal #name").val();
                if(typeof finder.object.parent != "undefined" && Object.keys(finder.object.parent).length > 0){
                    $.each(finder.object.parent, function(e,v){
                        strTags+=","+v.name;
                    });
                }
                $("#ajaxFormModal #tags").val(strTags);
            } 
        },
        afterSave : function(data,callB){
            dyFObj.commonAfterSave();
            //window.location.reload();
           /* if( $(uploadObj.domTarget).fineUploader('getUploads').length > 0 ){
                $(uploadObj.domTarget).fineUploader('uploadStoredFiles');   
            }
            */
            
        },
	    properties : {
            name : dyFInputs.name("badge"),//dyFInputs.inputText("Name", "Must be short and explicit", { required : true }),
			parent : {
				inputType : "finder",
				label : "Ajouter un badge parent",
				//multiple : true,
                initMe:false,
                placeholder:"Rechercher un badge existant",
				rules : { required : false, lengthMin:[1, "parent"]}, 
				initType: ["badges"],
				openSearch :true
			},
            tags :dyFInputs.tags(),
            icon : dyFInputs.inputText("font awesome icon","fa-user, fa-pencil, fa-clock..."),
            color : dyFInputs.inputText("Color", "hexa or web colors allowed"),
            img : dyFInputs.image(),
            description : dyFInputs.textarea(tradDynForm.longDescription, "...",null,true),
            structags :dyFInputs.tags(null,"Structurer le contenu","option : Structure et Hierarchie (parent ou parent.enfant)","fa-user, fa-pencil, fa-clock...")
	    }
	}
};

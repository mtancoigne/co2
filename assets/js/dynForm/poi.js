dynForm = {
    jsonSchema : {
	    title : tradDynForm.addpoi,
	    in : "map-marker",
	    type : "object",
	    onLoads : {
	    	//pour creer un subevnt depuis un event existant
	    	sub : function(){
	    		dyFInputs.setSub("bg-green-poi");
	    	},
	    	onload : function(data){
	    		dyFInputs.setHeader("bg-green-poi");
	    		if(data && data.type){
	    			if(typeof tradCategory[data.type] != "undefined")
	    				tradCat=tradCategory[data.type];
	    			else if(typeof poi.filters != "undefined" && typeof poi.filters[data.type] != "undefined" && typeof poi.filters[data.type].label != "undefined"){
	    				if(typeof tradCategory[poi.filters[data.type].label] != "undefined")
	    					tradCat=tradCategory[poi.filters[data.type].label];
	    				else
	    					tradCat=poi.filters[data.type].label;
	    			}else
	    				tradCat=data.type;
	    			$(".breadcrumbcustom").html( "<h4><a href='javascript:;'' class='btn btn-xs btn-danger'  onclick='dyFObj.elementObj.dynForm.jsonSchema.actions.clear()'><i class='fa fa-times'></i></a> "+tradCat+"</h4>");
					$(".sectionBtntagList").hide();
	    		} else
	    			$(".nametext, .descriptiontextarea, .contactInfotext, .formLocalityformLocality, .urlsarray, .parentfinder, .imageuploader, .tagstags, #btn-submit-form").hide();
	    		dataHelper.activateMarkdown("#ajaxFormModal #description");
	    		
	    	},
	    },
	    beforeSave : function(){
		
	    	if( $("#ajaxFormModal #section").val() )
	    		$("#ajaxFormModal #type").val($("#ajaxFormModal #section").val());

	    	if( typeof $("#ajaxFormModal #description").code === 'function' )  
	    		$("#ajaxFormModal #description").val( $("#ajaxFormModal #description").code() );
	    },
	    beforeBuild : function(){
	    	dyFObj.setMongoId('poi',function(){
	    		uploadObj.gotoUrl = (contextData != null && contextData.type && contextData.id ) ? "#page.type."+contextData.type+".id."+contextData.id+".view.directory.dir.poi" : location.hash;
	    	});
	    },
		afterSave : function(data, callB){
			mylog.log("afterSave poi", data, typeof callB);
			dyFObj.commonAfterSave(data, callB);
			/*
			listObject=$(uploadObj.domTarget).fineUploader('getUploads');
	    	goToUpload=false;
	    	if(listObject.length > 0){
	    		$.each(listObject, function(e,v){
	    			if(v.status == "submitted")
	    				goToUpload=true;
	    		});
	    	}
			if( goToUpload )
		    	$(uploadObj.domTarget).fineUploader('uploadStoredFiles');
		    else { 
		        dyFObj.closeForm(); 
		        urlCtrl.loadByHash( (uploadObj.gotoUrl) ? uploadObj.gotoUrl : location.hash );
	        }*/
	    },
	    canSubmitIf : function () { 
	    	 return ( $("#ajaxFormModal #type").val() ) ? true : false ;
	    },
	    actions : {
	    	clear : function() {
	    		
	    		$("#ajaxFormModal #section, #ajaxFormModal #type, #ajaxFormModal #subtype").val("");

	    		$(".breadcrumbcustom").html( "");
	    		$(".sectionBtntagList").show(); 
	    		$(".typeBtntagList").hide(); 
	    		$(".subtypeSection").html("");
	    		$(".subtypeSectioncustom").show();
	    		$(".nametext, .descriptiontextarea, .contactInfotext, .formLocalityformLocality, .urlsarray, .parentfinder, .imageuploader, .tagstags, #btn-submit-form").hide();
	    	}
	    },
	    properties : {
	    	info : {
                inputType : "custom",
                html:"<p class='text-"+typeObj["poi"].color+"'>"+
                		tradDynForm.infocreatepoi+
                		"<hr>"+
					 "</p>",
            },
            breadcrumb : {
                inputType : "custom",
                html:"",
            },
            sectionBtn :{
                label : tradDynForm.whichkindofpoi+" ? ",
	            inputType : "tagList",
                placeholder : "Choisir un type",
                list : poi.filters,
                trad : tradCategory,
                init : function(){
                	$(".sectionBtn").off().on("click",function()
	            	{
	            		$(".typeBtntagList").show();
	            		$(".sectionBtn").removeClass("active btn-dark-blue text-white");
	            		$( "."+$(this).data('key')+"Btn" ).toggleClass("active btn-dark-blue text-white");
	            		$("#ajaxFormModal #type").val( ( $(this).hasClass('active') ) ? $(this).data('key') : "" );
						//$(".sectionBtn:not(.active)").hide();
						
						$(".breadcrumbcustom").html( "<h4><a href='javascript:;'' class='btn btn-xs btn-danger'  onclick='dyFObj.elementObj.dynForm.jsonSchema.actions.clear()'>"+
							"<i class='fa fa-times'></i></a> "
							+$(this).data('tag')+"</h4>" );
						
						$(".sectionBtntagList").hide();
						$(".nametext, .descriptiontextarea, .contactInfotext, .formLocalityformLocality, .urlsarray, .parentfinder, .imageuploader, .tagstags").show();
						dyFObj.canSubmitIf();
	            	});
	            }
            },
            type : dyFInputs.inputHidden(),
	        name : dyFInputs.name("poi"),
	        parent : {
	            inputType : "finder",
	            label : tradDynForm.whoiscarrypoint,
	           	multiple : true,
	           	rules : { lengthMin:[1, "parent"]}, 
    			initType: ["organizations", "projects", "events"],
    			openSearch :true
	        },
	        image : dyFInputs.image(),
            //description : dyFInputs.description,
            description : dyFInputs.textarea(tradDynForm.longDescription, "...",null,true),
            formLocality : dyFInputs.formLocality(tradDynForm.addLocality, tradDynForm.addLocality),
			location : dyFInputs.location,
            tags :dyFInputs.tags(),
            urls : dyFInputs.urls,
            //parentId : dyFInputs.inputHidden(),
            //parentType : dyFInputs.inputHidden(),
	    }
	}
};
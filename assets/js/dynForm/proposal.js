dynForm = {
	jsonSchema : {
		title : trad.addProposal,
		icon : "gavel",
		type : "object",
		onLoads : {
			
			sub : function(){
			
			},
			onload : function(data){
				if( location.hash.indexOf(".coop.room") >= 0 ){
					mylog.log("typeof contextData.currentRoomId", typeof contextData.currentRoomId);
					mylog.log("init input hidden parentdata : ", contextData.id, contextData.type, contextData.currentRoomId);
					$("#ajaxFormModal #parentId").val(contextData.id);
					$("#ajaxFormModal #parentType").val(contextData.type);
					$("#ajaxFormModal #idParentRoom").val(currentRoomId);

					if (typeof contextData.name != "undefined" && contextData.name != "" && typeof currentRoomName != "undefined"){
						$("#ajax-modal-modal-title").html($("#ajax-modal-modal-title").html()+
															"<small class='text-white'><br>" + 
																tradDynForm.inSpace + 
																" : <i class='text-white'>#"+currentRoomName+"</i>"+
															"</small>" );
					}
					
					$("#ajaxFormModal .publiccheckboxSimple").remove();
					//$("#ajaxFormModal .infoScopecustom").remove();
					$("#ajaxFormModal .locationlocation").remove();
					$("#ajaxFormModal .shortDescription").remove();
					
				} else {
					//alert("HERE");

					pid = ((typeof costum == "undefined" || costum == null) && contextData && contextData.id ) ? contextData.id : userId;
					ptype = ((typeof costum == "undefined" || costum == null) && contextData && contextData.type) ? contextData.type : "citoyens";
					mylog.log("init input hidden parentdata : ", pid, ptype, "");
					$("#ajaxFormModal #parentId").val( pid );
					$("#ajaxFormModal #parentType").val( ptype );

					$("#ajax-modal-modal-title").html(tradDynForm.createsurvey);

					$("#ajax-modal #ajaxFormModal .titletext label").html("<i class='fa fa-chevron-down'></i> "+tradDynForm.surveyname);
					$("#ajax-modal #ajaxFormModal .titletext input#title").attr("placeholder", tradDynForm.surveyname);
					$("#ajaxFormModal .locationBtn").html("<i class='fa fa-home'></i> Sélectionner une commune");
					$("#ajaxFormModal .publiccheckboxSimple").hide();
				}
				
				
				dataHelper.activateMarkdown("#ajaxFormModal #description");
				dataHelper.activateMarkdown("#ajaxFormModal #arguments");

				mylog.log("data", data, typeof data, contextData);


				if(typeof data.amendementActivated != "undefined"){
					data.amendementActivated = (data.amendementActivated == "true" || data.amendementActivated == true) ? true : false;
					data.voteActivated 		 = (data.voteActivated == "true" || data.voteActivated == true) 			? true : false;
					
					mylog.log("checkcheck1", data);

					if(data.amendementActivated == false){
						var idTrue = "#ajaxFormModal .amendementActivatedcheckboxSimple .btn-dyn-checkbox[data-checkval='true']";
						var idFalse = "#ajaxFormModal .amendementActivatedcheckboxSimple .btn-dyn-checkbox[data-checkval='false']";
						
						$("#ajaxFormModal #amendementActivated").val("false");
						$("#ajaxFormModal .amendementActivatedcheckboxSimple .btn-dyn-checkbox[data-checkval='false']").trigger( "click" );

						$(idFalse).addClass("bg-red").removeClass("letter-red");
						$(idTrue).removeClass("bg-green-k").addClass("letter-green");

						$("#ajaxFormModal .amendementActivatedcheckboxSimple .lbl-status-check").html(
							'<span class="letter-red"><i class="fa fa-minus-circle"></i> désactivés</span>');

						if(params && typeof params != "undefined" && typeof params["inputId"] != "undefined") $(params["inputId"]).hide(400);
					}
				}else{

					var idTrue = "#ajaxFormModal .amendementActivatedcheckboxSimple .btn-dyn-checkbox[data-checkval='true']";
					var idFalse = "#ajaxFormModal .amendementActivatedcheckboxSimple .btn-dyn-checkbox[data-checkval='false']";
					$("#ajaxFormModal #amendementActivated").val("false");
					$("#ajaxFormModal .amendementActivatedcheckboxSimple .btn-dyn-checkbox[data-checkval='false']").trigger( "click" );
					$(idFalse).addClass("bg-red").removeClass("letter-red");
					$(idTrue).removeClass("bg-green-k").addClass("letter-green");

					$("#ajaxFormModal .amendementActivatedcheckboxSimple .lbl-status-check").html(
						'<span class="letter-red"><i class="fa fa-minus-circle"></i> désactivés</span>');

					if(params && typeof params != "undefined" && typeof params["inputId"] != "undefined") $(params["inputId"]).hide(400);
				}

				if(typeof data.amendementAndVote != "undefined"){
					data.amendementAndVote = (data.amendementAndVote == "true" || data.amendementAndVote == true) ? true : false;
					
					mylog.log("checkcheck1", data);

					if(data.amendementAndVote == false){
						var idTrue = "#ajaxFormModal .amendementAndVotecheckboxSimple .btn-dyn-checkbox[data-checkval='true']";
						var idFalse = "#ajaxFormModal .amendementAndVotecheckboxSimple .btn-dyn-checkbox[data-checkval='false']";
						
						$("#ajaxFormModal #amendementAndVote").val("false");
						$("#ajaxFormModal .amendementAndVotecheckboxSimple .btn-dyn-checkbox[data-checkval='false']").trigger( "click" );
						$(idFalse).addClass("bg-red").removeClass("letter-red");
						$(idTrue).removeClass("bg-green-k").addClass("letter-green");
						$("#ajaxFormModal .amendementAndVotecheckboxSimple .lbl-status-check").html(
							'<span class="letter-red"><i class="fa fa-minus-circle"></i> désactivés</span>');
						//$("#ajaxFormModal .amendementDateEnddatetime").show();
					}
				} else {
					var idTrue = "#ajaxFormModal .amendementAndVotecheckboxSimple .btn-dyn-checkbox[data-checkval='true']";
					var idFalse = "#ajaxFormModal .amendementAndVotecheckboxSimple .btn-dyn-checkbox[data-checkval='false']";
					$("#ajaxFormModal #amendementAndVote").val("false");
					$("#ajaxFormModal .amendementAndVotecheckboxSimple .btn-dyn-checkbox[data-checkval='false']").trigger( "click" );
					$(idFalse).addClass("bg-red").removeClass("letter-red");
					$(idTrue).removeClass("bg-green-k").addClass("letter-green");

					$("#ajaxFormModal .amendementAndVotecheckboxSimple .lbl-status-check").html(
						'<span class="letter-red"><i class="fa fa-minus-circle"></i> désactivés</span>');
					//$("#ajaxFormModal .amendementDateEnddatetime").show();
				}

				

				if(typeof data.voteDateEnd != "undefined"){
					var d = new Date(data.voteDateEnd);
					var voteDateEnd = moment(d).format("DD/MM/YYYY HH:mm");
					mylog.log("voteDateEnd", d, voteDateEnd);
					$("#ajaxFormModal #voteDateEnd").val(voteDateEnd);
				}

				if(typeof data.amendementDateEnd != "undefined"){
					d = new Date(data.amendementDateEnd);
					var amendementDateEnd = moment(d).format("DD/MM/YYYY HH:mm");
					$("#ajaxFormModal #amendementDateEnd").val(amendementDateEnd);
				}else{
					$("#ajaxFormModal #amendementDateEnd").val("");
				}

				$("#ajaxFormModal .majoritytext").append(
						"<small class='pull-left margin-top-5' id='info'><i class='fa fa-info-circle'></i> "+
						 tradDynForm.proposalMustHaveMore + "<b>"+
						 "50%</b> "+tradDynForm.ofvotes+" <span class='letter-green'>"+tradDynForm.favorables+"</span> "+tradDynForm.tobevalidated +
						"</small>");

				$("#ajaxFormModal #majority").val("50");
				$('#ajaxFormModal #majority').filter_input({regex:'^(0|[1-9][0-9]*)$'});
				
				$('#ajaxFormModal #majority').keyup (function(){
					var strval = $(this).val();
					var intval = strval != "" ? parseInt(strval) : 0;
					mylog.log("intval1", intval);
					if(intval > 100) 
						$("#ajaxFormModal .majoritytext small#info").html(
						"<i class='fa fa-info-circle'></i> "+ tradDynForm.nowayover100);

					if(intval < 50) 
						$("#ajaxFormModal .majoritytext small#info").html(
						"<i class='fa fa-info-circle'></i> "+ tradDynForm.nowayunder50);

					if(intval >= 50 && intval <= 100) 
					$("#ajaxFormModal .majoritytext small#info").html(
						"<i class='fa fa-info-circle'></i> "+tradDynForm.proposalMustHaveMore+" <b>"+
						intval + "%</b> "+tradDynForm.ofvotes+" <span class='letter-green'>"+tradDynForm.favorables+"</span> "+tradDynForm.tobevalidated);
				});

				$('#ajaxFormModal #majority').focusout (function(){
					var strval = $(this).val();
					var intval = strval != "" ? parseInt(strval) : 0;
					mylog.log("intval1", intval);
					if(intval > 100) intval = 100;
					if(intval < 50) intval = 50;
					mylog.log("intval2", intval);
					$('#ajaxFormModal #majority').val(intval);
					$("#ajaxFormModal .majoritytext small#info").html(
						"<i class='fa fa-info-circle'></i> "+tradDynForm.proposalMustHaveMore+" <b>"+
						intval + "%</b> "+tradDynForm.ofvotes+" <span class='letter-green'>"+tradDynForm.favorables+"</span> "+tradDynForm.tobevalidated);
				});

				$("#ajaxFormModal .form-group.answersarray").hide();

				$("#ajaxFormModal .multiChoicecheckboxSimple .btn-dyn-checkbox").click(function(){
					var checkval = $(this).data("checkval");
					mylog.log("checkval", checkval);
					if(checkval==true) {
						$("#ajaxFormModal .form-group.answersarray").hide(200);
						//enable amendement when simple answer 
						if(typeof dyFObj.elementObj.dynFormCostum == "undefined" || 
							typeof dyFObj.elementObj.dynFormCostum.onload == "undefined" ||
								typeof dyFObj.elementObj.dynFormCostum.onload.actions == "undefined" || 
									typeof dyFObj.elementObj.dynFormCostum.onload.actions.hide == "undefined"){
							if(typeof dyFObj.elementObj.dynFormCostum.onload.actions.hide.amendementActivatedcheckboxSimple == "undefined"){
								$("#ajaxFormModal .amendementActivatedcheckboxSimple").show(200);
								$("#ajaxFormModal .amendementActivatedcheckboxSimple .btn-dyn-checkbox[data-checkval='true']").click();
							}
							if(typeof dyFObj.elementObj.dynFormCostum.onload.actions.hide.majoritytext == "undefined")
								$("#ajaxFormModal .majoritytext").show(200);
						}
					}else{
						$("#ajaxFormModal .form-group.answersarray").show(200);
						//disable amendement when multi answer
						$("#ajaxFormModal .amendementActivatedcheckboxSimple .btn-dyn-checkbox[data-checkval='false']").click();
						$("#ajaxFormModal .amendementActivatedcheckboxSimple").hide(200);
						$("#ajaxFormModal .majoritytext").hide(200);

					}
				});

				$("#ajaxFormModal .multiChoicecheckboxSimple #multiChoice").remove();
			}
		},
		beforeBuild : function(){
			//alert("before Build orga");
			dyFObj.setMongoId('proposals', function(){
				uploadObj.gotoUrl = '#dda'; //+uploadObj.id;
			});
		},
		beforeSave : function(){
			if($("#ajaxFormModal #amendementActivated").val() == "true"){
				
				if($("#ajaxFormModal #amendementAndVote").val() == "true"){
					$("#ajaxFormModal #status").val("amendementAndVote");
					$("#ajaxFormModal #amendementAndVote").remove();
				}else
					$("#ajaxFormModal #status").val("amendable");
			}
			else if($("#ajaxFormModal #voteActivated").val() == "true"){
				$("#ajaxFormModal #status").val("tovote");
			}
			mylog.log("beforeSave", $("#ajaxFormModal #voteActivated").val(), $("#ajaxFormModal #status").val());

			var dateformat = "DD/MM/YYYY HH:mm";
			var outputFormat="YYYY-MM-DD HH::mm";

			$("#ajaxFormModal #voteDateEnd").val( moment(   $("#ajaxFormModal #voteDateEnd").val(), dateformat).format() );

			if($("#ajaxFormModal #amendementAndVote").val() == "true")
				$("#ajaxFormModal #amendementDateEnd").val( $("#ajaxFormModal #voteDateEnd").val() );
			else
				$("#ajaxFormModal #amendementDateEnd").val( moment( $("#ajaxFormModal #amendementDateEnd").val(), dateformat).format() );
			
			
		},
		actions : {
	    	formatAmendVote : function() {
	    		$(".multiChoicecheckboxSimple").hide();
	    		$("#amendementActivated").val(true);
	    		$(".amendementActivatedcheckboxSimple").hide();
	    		$(".amendementDateEnddatetime").hide();
	    		$("#amendementAndVote").val(true);
	    		$(".amendementAndVotecheckboxSimple").hide();
	    		$(".majoritytext").hide();
	    		$(".voteAnonymouscheckboxSimple").hide();
	    		$(".voteCanChangecheckboxSimple").hide();
	    		$(".producerfinder").hide();
	    		//$(".scopescope").hide();

	    	},
	    	adminOnly : function() { 
	    		if(  typeof custom != "undefined" 
	    			&& typeof custom.admins != "undefined" 
	    			&& typeof custom.admins[userId] != "undefined" 
	    			&& typeof custom.admins[userId].isAdmin != "undefined" 
	    			&& custom.admins[userId].isAdmin == true ){
						if(typeof custom.dynForm != "undefined"){
							if(typeof custom.dynForm.proposal != "undefined" 
								&& typeof custom.dynForm.proposal.adminOnly != "undefined" ){
									$.each(custom.dynForm.proposal.adminOnly,function(i,p) {
										$("."+p).show();
								 	});
							}
						}
				}
	    	}
	    },
		afterSave : function(data){
			if( $(uploadObj.domTarget).fineUploader('getUploads').length > 0 ){
				uploadObj.callBackData=data;
					$(uploadObj.domTarget).fineUploader('uploadStoredFiles');
			}
			
			if( location.hash.indexOf(".coop.room") >= 0 )
				dyFObj.coopAfterSave(data);
			else if(typeof startNewsSearch != "undefined" && location.hash == "#live") 
				startNewsSearch(true);
			else if(typeof loadNewsStream != "undefined") 
				loadNewsStream(true);
			else if(typeof initSectionNews != "undefined") {
				$('#timeline-page').html("<div class='col-xs-12 text-center'><i class='fa fa-spin fa-circle-o-notch fa-2x'></i></div>");
				setTimeout(function(){ initSectionNews(); }, 2000);
			} else if(typeof initSectionNews != "undefined") {
				pageCount=true;
				searchObject.count=true;
				searchObject.page=0;
				startSearch(0, searchObject.indexStep, searchCallback);
			} else
				urlCtrl.loadByHash(location.hash);
			
				
			// TODO : AFTER save de survey.js a voir si il faut utiliser ou pas 
			
				dyFObj.closeForm();
		},
		properties : {
			 // TODO : sur survey on utilise tradDynForm.infoSurvey

			info : {
				inputType : "custom",
				html:"<br><p><i class='fa fa-info-circle'></i> "+tradDynForm.infoProposal3+"</p>",
			},
			id : dyFInputs.inputHidden(),
			idParentRoom : dyFInputs.inputHidden(),
			title : dyFInputs.name("proposal", { required : false }),
			image : dyFInputs.image(),
			formLocality : dyFInputs.formLocality(tradDynForm.addLocality, tradDynForm.addLocality),
			location : dyFInputs.location,        
			producer : {
	            inputType : "finder",
	            label : tradDynForm.proposedby,
	           	multiple : true,
	           	openSearch:false,
    			initType: ["organizations"]
			},
			shortDescription : dyFInputs.textarea(tradDynForm["shortDescription"], "...",{ maxlength: 140 }),
			description : dyFInputs.textarea(tradDynForm.textproposal, "..."),
			multiChoice : dyFInputs.checkboxSimple("true", "multiChoice", 
													{ "onText" : "pour / contre",//trad.yes,
													  "offText": "choix multiples", //trad.no,
													  "onLabel" : "pour / contre",//tradDynForm.anonymous,
													  "offLabel": "choix multiples", //tradDynForm.nominative,
													  //"inputId" : ".amendementDateEnddatetime",
													  "labelText": "Type de réponse",
													  //"labelInInput": "Activer les amendements",
													  "labelInformation": "<i class='fa fa-info-circle'></i> " + "Choisissez <b>choix multiples</b> pour définir une liste de réponses personnalisées" // tradDynForm.keepSecretIdentityVote

			}),
			answers : dyFInputs.multiChoice,
			//arguments : dyFInputs.textarea(tradDynForm.textargumentsandmore, "..."),
			amendementActivated : dyFInputs.checkboxSimple("true", "amendementActivated", 
													{ "onText" : trad.yes,
													  "offText": trad.no,
													  "onLabel" : trad.activated,
													  "offLabel": trad.disabled,
													  "inputId" : ".amendementDateEnddatetime, .amendementAndVotecheckboxSimple",
													  "labelText": tradDynForm.lblAmmendementEnabled + " ?",
													  "labelInInput": tradDynForm.lblAmmendementEnabled }),

			amendementAndVote : dyFInputs.checkboxSimple("true", "amendementAndVote", 
													{ "onText" : trad.yes,
													  "offText": trad.no,
													  "onLabel" : trad.activated,
													  "offLabel": trad.disabled,
													  "inputTrue" : ".amendementDateEnddatetime",
													  "labelText": tradDynForm["Amendments and votes at the same time"] + " ?",
													  "labelInInput": tradDynForm["Amendments and votes at the same time"],
													  "labelInformation": "<i class='fa fa-info-circle'></i> "+
													  					  tradDynForm["If Yes, votes are enabled during the amendment period"], }),
			amendementDateEnd : dyFInputs.amendementDateEnd,
			voteActivated : dyFInputs.inputHidden( true ),
			voteDateEnd : dyFInputs.voteDateEnd,
			majority: dyFInputs.inputText( trad.ruleOfMajority + " (%) <small class='letter-green'>"+trad.giveValueMajority+"</small>", "50%" ),
			voteAnonymous : dyFInputs.checkboxSimple("true", "voteAnonymous", 
													{ "onText" : trad.yes,
													  "offText": trad.no,
													  "onLabel" : tradDynForm.anonymous,
													  "offLabel": tradDynForm.nominative,
													  //"inputId" : ".amendementDateEnddatetime",
													  "labelText": tradDynForm.voteAnonymous + " ?",
													  //"labelInInput": "Activer les amendements",
													  "labelInformation": "<i class='fa fa-info-circle'></i> " + tradDynForm.keepSecretIdentityVote

			}),
			
			voteCanChange : dyFInputs.checkboxSimple("true", "voteCanChange", 
													{ "onText" : trad.yes,
													  "offText": trad.no,
													  "onLabel" : tradDynForm.changeVoteEnabled,
													  "offLabel": tradDynForm.changeVoteForbiden,
													  //"inputId" : ".amendementDateEnddatetime",
													  "labelText": tradDynForm.authorizeChangeVote,
													  //"labelInInput": "Activer les amendements",
													  "labelInformation": "<i class='fa fa-info-circle'></i> " + tradDynForm.allowChangeVote

			}),
			//scope : dyFInputs.scope,
			tags : dyFInputs.tags(),
			urls : dyFInputs.urls,
			status: dyFInputs.inputHidden( "amendable" ),
			public : dyFInputs.checkboxSimple("true", "public", {
												"onText" : trad.yes,
												"offText": trad.no,
												"onLabel" : tradDynForm.public,
												"offLabel": tradDynForm.private,
												"labelText": tradDynForm.makeprojectvisible+" ?",
												"labelInformation": tradDynForm.explainvisibleproject
			}),
			parentId : dyFInputs.inputHidden(),
			parentType : dyFInputs.inputHidden(),
		}
	}
};
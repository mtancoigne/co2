dynForm = {
    jsonSchema : {
	    title : "Add a Costum",
	    icon : " photo",
	    type : "object",
	    onLoads : {
	    	onload : function(){
    		 	dyFInputs.setSub("bg-dark");
    		}
	    },
        beforeBuild : function(){
            //alert("before Build orga");
            dyFObj.setMongoId('costum', function(){
                uploadObj.gotoUrl = '/costum';
            });

            dyFObj.deepPropertyBuild(dyFObj.elementData.map);

        },
        afterSave : function(data,callB){
            if( $(uploadObj.domTarget).fineUploader('getUploads').length > 0 ){
                $(uploadObj.domTarget).fineUploader('uploadStoredFiles');   
            }
            window.location.reload();
            
        },
	    properties : {
            slug : dyFInputs.inputText("Slug", "Must correspond to an existing element ", { required : true }),
	    }
	}
};
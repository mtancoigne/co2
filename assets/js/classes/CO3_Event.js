var CO3_Event = class extends CO3_Element{
  constructor(pRequeteAjax,pPHDB_Obj){
    super(pRequeteAjax,pPHDB_Obj);
    this.COLLECTION = "events";
    this.CONTROLLER = "event";
    this.MODULE = "event";
    this.ICON = "fa-calendar";
    this.COLOR="#F9B21A";
  }

  PrepareRequeteAjax(pRequeteAjax){
    pRequeteAjax=super.PrepareRequeteAjax(pRequeteAjax);
    pRequeteAjax["where"]["type"]="event";
    return pRequeteAjax;
  }

  ElementRenderHtml(params){
    let str = "";
    if(this.IsDefined(params)){
      
      var colNumStr = "col-xs-12";
      if(typeof params.colNum!=='undefined'){
        if(params.colNum==2) colNumStr="col-md-6 col-sm-12";
        if(params.colNum==3) colNumStr="col-md-4 col-sm-12";
        if(params.colNum==4) colNumStr="col-md-3 col-sm-6 col-xs-12";
      }

      var carousable="";
      if(typeof params.carousable  != "undefined" && (params.carousable))
        carousable=" item";

      var carousel_active="";
      if(typeof params.carousel_active  != "undefined" && (params.carousel_active))
        carousel_active=" active";

      str += "<div class='"+colNumStr+carousable+carousel_active+" searchEntityContainer "+params.type+" "+params.elTagsList+" contain_"+params.type+"_"+params.id+" '>";
      str +=    "<div class='searchEntity' id='entity"+params.id+"'>";

        if(params.updated != null && params.updated.indexOf(trad.ago)>=0 && location.hash == "#agenda")
            params.updated = trad.rightnow;

        if(params.updated != null && !params.useMinSize)
          str += "<div class='dateUpdated'><i class='fa fa-flash'></i> " + params.updated + "</div>";

        var dateFormated = directory.getDateFormated(params);

        params.attendees = "";
        var cntP = 0;
        var cntIv = 0;
        var cntIt = 0;
        if(typeof params.links != "undefined")
          if(typeof params.links.attendees != "undefined"){
            $.each(params.links.attendees, function(key, val){ 
              if(typeof val.isInviting != "undefined" && val.isInviting == true)
                cntIv++; 
              else
                cntP++; 
            });
          }

        params.attendees = "<hr class='margin-top-10 margin-bottom-10'>";
        
        var isFollowed=false;
        if(typeof params.isFollowed != "undefined" ) isFollowed=true;
          
        if(userId != null && userId != "" && params.id != userId){
          // params.attendees += "<button id='btn-participate' class='text-dark btn btn-link followBtn no-padding'"+
          //                     " data-ownerlink='follow' data-id='"+params.id+"' data-type='"+params.type+"' data-name='"+params.name+"'"+
          //                     " data-isFollowed='"+isFollowed+"'>"+
          //                     "<i class='fa fa-street-view'></i> Je participe</button>";
          var isShared = false;
          // params.attendees += "<button id='btn-interested' class='text-dark btn btn-link no-padding margin-left-10'><i class='fa fa-thumbs-up'></i> Ça m'intéresse</button>";
          params.attendees += "<button id='btn-share-event' class='text-dark btn btn-link no-padding margin-left-10 btn-share'"+
                              " data-ownerlink='share' data-id='"+params.id+"' data-type='"+params.type+"' "+//data-name='"+params.name+"'"+
                              " data-isShared='"+isShared+"'>"+
                              "<i class='fa fa-retweet'></i> "+trad["share"]+"</button>";
        }
        //if(typeof params.edit  != "undefined"){
          //params.attendees += "<button class='text-dark btn btn-link no-padding margin-left-10 disconnectConnection'"+
            //                  " data-id='"+params.id+"' data-type='"+params.type+"' data-connection='"+params.edit+"' data-parent-hide='4'>"+
              //                "<i class='fa fa-unlink'></i> "+trad["notparticipateanymore"]+"</button>";
        //}
        params.attendees += "<small class='light margin-left-10 tooltips pull-right'  "+
                                    "data-toggle='tooltip' data-placement='bottom' data-original-title='"+trad["attendee-s"]+"'>" + 
                              cntP + " <i class='fa fa-street-view'></i>"+
                            "</small>";

       // params.attendees += "<small class='light margin-left-10 tooltips pull-right'  "+
         //                           "data-toggle='tooltip' data-placement='bottom' data-original-title='"+trad["concerned"]+"'>" +
           //                    cntIt + " <i class='fa fa-thumbs-up'></i>"+
             //               "</small>";

        params.attendees += "<small class='light margin-left-10 tooltips pull-right'  "+
                                    "data-toggle='tooltip' data-placement='bottom' data-original-title='"+trad["guest-s"]+"'>" +
                               cntIv + " <i class='fa fa-envelope'></i>"+
                            "</small>";

           
        //if(params.imgProfil.indexOf("fa-2x")<0)
        var countSubEvents = ( params.links && params.links.subEvents ) ? "<br/><i class='fa fa-calendar'></i> "+Object.keys(params.links.subEvents).length+" "+trad["subevent-s"]  : "" ; 
        str += '<div class="col-xs-12 col-sm-4 col-md-4 no-padding">'+
                  '<a href="'+params.hash+'" class="container-img-profil lbh add2fav block">'+params.imgMediumProfil+'</a>'+  
                '</div>';
        
        if(userId != null && userId != "" && params.id != userId /*&& !inMyContacts(params.typeSig, params.id)*/){
          var tip = trad["interested"];
          var actionConnect="follow";
          var icon="chain";
          var classBtn="";
          if(isFollowed){
            actionConnect="unfollow";
            icon="unlink";
            classBtn="text-green";
          }
          str += "<a href='javascript:;' class='btn btn-default btn-sm btn-add-to-directory bg-white tooltips followBtn "+classBtn+"'" + 
                      'data-toggle="tooltip" data-placement="left" data-original-title="'+tip+'"'+
                      " data-ownerlink='"+actionConnect+"' data-id='"+params.id+"' data-type='"+params.type+"' data-name='"+params.name+"'"+
                      " data-isFollowed='"+isFollowed+"'>"+
                      "<i class='fa fa-"+icon+"'></i>"+ //fa-bookmark fa-rotate-270
                    "</a>";
        }

        str += "<div class='col-md-8 col-sm-8 col-xs-12 margin-top-25'>";
        str += dateFormated+countSubEvents;
        str += "</div>";

       
        if("undefined" != typeof params.organizerObj && params.organizerObj != null){ 

          str += "<div class='col-md-8 col-sm-8 col-xs-12 entityOrganizer margin-top-10'>";
            if("undefined" != typeof params.organizerObj.profilThumbImageUrl &&
              params.organizerObj.profilThumbImageUrl != ""){
              
                str += "<img class='pull-left img-responsive' src='"+baseUrl+params.organizerObj.profilThumbImageUrl+"' height='50'/>";
                
            }            
            
            var elem = dyFInputs.get(params.organizerObj.type);
            str += "<h5 class='no-margin padding-top-5'><small>"+tradDynForm.organizedby+"</small></h5>";
            str += "<a href='#page.type."+elem.col+".id."+params.organizerObj["_id"]["$id"]+"' class='lbh' > <small class='entityOrganizerName'>"+params.organizerObj.name+"</small></a>";
          str += "</div>";

        }
        

        str += "<div class='col-md-8 col-sm-8 col-xs-12 entityRight padding-top-10 margin-top-10 pull-right' style='border-top: 1px solid rgba(0,0,0,0.2);'>";

        var thisLocality = "";
        if(params.fullLocality != "" && params.fullLocality != " ")
             thisLocality = //"<h4 class='pull-right no-padding no-margin lbh add2fav'>" +
                              "<small class='margin-left-5 letter-red'><i class='fa fa-map-marker'></i> " + params.fullLocality + "</small>" ;
                            //"</h4>";
        else thisLocality = "";
                
       // str += thisLocality;

        var typeEvent = //notEmpty(params.typeEvent) ? 
                        (typeof tradCategory[params.typeEvent] != "undefined") ? 
                        tradCategory[params.typeEvent] : 
                       // trad["event"]) : 
                        trad.event;
        
        str += "<h5 class='text-dark lbh add2fav no-margin'>"+
                  "<i class='fa fa-reply fa-rotate-180'></i> " + typeEvent + thisLocality +
               "</h5>";

        str += "<a href='"+params.hash+"' class='entityName text-dark lbh add2fav'>"+
                  params.name + 
               "</a>";
        
          
        str +=    "<div class='entityDescription margin-bottom-10'>" + 
                    params.description + 
                  "</div>";


        if(typeof params.carousable  == "undefined" || (!params.carousable))
          str +=    "<div class='margin-bottom-10 col-md-12 no-padding'>" + 
                    params.attendees + "</div>";
                    
        str +=    "<div class='tagsContainer text-red'>"+params.tagsLbl+"</div>";

        if(typeof params.socialBarOn  != "undefined" && (params.socialBarOn==true))
          str+=directory.socialBarHtml(params); 
  
        str += "</div>";
            
        str += "</div>";
      str += "</div>";

      str += "</div>";
    }
    return str;
  }

}

CO3_Event.name = "CO3_Event";
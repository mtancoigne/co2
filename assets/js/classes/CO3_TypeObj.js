var CO3_TypeObj = class {
  constructor(pRequeteAjax,pPHDB_Obj){ 
    if(this.IsValidRequete(pRequeteAjax)){       
      this.params=this.MakeRequeteAjax(pRequeteAjax);
    } else {
      this.params=pPHDB_Obj||null;
    }
  }

  PrepareRequeteAjax(pRequeteAjax){
    pRequeteAjax["prepared"]=true;
    return pRequeteAjax;
  }

  MakeRequeteAjax(pRequeteAjax){
    if(this.IsValidRequete(pRequeteAjax)){
      this.RequeteAjax=this.PrepareRequeteAjax(pRequeteAjax); //Stockage optional ?
      let res = null;
      //TODO : Requete AJAX
      res = this.RequeteAjax["where"];
      //renvoie obj 
      return res;
    } else {
      return null;
    }    
  }

  Count(){
    if(this.IsDefined(this.params)){
      return Object.keys(this.params).length;
    }else{
      return 0;
    } 
  }

  PreElementRenderHtml(pElementParams){
    return this.OldshowResultsDirectoryHtml(pElementParams,null,null,null);
  }

  ElementRenderHtml(pElementParams){
    let str = "";
    str+="Objet";    
    return str;
  }

  PostElementRenderHtml(pElementStr){
    return pElementStr;
  }

  RenderHtml(){
    let str="";
    for(let id in this.params){
      if(this.IsDefined(this.params[id])){
        str+=this.PostElementRenderHtml(this.ElementRenderHtml(this.PreElementRenderHtml(this.params[id])));
      }
    }
    return str;
  }

  IsDefined(obj){
    return ((typeof obj !=="undefined")&&(obj!=null)&&(obj!=""));
  }

  IsValidRequete(pRequeteAjax){
    return this.IsDefined(pRequeteAjax);
  }

  SetCarousable(){
    let first=true;
    for(let id in this.params){
      if(this.IsDefined(this.params[id])){
        this.params[id].carousable=true;
        if(first){
          this.params[id].carousel_active=true;
          first=false;
        }
      }
    }
    return this;       
  }
 
  OldshowResultsDirectoryHtml( params, size, edit, viewMode){ //size == null || min || max
  
    if ((typeof(params.id) == "undefined") && (typeof(params["_id"]) !== "undefined")) {
      params.id = params['_id'];
    } else if (typeof(params.id) == "undefined" && location.hash.indexOf("#interoperability") >= 0) {
      params.id = Math.random();
      params.type = "poi";
    }
    if((params["_id"]!=null) || (params.id!=null)){
        
      params.size = size;
      params.id = getObjectId(params);
      mylog.log(params.id);
      params.name = notEmpty(params.name) ? params.name : "";
      params.description = notEmpty(params.shortDescription) ? params.shortDescription : 
                          (notEmpty(params.message)) ? params.message : 
                          (notEmpty(params.description)) ? params.description : 
                          "";

      var itemType="article";                   
      //mapElements.push(params);
      //alert("TYPE ----------- "+contentType+":"+params.name);
      if(typeof edit != "undefined" && edit != false)
        params.edit = edit;
      
      if ( params.type && typeof typeObj.classifieds != "undefined" && $.inArray(params.type, typeObj.classifieds.subTypes )>=0  ) {
        itemType = "classifieds";
      } else if(typeof( typeObj[itemType] ) == "undefined") {
        itemType="poi";
      }

      if( dyFInputs.get( itemType ) == null){
        itemType="poi";
      }

      var typeIco = itemType;
      if(directory.dirLog) mylog.warn("itemType",itemType,"typeIco",typeIco);

      if(typeof params.typeOrga != "undefined")
        typeIco = params.typeOrga;
      if(typeof params.typeClassified != "undefined")
        typeIco = params.typeClassified;
      var obj = (dyFInputs.get(typeIco)) ? dyFInputs.get(typeIco) : typeObj["default"] ;
      params.ico =  "fa-"+obj.icon;
      params.color = obj.color;
      if(params.parentType){
          if(directory.dirLog) mylog.log("params.parentType",params.parentType);
          var parentObj = (dyFInputs.get(params.parentType)) ? dyFInputs.get(params.parentType) : typeObj["default"] ;
          params.parentIcon = "fa-"+parentObj.icon;
          params.parentColor = parentObj.color;
      }
      if((typeof searchObject.countType != "undefined" && searchObject.countType.length==1) && params.type == "classifieds" && typeof params.category != "undefined" && typeof modules[params.typeClassified] != "undefined"){
        getIcoInModules=modules[params.typeClassified].categories;
        params.ico = (typeof getIcoInModules.filters != "undefined" && typeof getIcoInModules.filters[params.category] != "undefined") ?
                     "fa-" + getIcoInModules.filters[params.category]["icon"] : "fa-bullhorn";
      }
      if(params.type=="poi" 
        && typeof modules.poi != "undefined" 
        && typeof modules.poi.categories != "undefined" 
        && typeof modules.poi.categories.filters != "undefined"
        && typeof modules.poi.categories.filters[params.typePoi] != "undefined"
        && typeof modules.poi.categories.filters[params.typePoi].icon != "undefined")
        params.ico="fa-"+modules.poi.categories.filters[params.typePoi].icon;
      params.htmlIco ="<i class='fa "+ params.ico +" fa-2x bg-"+params.color+"'></i>";

      params.useMinSize = typeof size != "undefined" && size == "min";

      params.imgProfil = ""; 
      if(!params.useMinSize){
          params.imgProfil = "<i class='fa fa-image fa-2x'></i>";
          params.imgMediumProfil = "<i class='fa fa-image fa-2x'></i>";
          params.imgFullProfil = "<i class='fa fa-image fa-2x'></i>";
      }
      if("undefined" != typeof directory.costum && (directory.costum!=null)  
        && typeof directory.costum.results != "undefined" 
        && typeof directory.costum.results[params.type] != "undefined" 
        && typeof directory.costum.results[params.type].defaultImg != "undefined")
      {      
        params.imgMediumProfil= "<img class='img-responsive' onload='directory.checkImage(this);' src='"+assetPath+directory.costum.results[params.type].defaultImg+"'/>";

        params.imgFullProfil= "<img class='img-responsive' onload='directory.checkImage(this);' src='"+assetPath+directory.costum.results[params.type].defaultImg+"'/>";
      }
    
      if("undefined" != typeof params.profilImageUrl && params.profilImageUrl != "")
        params.imgFullProfil= "<img class='img-responsive' onload='directory.checkImage(this);' src='"+baseUrl+params.profilImageUrl+"'/>";
      else if(this.getVideoThumb(params)!="")
        params.imgFullProfil= "<img class='img-responsive' src='"+this.getVideoThumb(params)+"'/>";

      if("undefined" != typeof params.profilMediumImageUrl && params.profilMediumImageUrl != "")
        params.imgMediumProfil= "<img class='img-responsive' onload='directory.checkImage(this);' src='"+baseUrl+params.profilMediumImageUrl+"'/>";                   
      else if(this.getVideoThumb(params)!="")
        params.imgMediumProfil= "<img class='img-responsive' src='"+this.getVideoThumb(params)+"'/>";
          
      if("undefined" != typeof params.profilThumbImageUrl && params.profilThumbImageUrl != "")
        params.imgProfil= "<img class='shadow2' src='"+baseUrl+params.profilThumbImageUrl+"'/>";
      else if(this.getVideoThumb(params)!="")
        params.imgProfil= "<img class='shadow2' src='"+this.getVideoThumb(params)+"'/>";

      params.imgBanner = ""; 
      if(!params.useMinSize)
        params.imgBanner = "<i class='fa fa-image fa-2x'></i>";


      if (false && typeof params.addresses != "undefined" && params.addresses != null) {
        $.each(params.addresses, function(key, val){
      
          var postalCode = val.address.postalCode ? val.address.postalCode : "";
          var cityName = val.address.addressLocality ? val.address.addressLocality : "";
        
          params.fullLocality += "<br>"+ postalCode + " " + cityName;
        });
      }
      params.type = dyFInputs.get(itemType).col;
      params.urlParent = (notEmpty(params.parentType) && notEmpty(params.parentId)) ? 
                    '#page.type.'+params.parentType+'.id.' + params.parentId : "";
      // var urlImg = "/upload/communecter/color.jpg";
      // params.profilImageUrl = urlImg;
      

      /*if(dyFInputs.get(itemType) && 
          dyFInputs.get(itemType).col == "poi" && 
          typeof params.medias != "undefined" && typeof params.medias[0].content.image != "undefined")
      params.imgProfil= "<img class='img-responsive' src='"+params.medias[0].content.image+"'/>";
      */
      params.insee = params.insee ? params.insee : "";
      params.postalCode = "", params.city="",params.cityName="";
      if (params.address != null) {
          params.city = params.address.addressLocality;
          params.postalCode = params.cp ? params.cp : params.address.postalCode ? params.address.postalCode : "";
          params.cityName = params.address.addressLocality ? params.address.addressLocality : "";
      }
      params.fullLocality = params.postalCode + " " + params.cityName;

      params.hash = '#page.type.'+params.type+'.id.' + params.id;

      if(typeof params.slug != "undefined" && params.slug != "" && params.slug != null)
        params.hash = "#@" + params.slug;

      if(typeof networkJson != "undefined" && typeof networkJson.dataSrc != "undefined")
        params.hash = params.source;

      params.onclick = 'urlCtrl.loadByHash("' + params.url + '");';
      if(params.type=="circuits")
          params.hash = '#circuit.index.id.' + params.id;
          params.onclick = 'urlCtrl.loadByHash("' + params.url + '");';

      if( params.type == "poi" && params.source  && ( (params.source.key!=null) && params.source.key.substring(0,7) == "convert")) {
        var interop_type = getTypeInteropData(params.source.key);
        params.type = "poi.interop."+interop_type;
      }
      // params.tags = "";
      params.elTagsList = "";
      var thisTags = "";
      if(typeof params.tags != "undefined" && params.tags != null){
        $.each(params.tags, function(key, value){
          if(typeof value != "undefined" && value != "" && value != "undefined"){
            var tagTrad = typeof tradCategory[value] != "undefined" ? tradCategory[value] : value;
            thisTags += "<span class='badge bg-transparent text-red btn-tag tag' data-tag-value='"+slugify(value, true)+"' data-tag-label='"+tagTrad+"'>#" + tagTrad + "</span> ";
            // mylog.log("sluggify", value, slugify(value, true));
            params.elTagsList += slugify(value, true)+" ";
          }
        });
        params.tagsLbl = thisTags;
      }else{
        params.tagsLbl = "";
      }
      params.elRolesList = "";
      var thisRoles = "";
      params.rolesLbl = "";
      if(typeof params.rolesLink != "undefined" && params.rolesLink != null){
        thisRoles += "<small class='letter-blue'><b>"+trad.roleroles+" :</b> ";
        thisRoles += params.rolesLink.join(", ");
        $.each(params.rolesLink, function(key, value){
          if(typeof value != "undefined" && value != "" && value != "undefined")
            params.elRolesList += slugify(value)+" ";
        });
        thisRoles += "</small>";
        params.rolesLbl = thisRoles;
      }
      params.updated   = (params.updatedLbl!="") ? params.updatedLbl : null;
      if((params.tobeactivated!=null) && params.tobeactivated == true){
        params.isInviting = true ;
      }
    }
    return params;
  }

  getVideoThumb(params){
    if("undefined" != typeof params.medias && params.medias.length>0){
      for(let i=0;i<params.medias.length;i++){
        if("undefined" != typeof params.medias[i].content.image && params.medias[i].content.image!="")
          return params.medias[i].content.image;
      }  
    }
    return "";
  }

}


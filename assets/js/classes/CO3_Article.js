var CO3_Article = class extends CO3_Poi{
  constructor(pRequeteAjax,pPHDB_Obj){
    super(pRequeteAjax,pPHDB_Obj);
    this.COLLECTION = "article";
    this.CONTROLLER = "article";
    this.MODULE = "article";
    this.ICON = "fa-map-marker";
  }

  PrepareRequeteAjax(pRequeteAjax){
    pRequeteAjax=super.PrepareRequeteAjax(pRequeteAjax);
    pRequeteAjax["where"]["type"]="article";
    return pRequeteAjax;
  }


/*
  PreElementRenderHtml(pElementParams){
    if(this.IsDefined(pElementParams.colNum)){
      if(pElementParams.colNum==2) pElementParams.colNumStr="col-md-6 col-sm-12";
      if(pElementParams.colNum==3) pElementParams.colNumStr="col-md-4 col-sm-12";
      if(pElementParams.colNum==4) pElementParams.colNumStr="col-md-3 col-sm-6 col-xs-12";
    } else{
      pElementParams.colNumStr = "col-xs-12";
    }
    
    if(this.IsDefined(pElementParams.carousable)){
      pElementParams.carousableStr="item";
    }else{
      pElementParams.carousableStr="";
    }

    return pElementParams;
  }
*/

  ElementRenderHtml(params){
    var str = "";

    var colNumStr="col-xs-12";
    if(this.IsDefined(params.colNum)){
      if(params.colNum==2) colNumStr="col-md-6 col-sm-12";
      if(params.colNum==3) colNumStr="col-md-4 col-sm-6 col-xs-12";
      if(params.colNum==4) colNumStr="col-md-3 col-sm-6 col-xs-12";
    } 
    var socialTools=(this.IsDefined(params.socialTools)) ? params.socialTools :true;
    var carousable="";
    if(typeof params.carousable  != "undefined" && (params.carousable))
      carousable=" item";

    var carousel_active="";
    if(typeof params.carousel_active  != "undefined" && (params.carousel_active))
      carousel_active=" active";
    
    str += "<div class='"+colNumStr+carousable+carousel_active+" searchEntityContainer poi "+params.elTagsList+" "+params.elRolesList+" contain_poi_"+params.id+"'>";
    str +=    '<div class="searchEntity" id="entity'+params.id+'">';

    if(typeof params.edit  != "undefined" && (params.edit!=null))
      str += this.getAdminToolBar(params);
    
    var timeAction= trad.posted;
    if(params.updated != null )
      str += "<div class='dateUpdated'><i class='fa fa-flash'></i> <span class='hidden-xs'>"+timeAction+" </span>" + params.updated + "</div>";
         
    var linkAction =  " lbh-preview-element" ;
    
    var dispNone = " medium";
    var imgLink = params.imgMediumProfil;
    if(typeof params.displayIm!=='undefined'){
      if(params.displayIm=="full"){
        dispNone = "";  
        imgLink=params.imgFullProfil;    
      } 
      /*if(params.displayIm=="medium"){
        dispNone = "";
        imgLink=params.imgMediumProfil;
      }*/
      if(params.displayIm=="thumb"){
        dispNone = "";  
        imgLink=params.imgProfil;    
      }      
      if(params.displayIm=="none"){
        dispNone = "hide";  
        imgLink="";    
      }    
    }

    str += "<a href='"+params.hash+"' class='container-img-profil"+dispNone+" add2fav imgRatioToForce "+linkAction+"'>" + imgLink + "</a>";
    str += "<div class='padding-10 informations tooltips'  data-toggle='tooltip' data-placement='top' data-original-title=''>";

    str += "<div class='entityRight profil no-padding'>";
    
    var iconFaReply ="";// notEmpty(params.parent) ? "<i class='fa fa-reply fa-rotate-180'></i> " : "";
    str += "<a  href='"+params.hash+"' class='"+params.size+" entityName bold text-dark add2fav "+linkAction+"'>"+
          iconFaReply + params.name +
        "</a>"; 

    str += "<div class='entityDescription'>" + ( (params.shortDescription == null ) ? "" : params.shortDescription ) + "</div>"; 
              
    var thisLocality = "";
    if(params.fullLocality != "" && params.fullLocality != " ")
      thisLocality = "<a href='"+params.hash+"' data-id='" + params.dataId + "'  class='entityLocality add2fav"+linkAction+"'>"+
                "<i class='fa fa-home'></i> " + params.fullLocality + "</a>";
    else thisLocality = "";
    
    str += thisLocality;
  
    str += "<div class='tagsContainer text-red'>"+params.tagsLbl+"</div>";
  
  /*
    var thisTwitter="<a href='https://www.twitter.com/intent/tweet/?url=journal-insoumis-chambery.com/"+params.hash+"&text="+encodeURIComponent(params.name)+"' target='_blank' data-id='" + params.dataId + "'  class='entityShare' style='float:right;margin-left:10px;'>"+
                "<i class='fa fa-hashtag'></i> " + "Twitter" + "</a>";

    str += thisTwitter;

    var thisShare="<a href='https://www.facebook.com/dialog/share?app_id=580780392452174&href=www.journal-insoumis-chambery.com/"+params.hash+"&name=JournalInsoumisChambery.com&picture="+encodeURIComponent(baseUrl+params.profilMediumImageUrl)+"&caption="+encodeURIComponent(params.name)+"&description="+encodeURIComponent(params.shortDescription)+"' target='_blank' data-id='" + params.dataId + "'  class='entityShare' style='float:right;margin-left:10px;'>"+
                "<i class='fa fa-thumbs-up'></i> " + "Facebook" + "</a>";

    str += thisShare;
    
    var thisLink="<a href='"+params.hash+"' target='_blank' data-id='" + params.dataId + "'  class='entityLink' style='float:right;margin-left:10px;'>"+
                "<i class='fa fa-share-alt'></i> " + "Lien" + "</a>";

    str += thisLink;
  */
    if(typeof params.socialBarOn  != "undefined" && (params.socialBarOn==true))
      str+=directory.socialBarHtml(params); 
  
    if(userId != null && userId != "" && typeof params.id != "undefined" && socialTools) 
      str+=directory.socialToolsHtml(params);  

    str += "</div>";
    str += "</div>";
    str += "</div>";
    str += "</div>";
    //str += "</div>";
    
    return str;
  }

}

CO3_Article.name = "CO3_Article";
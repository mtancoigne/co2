var addressObj = {
	container : null,
	resultSearch : {},
	result : [],
	init : function(pInit){
		mylog.log("addressObj.init", pInit);
		//Init variable
		var copyAdd = null;
		if (pInit != null &&
			typeof pInit.container != "undefined" &&
			pInit.container != null &&
			$(pInit.container).length > 0) {
			copyAdd = jQuery.extend(true, {}, addressObj);
			copyAdd.initVar(pInit);
			copyAdd.initHTML(pInit);
		}
		return copyAdd;

	},
	initHTML: function (pInit) {
		mylog.log("addressObj.initHTML", pInit);
		var str = '';

		str += '<input type="text" ';

		var strC =  ' class="col-xs-11 ' ;
		if( typeof pInit["input"] != "undefined" ){
			if(typeof pInit["input"]["id"] != "undefined" )
				str += ' id="'+pInit["input"]["id"]+'"';

			if(typeof pInit["input"]["class"] != "undefined" ){
				mylog.log("addressObj.initHTML class", pInit["input"]["class"]);
				strC += ' '+pInit["input"]["class"];
			}

			if(typeof pInit["input"]["aria-describedby"] != "undefined" )
				str += ' aria-describedby="'+pInit["input"]["aria-describedby"]+'"';

			if(typeof pInit["input"]["placeholder"] != "undefined" )
				str += ' placeholder="'+pInit["input"]["placeholder"]+'"';
		}
		strC +=  ' "' ;
		str += strC+'>';

		str += '<a href="javascript:;" class="btn btn-primary btnSearchAddress col-xs-1"><i class="fa fa-search"></i></a>';

		str += '<div class="dropdown pull-left col-xs-12 no-padding" style="top: -35px;">'+
					'<ul class="dropdown-menu dropdown-address" style="margin-top: -2px; width:100%;background-color : #ea9d13; max-height : 300px ; overflow-y: auto">'+
						'<li><a href="javascript:" class="disabled">'+trad.currentlyresearching+'</a></li>'+
					'</ul>'+
				'</div>';

		str += '<table class="tableResult table table-striped table-bordered table-hover directoryTable">'+
					'<thead>'+
						'<tr>'+
							'<td>N°</td>'+
							'<td>Non Complet</td>'+
							'<td>lat/lon</td>'+
							'<td>Supprimer</td>'+
						'</tr>'+
					'</thead>'+
					'<tbody class="tableResultBody">'+
					'</tbody>'+
				'</table>';
		mylog.log("addressObj.initHTML str", str);
		$(this.container).html(str);
		$(this.container +" .tableResult").hide();
		this.bindAddress(pInit, this);
		this.addResultInTable(this);
	},
	initVar: function (pInit) {
		mylog.log("addressObj.initVar", pInit);
		this.container = ((pInit != null && typeof pInit.container != "undefined") ? pInit.container : null );
		this.result = ((pInit != null && typeof pInit.result != "undefined") ? pInit.result : [] );
		
	},
	bindAddress: function (pInit, aObj) {
		$(aObj.container+" .btnSearchAddress").off().click(function () {
			aObj.search();
		});
	},
	search: function () {
		mylog.log("addressObj.search");
		var requestPart = addressObj.addToRequest($(this.container+" #inputAdd").val(), "");
		mylog.log("addressObj.search requestPart",requestPart);
		addressObj.callNominatim(requestPart, "FR", this);

	},
	addToRequest : function(request, dataStr){
		mylog.log("addressObj.addToRequest");
		if(dataStr == "") return request;
		if(request != "") dataStr = " " + dataStr;
		return addressObj.transformNominatimUrl(request + dataStr);
	},
	//remplace les espaces par des +
	transformNominatimUrl : function(str){
		mylog.log("addressObj.transformNominatimUrl", str);
		var res = "";
		for(var i = 0; i<str.length; i++){
			res += (str.charAt(i) == " ") ? "+" : str.charAt(i);
		}
		mylog.log("addressObj.transformNominatimUrl res", res);
		return res;
	},
	callNominatim : function (requestPart, countryCode, aObj){
		mylog.log('addressObj.callNominatim', requestPart, countryCode);
		$(aObj.container +" .dropdown-address").show();
		$(aObj.container +" .dropdown-address").html("<i class='fa fa-spin fa-refresh'></i> "+trad.currentlyresearching+" <small>Nominatim</small>");
		
		addressObj.callGeoWebService("nominatim", requestPart, countryCode,
			function(objNomi){
				mylog.log("addressObj.callNominatim SUCCESS nominatim", objNomi); 

				if(objNomi.length != 0){
					mylog.log('addressObj.callNominatim Résultat trouvé chez Nominatim !');
					var commonGeoObj = aObj.getCommonGeoObject(objNomi, "nominatim", aObj);
					aObj.resultSearch = commonGeoObj;
					var res = addressObj.addResultsInForm(commonGeoObj, countryCode, aObj);
				}else{
					var res = addressObj.addResultsInForm(null, null, aObj);
					mylog.log('addressObj.callNominatim Aucun résultat chez Nominatim');
				}
			}, 
			function(thisError){ /*error nominatim*/
				mylog.log("addressObj.callNominatim ERROR nominatim");
				mylog.dir(thisError);
			}
		);
	},
	callGeoWebService : function (providerName, requestPart, countryCode, callBack, error){
		mylog.log("addressObj.callGeoWebService", providerName, requestPart, countryCode, callBack, error)
		var url = "";
		var data = {};

		if(providerName == "nominatim") {
			//if(typeof formInMap != "undefined" && formInMap.typeSearchInternational == "address" || addressObj.typeSearchInternational == "address")
			url = "//nominatim.openstreetmap.org/search?q=" + requestPart + "," + countryCode + "&format=json&polygon=0&addressdetails=1";
			// else if(typeof formInMap != "undefined" && formInMap.typeSearchInternational == "city" || addressObj.typeSearchInternational == "city")
			// url = "//nominatim.openstreetmap.org/search?city=" + requestPart + "&country=" + countryCode + "&format=json&polygon=0&addressdetails=1";
		
			if(notNull(mainLanguage))
				url += "&accept-language="+ mainLanguage ;
		}
		
		// if(providerName == "google") {
		// 	if(typeof formInMap != "undefined" && formInMap.typeSearchInternational == "address" || addressObj.typeSearchInternational == "address")
		// 	url = "//maps.googleapis.com/maps/api/geocode/json?address=" + requestPart + "," + countryCode;
		// 	else if(typeof formInMap != "undefined" && formInMap.typeSearchInternational == "city" || addressObj.typeSearchInternational == "city")
		// 	url = "//maps.googleapis.com/maps/api/geocode/json?locality=" + requestPart + "&country=" + countryCode;
		// }	
		// if(providerName == "data.gouv") {
		// 	if(typeof formInMap != "undefined" && formInMap.typeSearchInternational == "address" || addressObj.typeSearchInternational == "address")
		// 	url = "//api-adresse.data.gouv.fr/search/?q=" + requestPart;
		// 	else if(typeof formInMap != "undefined" && formInMap.typeSearchInternational == "city" || addressObj.typeSearchInternational == "city")
		// 	url = "//api-adresse.data.gouv.fr/search/?q=" + requestPart + "&type=city";
		// }
		// if(providerName == "communecter") {
		// 	url = baseUrl+"/"+moduleId+"/search/globalautocomplete";
		// 	data = {"name" : requestPart, "country" : countryCode, "searchType" : [ "cities" ], "searchBy" : "ALL"  };
		// }

		mylog.log("addressObj.callGeoWebService url : " + url);
		if(url != ""){
			if(providerName != "communecter"){
				$.ajax({
					url: url,
					type: 'GET',
					dataType: 'json',
					async:false,
					crossDomain:true,
					complete: function () {},
					success: function (obj){
						callBack(obj);
					},
					error: function (thisError) {
						error(thisError);
					}
				});
			}else{
				$.ajax({
					url: url,
					type: 'POST',
					dataType: 'json',
					data:data,
					complete: function () {},
					success: function (obj){
						callBack(obj);
					},
					error: function (thisError) {
						error(thisError);
					}
				});
			}
		}else{
			toastr.error('provider inconnu', providerName);
			return false;
		}
	},
	addResultsInForm : function (commonGeoObj, countryCode,aObj){
		//success
		mylog.log("addressObj.addResultsInForm", commonGeoObj, countryCode);
		//mylog.dir(objs);
		var res = commonGeoObj; //getCommonGeoObject(objs, providerName);
		mylog.dir("addressObj.addResultsInForm res", res);
		var html = "";
		if(typeof res != "undefined" && res != null){
			$.each(res, function(key, value){ //mylog.log(allCities);
				if(notEmpty(value.countryCode)){
					mylog.log("addressObj.addResultsInForm each",key, value);
					if(value.countryCode.toLowerCase() == countryCode.toLowerCase()){
						html += "<li><a href='javascript:;' class='item-street-found' data-key='"+key+"' "+
									"data-lat='"+value.geo.latitude+"' "+
									"data-lng='"+value.geo.longitude+"'> "+
									"<i class='fa fa-marker-map'></i> ";
									if(typeof value.streetNumber != "undefined")
										html += value.streetNumber + ", ";

									if(typeof value.street != "undefined")
										html += value.street + ", ";

									if(typeof value.postalCode != "undefined")
										html += value.postalCode + ", ";

									if(typeof value.cityName != "undefined")
										html += value.cityName + ", ";

									if(typeof value.country != "undefined")
										html += value.country ;
						html += "</a></li>";
					}
				}
			});
		}
		
		if(html == "") html = "<i class='fa fa-ban'></i> "+trad.noresult;

		mylog.dir("addressObj.addResultsInForm html", html);
		$(aObj.container +" .dropdown-address").html(html);
		$(aObj.container +" .dropdown-address").show();
		//$("#newElement_btnSearchAddress").html("<i class='fa fa-search'></i>");
		$(".item-street-found").click(function(){
			aObj.result.push(aObj.resultSearch[$(this).data("key")]);
			$(aObj.container +" .dropdown-address").hide();
			aObj.addResultInTable(aObj);


			if(typeof aObj.saveAuto == "function")
				aObj.saveAuto(aObj);
			
		});
	},
	addResultInTable : function(aObj){

		if(aObj.result.length == 0){
			$(aObj.container +" .tableResult").hide();
		} else {
			var str = "";
			$.each(aObj.result, function(keyR, valR){
				str += "<tr>"+
						"<td>"+valR.streetNumber+"</td>"+
						"<td>"+valR.name+"</td>"+
						"<td>"+valR.geo.latitude+" / "+valR.geo.longitude+"</td>"+
						"<td><a href='javascript:;' class='removeEltInTable' data-id='"+keyR+"'>Remove</a></td>"+
						"</tr>";
			});
			$(aObj.container +" .tableResultBody").html(str);
			$(aObj.container +" .tableResult").show();

			$(aObj.container+" .removeEltInTable").off().click(function () {
				//aObj.removeEltInTable($(this).data("id"));
				aObj.result.splice($(this).data("id"),1);
				aObj.addResultInTable(aObj);
				if(typeof aObj.saveAuto == "function")
					aObj.saveAuto(aObj);
			});
		}		
	},
	getCommonGeoObject : function(objs, providerName, aObj){

		var commonObjs = new Array();
		var multiCpName = new Array();
		$.each(objs, function(key, obj){
			
			var commonObj = {};
			if(providerName == "nominatim"){
				var address = typeof obj["address"] != "undefined" ?  obj["address"] :  obj["address_components"];
				commonObj = aObj.addAttObjNominatim(commonObj, address, "streetNumber", "house_number");
				
				commonObj = aObj.addAttObjNominatim(commonObj, address, "street", "road");
				if(typeof commonObj["street"] == "undefined")
				commonObj = aObj.addAttObjNominatim(commonObj, address, "street", "locality");
				
				commonObj = aObj.addAttObjNominatim(commonObj, address, "cityName", "county");
				commonObj = aObj.addAttObjNominatim(commonObj, address, "cityName", "city");
				commonObj = aObj.addAttObjNominatim(commonObj, address, "cityName", "village");
				commonObj = aObj.addAttObjNominatim(commonObj, address, "cityName", "town");
				commonObj = aObj.addAttObjNominatim(commonObj, address, "suburb", "suburb");
				commonObj = aObj.addAttObjNominatim(commonObj, address, "country", "country");
				commonObj = aObj.addAttObjNominatim(commonObj, address, "state", "state");
				commonObj = aObj.addAttObjNominatim(commonObj, address, "countryCode", "country_code");
				commonObj = aObj.addAttObjNominatim(commonObj, address, "postalCode", "postcode");
				// commonObj = aObj.addAttObjNominatim(commonObj, obj, "placeId", "place_id");
				// commonObj = aObj.addAttObjNominatim(commonObj, obj, "typeNom", "type");
				// commonObj = aObj.addAttObjNominatim(commonObj, obj, "classNom", "class");

			}

			commonObj = aObj.addCoordinates(commonObj, obj, providerName);
			// commonObj["type"] = "addressEntity";
			// if(typeof formInMap != "undefined" && typeof formInMap.formType != "undefined")
			// 	commonObj["typeSig"] = formInMap.formType;
			commonObj["name"] = aObj.getFullAddress(commonObj);

			if(typeof commonObj["postalCode"] != "undefined" && commonObj["postalCode"].indexOf(";") >= 0){
				var CPS = commonObj["postalCode"].split(";");
				$.each(CPS, function(index, value){
					var oneCity = commonObj;
					oneCity["postalCode"] = value;
					oneCity["name"] = oneCity["cityName"] + ", " + value + ", " + oneCity["country"];			

					if($.inArray(oneCity["name"], multiCpName) < 0){
						multiCpName.push(oneCity["name"]);
						commonObjs.push(oneCity);
					}
				});
			}else{
				commonObjs.push(commonObj);
			}
		});

		

		return commonObjs;
	},
	addAttObjNominatim : function(obj1, obj2, att1, att2){
		if(typeof obj2[att2] != "undefined") obj1[att1] = obj2[att2];
		return obj1;
	},
	getFullAddress(obj){
		var strResult = "";
		if(typeof obj.street != "undefined") strResult += obj.street;
		if(typeof obj.cityName != "undefined") strResult += (strResult != "") ? ", " + obj.cityName : obj.cityName;
		//if(typeof obj.village != "undefined") strResult += (strResult != "") ? ", " + obj.village : obj.village;
		if(typeof obj.postalCode != "undefined") strResult += (strResult != "") ? ", " + obj.postalCode : obj.postalCode;
		if(typeof obj.country != "undefined") strResult += (strResult != "") ? ", " + obj.country : obj.country;
		return strResult;
	},
	//récupère les coordonnées et transforme la data dans notre format geo
	addCoordinates : function(commonObj, obj, providerName){

		var lat = null; var lng = null; var geoShape = null;

		if(providerName == "nominatim"){
			lat = (typeof obj["lat"] != "undefined") ? obj["lat"] : null;
			lng = (typeof obj["lon"] != "undefined") ? obj["lon"] : null;
			geoShape = (typeof obj["boundingbox"] != "undefined") ? obj["boundingbox"] : null;
		}else 
		if(providerName == "google"){ mylog.log("coordinates google"); mylog.dir(obj);
			if(typeof obj["geometry"] != "undefined" && typeof obj["geometry"]["location"] != "undefined"){
				lat = (typeof obj["geometry"]["location"]["lat"] != "undefined") ? obj["geometry"]["location"]["lat"] : null;
				lng = (typeof obj["geometry"]["location"]["lng"] != "undefined") ? obj["geometry"]["location"]["lng"] : null;
				//geoShape = (typeof obj["boundingbox"] != "undefined") ? obj["boundingbox"] : null; 
				//TODO : rajouter le boundingbox (transfoormer format google to leaflet polygon)
			}
		}else 
		if(providerName == "data.gouv"){
			if(typeof obj["geometry"] != "undefined" && typeof obj["geometry"]["coordinates"] != "undefined"){
				lat = (typeof obj["geometry"]["coordinates"][1] != "undefined") ? obj["geometry"]["coordinates"][1] : null;
				lng = (typeof obj["geometry"]["coordinates"][0] != "undefined") ? obj["geometry"]["coordinates"][0] : null;
				//TODO : geoshape
			}
		}
		else 
		if(providerName == "communecter"){
			if(typeof obj["geo"] != "undefined") commonObj["geo"] = obj["geo"];
			if(typeof obj["geoPosition"] != "undefined") commonObj["geoPosition"] = obj["geoPosition"];
			return commonObj;
		}

		if(lat != null && lng != null){
			commonObj["geo"] = { "@type" : "GeoCoordinates", 
								 "latitude" : lat,  "longitude" : lng };

			commonObj["geoPosition"] = { "type" : "Point", "float" : true, 
										 "coordinates" : new Array(lat, lng) };
		}

		// if(geoShape != null){
		// 	commonObj["geoShape"] = { "type" : "Polygon", 
		// 						 "coordinates" : geoShape };
		// }

		return commonObj;
	}

	// callDataGouv(requestPart, countryCode){ /*countryCode=="FR"*/
	// 	mylog.log('addressObj.callDataGouv', requestPart, countryCode);
	// 	showMsgListRes("<i class='fa fa-spin fa-refresh'></i> "+trad.currentlyresearching+" <small>Data Gouv</small>");
	// 	callGeoWebService("data.gouv", requestPart, countryCode,
	// 		function(objDataGouv){ /*success nominatim*/
	// 			mylog.log("SUCCESS DataGouv"); 

	// 			if(objDataGouv.features.length != 0){
	// 				mylog.log('Résultat trouvé chez DataGouv !'); mylog.dir(objDataGouv);
	// 				var commonGeoObj = getCommonGeoObject(objDataGouv.features, "data.gouv");
	// 				var res = addressObj.addResultsInForm(commonGeoObj, countryCode);
	// 				if(res == 0) 
	// 					addressObj.callNominatim(requestPart, countryCode);
	// 			}else{
	// 				mylog.log('Aucun résultat chez DataGouv');
	// 				addressObj.callNominatim(requestPart, countryCode);
	// 			}
	// 		}, 
	// 		function(thisError){ /*error nominatim*/
	// 			mylog.log("ERROR DataGouv");
	// 			mylog.dir(thisError);
	// 		}
	// 	);
	// },
};
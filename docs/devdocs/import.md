# Import

Exécuté le code suivant pour installé le parserCSV coté php 
`composer require parsecsv/php-parsecsv`

Process import en js
`co2/js/default/admin.js`

Process import coté php
`Import.php`

## CRESS

Gestion admin coté cress
`costum/assets/js/cressreunion/admin.js`

Dans le vue de la cress
`importsiren.php` Vu de la parti import des siren de la cress. Les spécificités concernant l'import sur les siren sont déclaré ici. Vous pouvez géré le mapping a cette endroit.

-> SaveSirenAction.php
    -> CressReunion::savesiren // va enregister les sirens
        -> Import::previewData // Process qui transforme CSV en JSON
            -> Import::checkElement // vérifie la conformité des elements

Meme process pour `importorga.php`

Activé ou désactivé le proccess d'import pour la cress.
Enlevé dans le JSON de la cress
```
"importsiret" : {
                    "label": "Import des SIREN",
                    "icon" : "upload",
                    "view" : "importsiret",
                    "class" : "text-blue"
                },
                "organizations" : {
                    "label": "Organisations CRESS Reunion",
                    "icon" : "users",
                    "view" : "organizations",
                    "class" : "text-green"
                },
                "importorga" : {
                    "label": "Import des organisations",
                    "icon" : "upload",
                    "view" : "importorga",
                    "class" : "text-green"
                }
```

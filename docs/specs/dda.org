# DDA : Discussion , Decision , Action 
> outil de gestion de communauté
https://docs.google.com/document/d/1RX-a5Os9sw7a9CMPCOOE1N3Q-P6XRxFL2NNZXrpWgWE/edit
schema Implementation DDA
https://docs.google.com/drawings/d/1sr7Gnikn94GPv7dQCZqHllgD1SaBLOWAlsR2QEaofHg/edit
DDA : Discuter Décider Agir
https://docs.google.com/document/d/1RX-a5Os9sw7a9CMPCOOE1N3Q-P6XRxFL2NNZXrpWgWE/edit
schema Interface refactor DDA - proposal
https://docs.google.com/drawings/d/14V8KVFpQyN9-IkOky7milI_Ex-wFFM7YnCtgTTj5XVA/edit

* home espace co 
[[~/d/modules/dda/views/cooperation/roomList.php]]

* room details
[[~/d/modules/dda/views/cooperation/action.php]]

* proposal page
[[~/d/modules/dda/views/co/proposal.php]]

* an action page 
[[~/d/modules/dda/views/cooperation/action.php]]



previewcoopdata

# in search module 
directory.js 
    coopPanelHtml

# in News 
```
$(".newsActivityStream"+parentId).html(directory.coopPanelHtml(proposalRes["proposal"]));
```

# CTK stuff
## dda module controllers
DeleteAmendementAction
GetCoopDataAction
GetMyDashboardCoopAction
IndexAction
PreviewCoopDataAction
SaveVoteAction
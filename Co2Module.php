<?php
/**
 * Communect Module
 *
 * @author Tibor Katelbach <oceatoon@mail.com>
 * @version 0.0.3
 *
*/

class Co2Module extends CWebModule {

	private $_assetsUrl;

	private $_version = "v0.3.0";
	private $_versionDate = "10/12/2018";
	private $_keywords = "societal, reseau, opensource, CO, communecter";
	private $_description = "Retrouvez la richesse qui vous entoure. Créez votre réseau, communiquez et agissez sur votre société. Communecter, le numérique libre au service du réel !";
	private $_pageTitle = "Communecter, le réseau social libre et connecté";
	private $_image = "/themes/CO2/assets/img/1+1=3.jpg";
	private $_relCanonical = "https://www.communecter.org";
	private $_favicon = "/themes/CO2/assets/img/favicon.ico";
	private $_author = "Pixel Humain";
	private $_share = null;

	public function getVersion(){return $this->_version;}
	public function getVersionDate(){return $this->_versionDate;}
	public function getKeywords(){return $this->_keywords;}
	public function getDescription(){return $this->_description;}
	public function getPageTitle(){return $this->_pageTitle;}
	public function getImage(){return $this->_image;}
	public function getFavicon(){return $this->_favicon;}
	public function getRelCanonical(){return $this->_relCanonical;}
	public function getAuthor(){return $this->_author;}
	public function getShare(){return $this->_share;}

	public function setPageTitle($title){ $this->_pageTitle = $title; }
	public function setAuthor($author){ $this->_author = $author; }
	public function setDescription($desc){ $this->_description = $desc; }
	public function setImage($image){ $this->_image = $image; }
	public function setKeywords($keywords){ $this->_keywords = $keywords; }
	public function setFavicon($favicon){ $this->_favicon = $favicon; }
	public function setRelCanonical($relCanonical){ $this->_relCanonical = $relCanonical; }
	public function setShare($share){ $this->_share = $share; }

	public function init()
	{
		// this method is called when the module is being created
		// you may place code here to customize the module or the application

		Yii::app()->setComponents(array(
		    'errorHandler'=>array(
		        'errorAction'=>'/'.$this->id.'/error'
		    )
		));
		
		Yii::app()->homeUrl = Yii::app()->createUrl($this->id);
		
		//Apply theme
		$themeName = $this->getTheme();
		Yii::app()->theme = $themeName;
		
		//Retrieve network params in the url
		if(@$_GET["network"]) {
            Yii::app()->params['networkParams'] = $_GET["network"];
        }
       if(@Yii::app()->request->cookies['lang'] && !empty(Yii::app()->request->cookies['lang']->value))
        	Yii::app()->language = (string)Yii::app()->request->cookies['lang'];
        else 
			Yii::app()->language = (isset(Yii::app()->session["lang"])) ? Yii::app()->session["lang"] : 'fr';
		
		Yii::app()->params["module"] = array(
			"name" => self::getPageTitle(),
			"parent" => "co2",
			"overwrite" => array(
				"views" => array(),
				"assets" => array(),
				"controllers" => array(),
			));

		// import the module-level models and components
		$this->setImport(array(
			'citizenToolKit.models.*',
			'eco.models.*',
			'costum.models.*',
			'places.models.*',
			'chat.models.*',
			'interop.models.*',
			'map.models.*',
			'news.models.*',
			'survey.models.*',
			'dda.models.*',
			$this->id.'.models.*',
			$this->id.'.components.*',
			$this->id.'.messages.*',
		));
		/*$this->components =  array(
            'class'=>'CPhpMessageSource',
            'basePath'=>'/messages'
        );*/
	}

	public function beforeControllerAction($controller, $action)
	{
		if (parent::beforeControllerAction($controller, $action))
		{
			// this method is called before any module controller action is performed
			// you may place customized code here
			return true;
		}
		else
			return false;
	}

	public function getAssetsUrl($noBase=null)
	{
		$baseAssets= ($noBase) ? "assets" : $this->id.'.assets';
		if ($this->_assetsUrl === null)
	        $this->_assetsUrl = Yii::app()->getAssetManager()->publish(
	            Yii::getPathOfAlias($baseAssets) );
	    return $this->_assetsUrl;
	}

	public function getParentAssetsUrl()
	{
		return ( @Yii::app()->params["module"]["parent"] ) ?  Yii::app()->getModule( Yii::app()->params["module"]["parent"] )->getAssetsUrl()  : self::getAssetsUrl();
	}

	/**
	 * Retourne le theme d'affichage de communecter.
	 * Si option "theme" dans paramsConfig.php : 
	 * Si aucune option n'est précisée, le thème par défaut est "ph-dori"
	 * Si option 'tpl' fixée dans l'URL avec la valeur "iframesig" => le theme devient iframesig
	 * Si option "network" fixée dans l'URL : theme est à network et la valeur du parametres fixe les filtres d'affichage
	 * @return type
	 */
	public function getTheme() {
		//$theme = "CO2";
		$theme = (@Yii::app()->session["theme"]) ? Yii::app()->session["theme"] : "CO2";
		//$theme = "notragora";
		if (!empty(Yii::app()->params['theme'])) {
			$theme = Yii::app()->params['theme'];
		} else if (empty(Yii::app()->theme)) {
			$theme = (@Yii::app()->session["theme"]) ? Yii::app()->session["theme"] : "CO2";
			//$theme = "CO2";
			//$theme = "notragora";
		}

		if(@$_GET["tpl"] == "iframesig"){ $theme = $_GET["tpl"]; }

		if(@$_GET["network"]) {
            $theme = "network";
            //Yii::app()->params['networkParams'] = $_GET["network"];
        }
        Yii::app()->session["theme"] = $theme;
		return $theme;
	}
}
